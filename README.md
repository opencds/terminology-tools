# README #

### What is this repository for? ###

* Terminology tools for OpenCDS-related projects.

### How do I get set up? ###

* Clone the repository
* Compile: ```mvn clean install```
* This will build the project and create executable jars for each supported terminology. 

### Running ###

* Edit the ```resources/terminology-tools.properties``` file
```
#!properties


umls.api.key=<UMLS key>
umls.datastore=<UMLS datastore location>

rxnav.datastore=<RxNav datastore location>
rxnav.wipe.existing.content=<true|false>
rxnav.skip.lookups=<true|false>

vsac.api.username=<VSAC username>
vsac.api.password=<VSAC password>
vsac.datastore=<VSAC datastore location>
```
This properties file must be placed in one of two locations:

* same folder where the executable jar will be run
* $HOME/.opencds (Linux/Mac) or %HOME%\.opencds (Windows) 

Run as follows:

* Double click the executable jar, or 
* run the following command (replacing "path" with the appropriate location of the "executable jar file"):
    * ```java -jar <path>/<executable jar file>``` (Linux/Mac)
    * ```java -jar <path>\<executable jar file>``` (Windows)


### Who do I talk to? ###

* Contact us on the opencds-users list:
[https://groups.google.com/group/opencds-users](https://groups.google.com/group/opencds-users)


### NLM Data Usage

This product uses publicly available data from the U.S. National Library of Medicine (NLM),
National Institutes of Health, Department of Health and Human Services; NLM is not
responsible for the product and does not endorse or recommend this or any other product.
