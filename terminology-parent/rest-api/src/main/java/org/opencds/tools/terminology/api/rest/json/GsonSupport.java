package org.opencds.tools.terminology.api.rest.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonSupport {
    private static final Gson gson;

    static {
        gson = new GsonBuilder().create();
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

    public static <T> String toJson(T value) {
        return gson.toJson(value);
    }
}
