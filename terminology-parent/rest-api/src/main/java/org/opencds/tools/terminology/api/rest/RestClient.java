package org.opencds.tools.terminology.api.rest;

import org.apache.commons.collections4.MultiValuedMap;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.opencds.tools.terminology.api.rest.util.Const.APIKEY;

public class RestClient {
    private static final String COMMA = ",";

    private final Set<String> EXPECTED_TYPES = new HashSet<>(Arrays.asList(
            "application/json",
            "application/fhir+json",
            "application/json+fhir",
            "application/xml",
            "application/xml+fhir",
            "application/fhir+xml",
            "application/fhir+xml",
            "text/xml"
    ));

    private final java.net.http.HttpClient client;
    private final String apiKey;

    public RestClient() {
        apiKey = null;
        client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build();
    }

    public RestClient(String apiKey) {
        // we end up doing this instead of using .authenticator() since
        // NLM/UMLS doesn't return a WWW-Authentication header on the 401
        this.apiKey = apiKey;
        client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build();
    }

    public boolean isExpectedType(String contentType) {
        return EXPECTED_TYPES.contains(contentType);
    }

    public void get(RestRequest restRequest,
                    BiConsumer<Integer, String> errorHandler,
                    Consumer<String> consumer) {
        get(
                restRequest.getUri(),
                restRequest.getHeaders(),
                restRequest.getQuery(),
                errorHandler,
                consumer);
    }

    public void get(RestRequest restRequest,
                    Consumer<String> handler) {
        get(
                restRequest.getUri(),
                restRequest.getHeaders(),
                restRequest.getQuery(),
                handler);
    }

    public void get(URI uri,
                    MultiValuedMap<String, String> headers,
                    Map<String, String> query,
                    Consumer<String> consumer) {
        var requestBuilder = java.net.http.HttpRequest.newBuilder()
                .uri(URI.create(addApiKey(addQuery(uri, query))))
                .GET();
        headers.asMap().forEach((key, value) -> requestBuilder.header(key, String.join(COMMA, value)));
        try {
            consumer.accept(client.send(
                    requestBuilder.build(),
                    HttpResponse.BodyHandlers.ofString()).body());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void get(URI uri,
                    MultiValuedMap<String, String> headers,
                    Map<String, String> query,
                    BiConsumer<Integer, String> errorHandler,
                    Consumer<String> consumer) {
        var requestBuilder = java.net.http.HttpRequest.newBuilder()
                .uri(URI.create(addApiKey(addQuery(uri, query))))
                .GET();
        headers.asMap().forEach((key, value) -> requestBuilder.header(key, String.join(COMMA, value)));
        try {
            var response = client.send(
                    requestBuilder.build(),
                    HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() >= 400) {
                errorHandler.accept(response.statusCode(), response.body());
            } else {
                consumer.accept(response.body());
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private String addApiKey(String uriString) {
        if (uriString.contains("?")) {
            return uriString + String.join("=", "&" + APIKEY, apiKey);
        } else {
            return uriString + String.join("=", "?" + APIKEY, apiKey);
        }
    }

    private String addQuery(URI uri, Map<String, String> query) {
        var uriString = uri.toString();
        if (query == null || query.isEmpty()) {
            return uriString;
        }
        if (uriString.contains("?")) {
            return uriString +
                    "&" +
                    query.entrySet().stream()
                            .map(entry -> String.join(
                                    "=",
                                    entry.getKey(), entry.getValue()))
                            .collect(Collectors.joining("&"));
        } else {
            return uriString +
                    "?" +
                    query.entrySet().stream()
                            .map(entry -> String.join(
                                    "=",
                                    entry.getKey(), entry.getValue()))
                            .collect(Collectors.joining("&"));
        }
    }
}
