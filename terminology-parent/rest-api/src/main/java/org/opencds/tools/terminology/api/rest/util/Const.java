package org.opencds.tools.terminology.api.rest.util;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import static org.apache.commons.collections4.multimap.UnmodifiableMultiValuedMap.unmodifiableMultiValuedMap;

public final class Const {
    public static final String COMMA = ",";
    public static final String EQUALS = "=";
    public static final String PIPE = "|";
    public static final String PLUS = "+";
    public static final String QUESTION = "?";
    public static final String AMPERSAND = "&";
    public static final String LB = "{";
    public static final String RB = "}";
    public static final String APIKEY = "apiKey";
    public static final MultiValuedMap<String, String> IMMUTABLE_EMPTY_HEADERS =
            unmodifiableMultiValuedMap(new HashSetValuedHashMap<>());
}
