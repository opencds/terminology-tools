package org.opencds.tools.terminology.api.rest;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.opencds.tools.terminology.api.rest.util.Const;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.multimap.UnmodifiableMultiValuedMap.unmodifiableMultiValuedMap;

public class RestRequest {
    private final URI uri;
    private final MultiValuedMap<String, String> headers;
    private final Map<String, String> query;
    private final String val;


    private RestRequest(URI uri, MultiValuedMap<String, String> headers, Map<String, String> query) {
        this.uri = uri;
        this.headers = headers == null ? Const.IMMUTABLE_EMPTY_HEADERS : unmodifiableMultiValuedMap(headers);
        this.query = query == null ?
                Collections.emptyMap() :
                query.entrySet().stream()
                        .map(e -> new AbstractMap.SimpleEntry<>(
                                e.getKey(),
                                URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8)))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        this.val = String.join(
                Const.PIPE,
                this.headers.isEmpty() ?
                        "" :
                        Const.LB + this.headers.asMap().entrySet().stream()
                                .map(h -> h.getKey() + Const.EQUALS + Const.LB + String.join(Const.COMMA, h.getValue()) + Const.RB)
                                .collect(Collectors.joining(Const.AMPERSAND)) +
                                Const.RB,
                uri + (this.query.isEmpty() ?
                        "" :
                        Const.QUESTION +
                                this.query.entrySet().stream()
                                        .map(e -> e.getKey() + Const.EQUALS + e.getValue())
                                        .collect(Collectors.joining(Const.AMPERSAND)))
        );
    }

    public static RestRequest create(String url, MultiValuedMap<String, String> headers, Map<String, String> query) {
        return new RestRequest(URI.create(url), headers, query);
    }

    public static RestRequest create(URI uri, MultiValuedMap<String, String> headers) {
        return new RestRequest(uri, headers, Collections.emptyMap());
    }

    public URI getUri() {
        return uri;
    }

    public MultiValuedMap<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getQuery() {
        return query;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RestRequest that = (RestRequest) o;

        return new EqualsBuilder().append(val, that.val).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(val).toHashCode();
    }

    @Override
    public String toString() {
        return val;
    }
}
