package org.opencds.tools.terminology.api.rest;

import java.net.http.HttpHeaders;

public class RestResult {
    private int statusCode;
    private String reasonPhrase;
    private HttpHeaders headers;

    public RestResult() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public void setReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }

    public String getHeader(String name) {
        return headers.firstValue(name)
                .orElse(null);
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }
}
