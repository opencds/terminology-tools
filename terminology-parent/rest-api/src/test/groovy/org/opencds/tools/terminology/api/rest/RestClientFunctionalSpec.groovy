package org.opencds.tools.terminology.api.rest

import org.apache.commons.collections4.multimap.ArrayListValuedHashMap
import spock.lang.Specification

import java.util.function.BiConsumer
import java.util.function.Consumer

class RestClientFunctionalSpec extends Specification {
    def 'test rxnav'() {
        given:
        RestClient restClient = new RestClient()
        var url = URI.create('https://kmm-app-d03.med.utah.edu:4443/REST/rxclass/classMembers.json')
        var headers = new ArrayListValuedHashMap()
        var query = [
                classId   : 'N0000008638',
                relaSource: 'DAILYMED',
                rela      : 'has_PE',
                trans     : '0',
                ttys      : 'IN+PIN'
        ]
        int status = -1
        String error = null
        String data = null

        when:
        restClient.get(
                url,
                headers,
                query,
                (i, s) -> {
                    status = i
                    error = s
                } as BiConsumer<Integer, String>,
                (s) -> { data = s } as Consumer<String>)

        then:
        notThrown(Exception)
        status == -1
        error == null
        data
        data != ''
    }

    def 'test rxnav 404'() {
        given:
        RestClient restClient = new RestClient()
        var url = URI.create('https://kmm-app-d03.med.utah.edu:4443/REST/rxclass/abcdEclassMembers.json')
        var headers = new ArrayListValuedHashMap()
        var query = [
                classId   : 'N0000008638',
                relaSource: 'DAILYMED',
                rela      : 'has_PE',
                trans     : '0',
                ttys      : 'IN+PIN'
        ]
        int status = -1
        String error = null
        String data = null

        when:
        restClient.get(
                url,
                headers,
                query,
                (i, s) -> {
                    status = i
                    error = s
                } as BiConsumer<Integer, String>,
                (s) -> { data = s } as Consumer<String>)

        then:
        notThrown(Exception)
        status == 404
        error == 'Not found'
        data == null
    }
}
