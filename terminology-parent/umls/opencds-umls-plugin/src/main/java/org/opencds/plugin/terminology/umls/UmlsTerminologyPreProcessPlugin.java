/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.plugin.terminology.umls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.plugin.api.PreProcessPlugin;
import org.opencds.plugin.api.PreProcessPluginContext;
import org.opencds.plugin.api.SupportingData;
import org.opencds.tools.terminology.api.store.AccessConnection;
import org.opencds.tools.terminology.api.store.DataStoreReader;
import org.opencds.tools.terminology.umls.tools.store.UmlsLocalDataStoreReader;
import org.opencds.tools.terminology.umls.tools.store.client.UmlsTerminologyClient;

import java.sql.Connection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * OpenCDS pre-process plugin that loads a UMLS data store to provide CDS
 * terminology support.
 *
 * @author Kensaku Kawamoto
 * @author phillip
 */
public class UmlsTerminologyPreProcessPlugin extends DataStoreReader implements PreProcessPlugin {
    private static final Log log = LogFactory.getLog(UmlsTerminologyPreProcessPlugin.class);
    private static final String ACCESS_DB_ID = "UMLS_DB";

    private static final String UMLS_CLIENT = "UmlsTerminologyClient";

    @Override
    public void execute(PreProcessPluginContext context) {
        log.debug(getClass().getSimpleName() + ": Processing input data.");
        Map<String, SupportingData> sdMap = context.getSupportingData();
        SupportingData sd = sdMap.get(ACCESS_DB_ID);
        log.debug("SD: " + sd);
        if (sd != null) {
            UmlsTerminologyClient client = context.getCache().get(sd);
            if (client == null) {
                UmlsLocalDataStoreReader reader = new UmlsLocalDataStoreReader(getConnectionSupplier(sd));
                client = new UmlsTerminologyClient(reader.getValueSetToUmlsConceptSet());
                context.getCache().put(sd, client);
            }
            context.getGlobals().put(UMLS_CLIENT, client);
        }
    }

    private Supplier<Connection> getConnectionSupplier(SupportingData sd) {
        return sd == null ? () -> null : AccessConnection.supplier(sd.getSupportingDataPackage().getFile());
    }

}
