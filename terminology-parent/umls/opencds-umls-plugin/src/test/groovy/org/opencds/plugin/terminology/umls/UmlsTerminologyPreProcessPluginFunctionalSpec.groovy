/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.plugin.terminology.umls

import java.sql.SQLException

import org.opencds.plugin.api.PluginDataCache
import org.opencds.plugin.api.SupportingData
import org.opencds.plugin.api.SupportingDataPackage
import org.opencds.plugin.support.PreProcessPluginContextImpl
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.terminology.umls.api.model.UmlsConcept

import spock.lang.Specification

import java.util.function.Supplier

class UmlsTerminologyPreProcessPluginFunctionalSpec extends Specification {
    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_UMLS_OpioidCds.accdb'

    def 'test execute '() {
        given:
        UmlsTerminologyPreProcessPlugin plugin = new UmlsTerminologyPreProcessPlugin()
        Map allFactLists = [:]
        Map namedObjects = [:]
        Map globals = [:]
        Map supportingData = [:]
        Supplier<SupportingDataPackage> supplier = () -> SupportingDataPackage.create(
                () -> new File(DBFILENAME),
                () -> new File(DBFILENAME).text.bytes)
        PluginDataCache cache = new PluginDataCacheImpl();
        SupportingData sd = SupportingData.create(
                'id',
                'kmid',
                'loadedby',
                DBFILENAME,
                'access',
                new Date(),
                supplier)
        supportingData.put(UmlsTerminologyPreProcessPlugin.ACCESS_DB_ID, sd)
        PreProcessPluginContextImpl context = PreProcessPluginContextImpl.create(
                supportingData as Map<String, SupportingData>,
                cache,
                allFactLists as Map<Class<?>, List<?>>,
                namedObjects as Map<String, Object>,
                globals as Map<String, Object>)

        when:
        plugin.execute(context)
        Map<String, Object> umlsNavGlobals = globals.get(UmlsTerminologyPreProcessPlugin.UMLS_GLOBALS)

        then:
        isConceptInValueSet(umlsNavGlobals, "ICD10CM", "C25.0", 1)
    }

    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public Set<UmlsConcept> getConceptsInValueSet(Map<String, Object> globals, int valueSetId) {
        return globals.myValueSetIdToUmlsConceptSetMap.get(Integer.valueOf(valueSetId));
    }

    /**
     * Returns true if concept in value set; returns false otherwise.
     *
     * @param sab
     * @param code
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public boolean isConceptInValueSet(Map<String, Object> globals, String sab, String code, int valueSetId) {
        Set<UmlsConcept> conceptSet = globals.myValueSetIdToUmlsConceptSetMap.get(Integer.valueOf(valueSetId));
        if (conceptSet != null) {
            // only SAB and code used for equivalency testing
            if (conceptSet.contains(new UmlsConcept(sab, code))) {
                return true;
            }
        }
        return false;
    }


}
