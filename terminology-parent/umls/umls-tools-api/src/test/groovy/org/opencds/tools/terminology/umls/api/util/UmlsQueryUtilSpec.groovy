package org.opencds.tools.terminology.umls.api.util

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsInputType
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsReturnIdType
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsSearchType
import spock.lang.Specification

class UmlsQueryUtilSpec extends Specification {
    def 'test parsing URL queryParams from URLs in UMLS query results'() {
        expect:
        var result = UmlsQueryUtil.queryParams(URI.create(url))
        result.get(UmlsQueryParam.includeObsolete) == includeObsolete
        result.get(UmlsQueryParam.includeAdditionalRelationLabels) == includeAdditionalRelationLabels
        result.get(UmlsQueryParam.includeAttributeNames) == includeAttributeNames
        result.get(UmlsQueryParam.includeRelationLabels) == includeRelationLabels
        result.get(UmlsQueryParam.includeSuppressible) == includeSuppressible
        result.get(UmlsQueryParam.inputType) == inputType
        result.get(UmlsQueryParam.language) == language
        result.get(UmlsQueryParam.pageNumber) == pageNumber
        result.get(UmlsQueryParam.pageSize) == pageSize
        result.get(UmlsQueryParam.partialSearch) == partialSearch
        result.get(UmlsQueryParam.returnIdType) == returnIdType
        result.get(UmlsQueryParam.sabs) == sabs
        result.get(UmlsQueryParam.searchType) == searchType
        result.get(UmlsQueryParam.string) == string
        result.get(UmlsQueryParam.targetSource) == targetSource
        result.get(UmlsQueryParam.ttys) == ttys

        where:
        url                                                                                                                             | includeObsolete | includeAdditionalRelationLabels | includeAttributeNames | includeRelationLabels | includeSuppressible | inputType              | language | pageNumber | pageSize | partialSearch | returnIdType              | sabs            | searchType           | string                    | targetSource | ttys
        'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms'                                                             | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | null                 | null                      | null         | null
        'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/parents'                                             | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | null                 | null                      | null         | null
        'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/relations'                                           | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | null                 | null                      | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934241000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'   | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '10934241000119100'       | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934281000119105&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'   | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '10934281000119105'       | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934321000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'   | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '10934321000119100'       | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=82065001&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'            | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '82065001'                | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=9468002&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'             | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '9468002'                 | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=9468002&inputType=sourceUi&searchType=exact&sabs=SNOMEDCT_US'            | null            | null                            | null                  | null                  | null                | UmlsInputType.sourceUi | null     | null       | null     | null          | null                      | ['SNOMEDCT_US'] | UmlsSearchType.exact | '9468002'                 | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=C0009044&sabs=SNOMEDCT_US&returnIdType=code'                             | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | UmlsReturnIdType.code     | ['SNOMEDCT_US'] | null                 | 'C0009044'                | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone&sabs=SNOMEDCT_US&returnIdType=code'        | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | UmlsReturnIdType.code     | ['SNOMEDCT_US'] | null                 | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone&searchType=exact'                          | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | UmlsSearchType.exact | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone'                                           | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | null                 | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=high%20cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi&' | null            | null                            | null                  | null                  | null                | null                   | null     | 1          | null     | null          | UmlsReturnIdType.sourceUi | ['SNOMEDCT_US'] | null                 | 'high cholesterol'        | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone&sabs=SNOMEDCT_US&returnIdType=code'              | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | UmlsReturnIdType.code     | ['SNOMEDCT_US'] | null                 | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone&searchType=exact'                                | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | UmlsSearchType.exact | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone'                                                 | null            | null                            | null                  | null                  | null                | null                   | null     | null       | null     | null          | null                      | null            | null                 | 'fracture of carpal bone' | null         | null
        'https://uts-ws.nlm.nih.gov/rest/search/current?string=high+cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi&'   | null            | null                            | null                  | null                  | null                | null                   | null     | 1          | null     | null          | UmlsReturnIdType.sourceUi | ['SNOMEDCT_US'] | null                 | 'high cholesterol'        | null         | null
    }

    def 'test toUmlsQuery from URLs in UMLS query results'() {
        expect:
        var result = UmlsQueryUtil.toUmlsQuery(URI.create(url))
        result.class == cls

        where:
        cls                          | url
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms/preferred'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/relations'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A2892933/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/ancestors'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/relations'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/CUI/C0155502'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/CUI/C0848586'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2024AB/source/CST/CV/CARD'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2024AB/source/CST/CV/CARD/GEN'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/HL7V3.0/14643'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/HL7V3.0/IBURSINJ'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/atoms'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/relations'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/relations'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/relations'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/111541001'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/82065001/atoms/preferred'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/children'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/descendants'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/relations'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934241000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934281000119105&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934321000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=82065001&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=9468002&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=9468002&inputType=sourceUi&searchType=exact&sabs=SNOMEDCT_US'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=C0009044&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone&searchType=exact'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture%20of%20carpal%20bone'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=high%20cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi&'
    }

    def 'test toUmlsQuery from non-UMLS URLs'() {
        expect:
        var result = UmlsQueryUtil.toUmlsQuery(URI.create(url))
        result.class == cls

        where:
        cls                          | url
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2015AB/CUI/C0009044/atoms/preferred'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2015AB/CUI/C0009044/relations'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/AUI/A2892933/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/AUI/A8317632/ancestors'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/AUI/A8317632/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/AUI/A8317632/parents'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/AUI/A8317632/relations'
        UmlsConceptInformation.class | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/CUI/C0155502'
        UmlsConceptInformation.class | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/CUI/C0848586'
        UmlsConceptInformation.class | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/HL7V3.0/14643'
        UmlsConceptInformation.class | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/HL7V3.0/IBURSINJ'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/atoms'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/parents'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/relations'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/ancestors'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/parents'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/relations'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/parents'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/relations'
        UmlsConceptInformation.class | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/111541001'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/82065001/atoms/preferred'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/ancestors'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms'
        UmlsConceptAtoms.class       | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/attributes'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/children'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/descendants'
        UmlsConceptRelatives.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/parents'
        UmlsConceptRelations.class   | 'https://myservice.org/path/one/two/three/umls/rest/content/2016AA/source/SNOMEDCT_US/9468002/relations'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/2016AA?string=10934241000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/2016AA?string=10934281000119105&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/2016AA?string=10934321000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/2016AA?string=82065001&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/2016AA?string=9468002&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=9468002&inputType=sourceUi&searchType=exact&sabs=SNOMEDCT_US'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=C0009044&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=fracture%20of%20carpal%20bone&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=fracture%20of%20carpal%20bone&searchType=exact'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=fracture%20of%20carpal%20bone'
        UmlsSearch.class             | 'https://myservice.org/path/one/two/three/umls/rest/search/current?string=high%20cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi&'
    }

    def 'test toUmlsQuery and toUri'() {
        expect:
        var result = UmlsQueryUtil.toUmlsQuery(URI.create(url))
        var uri = UmlsQueryUtil.toUri(result)
        result.class == cls
        uri.toString() == url
        uri.getPath() == url.replaceFirst('\\?.*', '').replaceFirst('umls:', '')

        where:
        cls                          | url
        UmlsConceptAtoms.class       | 'umls:/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'umls:/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'umls:/content/2015AB/CUI/C0009044/atoms/preferred'
        UmlsConceptRelations.class   | 'umls:/content/2015AB/CUI/C0009044/relations'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/AUI/A2892933/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/AUI/A8317632/ancestors'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/AUI/A8317632/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/AUI/A8317632/parents'
        UmlsConceptRelations.class   | 'umls:/content/2016AA/AUI/A8317632/relations'
        UmlsConceptInformation.class | 'umls:/content/2016AA/CUI/C0155502'
        UmlsConceptInformation.class | 'umls:/content/2016AA/CUI/C0848586'
        UmlsConceptInformation.class | 'umls:/content/2016AA/source/HL7V3.0/14643'
        UmlsConceptInformation.class | 'umls:/content/2016AA/source/HL7V3.0/IBURSINJ'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934241000119100/ancestors'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/10934241000119100/atoms'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/source/SNOMEDCT_US/10934241000119100/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934241000119100/parents'
        UmlsConceptRelations.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934241000119100/relations'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/ancestors'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms/preferred'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/parents'
        UmlsConceptRelations.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934281000119105/relations'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/ancestors'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms/preferred'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/parents'
        UmlsConceptRelations.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/10934321000119100/relations'
        UmlsConceptInformation.class | 'umls:/content/2016AA/source/SNOMEDCT_US/111541001'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/82065001/atoms/preferred'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/ancestors'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/atoms'
        UmlsConceptAtoms.class       | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/atoms/preferred'
        UmlsConceptAttributes.class  | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/attributes'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/children'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/descendants'
        UmlsConceptRelatives.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/parents'
        UmlsConceptRelations.class   | 'umls:/content/2016AA/source/SNOMEDCT_US/9468002/relations'
        UmlsSearch.class             | 'umls:/search/2016AA?string=10934241000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'umls:/search/2016AA?string=10934281000119105&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'umls:/search/2016AA?string=10934321000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'umls:/search/2016AA?string=82065001&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'umls:/search/2016AA?string=9468002&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'umls:/search/current?string=9468002&inputType=sourceUi&searchType=exact&sabs=SNOMEDCT_US'
        UmlsSearch.class             | 'umls:/search/current?string=C0009044&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'umls:/search/current?string=fracture+of+carpal+bone&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'umls:/search/current?string=fracture+of+carpal+bone&searchType=exact'
        UmlsSearch.class             | 'umls:/search/current?string=fracture+of+carpal+bone'
        UmlsSearch.class             | 'umls:/search/current?string=high+cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi'
    }

    def 'test toUmlsQuery and toUri with baseUri'() {
        expect:
        var result = UmlsQueryUtil.toUmlsQuery(URI.create(url))
        var baseUri = URI.create('https://uts-ws.nlm.nih.gov/rest')
        var uri = UmlsQueryUtil.toUri(baseUri, result)
        result.class == cls
        uri.toString() == url

        where:
        cls                          | url
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/atoms/preferred'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2015AB/CUI/C0009044/relations'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A2892933/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/ancestors'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/AUI/A8317632/relations'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/CUI/C0155502'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/CUI/C0848586'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/HL7V3.0/14643'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/HL7V3.0/IBURSINJ'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/atoms'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934241000119100/relations'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934281000119105/relations'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/10934321000119100/relations'
        UmlsConceptInformation.class | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/111541001'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/82065001/atoms/preferred'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/ancestors'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms'
        UmlsConceptAtoms.class       | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/atoms/preferred'
        UmlsConceptAttributes.class  | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/attributes'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/children'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/descendants'
        UmlsConceptRelatives.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/parents'
        UmlsConceptRelations.class   | 'https://uts-ws.nlm.nih.gov/rest/content/2016AA/source/SNOMEDCT_US/9468002/relations'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934241000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934281000119105&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=10934321000119100&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=82065001&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/2016AA?string=9468002&sabs=SNOMEDCT_US&searchType=exact&inputType=sourceUi'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=9468002&inputType=sourceUi&searchType=exact&sabs=SNOMEDCT_US'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=C0009044&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone&sabs=SNOMEDCT_US&returnIdType=code'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone&searchType=exact'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=fracture+of+carpal+bone'
        UmlsSearch.class             | 'https://uts-ws.nlm.nih.gov/rest/search/current?string=high+cholesterol&pageNumber=1&sabs=SNOMEDCT_US&returnIdType=sourceUi'
    }
}
