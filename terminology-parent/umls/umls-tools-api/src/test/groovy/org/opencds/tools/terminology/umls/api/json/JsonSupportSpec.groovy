package org.opencds.tools.terminology.umls.api.json

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtom
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomClusterRelation
import org.opencds.tools.terminology.umls.api.model.response.UmlsAttribute
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptInfo
import org.opencds.tools.terminology.umls.api.model.response.UmlsDefinition
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult
import org.opencds.tools.terminology.umls.api.model.response.UmlsSearchResultComponent
import org.opencds.tools.terminology.umls.api.model.response.UmlsSourceAtomCluster
import spock.lang.Specification

import java.time.LocalDate

class JsonSupportSpec extends Specification {
    private static final String ANCES = "src/test/resources/ancestors-snomed-30632004.json"
    private static final String ATMRD = "src/test/resources/atom-read-snomed-64572001.json"
    private static final String ATOMS = "src/test/resources/atoms-snomed-30632004.json"
    private static final String ATTRS = "src/test/resources/attributes-snomed-208373008.json"
    private static final String DEFNS = "src/test/resources/definitions-umls-C0155502.json"
    private static final String DESCS = "src/test/resources/descendants-snomed-9468002.json"
    private static final String CHLDR = "src/test/resources/children-snomed-41763006.json"
    private static final String CNPTC = "src/test/resources/conceptinfo-read-cui-C0151744.json"
    private static final String CNPTR = "src/test/resources/conceptinfo-read-icd10-i20.1.json"
    private static final String PRNTS = "src/test/resources/parents-snomed-407163006.json"
    private static final String RLTNS = "src/test/resources/relations-snomed-417163006.json"
    private static final String SRCH1 = "src/test/resources/search-results-snomed-417163006.json"
    private static final String SRCH2 = "src/test/resources/search-results-icd10cm-i20.json"

    private static JsonSupport jsonSupport

    def setupSpec() {
        jsonSupport = new JsonSupport()
    }

    def 'test ancestors deserialization'() {
        given:
        var json = new File(ANCES).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 2
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 25
        results.result()*.class == [UmlsSourceAtomCluster.class]*25
    }

    def 'test atom read deserialization'() {
        given:
        var json = new File(ATMRD).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 1
        results.result()*.class == [UmlsSourceAtomCluster.class]*1
    }

    def 'test atoms deserialization'() {
        given:
        var json = new File(ATOMS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 4
        results.result()*.class == [UmlsAtom.class]*4
    }

    def 'test attributes deserialization'() {
        given:
        var json = new File(ATTRS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 4
        results.result()*.class == [UmlsAttribute.class]*4
        results.result()*.ui() == ['AT171513278', 'AT200786798', 'AT22757459', 'AT174425003']
        results.result()*.sourceUi() == [null]*4
        results.result()*.rootSource() == ['SNOMEDCT_US']*4
        results.result()*.name() == ['ACTIVE', 'EFFECTIVE_TIME', 'CTV3ID', 'DEFINITION_STATUS_ID']
        results.result()*.value() == ['1', '20070731', 'S240D', '900000000000073002']
    }

    def 'test children deserialization'() {
        given:
        var json = new File(CHLDR).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 3
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 25
        results.result()*.class == [UmlsSourceAtomCluster.class]*25
    }

    def 'test SourceAtomCluster read icd10 I20.1 deserialization'() {
        given:
        var json = new File(CNPTR).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 1
        results.result()*.class == [UmlsSourceAtomCluster.class]*1
        results.result()[0].ui() == 'I20.1'
        results.result()[0].suppressible() == false
        results.result()[0].obsolete() == false
        results.result()[0].rootSource() == 'ICD10CM'
        results.result()[0].atomCount() == 6
        results.result()[0].cvMemberCount() == 0
        results.result()[0].attributes()
        results.result()[0].attributes().class == UmlsConceptAttributes.class
        results.result()[0].atoms()
        results.result()[0].atoms().class == UmlsConceptAtoms.class
        results.result()[0].ancestors()
        results.result()[0].ancestors().class == UmlsConceptRelatives.class
        results.result()[0].parents()
        results.result()[0].parents().class == UmlsConceptRelatives.class
        results.result()[0].children() == null
        results.result()[0].descendants() == null
        results.result()[0].relations()
        results.result()[0].relations().class == UmlsConceptRelations.class
        results.result()[0].definitions() == null
        results.result()[0].concepts()
        results.result()[0].concepts().class == UmlsSearch.class
        results.result()[0].defaultPreferredAtom()
        results.result()[0].defaultPreferredAtom().class == UmlsConceptAtoms.class
        results.result()[0].name == 'Angina pectoris with documented spasm'
    }

    def 'test concept read cui C0151744 deserialization'() {
        given:
        var json = new File(CNPTC).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 1
        results.result()[0].class == UmlsConceptInfo.class
        results.result()[0].ui() == 'C0151744'
        results.result()[0].name() == 'Myocardial Ischemia'
        results.result()[0].dateAdded() == LocalDate.of(1992, 8, 30)
        results.result()[0].majorRevisionDate() == LocalDate.of(2021, 8, 5)
        results.result()[0].suppressible() == false
        results.result()[0].status() == 'R'
        results.result()[0].semanticTypes()
        results.result()[0].semanticTypes().size() == 1
        results.result()[0].semanticTypes().get(0).name() == 'Disease or Syndrome'
        // FIXME: SemanticTypes.URI should be converted to a semantic-network request object
        results.result()[0].semanticTypes().get(0).uri() == 'https://uts-ws.nlm.nih.gov/rest/semantic-network/2024AA/TUI/T047'
        results.result()[0].atoms()
        results.result()[0].atoms().class == UmlsConceptAtoms.class
        results.result()[0].definitions()
        results.result()[0].definitions().class == UmlsConceptDefinitions.class
        results.result()[0].relations()
        results.result()[0].relations().class == UmlsConceptRelations.class
        results.result()[0].defaultPreferredAtom()
        results.result()[0].defaultPreferredAtom().class == UmlsConceptAtoms.class
        results.result()[0].atomCount() == 424
        results.result()[0].cvMemberCount() == 0
        results.result()[0].attributeCount() == 0
        results.result()[0].relationCount() == 35
    }

    def 'test definitions deserialization'() {
        given:
        var json = new File(DEFNS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.result()
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result().size() == 5
        results.result()*.class == [UmlsDefinition.class]*5
    }

    def 'test descendants deserialization'() {
        given:
        var json = new File(DESCS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.result()
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 1000
        results.result().size() == 44
        results.result()*.class == [UmlsSourceAtomCluster.class]*44
    }

    def 'test parents deserialization'() {
        given:
        var json = new File(PRNTS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 1
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 1
        results.result()*.class == [UmlsSourceAtomCluster.class]*1
    }

    def 'test relations deserialization'() {
        given:
        var json = new File(RLTNS).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageCount() == 6
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.result().size() == 25
        results.result()*.class == [UmlsAtomClusterRelation.class]*25
    }

    def 'test search1 deserialization'() {
        given:
        var json = new File(SRCH1).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.recCount() == 1
        results.result().size() == 1
        results.result()*.class == [UmlsSearchResultComponent.class]*1
    }

    def 'test search2 deserialization'() {
        given:
        var json = new File(SRCH2).text

        when:
        var results = jsonSupport.fromJson(json, UmlsResult.class)

        then:
        noExceptionThrown()
        results
        results.pageNumber() == 1
        results.pageSize() == 25
        results.result()
        results.recCount() == 18
        results.result().size() == 18
        results.result()*.class == [UmlsSearchResultComponent.class]*18
        results.result()*.name() == ['Myocardial Ischemia', 'de novo effort; angina', 'angiospastic; angina', 'Other forms of angina pectoris', 'Coronary slow flow syndrome', 'Angina Pectoris', 'Angina Pectoris, Variant', 'Angina, Unstable', 'Exercise-induced angina', 'Refractory angina', 'Anginal equivalent', 'Angina pectoris with documented spasm', 'Myocardial Preinfarction Syndrome', 'Stable angina', 'angina pectoris with coronary microvascular dysfunction', 'Angina pectoris with coronary microvascular disease', 'Accelerated angina', 'Worsening effort angina']
        results.result()*.ui() == ['C0151744', 'C1394765', 'C1387794', 'C0348588', 'C5819577', 'C0002962', 'C0002963', 'C0002965', 'C0577698', 'C0741032', 'C0741034', 'C0235470', 'C0086666', 'C0340288', 'C5819181', 'C5819576', 'C2882087', 'C2882088']
        results.result()*.rootSource() == ['MTH', 'ICPC2ICD10ENG', 'ICPC2ICD10ENG', 'ICD10', 'ICD10CM', 'MTH', 'MSH', 'MTH', 'MTH', 'SNOMEDCT_US', 'MDR', 'SNOMEDCT_US', 'MTH', 'MTH', 'MEDCIN', 'ICD10CM', 'ICD10CM', 'ICD10CM']
        results.result()*.uri()*.class == [UmlsConceptInformation.class]*18
    }
}
