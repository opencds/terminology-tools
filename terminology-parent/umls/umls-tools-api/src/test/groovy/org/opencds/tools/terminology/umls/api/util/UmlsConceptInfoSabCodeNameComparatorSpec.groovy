/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.umls.api.util

import org.opencds.tools.terminology.umls.api.model.UmlsConcept
import org.opencds.tools.terminology.umls.api.model.UmlsSource
import org.opencds.tools.terminology.umls.api.util.UmlsConceptSabCodeNameComparator

import spock.lang.Specification

class UmlsConceptInfoSabCodeNameComparatorSpec extends Specification {
    static UmlsConceptSabCodeNameComparator comp

    def setupSpec() {
        comp = new UmlsConceptSabCodeNameComparator()
    }

    def 'test same concept'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'code','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'code','name','loinc')

        expect:
        comp.compare(c1, c2) == 0
    }

    def 'test different sab'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'code','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD9CM,'code','name','loinc')

        expect:
        comp.compare(c1, c2) == -6
    }

    def 'test different concept name'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'code','name1','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'code','name2','loinc')

        expect:
        comp.compare(c1, c2) == -1
    }

    def 'test same numeric codes'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'3927.1','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'3927.1','name','loinc')

        expect:
        comp.compare(c1, c2) == 0
    }

    def 'test numeric codes not equal'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'3927.0','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'3927.1','name','loinc')

        expect:
        comp.compare(c1, c2) == -1
    }

    def 'test non-numeric codes equal'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'E3927.1','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'E3927.1','name','loinc')

        expect:
        comp.compare(c1, c2) == 0
    }
    def 'test non-numeric codes not equal'() {
        given:
        UmlsConcept c1 = new UmlsConcept(UmlsSource.ICD10,'E3927.1','name','loinc')
        UmlsConcept c2 = new UmlsConcept(UmlsSource.ICD10,'A123.9','name','loinc')

        expect:
        comp.compare(c1, c2) == 4
    }

}
