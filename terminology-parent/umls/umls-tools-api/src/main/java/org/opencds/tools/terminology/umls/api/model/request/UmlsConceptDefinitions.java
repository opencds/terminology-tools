package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.CUI;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;

public record UmlsConceptDefinitions(Map<UmlsQueryParam, Object> queryParams,
                                     String version,
                                     UmlsVocabulary umlsVocabulary,
                                     String source,
                                     String ui) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedParams = Set.of(
            sabs,
            pageNumber,
            pageSize);

    public UmlsConceptDefinitions {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);
        if (umlsVocabulary == null) {
            if (StringUtils.isNotBlank(source)) {
                umlsVocabulary = UmlsVocabulary.source;
            } else {
                umlsVocabulary = CUI;
                source = null;
            }
        }
        switch (umlsVocabulary) {
            case AUI, CUI -> {
            }
            case source -> {
                Utils.requireNull(
                        queryParams.get(sabs),
                        "sabs must be null for source queries");
                Utils.requireNonEmptyString(
                        source,
                        "source");
            }
        }
        Utils.requireNonEmptyString(ui, "unique identifier required");
    }

    public static UmlsConceptDefinitions create(Map<UmlsQueryParam, Object> queryComponents,
                                                String version,
                                                UmlsVocabulary umlsVocabulary,
                                                String source,
                                                String ui) {
        return new UmlsConceptDefinitions(queryComponents, version, umlsVocabulary, source, ui);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        return List.of(content.getValue(), version, umlsVocabulary.getValue(), ui, Variant.definitions.getValue());
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptDefinitions> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder sabs(List<String> sabs) {
            if (sabs == null || !sabs.isEmpty()) {
                queryParams.remove(UmlsQueryParam.sabs);
            } else {
                queryParams.put(UmlsQueryParam.sabs, List.copyOf(sabs));
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        public Builder cui(String cui) {
            umlsVocabulary = CUI;
            source = null;
            this.ui = cui;
            return this;
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder source(String source, String ui) {
            umlsVocabulary = UmlsVocabulary.source;
            this.source = source;
            this.ui = ui;
            return this;
        }

        Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        Builder source(String source) {
            this.source = source;
            return this;
        }

        Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public UmlsConceptDefinitions build() {
            return new UmlsConceptDefinitions(queryParams, version, umlsVocabulary, source, ui);
        }
    }
}
