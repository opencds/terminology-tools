package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;

public record UmlsAtomRelation(String ui,
                               String sourceUi,
                               String rootSource,
                               String relationLabel,
                               String additionalRelationLabel,
                               String groupId,
                               UmlsConceptInformation relatedId,
                               String relatedIdName,
                               UmlsConceptInformation relatedFromId,
                               String relatedFromIdName,
                               int attributeCount,
                               boolean suppressible,
                               boolean obsolete,
                               boolean sourceOriginated) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String additionalRelationLabel;
        private String ui;
        private String sourceUi;
        private String rootSource;
        private String relationLabel;
        private String groupId;
        private UmlsConceptInformation relatedId;
        private String relatedIdName;
        private UmlsConceptInformation relatedFromId;
        private String relatedFromIdName;
        private int attributeCount;
        private boolean suppressible;
        private boolean obsolete;
        private boolean sourceOriginated;

        private Builder() {
        }

        public Builder additionalRelationLabel(String additionalRelationLabel) {
            this.additionalRelationLabel = additionalRelationLabel;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder sourceUi(String sourceUi) {
            this.sourceUi = sourceUi;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder relationLabel(String relationLabel) {
            this.relationLabel = relationLabel;
            return this;
        }

        public Builder groupId(String groupId) {
            this.groupId = groupId;
            return this;
        }

        public Builder relatedId(UmlsConceptInformation relatedId) {
            this.relatedId = relatedId;
            return this;
        }

        public Builder relatedIdName(String relatedIdName) {
            this.relatedIdName = relatedIdName;
            return this;
        }

        public Builder relatedFromId(UmlsConceptInformation relatedFromId) {
            this.relatedFromId = relatedFromId;
            return this;
        }

        public Builder relatedFromIdName(String relatedFromIdName) {
            this.relatedFromIdName = relatedFromIdName;
            return this;
        }

        public Builder attributeCount(int attributeCount) {
            this.attributeCount = attributeCount;
            return this;
        }

        public Builder suppressible(boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public Builder obsolete(boolean obsolete) {
            this.obsolete = obsolete;
            return this;
        }

        public Builder sourceOriginated(boolean sourceOriginated) {
            this.sourceOriginated = sourceOriginated;
            return this;
        }

        public UmlsAtomRelation build() {
            return new UmlsAtomRelation(this.ui, this.sourceUi, this.rootSource, this.relationLabel,
                    this.additionalRelationLabel, this.groupId, this.relatedId, this.relatedIdName,
                    this.relatedFromId, this.relatedFromIdName, this.attributeCount, this.suppressible,
                    this.obsolete, this.sourceOriginated);
        }
    }
}
