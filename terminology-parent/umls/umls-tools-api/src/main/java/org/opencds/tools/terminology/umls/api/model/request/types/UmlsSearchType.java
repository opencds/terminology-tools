package org.opencds.tools.terminology.umls.api.model.request.types;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum UmlsSearchType {
    words("words"),
    exact("exact"),
    normalizedString("normalizedString"),
    normalizedWords("normalizedWords"),
    rightTruncation("rightTruncation"),
    leftTruncation("leftTruncation");

    private final String value;

    UmlsSearchType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static UmlsSearchType resolve(String value) {
        if (value == null) {
            return null;
        }
        return Stream.of(values())
                .filter(uqp ->
                        StringUtils.equalsIgnoreCase(
                                uqp.value,
                                value))
                .findFirst()
                .orElse(null);
    }
}
