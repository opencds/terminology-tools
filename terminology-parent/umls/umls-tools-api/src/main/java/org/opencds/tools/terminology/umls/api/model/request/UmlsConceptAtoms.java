package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeObsolete;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeSuppressible;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.language;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.ttys;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.AUI;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.CUI;

public record UmlsConceptAtoms(Map<UmlsQueryParam, Object> queryParams,
                               String version,
                               UmlsVocabulary umlsVocabulary,
                               String source,
                               String ui,
                               boolean preferred) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedParams = Set.of(
            sabs,
            ttys,
            language,
            includeObsolete,
            includeSuppressible,
            pageNumber,
            pageSize);

    public UmlsConceptAtoms {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);
        version = version == null ? VERSION_CURRENT : version;
        if (umlsVocabulary == null) {
            if (StringUtils.isNotBlank(source)) {
                umlsVocabulary = UmlsVocabulary.source;
            } else {
                umlsVocabulary = CUI;
            }
        }

        switch (umlsVocabulary) {
            case CUI -> {
                Utils.allowed(
                        Set.of(preferred),
                        preferred,
                        "onlyAllowed variants");
                Utils.requireEmpty(
                        source,
                        "sourceVocabulary");
            }
            case AUI -> Utils.requireFalse(preferred, "preferred must be false for AUI queries");
            case source -> Utils.requireNonEmptyString(
                    source,
                    "source vocabulary must be specified");
        }
    }

    public static UmlsConceptAtoms create(Map<UmlsQueryParam, Object> queryComponents,
                                          String version,
                                          UmlsVocabulary umlsVocabulary,
                                          String source,
                                          String ui,
                                          boolean preferred) {
        return new UmlsConceptAtoms(queryComponents, version, umlsVocabulary, source, ui, preferred);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        switch (umlsVocabulary) {
            case CUI -> {
                if (preferred) {
                    return List.of(content.getValue(), version, umlsVocabulary().getValue(), ui, Variant.atoms.getValue(), PREFERRED);
                }
                return List.of(content.getValue(), version, umlsVocabulary().getValue(), ui, Variant.atoms.getValue());
            }
            case AUI -> {
                return List.of(content.getValue(), version, umlsVocabulary().getValue(), ui);
            }
            case source -> {
                if (preferred) {
                    return List.of(content.getValue(), version, umlsVocabulary().getValue(), source, ui, Variant.atoms.getValue(), PREFERRED);
                }
                return List.of(content.getValue(), version, umlsVocabulary().getValue(), source, ui, Variant.atoms.getValue());
            }
            default -> throw new RuntimeException("Unexpected value: " + umlsVocabulary);
        }
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui)
                .preferred(preferred);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptAtoms> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;
        private boolean preferred;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder sabs(List<String> sabs) {
            if (sabs == null || sabs.isEmpty()) {
                queryParams.remove(UmlsQueryParam.sabs);
            } else {
                queryParams.put(UmlsQueryParam.sabs, List.copyOf(sabs));
            }
            return this;
        }

        public Builder ttys(List<String> ttys) {
            if (ttys == null || ttys.isEmpty()) {
                queryParams.remove(UmlsQueryParam.ttys);
            } else {
                queryParams.put(UmlsQueryParam.ttys, List.copyOf(ttys));
            }
            return this;
        }

        public Builder language(String language) {
            if (language == null) {
                queryParams.remove(UmlsQueryParam.language);
            } else {
                queryParams.put(UmlsQueryParam.language, language);
            }
            return this;
        }

        public Builder includeObsolete(Boolean includeObsolete) {
            if (includeObsolete == null) {
                queryParams.remove(UmlsQueryParam.includeObsolete);
            } else {
                queryParams.put(UmlsQueryParam.includeObsolete, includeObsolete);
            }
            return this;
        }

        public Builder includeSuppressible(Boolean includeSuppressible) {
            if (includeSuppressible == null) {
                queryParams.remove(UmlsQueryParam.includeSuppressible);
            } else {
                queryParams.put(UmlsQueryParam.includeSuppressible, includeSuppressible);
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }


        public Builder aui(String aui) {
            umlsVocabulary = AUI;
            source = null;
            ui = aui;
            return this;
        }

        public Builder cui(String cui) {
            umlsVocabulary = CUI;
            source = null;
            ui = cui;
            return this;
        }

        Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        Builder source(String source) {
            this.source = source;
            return this;
        }

        public Builder source(String source, String ui) {
            umlsVocabulary = UmlsVocabulary.source;
            this.source = source;
            this.ui = ui;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder preferred() {
            preferred = true;
            return this;
        }

        Builder preferred(boolean preferred) {
            this.preferred = preferred;
            return this;
        }

        public UmlsConceptAtoms build() {
            return new UmlsConceptAtoms(
                    queryParams,
                    version,
                    umlsVocabulary,
                    source,
                    ui,
                    preferred);
        }
    }
}
