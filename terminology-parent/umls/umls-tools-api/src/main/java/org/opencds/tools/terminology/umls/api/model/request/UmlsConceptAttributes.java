package org.opencds.tools.terminology.umls.api.model.request;

import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeAttributeNames;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;

public record UmlsConceptAttributes(Map<UmlsQueryParam, Object> queryParams,
                                    String version,
                                    UmlsVocabulary umlsVocabulary,
                                    String source,
                                    String ui) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedParams = Set.of(
            includeAttributeNames,
            pageNumber,
            pageSize);

    public UmlsConceptAttributes {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);

        Utils.requireNonEmptyString(ui, "ui must be specified");
    }

    public static UmlsConceptAttributes create(Map<UmlsQueryParam, Object> queryComponents,
                                               String version,
                                               UmlsVocabulary umlsVocabulary,
                                               String source,
                                               String ui) {
        return new UmlsConceptAttributes(queryComponents, version, umlsVocabulary, source, ui);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        return List.of(content.getValue(), version, umlsVocabulary.getValue(), source, ui, Variant.attributes.getValue());
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptAttributes> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder includeAttributeNames(List<String> attributeNames) {
            if (attributeNames == null) {
                queryParams.remove(includeAttributeNames);
            } else {
                queryParams.put(includeAttributeNames, attributeNames);
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        public Builder source(String source) {
            this.source = source;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public UmlsConceptAttributes build() {
            return new UmlsConceptAttributes(
                    queryParams,
                    version,
                    umlsVocabulary,
                    source,
                    ui);
        }
    }
}
