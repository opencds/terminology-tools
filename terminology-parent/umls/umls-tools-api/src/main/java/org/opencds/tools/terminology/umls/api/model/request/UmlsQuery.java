package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public interface UmlsQuery {
    enum Variant {
        none(null),
        atoms("atoms"),
        attributes("attributes"),
        definitions("definitions"),
        relations("relations"),
        parents("parents"),
        children("children"),
        ancestors("ancestors"),
        descendants("descendants");

        private final String value;

        Variant(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Variant fromValue(String value) {
            return Arrays.stream(values())
                    .filter(v -> StringUtils.equalsIgnoreCase(v.getValue(), value))
                    .findFirst()
                    .orElse(none);
        }
    }

    String VERSION_CURRENT = "current";
    String PREFERRED = "preferred";

    boolean parameterAllowed(UmlsQueryParam... params);

    String version();

    default UmlsVocabulary umlsVocabulary() {
        throw new NotImplementedException();
    }

    default String source() {
        throw new NotImplementedException();
    }

    default String ui() {
        throw new NotImplementedException();
    }

    Map<UmlsQueryParam, Object> queryParams();

    List<String> components(String version);

    default Map<String, String> query() {
        return queryParams().entrySet().stream()
                .filter(entry -> Objects.nonNull(entry.getValue()))
                .map(entry -> Map.entry(
                        entry.getKey().getValue(),
                        entry.getKey().transformToString(entry.getValue())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    <UQ extends UmlsQuery, UQB extends UmlsQuery.Builder<UQ>> UQB copy();

    interface Builder<UQ extends UmlsQuery> {
        Builder<UQ> pageNumber(Integer pageNumber);

        Builder<UQ> pageSize(Integer pageSize);

        UQ build();

        Builder<UQ> queryParam(UmlsQueryParam umlsQueryParam, Object value);
    }
}
