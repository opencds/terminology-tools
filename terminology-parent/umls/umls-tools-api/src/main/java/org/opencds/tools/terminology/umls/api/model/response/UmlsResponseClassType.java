package org.opencds.tools.terminology.umls.api.model.response;

public enum UmlsResponseClassType {
    Atom,
    AtomClusterRelation,
    AtomRelation,
    Attribute,
    Concept,
    ConceptRelation,
    Definition,
    SourceAtomCluster,
    searchResults
}
