package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSourceAtomCluster;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ANCESTORS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATOMS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATOM_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATTRIBUTES;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CHILDREN;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CONCEPTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.C_V_MEMBER_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DEFAULT_PREFERRED_ATOM;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DEFINITIONS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DESCENDANTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.OBSOLETE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.PARENTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATIONS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SUPPRESSIBLE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsSourceAtomCluster implements JsonDeserializer<UmlsSourceAtomCluster> {

    @Override
    public UmlsSourceAtomCluster deserialize(JsonElement jsonElement,
                                     Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsSourceAtomCluster.builder()
                .ui(asString(jo.get(UI)))
                .name(asString(jo.get(NAME)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .atomCount(asInt(jo.get(ATOM_COUNT)))
                .cvMemberCount(asInt(jo.get(C_V_MEMBER_COUNT)))
                .attributes(context.deserialize(jo.get(ATTRIBUTES), UmlsConceptAttributes.class))
                .atoms(context.deserialize(jo.get(ATOMS), UmlsConceptAtoms.class))
                .parents(context.deserialize(jo.get(PARENTS), UmlsConceptRelatives.class))
                .children(context.deserialize(jo.get(CHILDREN), UmlsConceptRelatives.class))
                .ancestors(context.deserialize(jo.get(ANCESTORS), UmlsConceptRelatives.class))
                .descendants(context.deserialize(jo.get(DESCENDANTS), UmlsConceptRelatives.class))
                .relations(context.deserialize(jo.get(RELATIONS), UmlsConceptRelations.class))
                .definitions(context.deserialize(jo.get(DEFINITIONS), UmlsConceptDefinitions.class))
                .concepts(context.deserialize(jo.get(CONCEPTS), UmlsConceptInformation.class))
                .defaultPreferredAtom(context.deserialize(jo.get(DEFAULT_PREFERRED_ATOM), UmlsConceptAtoms.class))
                .suppressible(context.deserialize(jo.get(SUPPRESSIBLE), Boolean.class))
                .obsolete(context.deserialize(jo.get(OBSOLETE), Boolean.class))
                .build();
    }
}
