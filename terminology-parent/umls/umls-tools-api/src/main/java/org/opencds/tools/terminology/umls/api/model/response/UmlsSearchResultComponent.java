package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;

public record UmlsSearchResultComponent(String ui,
                                        String rootSource,
                                        String name,
                                        UmlsConceptInformation uri) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String name;

        private String ui;

        private String rootSource;

        private UmlsConceptInformation uri;

        private Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder uri(UmlsConceptInformation uri) {
            this.uri = uri;
            return this;
        }

        public UmlsSearchResultComponent build() {
            return new UmlsSearchResultComponent(ui, rootSource, name, uri);
        }
    }
}
