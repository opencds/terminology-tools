package org.opencds.tools.terminology.umls.api.model.response;

/**
 * TODO: uri should be converted to a UmlsSemanticNetwork request (TBD)
 *
 * @param name semanticType name
 * @param uri semanticType uri
 */
public record UmlsSemanticType(String name,
                               String uri) {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String name;
        private String uri;

        private Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder uri(String uri) {
            this.uri = uri;
            return this;
        }

        public UmlsSemanticType build() {
            return new UmlsSemanticType(this.name, this.uri);
        }
    }
}
