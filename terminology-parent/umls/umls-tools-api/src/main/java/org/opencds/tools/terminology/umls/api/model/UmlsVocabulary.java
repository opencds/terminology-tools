package org.opencds.tools.terminology.umls.api.model;

public enum UmlsVocabulary {
    AUI("AUI", "Unique identifier for atom"),
    CUI("CUI", "Unique identifier of concept"),
    source("source", "Source-asserted Identifier");

    private final String value;
    private final String description;

    UmlsVocabulary(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public static UmlsVocabulary resolve(String path) {
        UmlsVocabulary result = null;
        for (var vocabulary : values()) {
            if (vocabulary.value.equals(path)) {
                result = vocabulary;
            }
        }
        return result;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
