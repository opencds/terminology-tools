package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtom;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ANCESTORS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATTRIBUTES;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CHILDREN;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CODE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CONCEPT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DESCENDANTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.LANGUAGE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.OBSOLETE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.PARENTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATIONS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_CONCEPT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_DESCRIPTOR;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SUPPRESSIBLE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.TERM_TYPE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsAtom implements JsonDeserializer<UmlsAtom> {

    @Override
    public UmlsAtom deserialize(JsonElement jsonElement,
                                Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsAtom.builder()
                .ui(asString(jo.get(UI)))
                .name(asString(jo.get(NAME)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .termType(asString(jo.get(TERM_TYPE)))
                .language(asString(jo.get(LANGUAGE)))
                .code(context.deserialize(jo.get(CODE), UmlsConceptInformation.class))
                .concept(context.deserialize(jo.get(CONCEPT), UmlsConceptInformation.class))
                .sourceConcept(context.deserialize(jo.get(SOURCE_CONCEPT), UmlsConceptInformation.class))
                .sourceDescriptor(context.deserialize(jo.get(SOURCE_DESCRIPTOR), UmlsConceptInformation.class))
                .attributes(context.deserialize(jo.get(ATTRIBUTES), UmlsConceptAttributes.class))
                .parents(context.deserialize(jo.get(PARENTS), UmlsConceptRelatives.class))
                .children(context.deserialize(jo.get(CHILDREN), UmlsConceptRelatives.class))
                .ancestors(context.deserialize(jo.get(ANCESTORS), UmlsConceptRelatives.class))
                .descendants(context.deserialize(jo.get(DESCENDANTS), UmlsConceptRelatives.class))
                .relations(context.deserialize(jo.get(RELATIONS), UmlsConceptRelations.class))
                .suppressible(asBoolean(jo.get(SUPPRESSIBLE)))
                .obsolete(asBoolean(jo.get(OBSOLETE)))
                .build();
    }
}
