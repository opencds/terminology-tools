package org.opencds.tools.terminology.umls.api.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.json.support.request.JsonSupportUmlsSearch;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsAtom;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsAtomClusterRelation;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsAtomRelation;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsAttribute;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsConceptInfo;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsConceptRelation;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsDefinition;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsResultType;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsSearchResultComponent;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsSemanticType;
import org.opencds.tools.terminology.umls.api.json.support.response.JsonSupportUmlsSourceAtomCluster;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtom;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomClusterRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAttribute;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptInfo;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsDefinition;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSearchResultComponent;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSemanticType;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSourceAtomCluster;

public class JsonSupport {
    private static final Log log = LogFactory.getLog(JsonSupport.class);
    private final Gson gson;

    private static final JsonSupport instance = new JsonSupport();

    public JsonSupport() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies();

        // register TA type adapters
        gsonBuilder.registerTypeAdapter(UmlsResult.class, new JsonSupportUmlsResultType());

        gsonBuilder.registerTypeAdapter(UmlsConceptAtoms.class, new JsonSupportUmlsConceptAtoms());
        gsonBuilder.registerTypeAdapter(UmlsConceptAttributes.class, new JsonSupportUmlsConceptAttributes());
        gsonBuilder.registerTypeAdapter(UmlsConceptDefinitions.class, new JsonSupportUmlsConceptDefinitions());
        gsonBuilder.registerTypeAdapter(UmlsConceptInformation.class, new JsonSupportUmlsConceptInformation());
        gsonBuilder.registerTypeAdapter(UmlsConceptRelations.class, new JsonSupportUmlsConceptRelations());
        gsonBuilder.registerTypeAdapter(UmlsConceptRelatives.class, new JsonSupportUmlsConceptRelatives());
        gsonBuilder.registerTypeAdapter(UmlsSearch.class, new JsonSupportUmlsSearch());

        gsonBuilder.registerTypeAdapter(UmlsAtom.class, new JsonSupportUmlsAtom());
        gsonBuilder.registerTypeAdapter(UmlsAtomRelation.class, new JsonSupportUmlsAtomRelation());
        gsonBuilder.registerTypeAdapter(UmlsAtomClusterRelation.class, new JsonSupportUmlsAtomClusterRelation());
        gsonBuilder.registerTypeAdapter(UmlsAttribute.class, new JsonSupportUmlsAttribute());
        gsonBuilder.registerTypeAdapter(UmlsConceptInfo.class, new JsonSupportUmlsConceptInfo());
        gsonBuilder.registerTypeAdapter(UmlsConceptRelation.class, new JsonSupportUmlsConceptRelation());
        gsonBuilder.registerTypeAdapter(UmlsDefinition.class, new JsonSupportUmlsDefinition());
        gsonBuilder.registerTypeAdapter(UmlsSemanticType.class, new JsonSupportUmlsSemanticType());
        gsonBuilder.registerTypeAdapter(UmlsSearchResultComponent.class, new JsonSupportUmlsSearchResultComponent());
        gsonBuilder.registerTypeAdapter(UmlsSourceAtomCluster.class, new JsonSupportUmlsSourceAtomCluster());

        gson = gsonBuilder.create();
    }

    public static JsonSupport instance() {
        return instance;
    }

    public <T> T fromJson(String json, Class<T> clazz) {
        try {
            return gson.fromJson(json, clazz);
        } catch (Exception e) {
            log.error("Error parsing JSON: " + json);
            throw e;
        }
    }

    public String toJson(Object object) {
        return gson.toJson(object);
    }
}
