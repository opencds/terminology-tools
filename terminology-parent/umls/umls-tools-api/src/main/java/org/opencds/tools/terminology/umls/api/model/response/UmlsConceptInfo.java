package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;

import java.time.LocalDate;
import java.util.List;

public record UmlsConceptInfo(String ui,
                              String name,
                              LocalDate dateAdded,
                              LocalDate majorRevisionDate,
                              String status,
                              List<UmlsSemanticType> semanticTypes,
                              int atomCount,
                              int attributeCount,
                              int cvMemberCount,
                              UmlsConceptAtoms atoms,
                              UmlsConceptDefinitions definitions,
                              UmlsConceptRelations relations,
                              UmlsConceptAtoms defaultPreferredAtom,
                              int relationCount,
                              boolean suppressible) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private int atomCount;
        private String ui;
        private String name;
        private LocalDate dateAdded;
        private LocalDate majorRevisionDate;
        private String status;
        private List<UmlsSemanticType> semanticTypes;
        private int attributeCount;
        private int cvMemberCount;
        private UmlsConceptAtoms atoms;
        private UmlsConceptDefinitions definitions;
        private UmlsConceptRelations relations;
        private UmlsConceptAtoms defaultPreferredAtom;
        private int relationCount;
        private boolean suppressible;

        private Builder() {
        }

        public Builder atomCount(int atomCount) {
            this.atomCount = atomCount;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder dateAdded(LocalDate dateAdded) {
            this.dateAdded = dateAdded;
            return this;
        }

        public Builder majorRevisionDate(LocalDate majorRevisionDate) {
            this.majorRevisionDate = majorRevisionDate;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder semanticTypes(List<UmlsSemanticType> semanticTypes) {
            this.semanticTypes = semanticTypes;
            return this;
        }

        public Builder attributeCount(int attributeCount) {
            this.attributeCount = attributeCount;
            return this;
        }

        public Builder cvMemberCount(int cvMemberCount) {
            this.cvMemberCount = cvMemberCount;
            return this;
        }

        public Builder atoms(UmlsConceptAtoms atoms) {
            this.atoms = atoms;
            return this;
        }

        public Builder definitions(UmlsConceptDefinitions definitions) {
            this.definitions = definitions;
            return this;
        }

        public Builder relations(UmlsConceptRelations relations) {
            this.relations = relations;
            return this;
        }

        public Builder defaultPreferredAtom(UmlsConceptAtoms defaultPreferredAtom) {
            this.defaultPreferredAtom = defaultPreferredAtom;
            return this;
        }

        public Builder relationCount(int relationCount) {
            this.relationCount = relationCount;
            return this;
        }

        public Builder suppressible(boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public UmlsConceptInfo build() {
            return new UmlsConceptInfo(this.ui, this.name, this.dateAdded, this.majorRevisionDate, this.status,
                    this.semanticTypes, this.atomCount, this.attributeCount, this.cvMemberCount, this.atoms,
                    this.definitions, this.relations, this.defaultPreferredAtom, this.relationCount,
                    this.suppressible);
        }
    }
}
