package org.opencds.tools.terminology.umls.api.model;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsInputType;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsReturnIdType;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsSearchType;

import java.util.List;
import java.util.stream.Stream;

public enum UmlsQueryParam {
    includeObsolete("includeObsolete", Boolean.class) {
        @Override
        public Object transform(String value) {
            return Boolean.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.booleanAsString(value);
        }
    },
    includeAdditionalRelationLabels("includeAdditionalRelationLabels", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    includeAttributeNames("includeAttributeNames", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    includeRelationLabels("includeRelationLabels", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    includeSuppressible("includeSuppressible", Boolean.class) {
        @Override
        public Object transform(String value) {
            return Boolean.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.booleanAsString(value);
        }
    },
    /**
     * Search only
     */
    inputType("inputType", UmlsInputType.class) {
        @Override
        public Object transform(String value) {
            return UmlsInputType.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.value(value, UmlsInputType::getValue);
        }
    },
    language("language", String.class) {
        @Override
        public Object transform(String value) {
            return value;
        }

        @Override
        public String transformToString(Object value) {
            return Utils.string(value);
        }
    },
    pageNumber("pageNumber", Integer.class) {
        @Override
        public Object transform(String value) {
            return Integer.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.integerAsString(value);
        }
    },
    pageSize("pageSize", Integer.class) {
        @Override
        public Object transform(String value) {
            return Integer.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.integerAsString(value);
        }
    },
    /**
     * Search only
     */
    partialSearch("partialSearch", Boolean.class) {
        @Override
        public Object transform(String value) {
            return Boolean.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.booleanAsString(value);
        }
    },
    /**
     * Search only
     */
    returnIdType("returnIdType", UmlsReturnIdType.class) {
        @Override
        public Object transform(String value) {
            return UmlsReturnIdType.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.value(value, UmlsReturnIdType::getValue);
        }
    },
    sabs("sabs", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    /**
     * Search only
     */
    searchType("searchType", UmlsSearchType.class) {
        @Override
        public Object transform(String value) {
            return UmlsSearchType.valueOf(value);
        }

        @Override
        public String transformToString(Object value) {
            return ((UmlsSearchType) value).getValue();
        }
    },
    /**
     * Search only
     */
    string("string", String.class) {
        @Override
        public Object transform(String value) {
            return value;
        }

        @Override
        public String transformToString(Object value) {
            return Utils.string(value);
        }
    },
    /**
     * Crosswalk only
     */
    targetSource("targetSource", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    ttys("ttys", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    },
    _include("_include", List.class) {
        @Override
        public Object transform(String value) {
            return Utils.commaDelimToList(value);
        }

        @Override
        public String transformToString(Object value) {
            return Utils.listAsString(value);
        }
    };

    private final String value;
    private final Class<?> type;

    UmlsQueryParam(String value, Class<?> type) {
        this.value = value;
        this.type = type;
    }

    public static UmlsQueryParam resolve(String value) {
        if (value == null) {
            return null;
        }
        return Stream.of(values())
                .filter(uqp ->
                        StringUtils.equalsIgnoreCase(
                                uqp.value,
                                value))
                .findFirst()
                .orElse(null);
    }

    public String getValue() {
        return value;
    }

    public Class<?> getType() {
        return type;
    }

    public abstract Object transform(String value);

    public abstract String transformToString(Object value);
}
