/*
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.umls.api.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.opencds.tools.terminology.api.model.ConceptReferenceImpl;

public class UmlsConcept extends ConceptReferenceImpl implements Comparable<UmlsConcept> {
    private final UmlsSource umlsSource;

    private String myLoincName;

    public UmlsConcept(UmlsSource sab, String code, String name) {
        super(sab.getCodeSystem(), code, name);
        umlsSource = sab;
    }

    public UmlsConcept(UmlsSource sab, String code, String name, String loincName) {
        super(sab == null ? null : sab.getCodeSystem(), code, name);
        umlsSource = sab;
        myLoincName = loincName;
    }

    public UmlsConcept(UmlsSource sab, String code) {
        super(sab == null ? null : sab.getCodeSystem(), code, null);
        umlsSource = sab;
    }

    public UmlsSource getSab() {
        return umlsSource;
    }

    public String getCode() {
        return super.getCode();
    }

    public String getLoincName() {
        return myLoincName;
    }

    public void setLoincName(String loincName) {
        myLoincName = loincName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(myLoincName).build();
    }

    @Override
    public int compareTo(UmlsConcept o) {
        int retval = 0;
        if (o == null) {
            return 1;
        }
        retval = umlsSource.compareTo(o.umlsSource);
        if (retval == 0) {
            retval = StringUtils.compare(myLoincName, o.myLoincName);
        }
        return retval;
    }
}
