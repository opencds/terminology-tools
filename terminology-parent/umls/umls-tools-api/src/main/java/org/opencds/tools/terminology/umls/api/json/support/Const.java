package org.opencds.tools.terminology.umls.api.json.support;

public class Const {
    public static final String ANCESTORS = "ancestors";
    public static final String ATOMS = "atoms";
    public static final String ATOM_COUNT = "atomCount";
    public static final String ATTRIBUTES = "attributes";
    public static final String CHILDREN = "children";
    public static final String CLASS_TYPE = "classType";
    public static final String CONCEPTS = "concepts";
    public static final String C_V_MEMBER_COUNT = "cVMemberCount";
    public static final String DEFAULT_PREFERRED_ATOM = "defaultPreferredAtom";
    public static final String DEFINITIONS = "definitions";
    public static final String DESCENDANTS = "descendants";
    public static final String MESSAGE = "message";
    public static final String NAME = "name";
    public static final String NONE = "NONE";
    public static final String OBSOLETE = "obsolete";
    public static final String PAGE_COUNT = "pageCount";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PARENTS = "parents";
    public static final String REC_COUNT = "recCount";
    public static final String RELATIONS = "relations";
    public static final String RESULT = "result";
    public static final String RESULTS = "results";
    public static final String ROOT_SOURCE = "rootSource";
    public static final String STATUS = "status";
    public static final String SUPPRESSIBLE = "suppressible";
    public static final String UI = "ui";
    public static final String TERM_TYPE = "termType";
    public static final String LANGUAGE = "language";
    public static final String CODE = "code";
    public static final String CONCEPT = "concept";
    public static final String SOURCE_CONCEPT = "sourceConcept";
    public static final String SOURCE_DESCRIPTOR = "sourceDescriptor";
    public static final String SOURCE_UI = "sourceUi";
    public static final String GROUP_ID = "groupId";
    public static final String ATTRIBUTE_COUNT = "attributeCount";
    public static final String RELATED_FROM_ID = "relatedFromId";
    public static final String RELATED_FROM_ID_NAME = "relatedFromIdName";
    public static final String RELATION_LABEL = "relationLabel";
    public static final String ADDITIONAL_RELATION_LABEL = "additionalRelationLabel";
    public static final String RELATED_ID = "relatedId";
    public static final String RELATED_ID_NAME = "relatedIdName";
    public static final String SOURCE_ORIGINATED = "sourceOriginated";
    public static final String VALUE = "value";
    public static final String DATE_ADDED = "dateAdded";
    public static final String MAJOR_REVISION_DATE = "majorRevisionDate";
    public static final String SEMANTIC_TYPES = "semanticTypes";
    public static final String CV_MEMBER_COUNT = "cvMemberCount";
    public static final String RELATION_COUNT = "relationCount";
    public static final String URI = "uri";
}
