package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;

public record UmlsAtomClusterRelation(String ui,
                                      String rootSource,
                                      String sourceUi,
                                      String groupId,
                                      int attributeCount,
                                      UmlsConceptInformation relatedFromId,
                                      String relatedFromIdName,
                                      String relationLabel,
                                      String additionalRelationLabel,
                                      UmlsConceptInformation relatedId,
                                      String relatedIdName,
                                      boolean suppressible,
                                      boolean obsolete,
                                      boolean sourceOriginated) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String additionalRelationLabel;
        private String ui;
        private String rootSource;
        private String sourceUi;
        private String groupId;
        private int attributeCount;
        private UmlsConceptInformation relatedFromId;
        private String relatedFromIdName;
        private String relationLabel;
        private UmlsConceptInformation relatedId;
        private String relatedIdName;
        private boolean suppressible;
        private boolean obsolete;
        private boolean sourceOriginated;

        private Builder() {
        }

        public Builder additionalRelationLabel(String additionalRelationLabel) {
            this.additionalRelationLabel = additionalRelationLabel;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder sourceUi(String sourceUi) {
            this.sourceUi = sourceUi;
            return this;
        }

        public Builder groupId(String groupId) {
            this.groupId = groupId;
            return this;
        }

        public Builder attributeCount(int attributeCount) {
            this.attributeCount = attributeCount;
            return this;
        }

        public Builder relatedFromId(UmlsConceptInformation relatedFromId) {
            this.relatedFromId = relatedFromId;
            return this;
        }

        public Builder relatedFromIdName(String relatedFromIdName) {
            this.relatedFromIdName = relatedFromIdName;
            return this;
        }

        public Builder relationLabel(String relationLabel) {
            this.relationLabel = relationLabel;
            return this;
        }

        public Builder relatedId(UmlsConceptInformation relatedId) {
            this.relatedId = relatedId;
            return this;
        }

        public Builder relatedIdName(String relatedIdName) {
            this.relatedIdName = relatedIdName;
            return this;
        }

        public Builder suppressible(boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public Builder obsolete(boolean obsolete) {
            this.obsolete = obsolete;
            return this;
        }

        public Builder sourceOriginated(boolean sourceOriginated) {
            this.sourceOriginated = sourceOriginated;
            return this;
        }

        public UmlsAtomClusterRelation build() {
            return new UmlsAtomClusterRelation(this.ui, this.rootSource, this.sourceUi, this.groupId,
                    this.attributeCount, this.relatedFromId, this.relatedFromIdName, this.relationLabel,
                    this.additionalRelationLabel, this.relatedId, this.relatedIdName, this.suppressible,
                    this.obsolete, this.sourceOriginated);
        }
    }
}
