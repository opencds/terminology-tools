package org.opencds.tools.terminology.umls.api.json.support.common;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;

import static org.opencds.tools.terminology.umls.api.json.support.Const.NONE;

public class GsonUtil {
    public static Boolean asBoolean(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::getAsBoolean)
                .orElse(null);
    }

    public static Integer asInt(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::getAsInt)
                .orElse(null);
    }

    public static JsonArray asJsonArray(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::getAsJsonArray)
                .orElse(null);
    }

    public static JsonObject asJsonObject(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::getAsJsonObject)
                .orElse(null);
    }

    public static List<JsonElement> asList(JsonArray jsonArray) {
        return Optional.ofNullable(jsonArray)
                .map(JsonArray::asList)
                .orElseGet(List::of);
    }

    public static String asString(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::getAsString)
                .filter(value -> !StringUtils.equalsIgnoreCase(NONE, value))
                .orElse(null);
    }

    public static String asString(JsonObject jsonObject) {
        return Optional.ofNullable(jsonObject)
                .map(JsonElement::getAsString)
                .filter(value -> !StringUtils.equalsIgnoreCase(NONE, value))
                .orElse(null);
    }

    public static boolean isJsonArray(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::isJsonArray)
                .orElse(false);
    }

    public static boolean isJsonObject(JsonElement jsonElement) {
        return Optional.ofNullable(jsonElement)
                .map(JsonElement::isJsonObject)
                .orElse(false);
    }
}
