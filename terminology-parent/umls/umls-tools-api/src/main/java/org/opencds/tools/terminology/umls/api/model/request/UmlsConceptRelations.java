package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeAdditionalRelationLabels;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeObsolete;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeRelationLabels;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeSuppressible;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.CUI;

public record UmlsConceptRelations(Map<UmlsQueryParam, Object> queryParams,
                                   String version,
                                   UmlsVocabulary umlsVocabulary,
                                   String source,
                                   String ui) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedCUIParams = Set.of(
            includeRelationLabels,
            includeAdditionalRelationLabels,
            includeObsolete,
            includeSuppressible,
            sabs,
            pageNumber,
            pageSize);

    private static final Set<UmlsQueryParam> allowedSourceParams = Set.of(
            includeRelationLabels,
            includeAdditionalRelationLabels,
            includeObsolete,
            includeSuppressible,
            sabs,
            pageNumber,
            pageSize);

    public UmlsConceptRelations {
        queryParams = queryParams == null ? Map.of() : Collections.unmodifiableMap(queryParams);
        if (umlsVocabulary == null) {
            if (StringUtils.isNotBlank(source)) {
                umlsVocabulary = UmlsVocabulary.source;
            } else {
                umlsVocabulary = CUI;
            }
        }
        switch (umlsVocabulary) {
            case CUI, AUI -> queryParams = Utils.onlyAllowedKeys(allowedCUIParams, queryParams);
            case source -> {
                Utils.requireNull(
                        queryParams.get(sabs),
                        "sabs must be null for source queries");
                queryParams = Utils.onlyAllowedKeys(allowedSourceParams, queryParams);
            }
            default -> queryParams = Map.of();
        }
    }

    public static UmlsConceptRelations create(Map<UmlsQueryParam, Object> queryComponents,
                                              String version,
                                              UmlsVocabulary umlsVocabulary,
                                              String source,
                                              String ui) {
        return new UmlsConceptRelations(queryComponents, version, umlsVocabulary, source, ui);
    }


    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return switch (umlsVocabulary) {
            case CUI -> Arrays.stream(params)
                    .anyMatch(allowedCUIParams::contains);
            case source -> Arrays.stream(params)
                    .anyMatch(allowedSourceParams::contains);
            default -> false;
        };
    }

    @Override
    public List<String> components(String version) {
        switch (umlsVocabulary) {
            case CUI -> {
                return List.of(content.getValue(), version, umlsVocabulary().getValue(), ui, Variant.relations.getValue());
            }
            case source -> {
                return List.of(content.getValue(), version, umlsVocabulary().getValue(), source, ui, Variant.relations.getValue());
            }
            default -> throw new RuntimeException("Unsupported value: " + umlsVocabulary);
        }
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptRelations> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder includeRelationLabels(List<String> relationLabels) {
            if (relationLabels == null || relationLabels.isEmpty()) {
                queryParams.remove(includeRelationLabels);
            } else {
                queryParams.put(includeRelationLabels, List.copyOf(relationLabels));
            }
            return this;
        }

        public Builder includeAdditionalRelationLabels(List<String> additionalRelationLabels) {
            if (additionalRelationLabels == null || additionalRelationLabels.isEmpty()) {
                queryParams.remove(includeAdditionalRelationLabels);
            } else {
                queryParams.put(includeAdditionalRelationLabels, List.copyOf(additionalRelationLabels));
            }
            return this;
        }

        public Builder includeObsolete(Boolean obsolete) {
            if (obsolete == null) {
                queryParams.remove(includeObsolete);
            } else {
                queryParams.put(includeObsolete, obsolete);
            }
            return this;
        }

        public Builder includeSuppressible(Boolean suppressible) {
            if (suppressible == null) {
                queryParams.remove(includeSuppressible);
            } else {
                queryParams.put(includeSuppressible, suppressible);
            }
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder sabs(List<String> sabs) {
            if (sabs == null || sabs.isEmpty()) {
                queryParams.remove(UmlsQueryParam.sabs);
            } else {
                queryParams.put(includeSuppressible, List.copyOf(sabs));
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        public Builder cui(String cui) {
            umlsVocabulary = CUI;
            source = null;
            this.ui = cui;
            return this;
        }

        public Builder source(String source, String ui) {
            umlsVocabulary = UmlsVocabulary.source;
            this.source = source;
            this.ui = ui;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public UmlsConceptRelations build() {
            return new UmlsConceptRelations(
                    queryParams,
                    version,
                    umlsVocabulary,
                    source,
                    ui);
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        Builder source(String source) {
            this.source = source;
            return this;
        }
    }
}
