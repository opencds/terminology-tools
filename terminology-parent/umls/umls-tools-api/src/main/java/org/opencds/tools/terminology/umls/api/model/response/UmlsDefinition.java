package org.opencds.tools.terminology.umls.api.model.response;

public record UmlsDefinition(String value,
                             String rootSource,
                             boolean sourceOriginated) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String rootSource;
        private String value;
        private boolean sourceOriginated;

        private Builder() {
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Builder sourceOriginated(boolean sourceOriginated) {
            this.sourceOriginated = sourceOriginated;
            return this;
        }

        public UmlsDefinition build() {
            return new UmlsDefinition(this.value, this.rootSource, this.sourceOriginated);
        }
    }
}
