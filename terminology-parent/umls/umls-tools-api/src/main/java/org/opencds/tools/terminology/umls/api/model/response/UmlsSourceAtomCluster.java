package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch;

public record UmlsSourceAtomCluster(String ui,
                                    String name,
                                    String rootSource,
                                    Integer atomCount,
                                    Integer cvMemberCount,
                                    UmlsConceptAttributes attributes,
                                    UmlsConceptAtoms atoms,
                                    UmlsConceptRelatives parents,
                                    UmlsConceptRelatives children,
                                    UmlsConceptRelatives ancestors,
                                    UmlsConceptRelatives descendants,
                                    UmlsConceptRelations relations,
                                    UmlsConceptDefinitions definitions,
                                    UmlsSearch concepts,
                                    UmlsConceptAtoms defaultPreferredAtom,
                                    Boolean suppressible,
                                    Boolean obsolete) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UmlsConceptRelatives ancestors;
        private String ui;
        private String name;
        private String rootSource;
        private Integer atomCount;
        private Integer cvMemberCount;
        private UmlsConceptAttributes attributes;
        private UmlsConceptAtoms atoms;
        private UmlsConceptRelatives parents;
        private UmlsConceptRelatives children;
        private UmlsConceptRelatives descendants;
        private UmlsConceptRelations relations;
        private UmlsConceptDefinitions definitions;
        private UmlsSearch concepts;
        private UmlsConceptAtoms defaultPreferredAtom;
        private Boolean suppressible;
        private Boolean obsolete;

        private Builder() {
        }

        public Builder ancestors(UmlsConceptRelatives ancestors) {
            this.ancestors = ancestors;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder atomCount(Integer atomCount) {
            this.atomCount = atomCount;
            return this;
        }

        public Builder cvMemberCount(Integer cvMemberCount) {
            this.cvMemberCount = cvMemberCount;
            return this;
        }

        public Builder attributes(UmlsConceptAttributes attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder atoms(UmlsConceptAtoms atoms) {
            this.atoms = atoms;
            return this;
        }

        public Builder parents(UmlsConceptRelatives parents) {
            this.parents = parents;
            return this;
        }

        public Builder children(UmlsConceptRelatives children) {
            this.children = children;
            return this;
        }

        public Builder descendants(UmlsConceptRelatives descendants) {
            this.descendants = descendants;
            return this;
        }

        public Builder relations(UmlsConceptRelations relations) {
            this.relations = relations;
            return this;
        }

        public Builder definitions(UmlsConceptDefinitions definitions) {
            this.definitions = definitions;
            return this;
        }

        public Builder concepts(UmlsSearch concepts) {
            this.concepts = concepts;
            return this;
        }

        public Builder defaultPreferredAtom(UmlsConceptAtoms defaultPreferredAtom) {
            this.defaultPreferredAtom = defaultPreferredAtom;
            return this;
        }

        public Builder suppressible(Boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public Builder obsolete(Boolean obsolete) {
            this.obsolete = obsolete;
            return this;
        }

        public UmlsSourceAtomCluster build() {
            return new UmlsSourceAtomCluster(ui, name, rootSource, atomCount,
                    cvMemberCount, attributes, atoms, parents, children, ancestors,
                    descendants, relations, definitions, concepts, defaultPreferredAtom,
                    suppressible, obsolete);
        }
    }
}
