package org.opencds.tools.terminology.umls.api.util;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsRelative;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.request.UmlsQuery;
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.opencds.tools.terminology.umls.api.model.UmlsPathComponent.preferred;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.PREFERRED;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.VERSION_CURRENT;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.Variant.atoms;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.Variant.attributes;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.Variant.definitions;
import static org.opencds.tools.terminology.umls.api.model.request.UmlsQuery.Variant.relations;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.search;

public class UmlsQueryUtil {
    private static final String SEARCH = "search";
    private static final String CONTENT = "content";
    private static final String PERCENT_20 = "%20";

    public static String encodeSpaces(String value) {
        return RegExUtils.replaceAll(value, SPACE, PERCENT_20);
    }

    public static UmlsQuery toUmlsQuery(URI url) {
        var queryParams = queryParams(url);
        var path = new ArrayList<String>();
        boolean skip = true;
        for (var elem : url.getPath().split("/")) {
            if (skip && (StringUtils.equalsIgnoreCase(SEARCH, elem) ||
                    StringUtils.equalsIgnoreCase(CONTENT, elem))) {
                skip = false;
            }
            if (!skip) {
                path.add(elem);
            }
        }
        var queryType = path(path, 0);
        var version = path(path, 1);
        if (StringUtils.equalsIgnoreCase(queryType, content.getValue())) {
            UmlsVocabulary umlsVocabulary;
            String source = null;
            String ui;
            UmlsQuery.Variant variant = UmlsQuery.Variant.none;
            boolean preferred = false;
            umlsVocabulary = UmlsVocabulary.resolve(path(path, 2));
            if (umlsVocabulary == UmlsVocabulary.source) {
                source = path(path, 3);
                ui = path(path, 4);
                if (path.size() > 5) {
                    variant = UmlsQuery.Variant.fromValue(path.get(5));
                }
                if (path.size() > 6) {
                    preferred = StringUtils.equalsIgnoreCase(PREFERRED, path.get(6));
                }
            } else {
                ui = path(path, 3);
                if (path.size() > 4) {
                    variant = UmlsQuery.Variant.fromValue(path.get(4));
                }
                if (path.size() > 5) {
                    preferred = StringUtils.equalsIgnoreCase(PREFERRED, path.get(5));
                }
            }
            try {
                return switch (variant) {
                    case atoms -> UmlsConceptAtoms.create(queryParams, version, umlsVocabulary, source, ui, preferred);
                    case attributes -> UmlsConceptAttributes.create(queryParams, version, umlsVocabulary, source, ui);
                    case definitions -> UmlsConceptDefinitions.create(queryParams, version, umlsVocabulary, source, ui);
                    case relations -> UmlsConceptRelations.create(queryParams, version, umlsVocabulary, source, ui);
                    case parents, children, ancestors, descendants -> UmlsConceptRelatives.create(
                            queryParams, version, umlsVocabulary, source, ui, UmlsRelative.valueOf(variant.getValue()));
                    case none -> UmlsConceptInformation.create(queryParams, version, umlsVocabulary, source, ui);
                };
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        } else if (StringUtils.equalsIgnoreCase(queryType, search.getValue())) {
            return UmlsSearch.fromComponents(queryParams, version);
        }
        throw new RuntimeException("Unable to create UmlsQuery instance: " + url);
    }

    private static String path(List<String> path, int index) {
        return Optional.ofNullable(path)
                .filter(list -> list.size() > index)
                .map(list -> list.get(index))
                .orElse(null);
    }

    public static Map<UmlsQueryParam, Object> queryParams(URI url) {
        return Optional.ofNullable(url)
                .map(URI::getQuery)
                .map(s -> Arrays.asList(s.split("&")))
                .stream()
                .flatMap(Collection::stream)
                .map(param -> param.split("="))
                .map(param -> Pair.of(UmlsQueryParam.resolve(param[0]), param[1]))
                .filter(pair -> pair.getLeft() != null && pair.getRight() != null)
                .map(entry -> Map.entry(
                        entry.getKey(),
                        entry.getKey().transform(URLDecoder.decode(
                                entry.getValue(),
                                StandardCharsets.UTF_8))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));
    }

    public static URI toUri(UmlsQuery umlsQuery) {
        return toUri(null, umlsQuery, VERSION_CURRENT);
    }

    public static URI toUri(URI baseUri, UmlsQuery umlsQuery) {
        return toUri(baseUri, umlsQuery, VERSION_CURRENT);
    }

    public static URI toUri(UmlsQuery umlsQuery, String version) {
        return toUri(null, umlsQuery, version);
    }

    public static URI toUri(URI baseUri, UmlsQuery umlsQuery, String version) {
        Stream<String> path = Stream.empty();
        var queryString = umlsQuery.queryParams().entrySet().stream()
                .map(entry -> String.join(
                        "=",
                        entry.getKey().getValue(),
                        URLEncoder.encode(
                                entry.getKey().transformToString(entry.getValue()),
                                StandardCharsets.UTF_8)))
                .collect(Collectors.joining("&"));
        if (umlsQuery instanceof UmlsConceptAtoms query) {
            path = Stream.concat(
                    content(query, version),
                    Stream.of(
                            atoms.getValue(),
                            query.preferred() ? preferred.getValue() : null));
        } else if (umlsQuery instanceof UmlsConceptAttributes) {
            path = Stream.concat(
                    content(umlsQuery, version),
                    Stream.of(attributes.getValue()));
        } else if (umlsQuery instanceof UmlsConceptDefinitions) {
            path = Stream.concat(
                    content(umlsQuery, version),
                    Stream.of(definitions.getValue()));
        } else if (umlsQuery instanceof UmlsConceptInformation) {
            path = content(umlsQuery, version);
        } else if (umlsQuery instanceof UmlsConceptRelations) {
            path = Stream.concat(
                    content(umlsQuery, version),
                    Stream.of(relations.getValue()));
        } else if (umlsQuery instanceof UmlsConceptRelatives query) {
            path = Stream.concat(
                    content(query, version),
                    Stream.of(query.umlsRelative().getValue()));
        } else if (umlsQuery instanceof UmlsSearch) {
            path = search(umlsQuery, version);
        }
        return URI.create(
                String.join(
                        "",
                        baseUri == null ? "umls:/" : lastSlash(baseUri.toString()),
                        path.filter(Objects::nonNull).collect(Collectors.joining("/")),
                        (queryString.isBlank() ? "" : "?" + queryString)));
    }

    private static String lastSlash(String string) {
        return string.endsWith("/") ? string : string + "/";
    }

    private static Stream<String> content(UmlsQuery umlsQuery, String version) {
        return Stream.of(
                content.getValue(),
                umlsQuery.version() == null ? version : umlsQuery.version(),
                umlsQuery.umlsVocabulary().getValue(),
                umlsQuery.source(),
                encodeSpaces(umlsQuery.ui()));
    }

    private static Stream<String> search(UmlsQuery umlsQuery, String version) {
        return Stream.of(
                search.getValue(),
                umlsQuery.version() == null ? version : umlsQuery.version());
    }
}
