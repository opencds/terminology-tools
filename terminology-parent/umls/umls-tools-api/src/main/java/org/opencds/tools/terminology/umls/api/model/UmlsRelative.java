package org.opencds.tools.terminology.umls.api.model;

public enum UmlsRelative {
    parents("parents"),
    children("children"),
    ancestors("ancestors"),
    descendants("descendants");

    private final String value;

    UmlsRelative(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
