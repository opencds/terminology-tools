/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.umls.api.util;

import java.util.*;

import org.opencds.tools.terminology.umls.api.model.UmlsConcept;

public class UmlsConceptSabNameCodeComparator implements Comparator<UmlsConcept> {
    public int compare(UmlsConcept c1, UmlsConcept c2) {
        int sabCompare = c1.getSab().compareTo(c2.getSab());

        if (sabCompare != 0) {
            return sabCompare;
        }

        int nameCompare = c1.getPreferredName().compareTo(c2.getPreferredName());
        if (nameCompare != 0) {
            return nameCompare;
        }

        return c1.getCode().compareTo(c2.getCode());
    }
}
