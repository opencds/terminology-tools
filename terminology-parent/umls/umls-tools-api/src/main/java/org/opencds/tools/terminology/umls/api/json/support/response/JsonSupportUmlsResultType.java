package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import org.apache.commons.lang3.function.TriFunction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtom;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomClusterRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAttribute;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptInfo;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptRelation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsDefinition;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResponseClassType;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResultComponent;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResults;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSearchResultComponent;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSourceAtomCluster;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import static org.opencds.tools.terminology.umls.api.json.support.Const.CLASS_TYPE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.MESSAGE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.PAGE_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.PAGE_NUMBER;
import static org.opencds.tools.terminology.umls.api.json.support.Const.PAGE_SIZE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.REC_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RESULT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RESULTS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.STATUS;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asJsonArray;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asJsonObject;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asList;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.isJsonArray;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.isJsonObject;

/**
 * Three types of results to support:
 * <ul>
 *     <li>single result
 *         <ul>
 *             <li>"result" is a JSON object</li>
 *         </ul>
 *     </li>
 *     <li>search result
 *         <ul>
 *             <li>"result" is a JSON object</li>
 *             <li>has a "results" element that is a JSON array</li>
 *         </ul>
 *     </li>
 *     <li>variant result
 *         <ul>
 *             <li>"result" is a JSON array</li>
 *             <li>URL includes, as last path component (with the exception of <tt>preferred</tt>), one of:
 *                 <ul>
 *                     <li><tt>atoms</tt></li>
 *                     <li><tt>attributes</tt></li>
 *                     <li><tt>definitions</tt></li>
 *                     <li><tt>relations</tt></li>
 *                     <li><tt>parents</tt></li>
 *                     <li><tt>children</tt></li>
 *                     <li><tt>ancestors</tt></li>
 *                     <li><tt>descendants</tt></li>
 *                 </ul>
 *               </li>
 *         </ul>
 *     </li>
 * </ul>
 */
public class JsonSupportUmlsResultType implements JsonDeserializer<UmlsResult> {
    private static final Log log = LogFactory.getLog(JsonSupportUmlsResultType.class);

    @Override
    public UmlsResult deserialize(JsonElement jsonElement,
                                  Type typeOfT,
                                  JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var json = jsonElement.getAsJsonObject();
        var pageSize = asInt(json.get(PAGE_SIZE));
        var pageCount = asInt(json.get(PAGE_COUNT));
        var pageNumber = asInt(json.get(PAGE_NUMBER));
        var name = asString(json.get(NAME));
        Integer status = asInt(json.get(STATUS));
        var message = asString(json.get(MESSAGE));

        UmlsResult result = null;

        var builderFunction = builderFunction(pageSize, pageNumber, pageCount, name, status, message);
        if (status != null && status >= 400) {
            return context.deserialize(asJsonObject(json), UmlsResults.class);
        }
        if (json.has(RESULT)) {
            if (isJsonObject(json.get(RESULT))) {
                var jo = asJsonObject(json.get(RESULT));
                if (jo.has(CLASS_TYPE) && jo.has(REC_COUNT)) {
                    // search result
                    var recCount = asInt(jo.get(REC_COUNT));
                    result = builderFunction.apply(
                            UmlsResults.builder(),
                            processUmlsResultComponent(jo, context),
                            recCount);
                } else if (jo.has(CLASS_TYPE)) {
                    // result with one component
                    // UMLS provides this as an object; ATOM is so far the only one I've encountered
                    result = builderFunction.apply(
                            UmlsResults.builder(),
                            processUmlsResultComponent(jo, context),
                            null);
                }
            } else if (isJsonArray(json.get(RESULT))) {
                result = builderFunction.apply(
                        UmlsResults.builder(),
                        processUmlsResultComponent(json.get(RESULT), context),
                        null);
            }
        }
        return result;
    }

    private TriFunction<UmlsResult.Builder, List<UmlsResultComponent>, Integer, UmlsResult>
    builderFunction(Integer pageSize,
                    Integer pageNumber,
                    Integer pageCount,
                    String name,
                    Integer status,
                    String message) {
        return (builder, result, recCount) -> builder
                .result(result)
                .pageSize(pageSize)
                .pageNumber(pageNumber)
                .pageCount(pageCount)
                .recCount(recCount)
                .name(name)
                .status(status)
                .message(message)
                .build();
    }

    private List<UmlsResultComponent> processUmlsResultComponent(JsonElement result,
                                                                 JsonDeserializationContext context) {
        return asList(asJsonArray(result)).stream()
                .filter(JsonElement::isJsonObject)
                .map(JsonElement::getAsJsonObject)
                .map(jo -> processUmlsResultComponent(jo, context))
                .flatMap(Collection::stream)
                .toList();
    }

    private List<UmlsResultComponent> processUmlsResultComponent(JsonObject jsonObject,
                                                                 JsonDeserializationContext context) {
        if (jsonObject != null) {
            UmlsResponseClassType classType = null;
            if (jsonObject.has(CLASS_TYPE)) {
                classType = UmlsResponseClassType.valueOf(asString(jsonObject.get(CLASS_TYPE)));
            }
            if (classType == null) {
                log.warn("Unknown classType in response: " + asString(jsonObject));
            } else {
                var results = switch (classType) {
                    case Atom -> context.deserialize(jsonObject, UmlsAtom.class);
                    case AtomClusterRelation -> context.deserialize(jsonObject, UmlsAtomClusterRelation.class);
                    case AtomRelation -> context.deserialize(jsonObject, UmlsAtomRelation.class);
                    case Attribute -> context.deserialize(jsonObject, UmlsAttribute.class);
                    case Concept -> context.deserialize(jsonObject, UmlsConceptInfo.class);
                    case ConceptRelation -> context.deserialize(jsonObject, UmlsConceptRelation.class);
                    case Definition -> context.deserialize(jsonObject, UmlsDefinition.class);
                    case SourceAtomCluster -> context.deserialize(jsonObject, UmlsSourceAtomCluster.class);
                    case searchResults -> {
                        if (isJsonArray(jsonObject.get(RESULTS))) {
                            yield asJsonArray(jsonObject.get(RESULTS)).asList().stream()
                                    .map(comp -> context.deserialize(comp, UmlsSearchResultComponent.class))
                                    .toList();
                        }
                        yield List.of();
                    }
                };
                if (results instanceof UmlsResultComponent urc) {
                    return List.of(urc);
                } else if (results instanceof List<?> list) {
                    return list.stream()
                            .map(UmlsResultComponent.class::cast)
                            .toList();
                }
                return List.of();
            }
        }
        return List.of();
    }
}
