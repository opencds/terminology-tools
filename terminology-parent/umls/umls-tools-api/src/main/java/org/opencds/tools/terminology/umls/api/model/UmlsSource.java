package org.opencds.tools.terminology.umls.api.model;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.opencds.tools.terminology.api.constants.CodeSystemEnum;
import org.opencds.tools.terminology.api.model.CodeSystem;

public enum UmlsSource {
    AIR("AIR","AI/RHEUM",null),
    ALT("ALT","Alternative Billing Concepts",null),
    AOD("AOD","Alcohol and Other Drug Thesaurus",null),
    AOT("AOT","Authorized Osteopathic Thesaurus",null),
    ATC("ATC","Anatomical Therapeutic Chemical Classification System",null),
    BI("BI","Beth Israel Problem List",null),
    CCC("CCC","Clinical Care Classification",null),
    CCPSS("CCPSS","Clinical Problem Statements",null),
    CCS("CCS","Clinical Classifications Software",null),
    CCSR_ICD10CM("CCSR_ ICD10CM", "Clinical Classifications Software Refined for ICD-10-CM", null),
    CCSR_ICD10PCS("CCSR_ICD10PCS", "Clinical Classifications Software Refined for ICD-10-PCS", null),
    CDCREC("CDCREC", "Race & Ethnicity - CDC",null),
    CDT("CDT","Current Dental Terminology",null),
    CHV("CHV","Consumer Health Vocabulary",null),
    COSTAR("COSTAR","COSTAR",null),
    CPM("CPM","Medical Entities Dictionary",null),
    CPT("CPT","Current Procedural Terminology", CodeSystemEnum.CPT),
    CSP("CSP","CRISP Thesaurus",null),
    CST("CST","COSTART",null),
    CVX("CVX","Vaccines Administered", CodeSystemEnum.CVX),
    DDB("DDB","Diseases Database",null),
    DRUGBANK("DRUGBANK","DrugBank",null),
    DSM_5("DSM-5","Diagnostic and Statistical Manual of Mental Disorders, Fifth Edition",null),
    DXP("DXP","DXplain",null),
    FMA("FMA","Foundational Model of Anatomy",null),
    GO("GO","Gene Ontology",null),
    GS("GS","Gold Standard Drug Database",null),
    HCDT("HCDT","Current Dental Terminology in HCPCS",null),
    HCPCS("HCPCS","Healthcare Common Procedure Coding System", CodeSystemEnum.HCPCS),
    HCPT("HCPT","CPT in HCPCS",null),
    HGNC("HGNC","HUGO Gene Nomenclature Committee",null),
    HL7V2_5("HL7V2.5","HL7 Version 2.5",null),
    HL7V3_0("HL7V3.0","HL7 Version 3.0", CodeSystemEnum.HL7_V3),
    HPO("HPO","Human Phenotype Ontology",null),
    ICD10("ICD10","International Classification of Diseases and Related Health Problems, Tenth Revision", CodeSystemEnum.ICD10CM),
    ICD10AE("ICD10AE","ICD-10, American English Equivalents",null),
    ICD10AM("ICD10AM","ICD-10, Australian Modification",null),
    ICD10AMAE("ICD10AMAE","ICD-10, Australian Modification, Americanized English Equivalents",null),
    ICD10CM("ICD10CM","International Classification of Diseases, Tenth Revision, Clinical Modification",CodeSystemEnum.ICD10CM),
    ICD10PCS("ICD10PCS","ICD-10 Procedure Coding System", CodeSystemEnum.ICD10PCS),
    ICD9CM("ICD9CM","International Classification of Diseases, Ninth Revision, Clinical Modification", CodeSystemEnum.ICD9CM),
    ICF("ICF","International Classification of Functioning, Disability and Health",null),
    ICF_CY("ICF-CY","International Classification of Functioning, Disability and Health for Children and Youth",null),
    ICNP("ICNP","International Classification for Nursing Practice",null),
    ICPC("ICPC","International Classification of Primary Care",null),
    ICPC2EENG("ICPC2EENG","International Classification of Primary Care, 2nd Edition, Electronic",null),
    ICPC2ICD10ENG("ICPC2ICD10ENG","ICPC2-ICD10 Thesaurus",null),
    ICPC2P("ICPC2P","ICPC-2 PLUS",null),
    JABL("JABL","Congenital Mental Retardation Syndromes",null),
    LCH("LCH","Library of Congress Subject Headings",null),
    LCH_NW("LCH_NW","Library of Congress Subject Headings, Northwestern University subset",null),
    LNC("LNC","LOINC", CodeSystemEnum.LOINC),
    MCM("MCM","Glossary of Clinical Epidemiologic Terms",null),
    MDR("MDR","MedDRA",null),
    MED_RT("MED-RT","MED-RT",null),
    MEDCIN("MEDCIN","MEDCIN",null),
    MEDLINEPLUS("MEDLINEPLUS","MedlinePlus Health Topics",null),
    MMSL("MMSL","Multum",null),
    MMX("MMX","Micromedex",null),
    MSH("MSH","MeSH",null),
    MTH("MTH","Metathesaurus Names",null),
    MTHCMSFRF("MTHCMSFRF","Metathesaurus CMS Formulary Reference File",null),
    MTHICD9("MTHICD9","ICD-9-CM Entry Terms",null),
    MTHICPC2EAE("MTHICPC2EAE","ICPC2E American English Equivalents",null),
    MTHICPC2ICD10AE("MTHICPC2ICD10AE","ICPC2E-ICD10 Thesaurus, American English Equivalents",null),
    MTHMST("MTHMST","Minimal Standard Terminology (UMLS)",null),
    MTHSPL("MTHSPL","FDA Structured Product Labels",null),
    MVX("MVX","Manufacturers of Vaccines",null),
    NANDA_I("NANDA-I","NANDA-I Taxonomy",null),
    NCBI("NCBI","NCBI Taxonomy",null),
    NCI("NCI","NCI Thesaurus",null),
    NDDF("NDDF","FDB MedKnowledge",null),
    NEU("NEU","Neuronames Brain Hierarchy",null),
    NIC("NIC","Nursing Interventions Classification",null),
    NOC("NOC","Nursing Outcomes Classification",null),
    NUCCHCPT("NUCCHCPT","National Uniform Claim Committee - Health Care Provider Taxonomy",null),
    OMIM("OMIM","Online Mendelian Inheritance in Man",null),
    OMS("OMS","Omaha System",null),
    ORPHANET("ORPHANET", "ORPHANET", null),
    PCDS("PCDS","Patient Care Data Set",null),
    PDQ("PDQ","Physician Data Query",null),
    PNDS("PNDS","Perioperative Nursing Data Set",null),
    PPAC("PPAC","Pharmacy Practice Activity Classification",null),
    PSY("PSY","Psychological Index Terms",null),
    QMR("QMR","Quick Medical Reference",null),
    RAM("RAM","Clinical Concepts by R A Miller",null),
    RCD("RCD","Read Codes",null),
    RCDAE("RCDAE","Read Codes Am Engl",null),
    RCDSA("RCDSA","Read Codes Am Synth",null),
    RCDSY("RCDSY","Read Codes Synth",null),
    RXNORM("RXNORM","RXNORM", CodeSystemEnum.RXNORM),
    SNM("SNM","SNOMED 1982",null),
    SNMI("SNMI","SNOMED Intl 1998",null),
    SNOMEDCT_US("SNOMEDCT_US","US Edition of SNOMED CT", CodeSystemEnum.SNOMEDCT),
    SNOMEDCT_VET("SNOMEDCT_VET","Veterinary Extension to SNOMED CT",null),
    SOP("SOP","Source of Payment Typology",null),
    SPN("SPN","Standard Product Nomenclature",null),
    SRC("SRC","Source Terminology Names (UMLS)",null),
    ULT("ULT","UltraSTAR",null),
    UMD("UMD","Universal Medical Device Nomenclature System",null),
    USP("USP", "USP Compendial Nomenclature", null),
    USPMG("USPMG","USP Model Guidelines",null),
    UWDA("UWDA","Digital Anatomist",null),
    VANDF("VANDF","National Drug File",null),
    WHO("WHO","WHOART",null)
    ;

    private final String name;
    private final String description;
    private final CodeSystemEnum codeSystem;

    UmlsSource(String name, String description, CodeSystemEnum codeSystem) {
        this.name = name;
        this.description = description;
        this.codeSystem = codeSystem;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public CodeSystemEnum getCodeSystem() {
        return codeSystem;
    }

    public static UmlsSource byName(String sab) {
        return Arrays.stream(values()).filter(s -> s.name.equalsIgnoreCase(sab)).findFirst().orElse(null);
    }

    public static Set<UmlsSource> byCodeSystem(CodeSystem cs) {
        return Arrays.stream(values()).filter(s -> Objects.equals(cs,s.codeSystem)).collect(Collectors.toSet());
    }

}
