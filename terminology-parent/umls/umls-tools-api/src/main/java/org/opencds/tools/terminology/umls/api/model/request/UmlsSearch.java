package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsInputType;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsReturnIdType;
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsSearchType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeObsolete;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.includeSuppressible;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.inputType;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.partialSearch;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.returnIdType;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.searchType;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.string;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.search;

public record UmlsSearch(Map<UmlsQueryParam, Object> queryParams,
                         String version) implements UmlsQuery {
    public static final Set<UmlsQueryParam> allowedParams = Set.of(
            string,
            inputType,
            includeObsolete,
            includeSuppressible,
            returnIdType,
            sabs,
            searchType,
            pageNumber,
            pageSize,
            partialSearch);

    public UmlsSearch {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);
    }

    public static UmlsSearch fromComponents(Map<UmlsQueryParam, Object> queryComponents, String version) {
        return new UmlsSearch(queryComponents, version);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        return List.of(search.getValue(), version);
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version);
    }

    public static class Builder implements UmlsQuery.Builder<UmlsSearch> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;

        public Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder includeObsolete(Boolean includeObsolete) {
            if (includeObsolete == null) {
                queryParams.remove(UmlsQueryParam.includeObsolete);
            } else {
                queryParams.put(UmlsQueryParam.includeObsolete, includeObsolete);
            }
            return this;
        }

        public Builder includeSuppressible(Boolean includeSuppressible) {
            if (includeSuppressible == null) {
                queryParams.remove(UmlsQueryParam.includeSuppressible);
            } else {
                queryParams.put(UmlsQueryParam.includeSuppressible, includeSuppressible);
            }
            return this;
        }

        public Builder inputType(UmlsInputType inputType) {
            if (inputType == null) {
                queryParams.remove(UmlsQueryParam.inputType);
            } else {
                queryParams.put(UmlsQueryParam.inputType, inputType);
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        public Builder partialSearch(Boolean partialSearch) {
            if (partialSearch == null) {
                queryParams.remove(UmlsQueryParam.partialSearch);
            } else {
                queryParams.put(UmlsQueryParam.partialSearch, partialSearch);
            }
            return this;
        }

        public Builder returnIdType(UmlsReturnIdType returnIdType) {
            if (returnIdType == null) {
                queryParams.remove(UmlsQueryParam.returnIdType);
            } else {
                queryParams.put(UmlsQueryParam.returnIdType, returnIdType);
            }
            return this;
        }

        public Builder sabs(List<String> sabs) {
            if (sabs == null || sabs.isEmpty()) {
                queryParams.remove(UmlsQueryParam.sabs);
            } else {
                queryParams.put(UmlsQueryParam.sabs, sabs);
            }
            return this;
        }

        public Builder searchType(UmlsSearchType searchType) {
            if (searchType == null) {
                queryParams.remove(UmlsQueryParam.searchType);
            } else {
                queryParams.put(UmlsQueryParam.searchType, searchType);
            }
            return this;
        }

        public Builder string(String string) {
            if (StringUtils.isBlank(string)) {
                queryParams.remove(UmlsQueryParam.string);
            } else {
                queryParams.put(UmlsQueryParam.string, string);
            }
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public UmlsSearch build() {
            return new UmlsSearch(queryParams, version);

        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }
    }
}
