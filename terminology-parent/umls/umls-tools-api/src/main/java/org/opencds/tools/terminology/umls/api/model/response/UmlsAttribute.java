package org.opencds.tools.terminology.umls.api.model.response;

public record UmlsAttribute(String ui,
                            String sourceUi,
                            String rootSource,
                            String name,
                            String value) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String name;
        private String ui;
        private String sourceUi;
        private String rootSource;
        private String value;

        private Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder sourceUi(String sourceUi) {
            this.sourceUi = sourceUi;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public UmlsAttribute build() {
            return new UmlsAttribute(this.ui, this.sourceUi, this.rootSource, this.name, this.value);
        }
    }
}
