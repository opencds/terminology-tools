package org.opencds.tools.terminology.umls.api.model.request.types;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum UmlsReturnIdType {
    aui("aui"),
    concept("concept"),
    code("code"),
    sourceConcept("sourceConcept"),
    sourceDescriptor("sourceDescriptor"),
    sourceUi("sourceUi");

    public final String value;

    UmlsReturnIdType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static UmlsReturnIdType resolve(String value) {
        if (value == null) {
            return null;
        }
        return Stream.of(values())
                .filter(uqp ->
                        StringUtils.equalsIgnoreCase(
                                uqp.value,
                                value))
                .findFirst()
                .orElse(null);
    }
}
