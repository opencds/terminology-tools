package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSemanticType;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.URI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsSemanticType implements JsonDeserializer<UmlsSemanticType> {
    @Override
    public UmlsSemanticType deserialize(JsonElement jsonElement,
                                     Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsSemanticType.builder()
                .name(asString(jo.get(NAME)))
                .uri(asString(jo.get(URI)))
                .build();
    }
}
