package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.response.UmlsDefinition;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_ORIGINATED;
import static org.opencds.tools.terminology.umls.api.json.support.Const.VALUE;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsDefinition implements JsonDeserializer<UmlsDefinition> {
    @Override
    public UmlsDefinition deserialize(JsonElement jsonElement,
                                      Type typeOfT,
                                      JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsDefinition.builder()
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .value(asString(jo.get(VALUE)))
                .sourceOriginated(asBoolean(jo.get(SOURCE_ORIGINATED)))
                .build();
    }
}
