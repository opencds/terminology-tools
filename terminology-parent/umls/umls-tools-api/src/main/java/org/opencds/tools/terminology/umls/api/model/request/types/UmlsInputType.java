package org.opencds.tools.terminology.umls.api.model.request.types;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum UmlsInputType {
    atom("atom"),
    code("code"),
    sourceConcept("sourceConcept"),
    sourceDescriptor("sourceDescriptor"),
    sourceUi("sourceUi"),
    tty("tty");

    private final String value;

    UmlsInputType(String value) {
        this.value = value;
    }

    public static UmlsInputType resolve(String value) {
        if (value == null) {
            return null;
        }
        return Stream.of(values())
                .filter(uit ->
                        StringUtils.equalsIgnoreCase(
                                uit.value,
                                value))
                .findFirst()
                .orElse(null);
    }

    public String getValue() {
        return value;
    }
}
