package org.opencds.tools.terminology.umls.api.model.request.types;

public enum UmlsQueryType {
    search("search"),
    content("content");

    private final String value;

    UmlsQueryType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
