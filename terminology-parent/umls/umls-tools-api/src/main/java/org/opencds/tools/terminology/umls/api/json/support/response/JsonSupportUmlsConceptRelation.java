package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptRelation;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ADDITIONAL_RELATION_LABEL;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATTRIBUTE_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.GROUP_ID;
import static org.opencds.tools.terminology.umls.api.json.support.Const.OBSOLETE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_ID;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_ID_NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATION_LABEL;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_ORIGINATED;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_UI;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SUPPRESSIBLE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsConceptRelation implements JsonDeserializer<UmlsConceptRelation> {
    @Override
    public UmlsConceptRelation deserialize(JsonElement jsonElement,
                                     Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsConceptRelation.builder()
                .ui(asString(jo.get(UI)))
                .sourceUi(asString(jo.get(SOURCE_UI)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .sourceOriginated(asBoolean(jo.get(SOURCE_ORIGINATED)))
                .groupId(asString(jo.get(GROUP_ID)))
                .attributeCount(asInt(jo.get(ATTRIBUTE_COUNT)))
                .relationLabel(asString(jo.get(RELATION_LABEL)))
                .additionalRelationLabel(asString(jo.get(ADDITIONAL_RELATION_LABEL)))
                .relatedId(context.deserialize(jo.get(RELATED_ID), UmlsConceptInformation.class))
                .relatedIdName(asString(jo.get(RELATED_ID_NAME)))
                .suppressible(asBoolean(jo.get(SUPPRESSIBLE)))
                .obsolete(asBoolean(jo.get(OBSOLETE)))
                .build();
    }
}
