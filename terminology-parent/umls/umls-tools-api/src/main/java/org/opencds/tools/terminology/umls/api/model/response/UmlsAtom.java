package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;

public record UmlsAtom(String ui,
                       String name,
                       String rootSource,
                       String termType,
                       String language,
                       UmlsConceptInformation code,
                       UmlsConceptInformation concept,
                       UmlsConceptInformation sourceConcept,
                       UmlsConceptInformation sourceDescriptor,
                       UmlsConceptAttributes attributes,
                       UmlsConceptRelatives parents,
                       UmlsConceptRelatives children,
                       UmlsConceptRelatives ancestors,
                       UmlsConceptRelatives descendants,
                       UmlsConceptRelations relations,
                       boolean suppressible,
                       boolean obsolete) implements UmlsResultComponent {

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UmlsConceptRelatives ancestors;
        private String ui;
        private String name;
        private String rootSource;
        private String termType;
        private String language;
        private UmlsConceptInformation code;
        private UmlsConceptInformation concept;
        private UmlsConceptInformation sourceConcept;
        private UmlsConceptInformation sourceDescriptor;
        private UmlsConceptAttributes attributes;
        private UmlsConceptRelatives parents;
        private UmlsConceptRelatives children;
        private UmlsConceptRelatives descendants;
        private UmlsConceptRelations relations;
        private boolean suppressible;
        private boolean obsolete;

        private Builder() {
        }

        public Builder ancestors(UmlsConceptRelatives ancestors) {
            this.ancestors = ancestors;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder termType(String termType) {
            this.termType = termType;
            return this;
        }

        public Builder language(String language) {
            this.language = language;
            return this;
        }

        public Builder code(UmlsConceptInformation code) {
            this.code = code;
            return this;
        }

        public Builder concept(UmlsConceptInformation concept) {
            this.concept = concept;
            return this;
        }

        public Builder sourceConcept(UmlsConceptInformation sourceConcept) {
            this.sourceConcept = sourceConcept;
            return this;
        }

        public Builder sourceDescriptor(UmlsConceptInformation sourceDescriptor) {
            this.sourceDescriptor = sourceDescriptor;
            return this;
        }

        public Builder attributes(UmlsConceptAttributes attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder parents(UmlsConceptRelatives parents) {
            this.parents = parents;
            return this;
        }

        public Builder children(UmlsConceptRelatives children) {
            this.children = children;
            return this;
        }

        public Builder descendants(UmlsConceptRelatives descendants) {
            this.descendants = descendants;
            return this;
        }

        public Builder relations(UmlsConceptRelations relations) {
            this.relations = relations;
            return this;
        }

        public Builder suppressible(boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public Builder obsolete(boolean obsolete) {
            this.obsolete = obsolete;
            return this;
        }

        public UmlsAtom build() {
            return new UmlsAtom(
                    ui,
                    name,
                    rootSource,
                    termType,
                    language,
                    code,
                    concept,
                    sourceConcept,
                    sourceDescriptor,
                    attributes,
                    parents,
                    children,
                    ancestors,
                    descendants,
                    relations,
                    suppressible,
                    obsolete);
        }
    }
}
