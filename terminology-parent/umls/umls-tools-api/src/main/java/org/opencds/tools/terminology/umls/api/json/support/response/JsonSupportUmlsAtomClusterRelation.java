package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.json.support.Const;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomClusterRelation;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsAtomClusterRelation implements JsonDeserializer<UmlsAtomClusterRelation> {

    @Override
    public UmlsAtomClusterRelation deserialize(JsonElement jsonElement,
                                               Type typeOfT,
                                               JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsAtomClusterRelation.builder()
                .ui(asString(jo.get(UI)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .sourceUi(asString(jo.get(Const.SOURCE_UI)))
                .groupId(asString(jo.get(Const.GROUP_ID)))
                .attributeCount(asInt(jo.get(Const.ATTRIBUTE_COUNT)))
                .relatedFromId(context.deserialize(jo.get(Const.RELATED_FROM_ID), UmlsConceptInformation.class))
                .relatedFromIdName(asString(jo.get(Const.RELATED_FROM_ID_NAME)))
                .relationLabel(asString(jo.get(Const.RELATION_LABEL)))
                .additionalRelationLabel(asString(jo.get(Const.ADDITIONAL_RELATION_LABEL)))
                .relatedId(context.deserialize(jo.get(Const.RELATED_ID), UmlsConceptInformation.class))
                .relatedIdName(asString(jo.get(Const.RELATED_ID_NAME)))
                .suppressible(asBoolean(jo.get(Const.SUPPRESSIBLE)))
                .obsolete(asBoolean(jo.get(Const.OBSOLETE)))
                .sourceOriginated(asBoolean(jo.get(Const.SOURCE_ORIGINATED)))
                .build();
    }
}
