package org.opencds.tools.terminology.umls.api.model.response;

import java.util.List;

public interface UmlsResult {
    List<UmlsResultComponent> result();

    Integer pageCount();

    Integer pageNumber();

    Integer pageSize();

    Integer recCount();

    String name();

    Integer status();

    String message();

    default boolean hasError() {
        return status() != null && status() >= 400;
    }

    default boolean noError() {
        return !hasError();
    }

    interface Builder {
        Builder result(List<UmlsResultComponent> result);

        Builder pageCount(Integer pageCount);

        Builder pageNumber(Integer pageNumber);

        Builder pageSize(Integer pageSize);

        Builder recCount(Integer recCount);

        Builder name(String name);

        Builder status(Integer status);

        Builder message(String message);

        UmlsResult build();
    }
}
