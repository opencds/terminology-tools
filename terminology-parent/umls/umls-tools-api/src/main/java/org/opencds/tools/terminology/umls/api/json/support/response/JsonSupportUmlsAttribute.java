package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.json.support.Const;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAttribute;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_UI;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsAttribute implements JsonDeserializer<UmlsAttribute> {

    @Override
    public UmlsAttribute deserialize(JsonElement jsonElement,
                                     Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsAttribute.builder()
                .ui(asString(jo.get(UI)))
                .sourceUi(asString(jo.get(SOURCE_UI)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .name(asString(jo.get(NAME)))
                .value(asString(jo.get(Const.VALUE)))
                .build();
    }
}
