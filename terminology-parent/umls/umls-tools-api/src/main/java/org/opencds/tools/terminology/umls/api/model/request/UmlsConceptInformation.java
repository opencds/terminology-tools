package org.opencds.tools.terminology.umls.api.model.request;

import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.CUI;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;

public record UmlsConceptInformation(Map<UmlsQueryParam, Object> queryParams,
                                     String version,
                                     UmlsVocabulary umlsVocabulary,
                                     String source,
                                     String ui) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedParams = Set.of(
            pageNumber,
            pageSize);

    public UmlsConceptInformation {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);
        Utils.requireNonNull(umlsVocabulary, "vocabulary source is required");

        switch (umlsVocabulary) {
            case AUI, CUI -> {
            }
            case source -> {
                Utils.requireNull(
                        queryParams.get(sabs),
                        "sabs must be null for source queries");
                Utils.requireNonEmptyString(
                        source,
                        "source");
            }
        }
        Utils.requireNonEmptyString(ui, "unique identifier required");
    }

    public static UmlsConceptInformation create(Map<UmlsQueryParam, Object> queryComponents,
                                                String version,
                                                UmlsVocabulary umlsVocabulary,
                                                String source,
                                                String ui) {
        return new UmlsConceptInformation(queryComponents, version, umlsVocabulary, source, ui);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        switch (umlsVocabulary) {
            case CUI -> {
                return List.of(content.getValue(), version, umlsVocabulary.getValue(), ui);
            }
            case source -> {
                return List.of(content.getValue(), version, umlsVocabulary.getValue(), source, ui);
            }
            default -> throw new RuntimeException("Unexpected value: " + umlsVocabulary);
        }
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptInformation> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        public Builder cui(String cui) {
            umlsVocabulary = CUI;
            source = null;
            this.ui = cui;
            return this;
        }

        public Builder source(String source, String ui) {
            umlsVocabulary = UmlsVocabulary.source;
            this.source = source;
            this.ui = ui;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public UmlsConceptInformation build() {
            return new UmlsConceptInformation(
                    queryParams,
                    version, umlsVocabulary,
                    source,
                    ui);
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        Builder source(String source) {
            this.source = source;
            return this;
        }
    }
}
