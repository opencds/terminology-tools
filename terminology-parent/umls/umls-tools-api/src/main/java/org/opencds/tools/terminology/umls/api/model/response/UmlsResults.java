package org.opencds.tools.terminology.umls.api.model.response;

import java.util.List;

public record UmlsResults(List<UmlsResultComponent> result,
                          Integer pageCount,
                          Integer pageSize,
                          Integer pageNumber,
                          Integer recCount,
                          String name,
                          Integer status,
                          String message) implements UmlsResult {

    public UmlsResults {
        result = result == null ? List.of() : List.copyOf(result);
    }

    public static UmlsResults create(List<UmlsResultComponent> result,
                                     Integer pageCount,
                                     Integer pageNumber,
                                     Integer pageSize,
                                     String name,
                                     Integer status,
                                     String message) {
        return new UmlsResults(result, pageCount, pageNumber, pageSize, null, name, status, message);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder implements UmlsResult.Builder {
        private List<UmlsResultComponent> result;
        private Integer pageCount;
        private Integer pageNumber;
        private Integer pageSize;
        private Integer recCount;
        private String name;
        private Integer status;
        private String message;

        private Builder() {
        }

        @Override
        public Builder result(List<UmlsResultComponent> result) {
            this.result = result;
            return this;
        }

        public Builder recCount(Integer recCount) {
            this.recCount = recCount;
            return this;
        }

        public Builder pageCount(Integer pageCount) {
            this.pageCount = pageCount;
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            this.pageNumber = pageNumber;
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder status(Integer status) {
            this.status = status;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public UmlsResults build() {
            return new UmlsResults(
                    result,
                    pageCount,
                    pageSize,
                    pageNumber,
                    recCount,
                    name,
                    status,
                    message);
        }
    }
}
