package org.opencds.tools.terminology.umls.api.json.support.request;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.umls.api.model.request.UmlsQuery;
import org.opencds.tools.terminology.umls.api.util.UmlsQueryUtil;

import java.lang.reflect.Type;
import java.net.URI;

import static org.opencds.tools.terminology.umls.api.json.support.Const.NONE;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUrlToUmlsQuery<UQ extends UmlsQuery> implements JsonDeserializer<UQ> {
    @Override
    public UQ deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        if (jsonElement.isJsonPrimitive()) {
            var value = asString(jsonElement);
            if (StringUtils.equalsIgnoreCase(NONE, value) || StringUtils.isBlank(value)) {
                return null;
            }
            return (UQ) UmlsQueryUtil.toUmlsQuery(URI.create(UmlsQueryUtil.encodeSpaces(value)));
        }
        return null;
    }
}
