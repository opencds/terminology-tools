package org.opencds.tools.terminology.umls.api.model.request;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;
import org.opencds.tools.terminology.umls.api.model.UmlsPathComponent;
import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam;
import org.opencds.tools.terminology.umls.api.model.UmlsRelative;
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageNumber;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.pageSize;
import static org.opencds.tools.terminology.umls.api.model.UmlsQueryParam.sabs;
import static org.opencds.tools.terminology.umls.api.model.UmlsRelative.ancestors;
import static org.opencds.tools.terminology.umls.api.model.UmlsRelative.children;
import static org.opencds.tools.terminology.umls.api.model.UmlsRelative.descendants;
import static org.opencds.tools.terminology.umls.api.model.UmlsRelative.parents;
import static org.opencds.tools.terminology.umls.api.model.UmlsVocabulary.AUI;
import static org.opencds.tools.terminology.umls.api.model.request.types.UmlsQueryType.content;

public record UmlsConceptRelatives(Map<UmlsQueryParam, Object> queryParams,
                                   String version,
                                   UmlsVocabulary umlsVocabulary,
                                   String source,
                                   String ui,
                                   UmlsRelative umlsRelative) implements UmlsQuery {
    private static final Set<UmlsQueryParam> allowedParams = Set.of(
            pageNumber,
            pageSize);

    public UmlsConceptRelatives {
        queryParams = Utils.onlyAllowedKeys(allowedParams, queryParams);
        if (umlsVocabulary == null) {
            if (StringUtils.isNotBlank(source)) {
                umlsVocabulary = UmlsVocabulary.source;
            } else {
                umlsVocabulary = AUI;
                source = null;
            }
        }

        switch (umlsVocabulary) {
            case CUI -> throw new AssertionError("CUI is not applicable to this query");
            case AUI -> {
            }
            case source -> {
                Utils.requireNull(
                        queryParams.get(sabs),
                        "sabs must be null for source queries");
                Utils.requireNonEmptyString(
                        source,
                        "source");
            }
        }
        Utils.requireNonEmptyString(ui, "unique identifier required");
        Utils.requireNonNull(umlsRelative, "umlsRelative required");
    }

    public static UmlsConceptRelatives create(Map<UmlsQueryParam, Object> queryComponents,
                                              String version,
                                              UmlsVocabulary umlsVocabulary,
                                              String source,
                                              String ui,
                                              UmlsRelative umlsRelative) {
        return new UmlsConceptRelatives(queryComponents, version, umlsVocabulary, source, ui, umlsRelative);
    }

    private static Builder builder() {
        return new Builder();
    }

    public static Builder parents() {
        return builder().parents();
    }

    public static Builder children() {
        return builder().children();
    }

    public static Builder ancestors() {
        return builder().ancestors();
    }

    public static Builder descendants() {
        return builder().descendants();
    }

    @Override
    public boolean parameterAllowed(UmlsQueryParam... params) {
        return Arrays.stream(params)
                .anyMatch(allowedParams::contains);
    }

    @Override
    public List<String> components(String version) {
        switch (umlsVocabulary) {
            case AUI -> {
                return List.of(
                        content.getValue(),
                        version,
                        source,
                        ui,
                        umlsRelative.getValue());
            }
            case source -> {
                return List.of(
                        content.getValue(),
                        version,
                        UmlsPathComponent.source.getValue(),
                        source,
                        ui,
                        umlsRelative.getValue());
            }
            default -> throw new RuntimeException("Unsupported value: " + umlsVocabulary);
        }
    }

    @Override
    public Builder copy() {
        return new Builder()
                .queryParams(queryParams)
                .version(version)
                .umlsVocabulary(umlsVocabulary)
                .source(source)
                .ui(ui)
                .umlsRelative(umlsRelative);
    }

    public static final class Builder implements UmlsQuery.Builder<UmlsConceptRelatives> {
        private final Map<UmlsQueryParam, Object> queryParams = new HashMap<>();
        private String version;
        private UmlsVocabulary umlsVocabulary;
        private String source;
        private String ui;
        private UmlsRelative umlsRelative;

        private Builder() {
        }

        @Override
        public Builder queryParam(UmlsQueryParam umlsQueryParam, Object value) {
            if (!queryParams.containsKey(umlsQueryParam)) {
                queryParams.put(umlsQueryParam, value);
            }
            return this;
        }

        public Builder pageNumber(Integer pageNumber) {
            if (pageNumber == null) {
                queryParams.remove(UmlsQueryParam.pageNumber);
            } else {
                queryParams.put(UmlsQueryParam.pageNumber, pageNumber);
            }
            return this;
        }

        public Builder pageSize(Integer pageSize) {
            if (pageSize == null) {
                queryParams.remove(UmlsQueryParam.pageSize);
            } else {
                queryParams.put(UmlsQueryParam.pageSize, pageSize);
            }
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder aui(String aui) {
            umlsVocabulary = AUI;
            source = null;
            this.ui = aui;
            return this;
        }

        public Builder source(String source, String ui) {
            umlsVocabulary = UmlsVocabulary.source;
            this.source = source;
            this.ui = ui;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        Builder parents() {
            this.umlsRelative = parents;
            return this;
        }

        Builder children() {
            this.umlsRelative = children;
            return this;
        }

        Builder ancestors() {
            this.umlsRelative = ancestors;
            return this;
        }

        Builder descendants() {
            this.umlsRelative = descendants;
            return this;
        }

        public UmlsConceptRelatives build() {
            return new UmlsConceptRelatives(
                    queryParams,
                    version,
                    umlsVocabulary,
                    source,
                    ui,
                    umlsRelative);
        }

        Builder queryParams(Map<UmlsQueryParam, Object> queryParams) {
            this.queryParams.putAll(queryParams);
            return this;
        }

        Builder umlsVocabulary(UmlsVocabulary umlsVocabulary) {
            this.umlsVocabulary = umlsVocabulary;
            return this;
        }

        Builder source(String source) {
            this.source = source;
            return this;
        }

        Builder umlsRelative(UmlsRelative umlsRelative) {
            this.umlsRelative = umlsRelative;
            return this;
        }
    }
}
