package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSearchResultComponent;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.CODE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.Const.URI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.isJsonObject;

public class JsonSupportUmlsSearchResultComponent implements JsonDeserializer<UmlsSearchResultComponent> {
    @Override
    public UmlsSearchResultComponent deserialize(JsonElement jsonElement,
                                                 Type typeOfT,
                                                 JsonDeserializationContext context) throws JsonParseException {
        if (isJsonObject(jsonElement)) {

            var jo = jsonElement.getAsJsonObject();
            return UmlsSearchResultComponent.builder()
                    .ui(asString(jo.get(UI)))
                    .name(asString(jo.get(NAME)))
                    .rootSource(asString(jo.get(ROOT_SOURCE)))
                    .uri(context.deserialize(jo.get(URI), UmlsConceptInformation.class))
                    .build();
        }
        return null;
    }
}
