package org.opencds.tools.terminology.umls.api.model.response;

import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;

public record UmlsConceptRelation(String ui,
                                  String sourceUi,
                                  String rootSource,
                                  boolean sourceOriginated,
                                  String groupId,
                                  int attributeCount,
                                  String relationLabel,
                                  String additionalRelationLabel,
                                  UmlsConceptInformation relatedId,
                                  String relatedIdName,
                                  boolean suppressible,
                                  boolean obsolete) implements UmlsResultComponent {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String additionalRelationLabel;
        private String ui;
        private String sourceUi;
        private String rootSource;
        private boolean sourceOriginated;
        private String groupId;
        private int attributeCount;
        private String relationLabel;
        private UmlsConceptInformation relatedId;
        private String relatedIdName;
        private boolean suppressible;
        private boolean obsolete;

        private Builder() {
        }

        public Builder additionalRelationLabel(String additionalRelationLabel) {
            this.additionalRelationLabel = additionalRelationLabel;
            return this;
        }

        public Builder ui(String ui) {
            this.ui = ui;
            return this;
        }

        public Builder sourceUi(String sourceUi) {
            this.sourceUi = sourceUi;
            return this;
        }

        public Builder rootSource(String rootSource) {
            this.rootSource = rootSource;
            return this;
        }

        public Builder sourceOriginated(boolean sourceOriginated) {
            this.sourceOriginated = sourceOriginated;
            return this;
        }

        public Builder groupId(String groupId) {
            this.groupId = groupId;
            return this;
        }

        public Builder attributeCount(int attributeCount) {
            this.attributeCount = attributeCount;
            return this;
        }

        public Builder relationLabel(String relationLabel) {
            this.relationLabel = relationLabel;
            return this;
        }

        public Builder relatedId(UmlsConceptInformation relatedId) {
            this.relatedId = relatedId;
            return this;
        }

        public Builder relatedIdName(String relatedIdName) {
            this.relatedIdName = relatedIdName;
            return this;
        }

        public Builder suppressible(boolean suppressible) {
            this.suppressible = suppressible;
            return this;
        }

        public Builder obsolete(boolean obsolete) {
            this.obsolete = obsolete;
            return this;
        }

        public UmlsConceptRelation build() {
            return new UmlsConceptRelation(ui, sourceUi, rootSource, sourceOriginated,
                    groupId, attributeCount, relationLabel, additionalRelationLabel,
                    relatedId, relatedIdName, suppressible, obsolete);
        }
    }
}
