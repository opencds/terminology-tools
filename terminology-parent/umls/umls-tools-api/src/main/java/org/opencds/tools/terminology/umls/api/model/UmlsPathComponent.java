package org.opencds.tools.terminology.umls.api.model;

public enum UmlsPathComponent {
    queryType("queryType"),
    version("version"),
    umlsVocabulary("umlsVocabulary"),
    source("source"),
    ui("ui"),
    variant("variant"),
    preferred("preferred");

    private final String value;

    UmlsPathComponent(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
