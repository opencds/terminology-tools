package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtomRelation;

import java.lang.reflect.Type;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ADDITIONAL_RELATION_LABEL;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATTRIBUTE_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.GROUP_ID;
import static org.opencds.tools.terminology.umls.api.json.support.Const.OBSOLETE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_FROM_ID;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_FROM_ID_NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_ID;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATED_ID_NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATION_LABEL;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ROOT_SOURCE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_ORIGINATED;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SOURCE_UI;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SUPPRESSIBLE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;

public class JsonSupportUmlsAtomRelation implements JsonDeserializer<UmlsAtomRelation> {
    @Override
    public UmlsAtomRelation deserialize(JsonElement jsonElement,
                                        Type typeOfT,
                                        JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsAtomRelation.builder()
                .ui(asString(jo.get(UI)))
                .sourceUi(asString(jo.get(SOURCE_UI)))
                .rootSource(asString(jo.get(ROOT_SOURCE)))
                .relationLabel(asString(jo.get(RELATION_LABEL)))
                .additionalRelationLabel(asString(jo.get(ADDITIONAL_RELATION_LABEL)))
                .groupId(asString(jo.get(GROUP_ID)))
                .relatedId(context.deserialize(jo.get(RELATED_ID), UmlsConceptInformation.class))
                .relatedIdName(asString(jo.get(RELATED_ID_NAME)))
                .relatedFromId(context.deserialize(jo.get(RELATED_FROM_ID), UmlsConceptInformation.class))
                .relatedFromIdName(asString(jo.get(RELATED_FROM_ID_NAME)))
                .attributeCount(asInt(jo.get(ATTRIBUTE_COUNT)))
                .suppressible(asBoolean(jo.get(SUPPRESSIBLE)))
                .obsolete(asBoolean(jo.get(OBSOLETE)))
                .sourceOriginated(asBoolean(jo.get(SOURCE_ORIGINATED)))
                .build();
    }
}
