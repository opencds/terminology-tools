package org.opencds.tools.terminology.umls.api.json.support.response;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptInfo;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSemanticType;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.opencds.tools.terminology.umls.api.json.support.Const.ATOMS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATOM_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.ATTRIBUTE_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.CV_MEMBER_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DATE_ADDED;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DEFAULT_PREFERRED_ATOM;
import static org.opencds.tools.terminology.umls.api.json.support.Const.DEFINITIONS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.MAJOR_REVISION_DATE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.NAME;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATIONS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.RELATION_COUNT;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SEMANTIC_TYPES;
import static org.opencds.tools.terminology.umls.api.json.support.Const.STATUS;
import static org.opencds.tools.terminology.umls.api.json.support.Const.SUPPRESSIBLE;
import static org.opencds.tools.terminology.umls.api.json.support.Const.UI;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asBoolean;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asInt;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asJsonArray;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asList;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.asString;
import static org.opencds.tools.terminology.umls.api.json.support.common.GsonUtil.isJsonArray;

public class JsonSupportUmlsConceptInfo implements JsonDeserializer<UmlsConceptInfo> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy");

    @Override
    public UmlsConceptInfo deserialize(JsonElement jsonElement,
                                       Type typeOfT,
                                       JsonDeserializationContext context) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        var jo = jsonElement.getAsJsonObject();
        return UmlsConceptInfo.builder()
                .ui(asString(jo.get(UI)))
                .name(asString(jo.get(NAME)))
                .dateAdded(Optional.ofNullable(asString(jo.get(DATE_ADDED)))
                        .map(date -> LocalDate.parse(date, FORMATTER))
                        .orElse(null))
                .majorRevisionDate(Optional.ofNullable(asString(jo.get(MAJOR_REVISION_DATE)))
                        .map(date -> LocalDate.parse(date, FORMATTER))
                        .orElse(null))
                .status(asString(jo.get(STATUS)))
                .semanticTypes(deserializeSemanticTypes(jo.get(SEMANTIC_TYPES), context))
                .atomCount(asInt(jo.get(ATOM_COUNT)))
                .attributeCount(asInt(jo.get(ATTRIBUTE_COUNT)))
                .cvMemberCount(asInt(jo.get(CV_MEMBER_COUNT)))
                .atoms(context.deserialize(jo.get(ATOMS), UmlsConceptAtoms.class))
                .definitions(context.deserialize(jo.get(DEFINITIONS), UmlsConceptDefinitions.class))
                .relations(context.deserialize(jo.get(RELATIONS), UmlsConceptRelations.class))
                .defaultPreferredAtom(context.deserialize(jo.get(DEFAULT_PREFERRED_ATOM), UmlsConceptAtoms.class))
                .relationCount(asInt(jo.get(RELATION_COUNT)))
                .suppressible(asBoolean(jo.get(SUPPRESSIBLE)))
                .build();
    }

    private List<UmlsSemanticType> deserializeSemanticTypes(JsonElement jsonElement,
                                                            JsonDeserializationContext context) {
        if (isJsonArray(jsonElement)) {
            return asList(asJsonArray(jsonElement)).stream()
                    .map(je -> context.<UmlsSemanticType>deserialize(je, UmlsSemanticType.class))
                    .toList();
        }
        return List.of();
    }
}
