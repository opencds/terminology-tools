/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.umls.tools.store;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.PreparedStatementFacade;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;

class SpecifiedConcept {
    private static final Log log = LogFactory.getLog(SpecifiedConcept.class);

    private static final String UPDATE_SPECIFIED_CONCEPT = "UPDATE SPECIFIED_CONCEPT SET CONCEPT_NAME = ?, CONCEPT_LOINC_NAME = ? WHERE CONCEPT_UMLS_SAB = ? AND CONCEPT_CODE = ?";
    private static final String SPECIFIED_CONCEPT_SELECT_WHERE_VALUE_SET_ID = "SELECT INCLUDE_OR_EXCLUDE, CONCEPT_UMLS_SAB, CONCEPT_CODE, CONCEPT_NAME, CONCEPT_LOINC_NAME FROM SPECIFIED_CONCEPT WHERE VALUE_SET_ID = ?";

    private static final String COL_CONCEPT_CODE = "CONCEPT_CODE";
    private static final String COL_CONCEPT_LOINC_NAME = "CONCEPT_LOINC_NAME";
    private static final String COL_CONCEPT_NAME = "CONCEPT_NAME";
    private static final String COL_CONCEPT_UMLS_SAB = "CONCEPT_UMLS_SAB";
    private static final String COL_INCLUDE_OR_EXCLUDE = "INCLUDE_OR_EXCLUDE";

    private static final String CONST_EXCLUDE = "Exclude";

    static Map<InclusionType, Set<UmlsConcept>> getInclusionAndExclusionValueSetSpecifieds(DbConnection conn, int valueSetId) {
        Set<UmlsConcept> inclusionConcepts = new HashSet<UmlsConcept>();
        Set<UmlsConcept> exclusionConcepts = new HashSet<UmlsConcept>();
        conn.executePreparedStatement(SPECIFIED_CONCEPT_SELECT_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, valueSetId);
        }, rsIter -> {
            rsIter.forEach(rs -> {
                String includeOrExclude = rs.getString(COL_INCLUDE_OR_EXCLUDE);
                UmlsConcept umlsConcept = new UmlsConcept(UmlsSource.byName(rs.getString(COL_CONCEPT_UMLS_SAB)), rs.getString(COL_CONCEPT_CODE),
                        rs.getString(COL_CONCEPT_NAME), rs.getString(COL_CONCEPT_LOINC_NAME));
                
                if ((includeOrExclude != null) && (includeOrExclude.equalsIgnoreCase(CONST_EXCLUDE))) {
                    exclusionConcepts.add(umlsConcept);
                } else {
                    inclusionConcepts.add(umlsConcept);
                }
            });
        });
        Map<InclusionType, Set<UmlsConcept>> map = new HashMap<>();
        map.put(InclusionType.INCLUDE, inclusionConcepts);
        map.put(InclusionType.EXCLUDE, exclusionConcepts);
        return map;
    }

    static void updateSpecifiedConceptsWithoutCommit(DbConnection conn3, UmlsConcept specifiedConcept) {
        conn3.executeUpdateNoCommit(UPDATE_SPECIFIED_CONCEPT, ps -> {
            updatePreparedStatement(ps, specifiedConcept);
        });
    }
    
    private static void updatePreparedStatement(PreparedStatementFacade ps, UmlsConcept concept) {
        ps.setString(1, concept.getPreferredName());
        ps.setString(2, concept.getLoincName());
        ps.setString(3, concept.getSab().getName());
        ps.setString(4, concept.getCode());
    }

}
