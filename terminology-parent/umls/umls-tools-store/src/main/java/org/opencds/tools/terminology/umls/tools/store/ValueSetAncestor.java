/**
 * Copyright 2017-2018 OpenCDS.org
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.opencds.tools.terminology.umls.tools.store;

import org.apache.commons.lang3.function.TriConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.PreparedStatementFacade;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;

public class ValueSetAncestor {
    private static final Log log = LogFactory.getLog(ValueSetAncestor.class);

    private static final String UPDATE_VALUE_SET_ANCESTOR = "UPDATE VALUE_SET_ANCESTOR SET ANCESTOR_NAME = ?, ANCESTOR_LOINC_NAME = ? WHERE ANCESTOR_UMLS_SAB = ? AND ANCESTOR_CODE = ?";
    private static final String VALUE_SET_ANCESTOR_SELECT_WHERE_VALUE_SET_ID = "SELECT INCLUDE_OR_EXCLUDE, ANCESTOR_UMLS_SAB, ANCESTOR_CODE, ANCESTOR_NAME, ANCESTOR_LOINC_NAME, INCLUDE_ANCESTOR FROM VALUE_SET_ANCESTOR WHERE VALUE_SET_ID = ?";

    private static final String COL_ANCESTOR_UMLS_SAB = "ANCESTOR_UMLS_SAB";
    private static final String COL_ANCESTOR_CODE = "ANCESTOR_CODE";
    private static final String COL_ANCESTOR_NAME = "ANCESTOR_NAME";
    private static final String COL_ANCESTOR_LOINC_NAME = "ANCESTOR_LOINC_NAME";
    private static final String COL_INCLUDE_ANCESTOR = "INCLUDE_ANCESTOR";
    private static final String COL_INCLUDE_OR_EXCLUDE = "INCLUDE_OR_EXCLUDE";

    static void getInclusionAndExclusionValueSetAncestors(DbConnection conn, int valueSetId,
                                                          TriConsumer<UmlsConcept, String, Integer> resultConsumer) {
        conn.executePreparedStatement(
                VALUE_SET_ANCESTOR_SELECT_WHERE_VALUE_SET_ID,
                ps -> ps.setInt(1, valueSetId),
                rsIter -> rsIter.forEach(rs -> {
                    var sab = rs.getString(COL_ANCESTOR_UMLS_SAB);
                    var code = rs.getString(COL_ANCESTOR_CODE);
                    var name = rs.getString(COL_ANCESTOR_NAME);
                    var loincName = rs.getString(COL_ANCESTOR_LOINC_NAME);
                    System.err.println("getInclExcl: " + sab + " (" + UmlsSource.byName(sab) + "), " + code + ", " + name + ", " + loincName);
                    resultConsumer.accept(new UmlsConcept(
                                    UmlsSource.byName(sab),
                                    code,
                                    name,
                                    loincName),
                            rs.getString(COL_INCLUDE_OR_EXCLUDE),
                            rs.getInt(COL_INCLUDE_ANCESTOR));
                }));
    }

    static void updateAncestorConceptWithoutCommit(DbConnection conn3, UmlsConcept concept) {
        conn3.executeUpdateNoCommit(UPDATE_VALUE_SET_ANCESTOR, ps -> updatePreparedStatement(ps, concept));
    }

    private static void updatePreparedStatement(PreparedStatementFacade ps, UmlsConcept concept) {
        ValueSetMember.updatePreparedStatement(ps, concept);
    }

}
