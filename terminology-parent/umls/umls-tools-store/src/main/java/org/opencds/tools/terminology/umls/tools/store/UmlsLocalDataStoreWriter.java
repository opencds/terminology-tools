/**
 * Copyright 2017-2018 OpenCDS.org
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.opencds.tools.terminology.umls.tools.store;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;
import org.opencds.tools.terminology.umls.client.UmlsClient;

/**
 * This class writes UMLS information to locally specified data store, including
 * for defined periodic updates while the process is running. Currently, the
 * local data store is expected to be a Microsoft Access database, and that it
 * has the default structure already defined. Steps to populate and update the
 * data store: 1) In DB - Define value sets in VALUE_SET 2) In DB - Define any
 * ancestor codes to include or exclude in VALUE_SET_ANCESTOR 3) In DB - Define
 * any referenced value sets to include or exclude in REFERENCED_VALUE_SET
 *
 * @author Kensaku Kawamoto
 */
public class UmlsLocalDataStoreWriter {
    private static final Log log = LogFactory.getLog(UmlsLocalDataStoreWriter.class);

    private static final String CONST_EXCLUDE = "Exclude";

    private final UmlsClient client;

    public UmlsLocalDataStoreWriter(UmlsClient client) {
        this.client = client;
    }

    /**
     * Updates the UMLS local data store as specified. Status of DB will be
     * updated in DB table.
     *
     * @param supplier supplier for the connection to a database (supplier must create a new connection each time)
     */
    public void updateUmlsLocalDataStore(Supplier<Connection> supplier) {
        log.info("> Running updateUmlsLocalDataStore...");

        DbConnection conn1 = new DbConnection(supplier);
        DbConnection conn2 = new DbConnection(supplier);
        DbConnection conn3 = new DbConnection(supplier);

        DatabaseStatus.ensureSingleStatusEntry(conn1);

        DatabaseStatus.updating(conn1);

        // Update the value sets
        log.info("> Evaluating value sets...");

        // NOTE: valueSetName is used only for logging purposes
        ValueSet.forEachValueSet(conn1, (valueSetId, valueSetName) -> {

            // get existing value set members (note only SAB and code matter for
            // equivalence testing)
            Set<UmlsConcept> existingUmlsConceptsForValueSet = new HashSet<>();
            ValueSetMember.findExistingConcepts(conn2, valueSetId, existingUmlsConceptsForValueSet::add);

            // get the inclusion and exclusion value set ancestor concepts
            Map<AncestorInclusionType, Set<UmlsConcept>> ancestorInclusions = new HashMap<>();
            Set<UmlsConcept> inclusionAncestorConcepts_includeAncestor = new HashSet<>();
            Set<UmlsConcept> exclusionAncestorConcepts_includeAncestor = new HashSet<>();
            Set<UmlsConcept> inclusionAncestorConcepts_excludeAncestor = new HashSet<>();
            Set<UmlsConcept> exclusionAncestorConcepts_excludeAncestor = new HashSet<>();
            ValueSetAncestor.getInclusionAndExclusionValueSetAncestors(conn2, valueSetId, (umlsConcept, includeOrExclude, includeAncestor) -> {
                if ((includeOrExclude != null) && (includeOrExclude.equalsIgnoreCase(CONST_EXCLUDE))) {
                    if (includeAncestor == 1) {
                        exclusionAncestorConcepts_includeAncestor.add(umlsConcept);
                    } else {
                        exclusionAncestorConcepts_excludeAncestor.add(umlsConcept);
                    }
                } else {
                    if (includeAncestor == 1) {
                        inclusionAncestorConcepts_includeAncestor.add(umlsConcept);
                    } else {
                        inclusionAncestorConcepts_excludeAncestor.add(umlsConcept);
                    }
                }
            });
            ancestorInclusions.put(AncestorInclusionType.INCLUSION_INCLUDE_ANCESTOR, inclusionAncestorConcepts_includeAncestor);
            ancestorInclusions.put(AncestorInclusionType.EXCLUSION_INCLUDE_ANCESTOR, exclusionAncestorConcepts_includeAncestor);
            ancestorInclusions.put(AncestorInclusionType.INCLUSION_EXCLUDE_ANCESTOR, inclusionAncestorConcepts_excludeAncestor);
            ancestorInclusions.put(AncestorInclusionType.EXCLUSION_EXCLUDE_ANCESTOR, exclusionAncestorConcepts_excludeAncestor);

            // get the inclusion and exclusion value set specified concepts
            Map<InclusionType, Set<UmlsConcept>> specifiedInclusions = SpecifiedConcept.getInclusionAndExclusionValueSetSpecifieds(conn2, valueSetId);

            // get the contents of referenced value sets
            Map<InclusionType, Set<UmlsConcept>> referencedInclusions = new HashMap<>();
            ValueSet.forEachReferencedValueSet(conn2, valueSetId, (refVsIncludeOrExclude, refVsValueSetId) -> {
                Set<UmlsConcept> inclusionReferencedConcepts = new HashSet<>();
                Set<UmlsConcept> exclusionReferencedConcepts = new HashSet<>();

                Set<UmlsConcept> umlsConceptsForReferencedValueSet = new HashSet<>();
                ValueSetMember.forEachReferencedConcept(conn3, refVsValueSetId, umlsConceptsForReferencedValueSet::add);
                if (CONST_EXCLUDE.equalsIgnoreCase(refVsIncludeOrExclude)) {
                    exclusionReferencedConcepts.addAll(umlsConceptsForReferencedValueSet);
                } else {
                    inclusionReferencedConcepts.addAll(umlsConceptsForReferencedValueSet);
                }
                referencedInclusions.put(InclusionType.INCLUDE, inclusionReferencedConcepts);
                referencedInclusions.put(InclusionType.EXCLUDE, exclusionReferencedConcepts);
            });

            // Conduct UMLS operations to get contents. If successful, delete
            // exsiting value set contents and proceed.
            // If error, show error message and skip this value set.

            try {
                // define the value sets
                Set<UmlsConcept> newUmlsConceptsForValueSet = new HashSet<>();

                // add inclusion referenced concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        referencedInclusions.getOrDefault(InclusionType.INCLUDE, new HashSet<>()),
                        ValueSetMember::updateValueSetMemberWithoutCommit);
                if (referencedInclusions.get(InclusionType.INCLUDE) != null) {
                    newUmlsConceptsForValueSet.addAll(referencedInclusions.get(InclusionType.INCLUDE));
                }

                // add inclusion specified concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        specifiedInclusions.getOrDefault(InclusionType.INCLUDE, new HashSet<>()),
                        SpecifiedConcept::updateSpecifiedConceptsWithoutCommit);
                if (specifiedInclusions.get(InclusionType.INCLUDE) != null) {
                    newUmlsConceptsForValueSet.addAll(specifiedInclusions.get(InclusionType.INCLUDE));
                }


                // add inclusion ancestor concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        ancestorInclusions.get(AncestorInclusionType.INCLUSION_INCLUDE_ANCESTOR),
                        ValueSetAncestor::updateAncestorConceptWithoutCommit);
                newUmlsConceptsForValueSet.addAll(client.getDescendantConcepts(ancestorInclusions.get(AncestorInclusionType.INCLUSION_INCLUDE_ANCESTOR), true, false));

                // add exclude ancestor concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        ancestorInclusions.get(AncestorInclusionType.INCLUSION_EXCLUDE_ANCESTOR),
                        ValueSetAncestor::updateAncestorConceptWithoutCommit);
                newUmlsConceptsForValueSet.addAll(client.getDescendantConcepts(ancestorInclusions.get(AncestorInclusionType.INCLUSION_EXCLUDE_ANCESTOR), false, false));

                // remove exclusion referenced concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        referencedInclusions.getOrDefault(InclusionType.EXCLUDE, new HashSet<>()),
                        ValueSetMember::updateValueSetMemberWithoutCommit);
                if (referencedInclusions.get(InclusionType.EXCLUDE) != null) {
                    newUmlsConceptsForValueSet.removeAll(referencedInclusions.get(InclusionType.EXCLUDE));
                }

                // remove exclusion specified concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        specifiedInclusions.getOrDefault(InclusionType.EXCLUDE, new HashSet<>()),
                        SpecifiedConcept::updateSpecifiedConceptsWithoutCommit);
                if (specifiedInclusions.get(InclusionType.EXCLUDE) != null) {
                    newUmlsConceptsForValueSet.removeAll(specifiedInclusions.get(InclusionType.EXCLUDE));
                }

                // remove exclusion ancestor include concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        ancestorInclusions.get(AncestorInclusionType.EXCLUSION_INCLUDE_ANCESTOR),
                        ValueSetAncestor::updateAncestorConceptWithoutCommit);
                newUmlsConceptsForValueSet.removeAll(client.getDescendantConcepts(ancestorInclusions.get(AncestorInclusionType.EXCLUSION_INCLUDE_ANCESTOR), true, false));

                // remove exclusion ancestor exclude concepts
                populateNamesForUmlsConceptIfNeeded(conn2, conn3,
                        ancestorInclusions.get(AncestorInclusionType.EXCLUSION_EXCLUDE_ANCESTOR),
                        ValueSetAncestor::updateAncestorConceptWithoutCommit);
                newUmlsConceptsForValueSet.removeAll(client.getDescendantConcepts(ancestorInclusions.get(AncestorInclusionType.EXCLUSION_EXCLUDE_ANCESTOR), false, false));

                // since no exception thrown up to here, ready to delete
                // existing contents of value set and then to
                // re-populate it, assuming there has been an udpate

                if (newUmlsConceptsForValueSet.equals(existingUmlsConceptsForValueSet)) {
                    // do nothing
                    log.info("No changes needed for value set " + valueSetId + ": " + valueSetName + "...");
                } else {
                    log.info("Updating value set " + valueSetId + ": " + valueSetName + "...");
                    ValueSetMember.deleteValueSetMembers(conn2, valueSetId);
                    // repopulate
                    newUmlsConceptsForValueSet.forEach(newConcept ->
                            conn2.executeBatch(c -> ValueSetMember.insertValueSetMemberWithoutCommit(
                                    conn2,
                                    valueSetId,
                                    newConcept.getSab().getName(),
                                    newConcept.getCode(),
                                    newConcept.getPreferredName(),
                                    newConcept.getLoincName())));
                    ValueSet.updateValueSetUpdateDtm(conn2, new Date(), valueSetId);
                }
            } catch (Exception e) {
                log.info("> Unable to define value set for value set " + valueSetId + ": " + valueSetName
                                + ".  Skipping attempt to update value set.  Details:",
                        e);
            }

        });

        log.info("> Updating database status...");
        Timestamp lastUpdateDtm = ValueSet.getLastUpdate(conn1);
        DatabaseStatus.readyToRead(conn1, lastUpdateDtm);

        log.info("> Database update completed.");
        conn1.close();
        conn2.close();
        conn3.close();
    }

    private void populateNamesForUmlsConceptIfNeeded(DbConnection conn2, DbConnection conn3, Set<UmlsConcept> concepts,
                                                     BiConsumer<DbConnection, UmlsConcept> consumer) {
        if (!concepts.isEmpty()) {
            conn2.executeBatch(c -> concepts.stream()
                    .filter(this::populateNamesForUmlsConceptIfNeeded)
                    .forEach(concept -> consumer.accept(conn3, concept)));
        }
    }

    /**
     * If concept has SAB and Code but no name, attempts to populate the name.
     * If concept has SAB and Code and SAB is LNC, but no LOINC name, attempts
     * to populate the LOINC name. If there is an error in the attempt, throws
     * Exception. Returns whether an update was made or not.
     * <p>
     * TODO: Is it important to return whether an update was made?
     *
     * @param concept the {@link UmlsConcept}
     */
    private boolean populateNamesForUmlsConceptIfNeeded(UmlsConcept concept) {
        boolean updateMade = false;
        if (concept != null) {
            UmlsSource sab = concept.getSab();
            String code = concept.getCode();
            String name = concept.getPreferredName();
            String loincName = concept.getLoincName();

            if ((sab != null) && (code != null)) {
                if (StringUtils.isBlank(name)) {
                    String newName = client.getName(sab.getName(), code);
                    if (StringUtils.isNotBlank(newName)) {
                        concept.setPreferredName(newName);
                        updateMade = true;
                    }
                }

                if (sab == UmlsSource.LNC) {
                    if (StringUtils.isBlank(loincName)) {
                        String newLoincName = client.getLoincLongName(code);
                        if (newLoincName != null) {
                            concept.setLoincName(newLoincName);
                            updateMade = true;
                        }
                    }
                }
            }
        }
        return updateMade;
    }


}
