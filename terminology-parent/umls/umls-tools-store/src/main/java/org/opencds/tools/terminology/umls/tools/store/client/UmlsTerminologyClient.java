/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.umls.tools.store.client;

import org.opencds.tools.terminology.api.client.TerminologyClient;
import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;
import org.opencds.tools.terminology.umls.tools.store.UmlsLocalDataStoreReader;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class UmlsTerminologyClient implements TerminologyClient {
    private Supplier<Connection> connectionSupplier;
    private Map<String, Set<ConceptReference>> valueSetToUmlsConceptSet;
    private Set<ConceptReference> umlsConcepts;

    public UmlsTerminologyClient() { }

    public UmlsTerminologyClient(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    public UmlsTerminologyClient(Map<String, Set<ConceptReference>> valueSetToUmlsConceptSet) {
        this.valueSetToUmlsConceptSet = valueSetToUmlsConceptSet;
        umlsConcepts = flattenValues(valueSetToUmlsConceptSet.values());
    }

    /**
     * Also refreshes the data.
     */
    @Override
    public void setConnectionSupplier(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    public void refresh() {
        if (connectionSupplier == null) {
            throw new RuntimeException("connection is null");
        }
        UmlsLocalDataStoreReader reader = new UmlsLocalDataStoreReader(connectionSupplier);
        valueSetToUmlsConceptSet = reader.getValueSetToUmlsConceptSet();
        umlsConcepts = flattenValues(valueSetToUmlsConceptSet.values());
    }

    private Set<ConceptReference> flattenValues(Collection<Set<ConceptReference>> collection) {
        return collection.stream().flatMap(Collection::stream).collect(Collectors.toSet());
    }

    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Override
    public Set<ConceptReference> getConceptsInValueSet(String valueSetId, String version) {
        return valueSetToUmlsConceptSet.get(valueSetId);
    }

    /**
     * Returns true if concept in value set; returns false otherwise.
     *
     * @param codeSystem
     * @param code
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Override
    public boolean isConceptInValueSet(CodeSystem codeSystem, String code, String valueSetId, String version) {
        Set<ConceptReference> conceptSet = valueSetToUmlsConceptSet.get(valueSetId);
        if (conceptSet == null) {
            return false;
        }
        // only SAB and code used for equivalency testing
        Set<UmlsSource> sources = UmlsSource.byCodeSystem(codeSystem);
        // Any given CodeSystem could map to many UmlsSource concepts; hence the
        // iteration.
        return conceptSet.stream().map(c -> (UmlsConcept) c)
                .anyMatch(c -> sources.contains(c.getSab()) && c.getCode().equalsIgnoreCase(code));
    }

    @Override
    public Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version) {
        return umlsConcepts.stream()
                .filter(vc -> vc.getCodeSystem().equals(codeSystem) && vc.getCode().equalsIgnoreCase(code))
                .collect(Collectors.toSet());
    }

    @Override
    public boolean supports(ValueSetSource terminologySource) {
        return ValueSetSource.UMLS == terminologySource;
    }

}
