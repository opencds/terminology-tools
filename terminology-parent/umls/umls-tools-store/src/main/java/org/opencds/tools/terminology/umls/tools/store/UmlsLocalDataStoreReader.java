/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.umls.tools.store;

import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.api.model.ConceptReference;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class UmlsLocalDataStoreReader {

    private Supplier<Connection> connectionSupplier;
    
    private Map<String, Set<ConceptReference>> valueSetToUmlsConceptSet;
    
    public UmlsLocalDataStoreReader(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }
    
    public UmlsLocalDataStoreReader(Map<String, Set<ConceptReference>> valueSetToUmlsConceptSet) {
        this.valueSetToUmlsConceptSet = valueSetToUmlsConceptSet;
    }
    
    public void refresh() {
        DbConnection conn1 = new DbConnection(connectionSupplier);
        DbConnection conn2 = new DbConnection(connectionSupplier);
        
        Map<String, Set<ConceptReference>> myValueSetToUmlsConceptSet = new HashMap<>();
        ValueSet.forEachValueSet(conn1, valueSetId -> {
            Set<ConceptReference> conceptSetForValueSet = new HashSet<>();
            ValueSetMember.forEachValueSetMemberByValueSetId(conn2, valueSetId, (concept) -> {
                conceptSetForValueSet.add(concept);
            });
            myValueSetToUmlsConceptSet.put(Integer.toString(valueSetId), conceptSetForValueSet);
        });
        valueSetToUmlsConceptSet = myValueSetToUmlsConceptSet;

    }

    public Map<String, Set<ConceptReference>> getValueSetToUmlsConceptSet() {
        return valueSetToUmlsConceptSet;
    }

}
