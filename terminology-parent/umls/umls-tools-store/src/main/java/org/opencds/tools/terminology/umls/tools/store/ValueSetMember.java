/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.umls.tools.store;

import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.PreparedStatementFacade;
import org.opencds.tools.db.api.Result;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;

class ValueSetMember {
    private static final Log log = LogFactory.getLog(ValueSetMember.class);
    
    private static final String INSERT_VALUE_SET_MEMBER = "INSERT INTO VALUE_SET_MEMBER(VALUE_SET_ID, MEMBER_UMLS_SAB, MEMBER_CODE, MEMBER_NAME, MEMBER_LOINC_NAME) VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE_VALUE_SET_MEMBER = "UPDATE VALUE_SET_MEMBER SET MEMBER_NAME = ?, MEMBER_LOINC_NAME = ? WHERE MEMBER_UMLS_SAB = ? AND MEMBER_CODE = ?";
    private static final String DELETE_VALUE_SET_MEMBER_WHERE_VALUE_SET_ID = "DELETE FROM VALUE_SET_MEMBER WHERE VALUE_SET_ID = ?";
    private static final String VALUE_SET_MEMBER_SELECT_MEMBER_UMLS_SAB_MEMBER_CODE_WHERE_VALUE_SET_ID = "SELECT MEMBER_UMLS_SAB, MEMBER_CODE FROM VALUE_SET_MEMBER WHERE VALUE_SET_ID = ?";
    private static final String VALUE_SET_MEMBER_SELECT_WHERE_VALUE_SET_ID = "SELECT MEMBER_UMLS_SAB, MEMBER_CODE, MEMBER_NAME, MEMBER_LOINC_NAME FROM VALUE_SET_MEMBER WHERE VALUE_SET_ID = ?";
    private static final String VALUE_SET_MEMBER_QUERY = "SELECT MEMBER_UMLS_SAB, MEMBER_CODE, MEMBER_NAME, MEMBER_LOINC_NAME FROM VALUE_SET_MEMBER WHERE VALUE_SET_ID = ?";

    private static final String COL_MEMBER_CODE = "MEMBER_CODE";
    private static final String COL_MEMBER_LOINC_NAME = "MEMBER_LOINC_NAME";
    private static final String COL_MEMBER_NAME = "MEMBER_NAME";
    private static final String COL_MEMBER_UMLS_SAB = "MEMBER_UMLS_SAB";

    static void insertValueSetMemberWithoutCommit(DbConnection conn2, Integer valueSetId,
            String sab, String code, String name, String loincName) {
        System.err.println("insertValueSetWOComm: " + sab + "," + code + ", " + name + ", " + loincName);
        conn2.executeUpdateNoCommit(INSERT_VALUE_SET_MEMBER, ps -> {
            ps.setInt(1, valueSetId);
            ps.setString(2, sab);
            ps.setString(3, code);
            ps.setString(4, name);
            ps.setString(5, loincName);
        });
    }
    
    static void updateValueSetMemberWithoutCommit(DbConnection conn3, UmlsConcept concept) {
        conn3.executeUpdateNoCommit(UPDATE_VALUE_SET_MEMBER, ps -> {
            updatePreparedStatement(ps, concept);
        });
    }
    
    static void updatePreparedStatement(PreparedStatementFacade ps, UmlsConcept concept) {
        ps.setString(1, concept.getPreferredName());
        ps.setString(2, concept.getLoincName());
        ps.setString(3, concept.getSab().getName());
        ps.setString(4, concept.getCode());
    }

    static void deleteValueSetMembers(DbConnection conn2, int valueSetId) {
        conn2.executeUpdate(DELETE_VALUE_SET_MEMBER_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, valueSetId);
        });
    }


    static void findExistingConcepts(DbConnection conn, int valueSetId, Consumer<UmlsConcept> resultConsumer) {
        conn.executePreparedStatement(VALUE_SET_MEMBER_SELECT_MEMBER_UMLS_SAB_MEMBER_CODE_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, valueSetId);
        }, rsIter -> {
            rsIter.forEach(rs -> {
                String sab = rs.getString(COL_MEMBER_UMLS_SAB);
                String code = rs.getString(COL_MEMBER_CODE);
                System.err.println("findExistingConcepts: " + sab + " (" + UmlsSource.byName(sab) + "), " + code);
                resultConsumer.accept(new UmlsConcept(UmlsSource.byName(sab), code));
            });
        });
    }
    
    static void forEachReferencedConcept(DbConnection conn3, int refVsValueSetId, Consumer<UmlsConcept> resultConsumer) {
        conn3.executePreparedStatement(VALUE_SET_MEMBER_SELECT_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, refVsValueSetId);
        }, rsIter3 -> {
            rsIter3.forEach(rs3 -> {
                String sab = rs3.getString(COL_MEMBER_UMLS_SAB);
                String code = rs3.getString(COL_MEMBER_CODE);
                String name = rs3.getString(COL_MEMBER_NAME);
                String loincName = rs3.getString(COL_MEMBER_LOINC_NAME);
                resultConsumer.accept(new UmlsConcept(UmlsSource.byName(sab), code, name, loincName));
            });
        });
    }
    
    static void forEachValueSetMemberByValueSetId(DbConnection conn2, int valueSetId, Consumer<UmlsConcept> resultConsumer) {
        conn2.executePreparedStatement(VALUE_SET_MEMBER_QUERY, (PreparedStatementFacade ps) -> {
            ps.setInt(1, valueSetId);
        }, rsIter -> {
            rsIter.forEach((Result r2) -> {
                String sab = r2.getString(COL_MEMBER_UMLS_SAB);
                String code = r2.getString(COL_MEMBER_CODE);
                String name = r2.getString(COL_MEMBER_NAME);
                String loincName = r2.getString(COL_MEMBER_LOINC_NAME);
                resultConsumer.accept(new UmlsConcept(UmlsSource.byName(sab), code, name, loincName));
            });
        });
    }

}
