/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.umls.tools.store;

import java.sql.Timestamp;
import java.util.Date;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;

class ValueSet {
    private static final Log log = LogFactory.getLog(ValueSet.class);
    
    private static final String VALUE_SET_SELECT_MAX_UPDATE_DTM = "SELECT MAX(UPDATE_DTM) FROM VALUE_SET";
    private static final String VALUE_SET_SELECT_EXECUTE_ORDER_VALUE_SET_ID_VALUE_SET_NAME = "SELECT EXECUTE_ORDER, VALUE_SET_ID, VALUE_SET_NAME FROM VALUE_SET WHERE SKIP != 1 ORDER BY EXECUTE_ORDER";
    private static final String UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_ID = "UPDATE VALUE_SET SET UPDATE_DTM = ? WHERE VALUE_SET_ID = ?";
    private static final String VALUE_SET_IJ_REFERENCED_VALUE_SET_IJ_VALUE_SET = "SELECT SRC_VS_VALUE_SET_ID, SRC_VS_EXECUTE_ORDER, SRC_VS_NAME, REF_VS_INCLUDE_OR_EXCLUDE, REF_VS_VALUE_SET_ID, REF_VS.EXECUTE_ORDER AS REF_VS_EXECUTE_ORDER, REF_VS.VALUE_SET_NAME AS REF_VS_NAME"
    + " FROM"
    + " (SELECT SRC_VS.VALUE_SET_ID AS SRC_VS_VALUE_SET_ID, SRC_VS.EXECUTE_ORDER AS SRC_VS_EXECUTE_ORDER, SRC_VS.VALUE_SET_NAME AS SRC_VS_NAME, REFERENCED_VALUE_SET.INCLUDE_OR_EXCLUDE AS REF_VS_INCLUDE_OR_EXCLUDE, REFERENCED_VALUE_SET.REFERENCED_VALUE_SET_ID AS REF_VS_VALUE_SET_ID "
    + "   FROM VALUE_SET AS SRC_VS " 
    + "   INNER JOIN REFERENCED_VALUE_SET ON SRC_VS.VALUE_SET_ID = REFERENCED_VALUE_SET.VALUE_SET_ID"
    + "   WHERE SRC_VS.VALUE_SET_ID = ?) AS A " 
    + " INNER JOIN VALUE_SET AS REF_VS ON REF_VS.VALUE_SET_ID = A.REF_VS_VALUE_SET_ID";
    private static final String VALUE_SET_QUERY = "SELECT VALUE_SET_ID FROM VALUE_SET ORDER BY VALUE_SET_ID ";

    private static final String COL_REF_VS_INCLUDE_OR_EXCLUDE = "REF_VS_INCLUDE_OR_EXCLUDE";
    private static final String COL_REF_VS_VALUE_SET_ID = "REF_VS_VALUE_SET_ID";
    private static final String COL_VALUE_SET_ID = "VALUE_SET_ID";
    private static final String COL_VALUE_SET_NAME = "VALUE_SET_NAME";


    static Timestamp getLastUpdate(DbConnection conn) {
        return conn.executeTimestampQuery(VALUE_SET_SELECT_MAX_UPDATE_DTM);
    }

    static void forEachValueSet(DbConnection conn1, BiConsumer<Integer, String> resultConsumer) {
        conn1.executeQuery(VALUE_SET_SELECT_EXECUTE_ORDER_VALUE_SET_ID_VALUE_SET_NAME, rsIter -> {
            rsIter.forEach(result -> {
                int valueSetId = result.getInt(COL_VALUE_SET_ID);
                String valueSetName = result.getString(COL_VALUE_SET_NAME);
                resultConsumer.accept(valueSetId, valueSetName);
            });
        });
    }
    
    static void forEachValueSet(DbConnection conn1, Consumer<Integer> resultConsumer) {
        conn1.executeQuery(VALUE_SET_QUERY, rsIterable -> {
            rsIterable.forEach(r -> {
                resultConsumer.accept(r.getInt("VALUE_SET_ID"));
            });
        });
    }


    static void forEachReferencedValueSet(DbConnection conn2, int valueSetId, BiConsumer<String, Integer> resultConsumer) {
        conn2.executePreparedStatement(VALUE_SET_IJ_REFERENCED_VALUE_SET_IJ_VALUE_SET, ps -> {
            ps.setInt(1, valueSetId);
        }, rsIter2 -> {
            rsIter2.forEach(rs2 -> {
                String refVsIncludeOrExclude = rs2.getString(COL_REF_VS_INCLUDE_OR_EXCLUDE);
                int refVsValueSetId = rs2.getInt(COL_REF_VS_VALUE_SET_ID);
                resultConsumer.accept(refVsIncludeOrExclude, refVsValueSetId);
            });
        });
    }

    static void updateValueSetUpdateDtm(DbConnection conn2, Date date, Integer valueSetId) {
        conn2.executeUpdateNoCommit(UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_ID, ps -> {
            ps.setTimestamp(1, new Timestamp(date.getTime()));
            ps.setInt(2, valueSetId);
        });

    }

}
