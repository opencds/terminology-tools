package org.opencds.tools.terminology.umls.util

import spock.lang.Specification

class UmlsUtilSpec extends Specification {

    def 'test createVersionQueue'() {
        given:
        int SPEC_NOW = 2021
        int EARLIEST_UMLS_VERSION_YEAR_TO_TRY = 2016

        when:
        Queue<String> versionQueue = UmlsUtil.createVersionQueue(SPEC_NOW, EARLIEST_UMLS_VERSION_YEAR_TO_TRY)

        then:
        notThrown(Exception)
        versionQueue == ['2021AB', '2021AA', '2020AB', '2020AA', '2019AB', '2019AA', '2018AB', '2018AA', '2017AB', '2017AA', '2016AB', '2016AA']
    }
}
