package org.opencds.tools.terminology.umls.client

import org.opencds.tools.terminology.api.rest.RestClient
import org.opencds.tools.terminology.umls.api.model.UmlsVocabulary
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAttributes
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptDefinitions
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelations
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsInputType
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsSearchType
import spock.lang.Specification

class UmlsApiSpec extends Specification {
    private static UmlsApi api

    def setupSpec() {
        RestClient restUtil = Mock()
        api = new UmlsApi(restUtil)
    }

    def 'test version'() {
        expect:
        version == api.version(version1, version2)

        where:
        version   | version1 | version2
        'abc'     | 'abc'    | '123'
        '123'     | null     | '123'
        'abc'     | 'abc'    | null
        'current' | null     | null
    }

}
