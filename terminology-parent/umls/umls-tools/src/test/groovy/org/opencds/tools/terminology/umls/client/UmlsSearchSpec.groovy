package org.opencds.tools.terminology.umls.client

import org.opencds.tools.terminology.umls.api.model.UmlsQueryParam
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch
import spock.lang.Specification

class UmlsSearchSpec extends Specification {
    def 'test null'() {
        when:
        var us = new UmlsSearch(null, null)

        then:
        noExceptionThrown()
    }

    def 'test empty map'() {
        when:
        var us = new UmlsSearch(Map.of(), null)

        then:
        noExceptionThrown()
    }

    def 'test disallowed parameter'() {
        when:
        var us = new UmlsSearch(Map.of(UmlsQueryParam.ttys, ''), null)

        then:
        noExceptionThrown()
        !us.queryParams().containsKey(UmlsQueryParam.ttys)
    }

    def 'test allowed parameter'() {
        when:
        var us = new UmlsSearch(Map.of(UmlsQueryParam.partialSearch, ''), null)

        then:
        noExceptionThrown()
    }
}
