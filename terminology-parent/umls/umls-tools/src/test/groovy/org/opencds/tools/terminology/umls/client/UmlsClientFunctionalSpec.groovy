/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.umls.client

import org.opencds.tools.terminology.api.config.Config
import org.opencds.tools.terminology.umls.api.model.UmlsConcept
import org.opencds.tools.terminology.umls.api.model.UmlsSource
import spock.lang.Ignore
import spock.lang.Specification

@Ignore // broken
class UmlsClientFunctionalSpec extends Specification {

    static UmlsClient client

    def setupSpec() {
        Config config = new Config()
        client = new UmlsClient(config)
    }

    def 'test regular names'() {
        expect:
        client.getName("LNC", "1975-2") == 'Bilirubin:MCnc:Pt:Ser/Plas:Qn'
    }

    def 'test LOINC long names'() {
        expect:
        client.getLoincLongName("1975-2") == 'Bilirubin.total [Mass/volume] in Serum or Plasma'
    }

    def 'test bad input'() {
        expect:
        client.getName("ICD9CM", "999999") == null
    }

    def 'test bad input (2)'() {
        client.getLoincLongName("999999") == null
    }

    def 'test LOINC long name'() {
        expect:
        client.getLoincLongName("1975-2") == 'Bilirubin.total [Mass/volume] in Serum or Plasma'
    }

    def 'test get descendants'() {
        given:
        Set<UmlsConcept> diabetesAndAsthma = new HashSet<UmlsConcept>()
        UmlsConcept diabetes = new UmlsConcept(UmlsSource.ICD9CM, "250", "Diabetes mellitus (manually entered)")
        UmlsConcept asthma = new UmlsConcept(UmlsSource.ICD9CM, "493", "Asthma (manually entered)")
        diabetesAndAsthma.add(diabetes)
        diabetesAndAsthma.add(asthma)

        and:
        boolean includeAncestorConcept = false
        boolean refreshNameForAncestorConcept = true

        when:
        Set<UmlsConcept> desc = client.getDescendantConcepts(diabetesAndAsthma, includeAncestorConcept, refreshNameForAncestorConcept, UmlsConceptSortType.SORT_BY_SAB_CODE_NAME)

        then:
        desc
        desc.size() == 69
        desc*.mySab == ['ICD9CM']*69
        desc*.myCode == [
            '250.0',
            '250.00',
            '250.01',
            '250.02',
            '250.03',
            '250.1',
            '250.10',
            '250.11',
            '250.12',
            '250.13',
            '250.2',
            '250.20',
            '250.21',
            '250.22',
            '250.23',
            '250.3',
            '250.30',
            '250.31',
            '250.32',
            '250.33',
            '250.4',
            '250.40',
            '250.41',
            '250.42',
            '250.43',
            '250.5',
            '250.50',
            '250.51',
            '250.52',
            '250.53',
            '250.6',
            '250.60',
            '250.61',
            '250.62',
            '250.63',
            '250.7',
            '250.70',
            '250.71',
            '250.72',
            '250.73',
            '250.8',
            '250.80',
            '250.81',
            '250.82',
            '250.83',
            '250.9',
            '250.90',
            '250.91',
            '250.92',
            '250.93',
            '493.0',
            '493.00',
            '493.01',
            '493.02',
            '493.1',
            '493.10',
            '493.11',
            '493.12',
            '493.2',
            '493.20',
            '493.21',
            '493.22',
            '493.8',
            '493.81',
            '493.82',
            '493.9',
            '493.90',
            '493.91',
            '493.92'
        ]
    }

    def 'test get descendants with LOINC'() {
        given:
        UmlsConcept a1cPanel = new UmlsConcept(UmlsSource.LNC, "LP100945-7", "Hemoglobin A1c | Bld-Ser-Plas")
        Set<UmlsConcept> a1cs = new HashSet<UmlsConcept>()
        a1cs.add(a1cPanel)

        and:
        boolean includeAncestorConcept = false
        boolean refreshNameForAncestorConcept = true

        when:
        Set<UmlsConcept> desc = client.getDescendantConcepts(a1cs, includeAncestorConcept, refreshNameForAncestorConcept, UmlsConceptSortType.SORT_BY_SAB_CODE_NAME)

        then:
        desc
        desc.size() == 8
        desc*.mySab == ['LNC']*8
        desc*.myCode == [
            '17855-8',
            '17856-6',
            '41995-2',
            '4548-4',
            '4549-2',
            '59261-8',
            '62388-4',
            '71875-9'
        ]
    }

}
