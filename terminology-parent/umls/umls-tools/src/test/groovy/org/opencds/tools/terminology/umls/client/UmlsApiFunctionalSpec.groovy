/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.umls.client

import org.opencds.tools.terminology.api.config.Config
import org.opencds.tools.terminology.api.rest.RestClient
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives
import org.opencds.tools.terminology.umls.api.model.request.UmlsSearch
import org.opencds.tools.terminology.umls.api.model.request.types.UmlsSearchType
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult
import org.opencds.tools.terminology.umls.api.model.response.UmlsResults
import org.opencds.tools.terminology.umls.api.model.response.UmlsSearchResultComponent
import spock.lang.Specification

class UmlsApiFunctionalSpec extends Specification {
    private static Config config
    private static UmlsApi api

    def setupSpec() {
        config = new Config()
        api = new UmlsApi(new RestClient(config.getUmlsApiKey()))
    }

    def 'test get descendants with SNOMEDCT_US 9468002'() {
        given:
        String sab = 'SNOMEDCT_US'
        String code = '9468002'
        int pageNumber = 1
        int pageSize = 1000

        when:
        var descendants = api.query(UmlsConceptRelatives.descendants()
                .source(sab, code)
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        descendants
        descendants.pageSize() == pageSize
        descendants.pageNumber() == pageNumber
        descendants.pageCount() == pageNumber
        descendants.result()
        descendants.result().size() == 44

        when:
        var children = api.query(UmlsConceptRelatives.children()
                .source(sab, code)
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        children
        children.pageSize() == pageSize
        children.pageNumber() == pageNumber
        children.pageCount() == pageNumber
        children.result()
        children.result().size() == 14

        when:
        var containsAll = descendants.result().containsAll(children.result())

        then:
        noExceptionThrown()
        containsAll
    }

    def 'test get descendants with LOINC LP189873-5'() {
        given:
        String ancestorSab = 'LNC'
        String ancestorCode = 'LP189873-5'
        int pageNumber = 1
        int pageSize = 1000

        when:
        var desc = api.query(UmlsConceptRelatives.descendants()
                .source(ancestorSab, ancestorCode)
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        desc
        desc.pageSize() == pageSize
        desc.pageNumber() == pageNumber
        desc.pageCount() == pageNumber
        desc.result()
        desc.result().size() == 7
    }

    def 'test get descendants with LOINC 52095-7'() {
        given:
        String ancestorSab = 'LNC'
        String ancestorCode = '52095-7'
        int pageNumber = 1
        int pageSize = 1000

        when:
        var desc = api.query(UmlsConceptRelatives.descendants()
                .source(ancestorSab, ancestorCode)
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        desc
        desc.pageSize() == null
        desc.pageNumber() == null
        desc.pageCount() == null
        desc.result() == []
        desc.name() == 'NotFoundError'
        desc.status() == 404
        desc.message() == '404 Resource Not Found'
    }

    def 'test get identifier information with LOINC 52095-7'() {
        given:
        String ancestorSab = 'LNC'
        String ancestorCode = '52095-7'
        int pageNumber = 1
        int pageSize = 25

        when:
        var desc = api.query(UmlsConceptInformation.builder()
                .source(ancestorSab, ancestorCode)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        desc
        desc.pageSize() == pageSize
        desc.pageNumber() == pageNumber
        desc.pageCount() == pageNumber
        desc.result()
        desc.result().size() == 1
    }

    def 'test get I20 with right truncation'() {
        given:
        String sab = 'ICD10CM'
        String code = 'I20'

        when:
        var umlsResult = api.query(UmlsSearch.builder()
                .searchType(UmlsSearchType.rightTruncation)
                .sabs([sab])
                .string(code)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        umlsResult
        umlsResult instanceof UmlsResults
        umlsResult.result()
        umlsResult.result() instanceof List
        umlsResult.result().size() == 18 // it's a searchresult object
        umlsResult.result()

        when:
        var compResults = umlsResult.result().stream()
                .map(UmlsSearchResultComponent.class::cast)
                .map(comp -> api.query(comp.uri()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(UmlsResult.class::cast)
                .toList()

        then:
        noExceptionThrown()
        compResults
        compResults.size() == 18
    }

    def 'test get "fracture of carpal bone" with right truncation'() {
        given:
        String sab = 'ICD10CM'
        String code = 'fracture of carpal bone'

        when:
        var umlsResult = api.query(UmlsSearch.builder()
                .searchType(UmlsSearchType.words)
                .sabs([sab])
                .string(code)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        umlsResult
        umlsResult instanceof UmlsResults
        umlsResult.recCount() == 26 // it's a searchresult object
        umlsResult.result()
        umlsResult.result() instanceof List
        umlsResult.result().size() == 25 // it's a searchresult object
        umlsResult.result()
    }

    def 'concept definition C0155502'() {
        given:
        String code = 'C0155502'

        when:
        var umlsResult = api.query(UmlsConceptInformation.builder()
                .cui(code)
                .build())
                .orElse(null)

        then:
        noExceptionThrown()
        umlsResult
    }
}
