package org.opencds.tools.terminology.umls.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.config.Config;
import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.umls.api.model.UmlsConcept;
import org.opencds.tools.terminology.umls.api.model.UmlsSource;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptAtoms;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptInformation;
import org.opencds.tools.terminology.umls.api.model.request.UmlsConceptRelatives;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAtom;
import org.opencds.tools.terminology.umls.api.model.response.UmlsAttribute;
import org.opencds.tools.terminology.umls.api.model.response.UmlsConceptInfo;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResultComponent;
import org.opencds.tools.terminology.umls.api.model.response.UmlsSourceAtomCluster;
import org.opencds.tools.terminology.umls.api.util.UmlsConceptSabCodeNameComparator;
import org.opencds.tools.terminology.umls.api.util.UmlsConceptSabNameCodeComparator;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class UmlsClient {
    private static final Log log = LogFactory.getLog(UmlsClient.class);
    private static final String TERMTYPE_LC = "LC";

    public enum UmlsConceptSortType {
        SORT_BY_NOTHING {
            @Override
            public Set<UmlsConcept> newSet() {
                return new TreeSet<>();
            }
        },
        SORT_BY_SAB_CODE_NAME {
            @Override
            public Set<UmlsConcept> newSet() {
                return new TreeSet<>(new UmlsConceptSabCodeNameComparator());
            }
        },
        SORT_BY_SAB_NAME_CODE {
            @Override
            public Set<UmlsConcept> newSet() {
                return new TreeSet<>(new UmlsConceptSabNameCodeComparator());
            }
        };

        public abstract Set<UmlsConcept> newSet();
    }


    private final UmlsApi api;

    public UmlsClient(Config config) {
        this.api = new UmlsApi(new RestClient(config.getUmlsApiKey()));
    }

    /**
     * Returns default name given code.  If no match found or UMLS ticket invalid, returns null.
     *
     * @param sab  SAB is the source vocabulary abbreviation in UMLS. See
     *             <a href="https://www.nlm.nih.gov/research/umls/sourcereleasedocs/index.html">Source Release Docs</a>.
     * @param code identifier
     * @return name of the identifier
     */
    public String getName(String sab, String code) {
        return api.query(
                        UmlsConceptInformation.builder().source(sab, code).build())
                .filter(UmlsResult::noError)
                .map(UmlsResult::result)
                .stream()
                .flatMap(Collection::stream)
                .findFirst()
                .flatMap(this::findName)
                .orElse(null);
    }

    private Optional<String> findName(UmlsResultComponent umlsResultComponent) {
        if (umlsResultComponent instanceof UmlsAtom a) {
            return Optional.ofNullable(a.name());
        } else if (umlsResultComponent instanceof UmlsAttribute a) {
            return Optional.ofNullable(a.name());
        } else if (umlsResultComponent instanceof UmlsConceptInfo c) {
            return Optional.ofNullable(c.name());
        } else if (umlsResultComponent instanceof UmlsSourceAtomCluster c) {
            return Optional.ofNullable(c.name());
        }
        return Optional.empty();
    }

    /**
     * Returns LOINC long name given code.  If no match found or UMLS ticket invalid, returns null.
     *
     * @param loincCode the LOINC code
     * @return the LOING long name
     */
    public String getLoincLongName(String loincCode) {
        return api.query(UmlsConceptAtoms.builder()
                        .source(UmlsSource.LNC.getName(), loincCode)
                        .build())
                .filter(UmlsResult::noError)
                .map(UmlsResult::result)
                .stream()
                .flatMap(Collection::stream)
                .filter(UmlsAtom.class::isInstance)
                .map(UmlsAtom.class::cast)
                .filter(result ->
                        UmlsSource.LNC.getName().equalsIgnoreCase(result.rootSource()) &&
                                TERMTYPE_LC.equalsIgnoreCase(result.termType()))
                .map(UmlsAtom::name)
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns descendant cocnepts.
     *
     * @param ancestorConcepts                the ancestor {@link UmlsConcept}
     * @param includeAncestorConcept          Whether to include the ancestor concept.  May want to se to false if the ancestor concept is a grouper concept that's not a valid concept on its own (e.g., ICD grouper concept, LOINC grouper concept).
     * @param refreshNamesForAncestorConcepts Whether names should be udpated
     * @param sortType                        Use sort types
     * @return the descendant {@link UmlsConcept}s of <tt>ancestorConcept</tt>
     */
    public Set<UmlsConcept> getDescendantConcepts(Set<UmlsConcept> ancestorConcepts, boolean includeAncestorConcept, boolean refreshNamesForAncestorConcepts, UmlsConceptSortType sortType) {
        Set<UmlsConcept> descendantConcepts = sortType.newSet();
        descendantConcepts.addAll(getDescendantConcepts(ancestorConcepts, includeAncestorConcept, refreshNamesForAncestorConcepts));
        return descendantConcepts;
    }

    /**
     * Returns a set with descendants.
     *
     * @param ancestorConcepts                the ancestor {@link UmlsConcept}
     * @param includeAncestorConcept          Whether to include the ancestor concept.  May want to se to false if the ancestor concept is a grouper concept that's not a valid concept on its own (e.g., ICD grouper concept, LOINC grouper concept).
     * @param refreshNamesForAncestorConcepts whether to refresh the ancestor concept name
     * @return the descendant {@link UmlsConcept}s of <tt>ancestorConcept</tt>
     */
    public Set<UmlsConcept> getDescendantConcepts(Set<UmlsConcept> ancestorConcepts, boolean includeAncestorConcept, boolean refreshNamesForAncestorConcepts) {
        return ancestorConcepts.stream()
                .flatMap(anc -> getDescendantConcepts(anc, includeAncestorConcept, refreshNamesForAncestorConcepts).stream())
                .collect(Collectors.toSet());
    }

    /**
     * Returns a set with descendants.
     *
     * @param ancestorConcept               the ancestor {@link UmlsConcept}
     * @param includeAncestorConcept        Whether to include the ancestor concept.  May want to se to false if the ancestor concept is a grouper concept that's not a valid concept on its own (e.g., ICD grouper concept, LOINC grouper concept).
     * @param refreshNameForAncestorConcept whether to refresh the ancestor concept name
     * @return the descendant {@link UmlsConcept}s of <tt>ancestorConcept</tt>
     */
    public Set<UmlsConcept> getDescendantConcepts(UmlsConcept ancestorConcept,
                                                  boolean includeAncestorConcept,
                                                  boolean refreshNameForAncestorConcept) {

        var ancestorSab = ancestorConcept.getSab().getName();
        var ancestorCode = ancestorConcept.getCode();
        var ancestorName = ancestorConcept.getPreferredName();
        var ancestorLoincName = ancestorConcept.getLoincName();

        Set<UmlsConcept> descendantConcepts = ConcurrentHashMap.newKeySet();
        if (includeAncestorConcept) {
            if (refreshNameForAncestorConcept) {
                ancestorName = getName(ancestorSab, ancestorCode);
                if ((ancestorSab != null) && (ancestorSab.equalsIgnoreCase("LNC"))) {
                    ancestorLoincName = getLoincLongName(ancestorCode);
                }
                descendantConcepts.add(new UmlsConcept(UmlsSource.byName(ancestorSab), ancestorCode, ancestorName, ancestorLoincName));
            } else {
                descendantConcepts.add(ancestorConcept);
            }
        }

        var pageSize = 1000;
        var pageNumber = 1;
        var results = descendants(ancestorSab, ancestorCode, pageNumber, pageSize)
                .orElse(null);
        if (results == null || results.hasError()) {
            return Set.of();
        }
        pageNumber = results.pageNumber();
        var pageCount = results.pageCount();

        consumeInto(descendantConcepts, results);
        while (pageNumber < pageCount) {
            pageNumber++;
            log.info("-   Processing page " + pageNumber + "...");
            results = descendants(ancestorSab, ancestorCode, pageNumber, pageSize)
                    .orElse(null);
            if (results != null && results.noError()) {
                consumeInto(descendantConcepts, results);
            }
        }
        return descendantConcepts;
    }

    private void consumeInto(Set<UmlsConcept> descendantConcepts, UmlsResult result) {
        result.result().stream()
                .filter(UmlsSourceAtomCluster.class::isInstance)
                .map(UmlsSourceAtomCluster.class::cast)
                .forEach(sourceAtomCluster -> consumeInto(descendantConcepts, sourceAtomCluster));
    }

    private void consumeInto(Set<UmlsConcept> descendantConcepts, UmlsSourceAtomCluster sourceAtomCluster) {
        var sab = UmlsSource.byName(sourceAtomCluster.rootSource());
        descendantConcepts.add(new UmlsConcept(
                sab,
                sourceAtomCluster.ui(),
                sourceAtomCluster.name(),
                sab == UmlsSource.LNC ? getLoincLongName(sourceAtomCluster.ui()) : null));
    }

    private Optional<UmlsResult> descendants(String ancestorSab, String ancestorCode, int pageNumber, int pageSize) {
        return api.query(UmlsConceptRelatives.descendants()
                .source(ancestorSab, ancestorCode)
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .build());
    }

}
