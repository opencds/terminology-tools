package org.opencds.tools.terminology.umls.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class UmlsUtil {
    private static final List<String> AB_LIST = Arrays.asList("AB", "AA");

    public static Queue<String> createVersionQueue(int start, int end) {
        Queue<String> queue = new LinkedList<>();
        int test = start;
        LinkedList<String> abq = new LinkedList<>(AB_LIST);
        while (test >= end) {
            String aaab = abq.poll();
            queue.offer(String.join(StringUtils.EMPTY, Integer.toString(test), aaab));
            abq.offer(aaab);
            aaab = abq.poll();
            queue.offer(String.join(StringUtils.EMPTY, Integer.toString(test), aaab));
            abq.offer(aaab);
            test--;
        }
        return queue;
    }


}
