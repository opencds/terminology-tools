package org.opencds.tools.terminology.umls.client;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.cache.ApiCache;
import org.opencds.tools.terminology.api.cache.TerminologyCache;
import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.api.rest.RestRequest;
import org.opencds.tools.terminology.umls.api.json.JsonSupport;
import org.opencds.tools.terminology.umls.api.model.request.UmlsQuery;
import org.opencds.tools.terminology.umls.api.model.response.UmlsResult;
import org.opencds.tools.terminology.umls.api.util.UmlsQueryUtil;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static org.opencds.tools.terminology.api.rest.util.Const.IMMUTABLE_EMPTY_HEADERS;
import static org.opencds.tools.terminology.umls.util.UmlsUtil.createVersionQueue;

public class UmlsApi {
    private static final URI URL_UMLS_BASE_API = URI.create("https://uts-ws.nlm.nih.gov/rest");

    private static final String API_DEFAULT_VERSION = "current";

    private static final JsonSupport jsonSupport = new JsonSupport();

    // the earliest year to use if latest version does not return any results.
    // Implemented due to LOINC panel code present in 2017AA disappearing in
    // 2017AB (LP41830-8, Drugs Identified | Urine).
    private static final int EARLIEST_UMLS_VERSION_YEAR_TO_TRY = 2016;

    private final URI baseUri;
    private final TerminologyCache<RestRequest, UmlsResult> cache;

    private static final Function<String, UmlsResult> handler = content ->
            jsonSupport.fromJson(content, UmlsResult.class);

    public static Function<RestRequest, UmlsResult> loader(RestClient restClient) {
        return request -> {
            var response = new AtomicReference<UmlsResult>();
            restClient.get(
                    request,
                    (status, message) -> response.set(handler.apply(message)),
                    message -> response.set(handler.apply(message)));
            return response.get();
        };
    }

    public UmlsApi(RestClient restClient) {
        this(restClient, null);
    }

    public UmlsApi(URI baseUri,
                   TerminologyCache<RestRequest, UmlsResult> cache) {
        this.baseUri = baseUri;
        this.cache = cache;
    }

    public UmlsApi(RestClient restClient, URI baseUri) {
        this.baseUri = baseUri;
        cache = new ApiCache<>(loader(restClient));
    }

    public Optional<UmlsResult> query(UmlsQuery query) {
        return getResult(query, null);
    }

    public Optional<UmlsResult> query(UmlsQuery query, String version) {
        return getResult(query, version);
    }

    public URI baseUri() {
        return Optional.ofNullable(baseUri)
                .orElse(URL_UMLS_BASE_API);
    }

    private UmlsResult getResult(URI uri) {
        return cache.get(
                RestRequest.create(uri, IMMUTABLE_EMPTY_HEADERS));
    }

    private Optional<UmlsResult> getResult(UmlsQuery umlsQuery, String version) {
        UmlsResult response = getResult(UmlsQueryUtil.toUri(
                baseUri(),
                umlsQuery,
                version(version, umlsQuery.version())));
        if (response.status() == null || response.status() >= 400) {
            return Optional.of(response);
        }
        return createVersionQueue(LocalDateTime.now().getYear(), EARLIEST_UMLS_VERSION_YEAR_TO_TRY).stream()
                .map(v -> UmlsQueryUtil.toUri(umlsQuery, v))
                .map(this::getResult)
                .filter(result -> result.status() == null)
                .findFirst();
    }

    /**
     * Returns the first non-null, non-empty value
     * @param version the version(s)
     * @return the first non-null, non-empty value from the list of version(s), or {@link UmlsApi#API_DEFAULT_VERSION}
     */
    private String version(String... version) {
        return Arrays.stream(version)
                .filter(StringUtils::isNotBlank)
                .findFirst()
                .orElse(API_DEFAULT_VERSION);
    }
}
