package org.opencds.plugin.terminology.valueset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.common.exceptions.OpenCDSRuntimeException;
import org.opencds.plugin.api.PreProcessPlugin;
import org.opencds.plugin.api.PreProcessPluginContext;
import org.opencds.plugin.api.SupportingData;
import org.opencds.plugin.terminology.valueset.config.Config;
import org.opencds.plugin.terminology.valueset.config.ConfigReader;
import org.opencds.plugin.terminology.valueset.config.ValueSetConfig;
import org.opencds.tools.terminology.api.client.TerminologyClient;
import org.opencds.tools.terminology.api.client.TerminologyClientService;
import org.opencds.tools.terminology.api.store.AccessConnection;
import org.opencds.tools.terminology.api.store.DataStoreReader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ValueSetTerminologyPreProcessPlugin extends DataStoreReader implements PreProcessPlugin {
    private static final Log log = LogFactory.getLog(ValueSetTerminologyPreProcessPlugin.class);
    private static final String SUPPORTING_DATA_ID = "ValueSetArchive";

    private static final String TERMINOLOGY_CLIENT = "TerminologyClient";
    private static final String ZIP = "ZIP";
    private static final String VALUE_SET_CONFIG = "value-set-config.json";

    @Override
    public void execute(PreProcessPluginContext context) {
        log.debug(getClass().getSimpleName() + ": Processing input data.");
        Map<String, SupportingData> sdMap = context.getSupportingData();
        SupportingData sd = sdMap.get(SUPPORTING_DATA_ID);
        log.debug("SD: " + sd);
        if (sd != null && ZIP.equalsIgnoreCase(sd.getPackageType())) {
            Map<String, Object> cachedSds = context.getCache().get(sd);
            if (cachedSds == null || !cachedSds.containsKey(TERMINOLOGY_CLIENT) || cachedSds.get(TERMINOLOGY_CLIENT) == null) {
                TerminologyClientService terminologyClient = new TerminologyClientService();
                try (ZipFile zip = new ZipFile(sd.getSupportingDataPackage().getFile())) {
                    ZipEntry configEntry = zip.getEntry(VALUE_SET_CONFIG);
                    Config config = ConfigReader.getConfig(zip.getInputStream(configEntry));
                    config.getValueSetConfigs().forEach(vsc ->
                            terminologyClient.registerDelegate(
                                    vsc.getValueSetSource(),
                                    loadClient(vsc, inputStreamFunction(zip))));
                    if (cachedSds == null) {
                        cachedSds = new HashMap<>();
                    }
                    cachedSds.put(TERMINOLOGY_CLIENT, terminologyClient);
                    context.getCache().put(sd, cachedSds);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                    throw new OpenCDSRuntimeException(e.getMessage(), e);
                }
            }
            context.getGlobals().putAll(cachedSds);
        }
    }

    private static Function<ValueSetConfig, InputStream> inputStreamFunction(ZipFile zip) {
        return (ValueSetConfig valueSetConfig) -> {
            try {
                return zip.getInputStream(zip.getEntry(valueSetConfig.getDbFile()));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                throw new OpenCDSRuntimeException(e.getMessage(), e);
            }
        };
    }

    private TerminologyClient loadClient(ValueSetConfig vsc, Function<ValueSetConfig, InputStream> inputStreamSupplier) {
        try {
            TerminologyClient client = (TerminologyClient)
                    Class.forName(vsc.getTerminologyClientClass()).getDeclaredConstructor().newInstance();
            if (client.supports(vsc.getValueSetSource())) {
                client.setConnectionSupplier(getConnectionSupplier(vsc.getDbFile(), inputStreamSupplier.apply(vsc)));
                return client;
            } else {
                log.error("Misconfiguration: TerminologyClient instance (" + client.getClass()
                        + ") does not support ValueSet source: " + vsc.getValueSetSource() + ". Returning null.");
                return null;
            }
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            log.error(e.getMessage(), e);
            throw new OpenCDSRuntimeException(e.getMessage(), e);
        } catch (InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private Supplier<Connection> getConnectionSupplier(String dbName, InputStream inputStream) {
        // copy input to a temp file
        Path folder;
        try {
            folder = Files.createTempDirectory("opencdsdb");
            Path dbPath = Paths.get(folder.toString(), dbName);
            Files.copy(inputStream, dbPath);
            dbPath.toFile().deleteOnExit();
            folder.toFile().deleteOnExit();
            log.debug(dbPath);
            return AccessConnection.supplier(dbPath.toFile());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new OpenCDSRuntimeException(e.getMessage(), e);
        }
    }
}
