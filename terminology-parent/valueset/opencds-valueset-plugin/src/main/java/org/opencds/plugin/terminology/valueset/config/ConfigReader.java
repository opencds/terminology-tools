package org.opencds.plugin.terminology.valueset.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.common.exceptions.OpenCDSRuntimeException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ConfigReader {
    private static final Log log = LogFactory.getLog(ConfigReader.class);
    private static final GsonBuilder gsonBuilder = new GsonBuilder();

    public static Config getConfig(String content) {
        Gson gson = gsonBuilder.create();
        return gson.fromJson(content, Config.class);
    }

    public static Config getConfig(InputStream inputStream) {
        StringWriter data = new StringWriter();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){
            String line;
            while((line = reader.readLine()) != null) {
                data.write(line);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new OpenCDSRuntimeException(e.getMessage(), e);
        }
        return getConfig(data.toString());
    }
    
}
