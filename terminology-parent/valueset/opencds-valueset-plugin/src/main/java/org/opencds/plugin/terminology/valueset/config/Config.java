package org.opencds.plugin.terminology.valueset.config;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Config {
    private final Set<ValueSetConfig> valueSets;

    public Config() {
        valueSets = new HashSet<>();
    }

    public Set<ValueSetConfig> getValueSetConfigs() {
        return valueSets;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append(valueSets).build();
    }

}
