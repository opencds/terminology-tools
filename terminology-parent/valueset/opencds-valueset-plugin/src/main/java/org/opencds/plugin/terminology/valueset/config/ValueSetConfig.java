package org.opencds.plugin.terminology.valueset.config;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.opencds.tools.terminology.api.constants.ValueSetSource;

public class ValueSetConfig {
    private ValueSetSource valueSetSource;
    private String dbFile;
    private String clientClass;

    public ValueSetSource getValueSetSource() {
        return valueSetSource;
    }

    public String getDbFile() {
        return dbFile;
    }

    public String getTerminologyClientClass() {
        return clientClass;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append(valueSetSource).append(dbFile)
                .append(clientClass).build();
    }

}
