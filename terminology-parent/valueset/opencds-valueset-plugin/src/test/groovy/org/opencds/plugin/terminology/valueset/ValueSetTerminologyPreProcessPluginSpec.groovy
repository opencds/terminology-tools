package org.opencds.plugin.terminology.valueset

import org.opencds.plugin.api.PluginDataCache
import org.opencds.plugin.api.SupportingData
import org.opencds.plugin.api.SupportingDataPackage
import org.opencds.plugin.support.PreProcessPluginContextImpl
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.terminology.api.client.TerminologyClient
import org.opencds.tools.terminology.api.constants.CodeSystemEnum
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.Supplier

class ValueSetTerminologyPreProcessPluginSpec extends Specification {
    static final String filename = 'src/test/resources/valueset-sd-archive.zip'

    static PreProcessPluginContextImpl context
    static PluginDataCache cache

    static TerminologyClient client

    def setupSpec() {
        ValueSetTerminologyPreProcessPlugin plugin = new ValueSetTerminologyPreProcessPlugin()
        Map allFactLists = [:]
        Map namedObjects = [:]
        Map globals = [:]
        SupportingDataPackage sdp = SupportingDataPackage.create(
                () -> new File(filename),
                () -> new File(filename).text.bytes)
        Supplier sdps = Mock()
        _ * sdps.get() >> sdp

        Map supportingData = [
            ValueSetArchive : SupportingData.create(
                'TerminologyClient',
                'kmid',
                null,
                filename,
                'ZIP',
                new Date(),
                sdps)]
        cache = new PluginDataCacheImpl();
        context = PreProcessPluginContextImpl.createPreProcessPluginContext(
                supportingData,
                cache,
                allFactLists as Map<Class<?>, List<?>>,
                namedObjects as Map<String, Object>,
                globals as Map<String, Object>)
        plugin.execute(context)
        client = context.getGlobals().get('TerminologyClient') as TerminologyClient
    }

    @Unroll
    def "test loading a supporting data package"() {
        expect:
        client.isConceptInValueSet(codeSystem, code, valueSetId, '') == result

        where:
        result | codeSystem | code | valueSetId
        true  | CodeSystemEnum.ICD9CM   |'196.5'     |'2.16.840.1.113762.1.4.1116.351'
        false | CodeSystemEnum.SNOMEDCT |'313296004' |'2.16.840.1.113762.1.4.1116.351'
        true  | CodeSystemEnum.SNOMEDCT |'313296004' |'2.16.840.1.113883.3.666.5.776'
        false | CodeSystemEnum.RXNORM   |'198058'    |'4'
        true  | CodeSystemEnum.RXNORM   |'198058'    |'5'
        true  | CodeSystemEnum.RXNORM   |'1042693'   |'2'
        false | CodeSystemEnum.SNOMEDCT |'231470001' |'3'
        true  | CodeSystemEnum.SNOMEDCT |'231470001' |'4'
    }

}
