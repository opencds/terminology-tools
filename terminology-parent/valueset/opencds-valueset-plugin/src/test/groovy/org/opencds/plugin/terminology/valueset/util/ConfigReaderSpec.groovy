package org.opencds.plugin.terminology.valueset.util

import org.opencds.plugin.terminology.valueset.config.ConfigReader

import spock.lang.Specification

class ConfigReaderSpec extends Specification {
    
    def 'test gson parsing'() {
        given:
        String content = new File('src/test/resources/value-set-config.json').text
        
        when:
        def result = ConfigReader.getConfig(content)
        
        then:
        notThrown(Exception)
        
        println result
        result
    }
    
}
