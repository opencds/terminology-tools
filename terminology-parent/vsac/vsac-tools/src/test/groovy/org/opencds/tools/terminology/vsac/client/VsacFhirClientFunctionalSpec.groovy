package org.opencds.tools.terminology.vsac.client

import ca.uhn.fhir.context.FhirVersionEnum
import org.hl7.fhir.r4.model.ValueSet
import org.opencds.hooks.lib.fhir.FhirContextFactory
import org.opencds.tools.terminology.api.config.Config
import spock.lang.Specification

class VsacFhirClientFunctionalSpec extends Specification {
    VsacFhirClient vsacFhirClient

    def setup() {
        vsacFhirClient = new VsacFhirClient(new Config())
    }

    def "readValueSetAndExpansion"() {
        given:
        String oid = '2.16.840.1.113883.4.642.3.155'
        when:
        ValueSet valueSet = vsacFhirClient.readValueSetAndExpansion(oid)

        println FhirContextFactory.get(FhirVersionEnum.R4).newJsonParser().encodeResourceToString(valueSet)

        then:
        notThrown(Exception)
        valueSet

    }

    def "readValueSetAndExpansion - Pulmonary Hypertension"() {
        given:
        String oid = '2.16.840.1.113762.1.4.1029.281'
        when:
        ValueSet valueSet = vsacFhirClient.readValueSetAndExpansion(oid)

        println FhirContextFactory.get(FhirVersionEnum.R4).newJsonParser().encodeResourceToString(valueSet)

        then:
        notThrown(Exception)
        valueSet

    }

    def "readValueSetAndExpansion unknown vs"() {
        given:
        String oid = '2.16.840.1.113883.4.642.3.1555555'
        when:
        ValueSet valueSet = vsacFhirClient.readValueSetAndExpansion(oid)

        then:
        notThrown(Exception)
        valueSet == null

    }
}
