package org.opencds.tools.terminology.vsac.client

import ca.uhn.fhir.context.FhirContext
import org.hl7.fhir.r4.model.CodeSystem
import org.hl7.fhir.r4.model.ValueSet
import org.opencds.tools.terminology.api.config.Config
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.Operation
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirResourceOperation
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirParametersOperation
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirRead
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacQueryParam
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacQueryParams
import spock.lang.Specification

class VsacFhirApiFunctionalSpec extends Specification {
    private static VsacFhirApi api
    private static FhirContext fhirContext

    def setupSpec() {
        var config = new Config()
        api = new VsacFhirApi(config)
        fhirContext = FhirContext.forR4()
    }

    def 'read intensional valueset'() {
        given:
        String id = '2.16.840.1.113883.4.642.3.155'

        when:
        var valueSet = api.read(VsacFhirRead.<ValueSet> builder()
                .id(id)
                .resourceClass(ValueSet.class)
                .build()).result()
        println fhirContext.newJsonParser().encodeResourceToString(valueSet)

        then:
        noExceptionThrown()
    }

    def 'read extensional valueset'() {
        given:
        String id = '2.16.840.1.113883.3.464.1003.113.11.1090'

        when:
        var valueSet = api.read(VsacFhirRead.<ValueSet> builder()
                .id(id)
                .resourceClass(ValueSet.class)
                .build()).result()
        println fhirContext.newJsonParser().encodeResourceToString(valueSet)

        then:
        noExceptionThrown()
    }

    def 'expand extensional valueset'() {
        given:
        String id = '2.16.840.1.113883.3.464.1003.113.11.1090'

        when:
        var valueSet = api.operation(VsacFhirResourceOperation.<ValueSet> builder()
                .id(id)
                .operation(Operation.expand)
                .vsacQueryParams(VsacQueryParams.builder().build())
                .resourceClass(ValueSet.class)
                .build()).result()
        valueSet.forEach(vs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(vs))


        then:
        noExceptionThrown()
    }

    def 'expand extensional valueset with filter'() {
        given:
        String id = '2.16.840.1.113883.3.464.1003.113.11.1090'

        when:
        var valueSet = api.operation(VsacFhirResourceOperation.<ValueSet> builder()
                .id(id)
                .operation(Operation.expand)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.filter, 'spine')
                        .build())
                .resourceClass(ValueSet.class)
                .build()).result()
        valueSet.forEach(vs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(vs))

        then:
        noExceptionThrown()
    }

    def 'expand intensional valueset'() {
        given:
        String id = '2.16.840.1.113883.11.20.9.46'

        when:
        var valueSet = api.operation(VsacFhirResourceOperation.<ValueSet> builder()
                .id(id)
                .operation(Operation.expand)
                .vsacQueryParams(VsacQueryParams.builder().build())
                .resourceClass(ValueSet.class)
                .build()).result()
        valueSet.forEach(vs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(vs))


        then:
        noExceptionThrown()
    }

    def 'valueset validate-code'() {
        given:
        String id = '2.16.840.1.113883.3.464.1003.113.11.1090'

        ///$validate-code?system=http://hl7.org/fhir/sid/icd-10-cm&code=M45.0
        when:
        var params = api.operation(VsacFhirParametersOperation.builder()
                .id(id)
                .operation(Operation.validateCode)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://hl7.org/sid/icd-10-cm')
                        .param(VsacQueryParam.code, 'M45.0')
                        .build())
                .resourceClass(ValueSet.class)
                .build()).result()
        println fhirContext.newJsonParser().encodeResourceToString(params)


        then:
        noExceptionThrown()
    }

    def 'read codeSystem'() {
        given:
        String id = 'LOINC'

        when:
        var codeSystem = api.read(VsacFhirRead.<CodeSystem> builder()
                .id(id)
                .resourceClass(CodeSystem.class)
                .build()).result()
        println fhirContext.newJsonParser().encodeResourceToString(codeSystem)

        then:
        noExceptionThrown()
    }

    def 'codeSystem $lookup'() {
        when:
        var codeSystem = api.operation(VsacFhirResourceOperation.<CodeSystem> builder()
                .operation(Operation.lookup)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://loinc.org')
                        .param(VsacQueryParam.code, '1963-8')
                        .build())
                .resourceClass(CodeSystem.class)
                .build()).result()

        codeSystem.forEach(cs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(cs))

        then:
        noExceptionThrown()
    }

    def 'codeSystem $lookup with version'() {
        when:
        var codeSystem = api.operation(VsacFhirResourceOperation.<CodeSystem> builder()
                .operation(Operation.lookup)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://loinc.org')
                        .param(VsacQueryParam.code, '1963-8')
                        .param(VsacQueryParam.version, '2.56')
                        .build())
                .resourceClass(CodeSystem.class)
                .build()).result()

        codeSystem.forEach(cs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(cs))

        then:
        noExceptionThrown()
    }

    def 'codeSystem $lookup with date'() {
        when:
        var codeSystem = api.operation(VsacFhirResourceOperation.<CodeSystem> builder()
                .operation(Operation.lookup)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://loinc.org')
                        .param(VsacQueryParam.code, '1963-8')
                        .param(VsacQueryParam.date, '20150501')
                        .build())
                .resourceClass(CodeSystem.class)
                .build()).result()

        codeSystem.forEach(cs ->
                println "\t" + fhirContext.newJsonParser().encodeResourceToString(cs))

        then:
        noExceptionThrown()
    }

    def 'codeSystem $subsumes a -> b'() {
        when:
        var params = api.operation(VsacFhirParametersOperation.<CodeSystem> builder()
                .operation(Operation.subsumes)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://snomed.info/sct')
                        .param(VsacQueryParam.codeA, '29857009')
                        .param(VsacQueryParam.codeB, '10000006')
                        .build())
                .resourceClass(CodeSystem.class)
                .build()).result()

        println "\t" + fhirContext.newJsonParser().encodeResourceToString(params)

        then:
        noExceptionThrown()
    }

    def 'codeSystem $subsumes a -> b given a code system version'() {
        when:
        var params = api.operation(VsacFhirParametersOperation.<CodeSystem> builder()
                .operation(Operation.subsumes)
                .vsacQueryParams(VsacQueryParams.builder()
                        .param(VsacQueryParam.system, 'http://hl7.org/fhir/sid/icd-10-cm')
                        .param(VsacQueryParam.version, '2018')
                        .param(VsacQueryParam.codeA, 'A01.01')
                        .param(VsacQueryParam.codeB, 'A00-A09')
                        .build())
                .resourceClass(CodeSystem.class)
                .build()).result()

        println "\t" + fhirContext.newJsonParser().encodeResourceToString(params)

        then:
        noExceptionThrown()
    }
}
