package org.opencds.tools.terminology.vsac.client.fhir.response;

import org.hl7.fhir.r4.model.Resource;

public record VsacResult<R extends Resource>(R resource) {
    public static <R extends Resource> Builder<R> builder() {
        return new Builder<>();
    }

    public static final class Builder<R extends Resource> {
        private R resource;

        private Builder() {
        }

        public Builder<R> resource(R resource) {
            this.resource = resource;
            return this;
        }

        public VsacResult<R> build() {
            return new VsacResult<>(this.resource);
        }
    }
}
