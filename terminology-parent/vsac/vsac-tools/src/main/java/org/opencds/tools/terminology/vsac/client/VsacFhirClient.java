package org.opencds.tools.terminology.vsac.client;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BasicAuthInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.ResourceType;
import org.hl7.fhir.r4.model.ValueSet;
import org.opencds.hooks.lib.fhir.FhirContextFactory;
import org.opencds.tools.terminology.api.config.Config;

/**
 * Uses VSAC's FHIR Terminology Service (see: https://cts.nlm.nih.gov/fhir/)
 *
 */
public class VsacFhirClient {
    private static final Log log = LogFactory.getLog(VsacFhirClient.class);
    private static final String VSAC_FHIR_BASE = "https://cts.nlm.nih.gov/fhir";
    private static final String EXPAND = "$expand";
    IGenericClient fhirClient;

    public VsacFhirClient(Config config) {
        fhirClient = FhirContextFactory.get(FhirVersionEnum.R4)
                .newRestfulGenericClient(VSAC_FHIR_BASE);
        fhirClient.registerInterceptor(new BasicAuthInterceptor(config.getVsacApiUsername(), config.getVsacApiPassword()));
    }

    public ValueSet readValueSetAndExpansion(String oid) {
        try {
            Parameters params = fhirClient
                    .operation()
                    .onInstanceVersion(new IdType(ResourceType.ValueSet.name(), oid))
                    .named(EXPAND)
                    .withNoParameters(Parameters.class)
                    .useHttpGet()
                    .execute();

            if (params.getParameter().size() == 1) {
                return (ValueSet) params.getParameter().get(0).getResource();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
