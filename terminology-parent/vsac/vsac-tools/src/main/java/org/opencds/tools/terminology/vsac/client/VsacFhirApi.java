package org.opencds.tools.terminology.vsac.client;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BasicAuthInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.r4.model.Resource;
import org.opencds.tools.terminology.api.config.Config;
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirOperation;
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirRead;
import org.opencds.tools.terminology.vsac.tools.api.client.fhir.VsacFhirResult;

public class VsacFhirApi {
    private static final Log log = LogFactory.getLog(VsacFhirApi.class);
    private static final String VSAC_FHIR_BASE = "https://cts.nlm.nih.gov/fhir";

    private final Config config;
    private final FhirContext fhirContext;

    public VsacFhirApi(Config config) {
        this.config = config;
        fhirContext = FhirContext.forR4();
    }

    private IGenericClient fhirClient() {
        var fhirClient = fhirContext.newRestfulGenericClient(VSAC_FHIR_BASE);
        fhirClient.registerInterceptor(new BasicAuthInterceptor(
                config.getVsacApiUsername(),
                config.getVsacApiPassword()));
        return fhirClient;
    }

    public <R extends Resource> VsacFhirResult<R> read(VsacFhirRead<R> vsacFhirRead) {
        return vsacFhirRead.execute(fhirClient());
    }

    public <T> VsacFhirResult<T> operation(VsacFhirOperation<T> vsacFhirOperation) {
        return vsacFhirOperation.execute(fhirClient());
    }
}
