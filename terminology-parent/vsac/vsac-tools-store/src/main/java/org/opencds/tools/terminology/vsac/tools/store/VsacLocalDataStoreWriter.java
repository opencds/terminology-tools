/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.vsac.client.VsacFhirClient;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.function.Supplier;

/**
 * This class writes UMLS information to locally specified data store, including
 * for defined periodic updates while the process is running. Currently, the
 * local data store is expected to be a Microsoft Access database, and that it
 * has the default structure already defined. Steps to populate and update the
 * data store: 1) In DB - Define value sets in VALUE_SET 2) In DB - Define any
 * ancestor codes to include or exclude in VALUE_SET_ANCESTOR 3) In DB - Define
 * any referenced value sets to include or exclude in REFERENCED_VALUE_SET
 * 
 * @author Kensaku Kawamoto
 */
public class VsacLocalDataStoreWriter {
    private static final Log log = LogFactory.getLog(VsacLocalDataStoreWriter.class);
    
    private final VsacFhirClient client;

    public VsacLocalDataStoreWriter(VsacFhirClient client) {
        this.client = client;
    }

    /**
     * Updates the VSAC local data store as specified. Status of DB will be
     * updated in DB table.
     * 
     * @param supplier supplier for the connection to a database (supplier must create a new connection each time)
     * @throws Exception
     *             If there is any error, exception will be thrown.
     */
    public void updateVsacLocalDataStore(Supplier<Connection> supplier) throws Exception {
        log.info("> Running updateVsacLocalDataStore...");

        DbConnection conn1 = new DbConnection(supplier);
        DbConnection conn2 = new DbConnection(supplier);

        // ensure there is 1 and only 1 DATABASE_STATUS entry
        DatabaseStatus.ensureSingleStatusEntry(conn1);

        // update status of database
        DatabaseStatus.updating(conn1);

        // Update the value sets
        log.info("> Evaluating value sets...");

//        ValueSet.updateEachValueSet(conn1, conn2, client, 1, ValueSetMember::getExistingVsacConcepts,
//                ValueSetMember::updateValueSetMembers);

        // TODO: FIXME: Broken
//        ValueSet.updateEachValueSet(conn1, conn2, 1, (valueSetOid, valueSetName, valueSetVersion) -> {
//            // get existing value set members (note only code system and code
//            // matter for equivalence testing)
//            ValueSet existingVsacConceptsForValueSet;
//            ValueSetMember.getExistingVsacConcepts(conn2, valueSetOid, valueSetVersion, vsacConcept -> {
//                existingVsacConceptsForValueSet.add(vsacConcept);
//            });
//
//            // Conduct VSAC operation to get contents. If successful, delete
//            // exsiting value set contents and proceed.
//            // If error, show error message and skip this value set.
//            try {
//                // define the value sets
//                var newVsacConceptsForValueSet =
//                        client.readValueSetAndExpansion(valueSetOid);
//
//                // since no exception thrown up to here, ready to delete
//                // existing contents of value set and then to
//                // re-populate it, assuming there has been an udpate
//
//                if (newVsacConceptsForValueSet.equals(existingVsacConceptsForValueSet)) {
//                    // do nothing
//                    log.info("No changes needed for value set " + valueSetOid + ": " + valueSetName + ", version "
//                            + valueSetVersion + "...");
//                } else {
//                    log.info("Updating value set " + valueSetOid + ": " + valueSetName + ", version " + valueSetVersion
//                            + "...");
//                    ValueSetMember.deleteValueSets(conn2, valueSetOid, valueSetVersion);
//                    ValueSetMember.addValueSetMembers(conn2, valueSetOid, valueSetVersion, newVsacConceptsForValueSet);
//                    ValueSet.updateValueSetUpdateDtm(conn2, valueSetOid);
//
//                }
//            } catch (Exception e) {
//                log.warn("> Unable to define value set for value set " + valueSetOid + ": " + valueSetName
//                        + ", version " + valueSetVersion + ".  Skipping attempt to update value set.  Details:");
//                e.printStackTrace();
//            }
//
//        });

        // Update Database Status
        log.info("> Updating database status...");

        Timestamp lastUpdateDtm = ValueSet.getLastUpdateDtm(conn1);
        DatabaseStatus.readyToRead(conn1, lastUpdateDtm);

        log.info("> Database update completed.");

        conn1.close();
        conn2.close();
    }

}
