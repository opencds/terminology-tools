/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.Result;

class DatabaseStatus {
    private static final Log log = LogFactory.getLog(DatabaseStatus.class);

    private static final String INSERT_DB_STATUS = "INSERT INTO DATABASE_STATUS(STATUS, LAST_UPDATE_DTM) VALUES (?, ?)";
    private static final String INSERT_DB_STATUS_UPDATING = "INSERT INTO DATABASE_STATUS(STATUS) VALUES ('UPDATING')";
    private static final String UPDATE_DB_STATUS = "UPDATE DATABASE_STATUS SET STATUS = ?";
    private static final String UPDATE_DB_STATUS_STATUS_LAST_UPDATE_DTM = "UPDATE DATABASE_STATUS SET STATUS = ?, LAST_UPDATE_DTM = ?";
    private static final String DELETE_DB_STATUS = "DELETE FROM DATABASE_STATUS";
    private static final String DB_STATUS_COUNT = "SELECT COUNT(*) FROM DATABASE_STATUS";
    private static final String DB_STATUS_SELECT_ALL = "SELECT * FROM DATABASE_STATUS ORDER BY LAST_UPDATE_DTM DESC, STATUS DESC";

    private static final String COL_LAST_UPDATE_DTM = "LAST_UPDATE_DTM";
    private static final String COL_STATUS = "STATUS";

    private static final String CONST_READY_TO_READ = "READY_TO_READ";
    private static final String CONST_UPDATING = "UPDATING";

    static void ensureSingleStatusEntry(DbConnection conn) {
        int count = conn.executeCountQuery(DB_STATUS_COUNT);
        if (count == 0) {
            conn.executeUpdate(INSERT_DB_STATUS_UPDATING);
        } else if (count > 1) {
            log.info("> Found more than one DATABASE_STATUS entry; taking latest and removing rest.");
            StringBuilder status = new StringBuilder();
            Map<String, Timestamp> tsMap = new HashMap<>();
            conn.executeQuery(DB_STATUS_SELECT_ALL, rsIter -> {
                Result rs = rsIter.iterator().next();
                status.append(rs.getString(COL_STATUS));
                tsMap.put(COL_LAST_UPDATE_DTM, rs.getTimestamp(COL_LAST_UPDATE_DTM));
            });
            conn.executeUpdate(DELETE_DB_STATUS);
            conn.executeUpdate(INSERT_DB_STATUS, ps -> {
                ps.setString(1, status.toString());
                ps.setTimestamp(2, tsMap.get(COL_LAST_UPDATE_DTM));
            });
        }
    }
    
    static void updateStatus(DbConnection conn, String status) {
        conn.executeUpdate(UPDATE_DB_STATUS, ps -> {
            ps.setString(1, status);
        });
    }

    static void readyToRead(DbConnection conn, Timestamp lastUpdateDtm) {
        updateStatus(conn, CONST_READY_TO_READ, lastUpdateDtm);
    }

    static void updating(DbConnection conn) {
        updateStatus(conn, CONST_UPDATING);
    }
    
    private static void updateStatus(DbConnection conn, String status, Timestamp lastUpdateDtm) {
        conn.executeUpdate(UPDATE_DB_STATUS_STATUS_LAST_UPDATE_DTM, ps -> {
            ps.setString(1, status);
            ps.setTimestamp(2, lastUpdateDtm);
        });
    }


    
}
