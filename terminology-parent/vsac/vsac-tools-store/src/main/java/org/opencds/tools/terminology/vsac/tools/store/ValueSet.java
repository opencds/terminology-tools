/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store;

import java.sql.Timestamp;
import java.util.Date;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.function.TriConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.Result;

class ValueSet {
    private static final Log log = LogFactory.getLog(ValueSet.class);

    private static final String UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_OID = "UPDATE VALUE_SET SET UPDATE_DTM = ? WHERE VALUE_SET_OID = ?";
    private static final String VALUE_SET_SELECT_MAX_UPDATE_DTM = "SELECT MAX(UPDATE_DTM) " + " FROM VALUE_SET";
    private static final String VALUE_SET_SELECT_WHERE_SKIP_NE_1_ORDER_BY_VALUE_SET_OID_VALUE_SET_VERSION = "SELECT VALUE_SET_OID, VALUE_SET_NAME, VALUE_SET_VERSION FROM VALUE_SET WHERE SKIP != 1 ORDER BY VALUE_SET_OID, VALUE_SET_VERSION";
    private static final String VALUE_SET_QUERY = "SELECT VALUE_SET_OID, VALUE_SET_VERSION FROM VALUE_SET ORDER BY VALUE_SET_OID, VALUE_SET_VERSION ";

    private static final String COL_VALUE_SET_OID = "VALUE_SET_OID";
    private static final String COL_VALUE_SET_VERSION = "VALUE_SET_VERSION";
    private static final String COL_VALUE_SET_NAME = "VALUE_SET_NAME";
    
    static void updateValueSetUpdateDtm(DbConnection conn2, String valueSetOid) {
        conn2.executeUpdateNoCommit(UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_OID, ps -> {
            ps.setTimestamp(1, new Timestamp(new Date().getTime()));
            ps.setString(2, valueSetOid);
        });
    }

    /**
     * 
     * @param conn1
     * @param conn2
     * @param skip
     * @param consumer expects a consumer that accepts the value set OID, Name and Version, in that order.
     */
    static void updateEachValueSet(DbConnection conn1, DbConnection conn2, int skip,
            TriConsumer<String, String, String> consumer) {
        conn1.executeQuery(VALUE_SET_SELECT_WHERE_SKIP_NE_1_ORDER_BY_VALUE_SET_OID_VALUE_SET_VERSION, rsIter -> {
            rsIter.forEach(result -> {
                String valueSetOid = result.getString(COL_VALUE_SET_OID);
                String valueSetName = result.getString(COL_VALUE_SET_NAME);
                String valueSetVersion = result.getString(COL_VALUE_SET_VERSION);
                consumer.accept(valueSetOid, valueSetName, valueSetVersion);
            });
        });
    }
    
    static Timestamp getLastUpdateDtm(DbConnection conn) {
        return conn.executeTimestampQuery(VALUE_SET_SELECT_MAX_UPDATE_DTM);
    }

    /**
     * 
     * @param connection
     * @param rsConsumer Expects a consumer that accepts the value set OID and version, in that order.
     */
    static void forEachValueSet(DbConnection connection, BiConsumer<String, String> rsConsumer) {
        connection.executeQuery(VALUE_SET_QUERY, rsIterable -> {
            rsIterable.forEach((Result r) -> {
                String valueSetOid = r.getString(COL_VALUE_SET_OID);
                String valueSetVersion = r.getString(COL_VALUE_SET_VERSION);
                rsConsumer.accept(valueSetOid, valueSetVersion);
            });
        });
    }

}
