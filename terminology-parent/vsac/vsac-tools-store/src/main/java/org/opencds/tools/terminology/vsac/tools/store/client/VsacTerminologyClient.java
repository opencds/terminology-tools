/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store.client;

import org.opencds.tools.terminology.api.client.TerminologyClient;
import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;
import org.opencds.tools.terminology.vsac.tools.api.client.model.VsacCodeSystem;
import org.opencds.tools.terminology.vsac.tools.api.client.model.VsacConcept;
import org.opencds.tools.terminology.vsac.tools.store.VsacLocalDataStoreReader;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class VsacTerminologyClient implements TerminologyClient {
    private Supplier<Connection> connectionSupplier;
    private Map<String, Set<ConceptReference>> valueSetOidCaratVersionToVsacConceptSet;
    private Set<ConceptReference> vsacConcepts;

    public VsacTerminologyClient() {
    }
    
    public VsacTerminologyClient(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    public VsacTerminologyClient(Map<String, Set<ConceptReference>> valueSetOidCaratVersionToVsacConceptSet) {
        this.valueSetOidCaratVersionToVsacConceptSet = valueSetOidCaratVersionToVsacConceptSet;
        this.vsacConcepts = flattenValues(valueSetOidCaratVersionToVsacConceptSet.values());
    }
    
    /**
     * Also refreshes the data.
     */
    @Override
    public void setConnectionSupplier(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    private Set<ConceptReference> flattenValues(Collection<Set<ConceptReference>> collection) {
        return collection.stream().flatMap(Collection::stream).collect(Collectors.toSet());
    }

    public void refresh() {
        if (connectionSupplier == null) {
            throw new RuntimeException("connection is null");
        }
        VsacLocalDataStoreReader reader = new VsacLocalDataStoreReader(connectionSupplier);
        this.valueSetOidCaratVersionToVsacConceptSet = reader.getValueSetOidCaratVersionToVsacConceptSet();
        this.vsacConcepts = flattenValues(valueSetOidCaratVersionToVsacConceptSet.values());
    }

    /**
     * Returns members of value set. Returns null if not found.
     * 
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    public Set<ConceptReference> getConceptsInValueSet(String valueSetOid,
            String valueSetVersion_mayBeNull) {
    	String str = getValueSetIdString(valueSetOid, valueSetVersion_mayBeNull);
    	Set<ConceptReference> conceptSet = valueSetOidCaratVersionToVsacConceptSet.get(str);
    	return conceptSet;
//        return valueSetOidCaratVersionToVsacConceptSet.get(getValueSetIdString(valueSetOid, valueSetVersion_mayBeNull));
    }

    /**
     * Returns true if concept in value set; returns false otherwise.
     * 
     * @param codeSystem
     * @param code
     * @param valueSetId
     * @param version
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Override
    public boolean isConceptInValueSet(CodeSystem codeSystem, String code,
                                       String valueSetId, String version) {
        Set<ConceptReference> conceptSet = valueSetOidCaratVersionToVsacConceptSet
                .get(getValueSetIdString(valueSetId, version));
        if (conceptSet == null) {
            return false;
        }
        Set<VsacCodeSystem> cses = VsacCodeSystem.byCodeSystem(codeSystem);
        return conceptSet.stream().map(c -> (VsacConcept) c)
                .anyMatch(vc -> cses.contains(vc.getVsacCodeSystem()) && vc.getCode().equalsIgnoreCase(code));
    }

    /**
     * Returns the single String version of the value set OID and optional
     * version.
     *
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     */
    private static String getValueSetIdString(String valueSetOid, String valueSetVersion_mayBeNull) {
        String stringToReturn = valueSetOid + "^";
        if ((valueSetVersion_mayBeNull != null) && (!valueSetVersion_mayBeNull.equals(""))) {
            stringToReturn += valueSetVersion_mayBeNull;
        }
        return stringToReturn;
    }

    @Override
    public Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version) {
        return vsacConcepts.stream()
                .filter(vc -> vc.getCodeSystem() == codeSystem && vc.getCode().equalsIgnoreCase(code))
                .collect(Collectors.toSet());
    }
    
    @Override
    public boolean supports(ValueSetSource terminologySource) {
        return ValueSetSource.VSAC == terminologySource;
    }

}
