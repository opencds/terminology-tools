/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store;

import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.api.model.ConceptReference;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class VsacLocalDataStoreReader {

    private Supplier<Connection> connectionSupplier;
    
    private Map<String, Set<ConceptReference>> valueSetOidCaratVersionToVsacConceptSet;

    public VsacLocalDataStoreReader(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }
    
    public VsacLocalDataStoreReader(Map<String, Set<ConceptReference>> valueSetOidCaratVersionToVsacConceptSet) {
        this.valueSetOidCaratVersionToVsacConceptSet = valueSetOidCaratVersionToVsacConceptSet;
    }

    public void refresh() {
        DbConnection conn1 = new DbConnection(connectionSupplier);
        DbConnection conn2 = new DbConnection(connectionSupplier);
        // Clear/initialize _new content
        // The one in use. OidCarrotVersion will always start with OID^.
        // If Version is null/empty, will be an empty string after.
        // E.g., 2.16.840.1.113762.1.4.1021.15^
        // E.g., 2.16.840.1.113762.1.4.1021.15^2017-03
        Map<String, Set<ConceptReference>> myValueSetOidCaratVersionToVsacConceptSetMap = new HashMap<>();
        ValueSet.forEachValueSet(conn1, (valueSetOid, valueSetVersion) -> {
            if (valueSetVersion == null || valueSetVersion.isEmpty()) {
                ValueSetMember.findWithNoValueSetVersion(conn2, valueSetOid, vsacConcept -> {
                    String id = getValueSetIdString(valueSetOid, null);
                    Set<ConceptReference> concepts = myValueSetOidCaratVersionToVsacConceptSetMap.getOrDefault(id, new HashSet<>());
                    concepts.add(vsacConcept);
                    myValueSetOidCaratVersionToVsacConceptSetMap.put(id, concepts);
                });
            } else {
                ValueSetMember.findWithValueSetVersion(conn2, valueSetOid, valueSetVersion, vsacConcept -> {
                    String id = getValueSetIdString(valueSetOid, valueSetVersion);
                    Set<ConceptReference> concepts = myValueSetOidCaratVersionToVsacConceptSetMap.getOrDefault(id, new HashSet<>());
                    concepts.add(vsacConcept);
                    myValueSetOidCaratVersionToVsacConceptSetMap.put(id, concepts);
                });
            }
        });
        valueSetOidCaratVersionToVsacConceptSet = myValueSetOidCaratVersionToVsacConceptSetMap;
    }

    public Map<String, Set<ConceptReference>> getValueSetOidCaratVersionToVsacConceptSet() {
        return valueSetOidCaratVersionToVsacConceptSet;
    }
    
    /**
     * Returns the single String version of the value set OID and optional
     * version.
     * 
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     */
    private String getValueSetIdString(String valueSetOid, String valueSetVersion_mayBeNull) {
        String stringToReturn = valueSetOid + "^";
        if ((valueSetVersion_mayBeNull != null) && (!valueSetVersion_mayBeNull.equals(""))) {
            stringToReturn += valueSetVersion_mayBeNull;
        }
        return stringToReturn;
    }

}
