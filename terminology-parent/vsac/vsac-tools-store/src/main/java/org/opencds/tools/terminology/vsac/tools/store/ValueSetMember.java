/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store;

import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.Result;
import org.opencds.tools.terminology.vsac.tools.api.client.model.VsacCodeSystem;
import org.opencds.tools.terminology.vsac.tools.api.client.model.VsacConcept;

import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

class ValueSetMember {
    private static final String INSERT_VALUE_SET_MEMBER = "INSERT INTO VALUE_SET_MEMBER(VALUE_SET_OID, VALUE_SET_VERSION, MEMBER_CODE_SYSTEM, MEMBER_CODE_SYSTEM_NAME, MEMBER_CODE, MEMBER_NAME) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String DELETE_VALUE_SET_MEMBER_WHERE_VALUE_SET_OID_AND_VALUE_SET_VERSION = "DELETE FROM VALUE_SET_MEMBER WHERE VALUE_SET_OID = ? AND VALUE_SET_VERSION = ?";
    private static final String VALUE_SET_MEMBER_SELECT_WHERE_VALUE_SET_OID_AND_VALUE_SET_VERSION = "SELECT MEMBER_CODE_SYSTEM, MEMBER_CODE_SYSTEM_NAME, MEMBER_CODE, MEMBER_NAME FROM VALUE_SET_MEMBER WHERE VALUE_SET_OID = ? AND VALUE_SET_VERSION = ?";
    private static final String MEMBER_QUERY_NO_VALUE_SET_VERSION = "SELECT MEMBER_CODE_SYSTEM, MEMBER_CODE_SYSTEM_NAME, MEMBER_CODE, MEMBER_NAME FROM VALUE_SET_MEMBER WHERE VALUE_SET_OID = ? AND  (VALUE_SET_VERSION IS NULL OR VALUE_SET_VERSION = '')";

    private static final String COL_MEMBER_CODE = "MEMBER_CODE";
    private static final String COL_MEMBER_CODE_SYSTEM = "MEMBER_CODE_SYSTEM";
    private static final String COL_MEMBER_CODE_SYSTEM_NAME = "MEMBER_CODE_SYSTEM_NAME";
    private static final String COL_MEMBER_NAME = "MEMBER_NAME";

    public static void addValueSetMembers(DbConnection conn, String valueSetOid, String valueSetVersion,
            Set<VsacConcept> newVsacConcpetsForValueSet) {
        conn.executeBatch(c -> {
            newVsacConcpetsForValueSet.forEach(newVsacConceptForValueSet -> {
                conn.executeUpdateNoCommit(INSERT_VALUE_SET_MEMBER, ps -> {
                    ps.setString(1, valueSetOid);
                    ps.setString(2, valueSetVersion);
                    ps.setString(3, newVsacConceptForValueSet.getCodeSystem() == null ? null : Objects.toString(newVsacConceptForValueSet.getCodeSystem().getFirstOid()));
                    // FIXME: what name are we talking about here?
                    ps.setString(4, Objects.toString(newVsacConceptForValueSet.getCodeSystem()));
                    ps.setString(5, newVsacConceptForValueSet.getCode());
                    ps.setString(6, newVsacConceptForValueSet.getPreferredName());
                });
            });
        });
    }

    static void deleteValueSets(DbConnection conn, String valueSetOid, String valueSetVersion) {
        conn.executeUpdate(DELETE_VALUE_SET_MEMBER_WHERE_VALUE_SET_OID_AND_VALUE_SET_VERSION, ps -> {
            ps.setString(1, valueSetOid);
            ps.setString(2, valueSetVersion);
        });
    }

    static void getExistingVsacConcepts(DbConnection conn, String valueSetOid, String valueSetVersion,
            Consumer<VsacConcept> resultConsumer) {
        conn.executePreparedStatement(VALUE_SET_MEMBER_SELECT_WHERE_VALUE_SET_OID_AND_VALUE_SET_VERSION, ps -> {
            ps.setString(1, valueSetOid);
            ps.setString(2, valueSetVersion);
        }, rsIter -> {
            rsIter.forEach(rs2 -> {
                System.err.println(rs2.getString(COL_MEMBER_CODE_SYSTEM_NAME) + ", " + rs2.getString(COL_MEMBER_CODE) + ", "
                        + rs2.getString(COL_MEMBER_NAME));
                resultConsumer.accept(new VsacConcept(
                        VsacCodeSystem.byName(rs2.getString(COL_MEMBER_CODE_SYSTEM_NAME)),
                        // rs2.getString(COL_MEMBER_CODE_SYSTEM_NAME),
                        rs2.getString(COL_MEMBER_CODE),
                        rs2.getString(COL_MEMBER_NAME)));
            });
        });
    }

    static void findWithNoValueSetVersion(DbConnection conn2, String valueSetOid,
            Consumer<VsacConcept> resultConsumer) {
        conn2.executePreparedStatement(MEMBER_QUERY_NO_VALUE_SET_VERSION, ps -> {
            ps.setString(1, valueSetOid);
        }, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(new VsacConcept(
                        VsacCodeSystem.byName(r.getString(COL_MEMBER_CODE_SYSTEM_NAME)),
                        r.getString(COL_MEMBER_CODE),
                        r.getString(COL_MEMBER_NAME)
                        ));
//                String codeSystemName = r.getString(COL_MEMBER_CODE_SYSTEM_NAME);
            });
        });
    }

    static void findWithValueSetVersion(DbConnection conn2, String valueSetOid, String valueSetVersion,
            Consumer<VsacConcept> resultConsumer) {
        conn2.executePreparedStatement(VALUE_SET_MEMBER_SELECT_WHERE_VALUE_SET_OID_AND_VALUE_SET_VERSION, ps -> {
            ps.setString(1, valueSetOid);
            ps.setString(2, valueSetVersion);
        }, rsIter -> {
            rsIter.forEach((Result r) -> {
//                String codeSystemName = r.getString(COL_MEMBER_CODE_SYSTEM_NAME);
                resultConsumer.accept(new VsacConcept(
                        VsacCodeSystem.byName(r.getString(COL_MEMBER_CODE_SYSTEM_NAME)),
                        r.getString(COL_MEMBER_CODE),
                        r.getString(COL_MEMBER_NAME)
                        ));
            });

        });
    }

}
