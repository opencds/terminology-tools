/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.store

import org.opencds.tools.db.api.DbConnection
import org.opencds.tools.terminology.api.config.Config
import org.opencds.tools.terminology.api.store.AccessConnection
import org.opencds.tools.terminology.vsac.client.VsacFhirClient
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.sql.Timestamp

class VsacLocalDataStoreWriterFunctionalSpec extends Specification {
    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_VSAC_OpioidCds.accdb'
    
    VsacLocalDataStoreWriter writer
    Config config
    
    File testFile
    String testFileName

    def setup() {
        testFile = File.createTempFile('test', 'accdb')
        Files.copy(new File(DBFILENAME).toPath(), testFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
        config = new Config()
        VsacFhirClient client = new VsacFhirClient(config)
        writer = new VsacLocalDataStoreWriter(client)
    }
    
    def 'test updateVsacLocalDataStore'() {
        given:
        DbConnection conn = new DbConnection(AccessConnection.supplier(testFile))
        
        when:
        Timestamp ts0 = ValueSet.getLastUpdateDtm(conn)
        writer.updateVsacLocalDataStore(AccessConnection.supplier(new File(testFile.absolutePath)))
        Timestamp ts1 = ValueSet.getLastUpdateDtm(conn)
        
        and:
        Files.copy(testFile.toPath(), new File('/Users/phillip/tmp/terminology/test-vsac.accdb').toPath(), StandardCopyOption.REPLACE_EXISTING)

        then:
        notThrown(Exception)
        ts0
        ts1
        println ts0
        println ts1
    }
    
    
}
