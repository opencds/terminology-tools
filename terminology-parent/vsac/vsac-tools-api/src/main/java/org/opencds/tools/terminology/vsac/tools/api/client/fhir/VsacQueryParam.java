package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

public enum VsacQueryParam {
    code("code"),
    codeA("codeA"),
    codeB("codeB"),
    date("date"),
    filter("filter"),
    system("system"),
    version("version");

    private final String value;

    VsacQueryParam(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
