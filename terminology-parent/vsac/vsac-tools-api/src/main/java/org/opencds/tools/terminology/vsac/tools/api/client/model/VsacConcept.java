/*
  * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.api.client.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.opencds.tools.terminology.api.model.ConceptReferenceImpl;

public class VsacConcept extends ConceptReferenceImpl {

    private final VsacCodeSystem vsacCodeSystem;

    public VsacConcept(VsacCodeSystem vsacCodeSystem, String code) {
        super(vsacCodeSystem.getCodeSystem(), code);
        this.vsacCodeSystem = vsacCodeSystem;
    }

    public VsacConcept(VsacCodeSystem vsacCodeSystem, String code, String name) {
        super(vsacCodeSystem == null ? null : vsacCodeSystem.getCodeSystem(), code, name);
        this.vsacCodeSystem = vsacCodeSystem;
    }

    public VsacCodeSystem getVsacCodeSystem() {
        return vsacCodeSystem;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(vsacCodeSystem).build();
    }
}
