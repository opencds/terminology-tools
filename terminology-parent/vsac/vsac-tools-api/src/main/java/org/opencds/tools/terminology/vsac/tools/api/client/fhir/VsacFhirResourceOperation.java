package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IOperationUnnamed;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Resource;
import org.opencds.tools.terminology.api.util.Utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public record VsacFhirResourceOperation<R extends Resource>(Operation operation,
                                                            VsacQueryParams vsacQueryParams,
                                                            String id,
                                                            Class<R> resourceClass) implements VsacFhirOperation<List<R>> {
    public VsacFhirResourceOperation {
        Utils.requireNonNull(operation, "operation");
        Utils.requireNonNull(vsacQueryParams, "vsacQueryParams");
        Utils.requireNonNull(resourceClass, "resourceClass");
    }

    @Override
    public VsacFhirResult<List<R>> execute(IGenericClient fhirClient) {
        IOperationUnnamed unnamedOperation;
        if (StringUtils.isNotBlank(id)) {
            unnamedOperation = fhirClient.operation()
                    .onInstance(new IdType(resourceClass.getSimpleName(), id));
        } else {
            unnamedOperation = fhirClient.operation()
                    .onType(resourceClass);
        }
        return VsacFhirResult.create(Optional.of(
                        unnamedOperation
                                .named(operation.getValue())
                                .withParameters(parameters(vsacQueryParams))
                                .useHttpGet()
                                .execute())
                .map(Parameters::getParameter)
                .stream()
                .flatMap(Collection::stream)
                .map(Parameters.ParametersParameterComponent::getResource)
                .filter(resourceClass::isInstance)
                .map(resourceClass::cast)
                .toList());
    }

    private static Parameters parameters(VsacQueryParams vsacQueryParams) {
        var parameters = new Parameters();
        Optional.ofNullable(vsacQueryParams)
                .map(VsacQueryParams::vsacQueryParams)
                .stream()
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .forEach(entry -> parameters.setParameter(
                        entry.getKey().getValue(),
                        entry.getValue()));
        return parameters;
    }

    public static <R extends Resource> Builder<R> builder() {
        return new Builder<>();
    }

    public static final class Builder<R extends Resource> {
        private String id;

        private Operation operation;

        private VsacQueryParams vsacQueryParams;

        private Class<R> resourceClass;

        private Builder() {
        }

        public Builder<R> id(String id) {
            this.id = id;
            return this;
        }

        public Builder<R> operation(Operation operation) {
            this.operation = operation;
            return this;
        }

        public Builder<R> vsacQueryParams(VsacQueryParams vsacQueryParams) {
            this.vsacQueryParams = vsacQueryParams;
            return this;
        }

        public Builder<R> resourceClass(Class<R> resourceClass) {
            this.resourceClass = resourceClass;
            return this;
        }

        public VsacFhirResourceOperation<R> build() {
            return new VsacFhirResourceOperation<>(operation, vsacQueryParams, id, resourceClass);
        }
    }
}
