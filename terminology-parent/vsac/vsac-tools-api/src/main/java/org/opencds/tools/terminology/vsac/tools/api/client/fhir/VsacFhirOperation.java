package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import ca.uhn.fhir.rest.client.api.IGenericClient;

public interface VsacFhirOperation<T> {
    VsacFhirResult<T> execute(IGenericClient fhirClient);
}
