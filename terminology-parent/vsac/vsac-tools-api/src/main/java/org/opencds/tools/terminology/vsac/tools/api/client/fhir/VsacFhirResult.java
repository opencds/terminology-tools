package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

public record VsacFhirResult<T>(T result) {
    public static <T> VsacFhirResult<T> create(T result) {
        return new VsacFhirResult<>(result);
    }
}
