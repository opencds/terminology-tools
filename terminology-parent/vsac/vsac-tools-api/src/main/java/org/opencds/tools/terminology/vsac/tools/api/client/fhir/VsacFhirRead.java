package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.r4.model.Resource;
import org.opencds.tools.terminology.api.util.Utils;

public record VsacFhirRead<R extends Resource>(String id,
                                               Class<R> resourceClass) implements VsacFhirQuery<R> {

    public VsacFhirRead {
        Utils.requireNonEmptyString(id, "id");
        Utils.requireNonNull(resourceClass, "resourceClass");
    }

    @Override
    public VsacFhirResult<R> execute(IGenericClient fhirClient) {
        return VsacFhirResult.create(fhirClient.read()
                .resource(resourceClass)
                .withId(id)
                .execute());
    }

    public static <R extends Resource> Builder<R> builder() {
        return new Builder<>();
    }

    public static final class Builder<R extends Resource> {
        private String id;
        private Class<R> resourceClass;

        private Builder() {
        }

        public Builder<R> id(String oid) {
            this.id = oid;
            return this;
        }

        public Builder<R> resourceClass(Class<R> resourceClass) {
            this.resourceClass = resourceClass;
            return this;
        }

        public VsacFhirRead<R> build() {
            return new VsacFhirRead<>(id, resourceClass);
        }
    }
}
