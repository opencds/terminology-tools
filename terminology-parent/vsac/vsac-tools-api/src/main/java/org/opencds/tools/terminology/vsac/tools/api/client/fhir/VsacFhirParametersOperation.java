package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IOperationUnnamed;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Resource;
import org.opencds.tools.terminology.api.util.Utils;

public record VsacFhirParametersOperation<R extends Resource>(Operation operation,
                                                              VsacQueryParams vsacQueryParams,
                                                              String id,
                                                              Class<R> resourceClass) implements VsacFhirOperation<Parameters> {
    public VsacFhirParametersOperation {
        Utils.requireNonNull(operation, "operation");
        Utils.requireNonNull(vsacQueryParams, "vsacQueryParams");
        Utils.requireNonNull(resourceClass, "resourceClass");
    }

    @Override
    public VsacFhirResult<Parameters> execute(IGenericClient fhirClient) {
        IOperationUnnamed unnamedOperation;
        if (StringUtils.isNotBlank(id)) {
            unnamedOperation = fhirClient.operation()
                    .onInstance(new IdType(resourceClass.getSimpleName(), id));
        } else {
            unnamedOperation = fhirClient.operation()
                    .onType(resourceClass);
        }
        return VsacFhirResult.create(
                unnamedOperation
                        .named(operation.getValue())
                        .withParameters(parameters(vsacQueryParams))
                        .useHttpGet()
                        .execute());
    }

    private static Parameters parameters(VsacQueryParams vsacQueryParams) {
        var parameters = new Parameters();
        vsacQueryParams.vsacQueryParams()
                .forEach((key, value) -> parameters.setParameter(key.getValue(), value));
        return parameters;
    }

    public static <R extends Resource> Builder<R, Parameters> builder() {
        return new Builder<>();
    }

    public static final class Builder<R extends Resource, P extends Parameters> {
        private String id;
        private Operation operation;
        private VsacQueryParams vsacQueryParams;
        private Class<R> resourceClass;

        private Builder() {
        }

        public Builder<R, P> id(String id) {
            this.id = id;
            return this;
        }

        public Builder<R, P> operation(Operation operation) {
            this.operation = operation;
            return this;
        }

        public Builder<R, P> vsacQueryParams(VsacQueryParams vsacQueryParams) {
            this.vsacQueryParams = vsacQueryParams;
            return this;
        }

        public Builder<R, P> resourceClass(Class<R> resourceClass) {
            this.resourceClass = resourceClass;
            return this;
        }

        public VsacFhirParametersOperation<R> build() {
            return new VsacFhirParametersOperation<>(
                    operation,
                    vsacQueryParams,
                    id,
                    resourceClass);
        }
    }
}
