package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Resource;

public enum Operation {
    expand("$expand", Resource.class),
    validateCode("$validate-code", Parameters.class),
    lookup("$lookup", Resource.class),
    subsumes("$subsumes", Parameters.class);

    private final String value;
    private final Class<? extends Resource> resourceClass;

    Operation(String value, Class<? extends Resource> resourceClass) {
        this.value = value;
        this.resourceClass = resourceClass;
    }

    public String getValue() {
        return value;
    }

    public Class<? extends Resource> getResourceClass() {
        return resourceClass;
    }
}
