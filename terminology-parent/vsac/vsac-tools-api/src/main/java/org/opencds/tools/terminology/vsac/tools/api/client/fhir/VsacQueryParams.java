package org.opencds.tools.terminology.vsac.tools.api.client.fhir;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public record VsacQueryParams(Map<VsacQueryParam, String> vsacQueryParams) {

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Map<VsacQueryParam, String> vsacQueryParams = new HashMap<>();

        private Builder() {
        }

        public Builder param(VsacQueryParam vsacQueryParam, String value) {
            if (this.vsacQueryParams != null && StringUtils.isNotBlank(value)) {
                vsacQueryParams.put(vsacQueryParam, value);
            }
            return this;
        }

        public Builder vsacQueryParams(Map<VsacQueryParam, String> vsacQueryParams) {
            this.vsacQueryParams = vsacQueryParams;
            return this;
        }

        public VsacQueryParams build() {
            return new VsacQueryParams(this.vsacQueryParams);
        }
    }
}
