package org.opencds.tools.terminology.vsac.tools.api.client.model;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.opencds.tools.terminology.api.constants.CodeSystemEnum;
import org.opencds.tools.terminology.api.model.CodeSystem;

public enum VsacCodeSystem {
    CDT("CDT", CodeSystemEnum.CDT),
    CPT("CPT", CodeSystemEnum.CPT),
    CVX("CVX", CodeSystemEnum.CVX),
    HCPCS("HCPCS", CodeSystemEnum.HCPCS),
    HSLOC("HSLOC", CodeSystemEnum.UNKNOWN),
    ICD10CM("ICD10CM", CodeSystemEnum.ICD10CM),
    ICD10PCS("ICD10PCS", CodeSystemEnum.ICD10PCS),
    ICD9CM("ICD9CM", CodeSystemEnum.ICD9CM),
    LOINC("LOINC", CodeSystemEnum.LOINC),
    MED_RT("MED-RT", CodeSystemEnum.UNKNOWN),
    NCI("NCI", CodeSystemEnum.NCI),
    NDFRT("NDFRT", CodeSystemEnum.NDFRT),
    NUCCPT("NUCCPT", CodeSystemEnum.UNKNOWN),
    RXNORM("RXNORM", CodeSystemEnum.RXNORM),
    SNOMEDCT("SNOMEDCT", CodeSystemEnum.SNOMEDCT),
    SOP("SOP", CodeSystemEnum.UNKNOWN),
    UCUM("UCUM", CodeSystemEnum.UCUM),
    UMLS("UMLS", CodeSystemEnum.UMLS),
    UNII("UNII", CodeSystemEnum.UNII),
    ActCode("ActCode", CodeSystemEnum.ActCode),
    ActMood("ActMood", CodeSystemEnum.ActMood),
    ActPriority("ActPriority", CodeSystemEnum.ActPriority),
    ActReason("ActReason", CodeSystemEnum.ActReason),
    ActRelationshipType("ActRelationshipType", CodeSystemEnum.ActRelationshipType),
    ActStatus("ActStatus", CodeSystemEnum.ActStatus),
    AddressUse("AddressUse", CodeSystemEnum.AddressUse),
    AdministrativeGender("AdministrativeGender", CodeSystemEnum.AdministrativeGender),
    AdministrativeSex("AdministrativeSex", CodeSystemEnum.AdministrativeSex),
    Confidentiality("Confidentiality", CodeSystemEnum.Confidentiality),
    DischargeDisposition("DischargeDisposition", CodeSystemEnum.UNKNOWN),
    EntityNamePartQualifier("EntityNamePartQualifier", CodeSystemEnum.EntityNamePartQualifier),
    EntityNameUse("EntityNameUse", CodeSystemEnum.EntityNameUse),
    LanguageAbilityMode("LanguageAbilityMode", CodeSystemEnum.LanguageAbilityMode),
    LanguageAbilityProficiency("LanguageAbilityProficiency", CodeSystemEnum.LanguageAbilityProficiency),
    LivingArrangement("LivingArrangement", CodeSystemEnum.LivingArrangement),
    MaritalStatus("MaritalStatus", CodeSystemEnum.MaritalStatus),
    NullFlavor("NullFlavor", CodeSystemEnum.NullFlavor),
    ObservationInterpretation("ObservationInterpretation", CodeSystemEnum.ObservationInterpretation),
    ObservationValue("ObservationValue", CodeSystemEnum.ObservationValue),
    ParticipationFunction("ParticipationFunction", CodeSystemEnum.ParticipationFunction),
    ParticipationMode("ParticipationMode", CodeSystemEnum.ParticipationMode),
    ParticipationType("ParticipationType", CodeSystemEnum.ParticipationType),
    ReligiousAffiliation("ReligiousAffiliation", CodeSystemEnum.ReligiousAffiliation),
    RoleClass("RoleClass", CodeSystemEnum.RoleClass),
    RoleCode("RoleCode", CodeSystemEnum.RoleCode),
    RoleStatus("RoleStatus", CodeSystemEnum.RoleStatus),
    mediaType("mediaType", CodeSystemEnum.mediaType)
    ;

    private final String name;
    private final CodeSystemEnum codeSystem;

    VsacCodeSystem(String name, CodeSystemEnum codeSystem) {
        this.name = name;
        this.codeSystem = codeSystem;
    }
    
    public String getName() {
        return name;
    }
    
    public CodeSystemEnum getCodeSystem() {
        return codeSystem;
    }
    
    public static VsacCodeSystem byName(String csName) {
        return Arrays.stream(values()).filter(cs -> cs.name.equalsIgnoreCase(csName)).findFirst().orElse(null);
    }

    /**
     * Find a VsacCodeSystem by OID by looking at the associated <tt>codeSystem</tt>.
     * 
     * FIXME: Currently returns the first match. However, the mapping between {@link VsacCodeSystem} and {@link CodeSystemEnum} is not bidirectional.
     * 
     * @see CodeSystemEnum
     * @param oid
     * @return
     */
    public static VsacCodeSystem byOid(String oid) {
        return Arrays.stream(values())
                .filter(cs -> cs.codeSystem != null
                        && cs.codeSystem.getOids().stream()
                        .anyMatch(o -> o.toString().equalsIgnoreCase(oid)))
                .findFirst().orElse(null);
    }

    public static VsacCodeSystem byUrn(String urn) {
        return Arrays.stream(values())
                .filter(cs -> urn.equalsIgnoreCase(cs.codeSystem.getUrnAsString()))
                .findFirst().orElse(null);
    }

    public static Set<VsacCodeSystem> byCodeSystem(CodeSystem codeSystem) {
        return Arrays.stream(values())
                .filter(cs -> cs.codeSystem != null
                        && cs.codeSystem.equals(codeSystem))
                .collect(Collectors.toSet());
    }
    
}
