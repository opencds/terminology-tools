The XML files contained in this folder are example configurations
for the plugin.  The contents will need to be placed in the
appropriate locations in OpenCDS.

Please note the following:

1. knowledgeModules.xml
   <preProcessPlugin> contains the information for the KnowledgeModule.
   
2. vsac-plugin.xml
   <pluginPackage> contains the configuration for the plugin
   The <plugins> defines the set of <plugin>, each of which are the
   plugins to be imported into the OpenCDS context.
   the <identifier> values are included in the <knowledgeModule> configuration.

3. vsac-terminology-supporting-data.xml
   <supportingData> defines the data that will support plugin processing.
   <package> defines the supporting data package, including the typeand packageId.
   The <packageId> points to the file that is stored in the plugins/packages folder
   within the OpenCDS configuration.
   <loadedBy> indicates the plugin that will load the supporting data.