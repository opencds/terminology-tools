/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.plugin.terminology.vsac;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.plugin.api.PreProcessPlugin;
import org.opencds.plugin.api.PreProcessPluginContext;
import org.opencds.plugin.api.SupportingData;
import org.opencds.tools.terminology.api.store.AccessConnection;
import org.opencds.tools.terminology.api.store.DataStoreReader;
import org.opencds.tools.terminology.vsac.tools.store.VsacLocalDataStoreReader;
import org.opencds.tools.terminology.vsac.tools.store.client.VsacTerminologyClient;

import java.sql.Connection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * OpenCDS pre-process plugin that loads a VSAC data store to provide CDS
 * terminology support.
 *
 * @author Kensaku Kawamoto
 * @author phillip
 */
public class VsacTerminologyPreProcessPlugin extends DataStoreReader implements PreProcessPlugin {
    private static final Log log = LogFactory.getLog(VsacTerminologyPreProcessPlugin.class);
    private static final String ACCESS_DB_ID = "VSAC_DB";

    private static final String VSAC_CLIENT = "VsacTerminologyClient";

    @Override
    public void execute(PreProcessPluginContext context) {
        log.debug(getClass().getSimpleName() + ": Processing input data.");
        Map<String, SupportingData> sdMap = context.getSupportingData();
        SupportingData sd = sdMap.get(ACCESS_DB_ID);
        log.debug("SD: " + sd);
        if (sd != null) {
            VsacTerminologyClient client = context.getCache().get(sd);
            if (client == null) {
                VsacLocalDataStoreReader reader = new VsacLocalDataStoreReader(getConnectionSupplier(sd));
                client = new VsacTerminologyClient(reader.getValueSetOidCaratVersionToVsacConceptSet());
                context.getCache().put(sd, client);
            }
            context.getGlobals().put(VSAC_CLIENT, client);
        }
    }

    private Supplier<Connection> getConnectionSupplier(SupportingData sd) {
        return sd == null ? () -> null : AccessConnection.supplier(sd.getSupportingDataPackage().getFile());
    }

}
