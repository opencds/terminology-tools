/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.plugin.terminology.vsac

import org.opencds.plugin.support.PreProcessPluginContextImpl
import org.opencds.plugin.api.PluginDataCache
import org.opencds.plugin.api.SupportingData
import org.opencds.plugin.api.SupportingDataPackage
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.terminology.vsac.tools.api.client.model.VsacConcept
import spock.lang.Specification

import java.sql.SQLException

class VsacTerminologyPreProcessPluginFunctionalSpec extends Specification {
    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_VSAC_OpioidCds.accdb'

    def 'test execute '() {
        given:
        VsacTerminologyPreProcessPlugin plugin = new VsacTerminologyPreProcessPlugin()
        Map allFactLists = [:]
        Map namedObjects = [:]
        Map globals = [:]
        Map supportingData = [:]
        var supplier = () -> SupportingDataPackage.create(
                () -> new File(DBFILENAME),
                () -> new File(DBFILENAME).text.bytes)
        PluginDataCache cache = new PluginDataCacheImpl();
        SupportingData sd = SupportingData.create('id', 'kmid', 'loadedby', DBFILENAME, 'access', new Date(), supplier)
        supportingData.put(VsacTerminologyPreProcessPlugin.ACCESS_DB_ID, sd)
        PreProcessPluginContextImpl context = PreProcessPluginContextImpl.create(
                supportingData as Map<String, SupportingData>,
                cache,
                allFactLists as Map<Class<?>, List<?>>,
                namedObjects as Map<String, Object>,
                globals as Map<String, Object>
        )

        when:
        plugin.execute(context)
        Map<String, Object> vsacNavGlobals = globals.get(VsacTerminologyPreProcessPlugin.VSAC_GLOBALS)
        def result = isConceptInValueSet(vsacNavGlobals, "2.16.840.1.113883.6.103", "196.2", "2.16.840.1.113762.1.4.1116.351", null)

        then:
        result
        println result
    }

    //    def 'old test'() {
    //        given:
    //        boolean periodicallyRefresh = true;
    //        int refreshFrequencyValue = 1;
    //        ChronoUnit refreshFrequencyCalendarTimeUnits = ChronoUnit.DAYS;
    //
    //        and:
    //        VsacTerminologyPreProcessPlugin reader = new VsacTerminologyPreProcessPlugin(DBFILENAME, periodicallyRefresh, refreshFrequencyValue, refreshFrequencyCalendarTimeUnits);
    //
    //        when:
    //        def result = reader.isConceptInValueSet("2.16.840.1.113883.6.103", "196.2", "2.16.840.1.113762.1.4.1116.351", null);
    //
    //        then:
    //        result
    //        println result
    //    }

    /**
     * Returns members of value set.  Returns null if not found.
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    private Set<VsacConcept> getConceptsInValueSet(Map<String, Object> globals, String valueSetOid, String valueSetVersion_mayBeNull) {
        return globals.myValueSetOidCaratVersionToVsacConceptSetMap.get(getValueSetIdString(valueSetOid, valueSetVersion_mayBeNull));
    }

    /**
     * Returns true if concept in value set; returns false otherwise.
     * @param codeSystemOid
     * @param code
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    private boolean isConceptInValueSet(Map<String, Object> globals, String codeSystemOid, String code, String valueSetOid, String valueSetVersion_mayBeNull) {
        Set<VsacConcept> conceptSet = globals.myValueSetOidCaratVersionToVsacConceptSetMap.get(getValueSetIdString(valueSetOid, valueSetVersion_mayBeNull));
        if (conceptSet != null) {
            // only OID and code used for equivalency testing
            if (conceptSet.contains(new VsacConcept(codeSystemOid, code)))  {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the single String version of the value set OID and optional
     * version.
     *
     * @param valueSetOid
     * @param valueSetVersion_mayBeNull
     * @return
     */
    private String getValueSetIdString(String valueSetOid, String valueSetVersion_mayBeNull) {
        String stringToReturn = valueSetOid + "^";
        if ((valueSetVersion_mayBeNull != null) && (!valueSetVersion_mayBeNull.equals(""))) {
            stringToReturn += valueSetVersion_mayBeNull;
        }
        return stringToReturn;
    }

}
