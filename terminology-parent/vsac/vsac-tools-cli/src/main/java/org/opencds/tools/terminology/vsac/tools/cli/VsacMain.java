/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.vsac.tools.cli;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.config.Config;
import org.opencds.tools.terminology.api.store.AccessConnection;
import org.opencds.tools.terminology.vsac.client.VsacFhirClient;
import org.opencds.tools.terminology.vsac.tools.store.VsacLocalDataStoreWriter;

public class VsacMain {
    private static final Log log = LogFactory.getLog(VsacMain.class);

    public static void main(String[] args) {
        Config config = new Config();
        VsacFhirClient client = new VsacFhirClient(config);
        VsacLocalDataStoreWriter writer = new VsacLocalDataStoreWriter(client);
        try {
            writer.updateVsacLocalDataStore(AccessConnection.supplier(new File(config.getVsacDatastore())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
    }

}
