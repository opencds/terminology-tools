/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.api.config

import org.opencds.tools.terminology.api.config.Config
import spock.lang.Specification

class ConfigFunctionalSpec extends Specification {

    def "test config loading"() {
        when:
        Config config = new Config()
        
        then:
        notThrown(Exception)
        println config.config
        println config.config.update.meds
        println "wipe? " + config.isRxNavWipeExistingContent()
        println "skip lookups? " + config.isSkipRxNavLookups()
        true
    }
    
}
