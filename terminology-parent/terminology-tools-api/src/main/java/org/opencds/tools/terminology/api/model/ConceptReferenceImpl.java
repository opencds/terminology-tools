package org.opencds.tools.terminology.api.model;

import org.opencds.tools.terminology.api.util.Utils;

import java.net.URI;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class ConceptReferenceImpl implements ConceptReference {

    private final CodeSystem codeSystem;

    private final String version;

    private final String code;

    private String preferredName; //Needed to relax this from final to support Phil's specializations.

    private final Set<ConceptDescription> descriptions;

    private static CodeSystem createCodeSystem(String system) {
        return Utils.isEmpty(system) ? null : new CodeSystemImpl(null, null, URI.create(system));
    }

    private static CodeSystem createCodeSystem(URI system) {
        return system == null ? null : new CodeSystemImpl(null, null, system);
    }

    /**
     * For deserialization.
     */
    @SuppressWarnings("unused")
    private ConceptReferenceImpl() {
        this((String) null, null);
    }

    public ConceptReferenceImpl(
            String system,
            String code) {
        this(system, code, null);
    }

    public ConceptReferenceImpl(
            URI system,
            String code) {
        this(system, code, null);
    }

    public ConceptReferenceImpl(
            CodeSystem system,
            String code) {
        this(system, code, null);
    }

    public ConceptReferenceImpl(
            String system,
            String code,
            String preferredName) {
        this(createCodeSystem(system), code, preferredName);
    }

    public ConceptReferenceImpl(
            URI system,
            String code,
            String preferredName) {
        this(createCodeSystem(system), code, preferredName);
    }

    public ConceptReferenceImpl(
            CodeSystem system,
            String code,
            String preferredName) {
        this(system, code, preferredName, null, null);
    }

    public ConceptReferenceImpl(
            CodeSystem system,
            String code,
            String preferredName,
            String version,
            Set<ConceptDescription> descriptions) {
        this.codeSystem = system;
        this.code = code;
        this.preferredName = preferredName;
        this.version = version;
        this.descriptions = descriptions == null ? Collections.emptySet() : descriptions;
    }

    @Override
    public CodeSystem getCodeSystem() {
        return codeSystem;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    @Override
    public Set<ConceptDescription> getConceptDescriptions() {
        return descriptions;
    }

    @Override
    public boolean equals(Object obj) {
        return areEqual(obj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodeSystem(), getCode());
    }

    @Override
    public String toString() {
        return "|" + getCodeSystemAsString() + "|" + getCodeAsString() + "|" + getPreferredName() + "|";
    }
}
