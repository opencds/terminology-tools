package org.opencds.tools.terminology.api.client;

import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;

import java.sql.Connection;
import java.util.Set;
import java.util.function.Supplier;

public interface TerminologyClient {

    boolean supports(ValueSetSource terminologySource);

    Set<ConceptReference> getConceptsInValueSet(String valueSetId, String valueSetVersion);

    boolean isConceptInValueSet(CodeSystem codeSystem, String code, String valueSetId, String version);

    Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version);

    void setConnectionSupplier(Supplier<Connection> connectionSupplier);
    
    // C translate(C sourceConcept, CodeSystem targetCodeSystem);
    //
    // C mapConcept(C fromConcept, List<Function<C, C>> mappingRules);

}
