/*
 * Copyright 2017-2018 OpenCDS.org
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.opencds.tools.terminology.api.constants;

import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.CodeSystemImpl;
import org.opencds.tools.terminology.api.model.Oid;

import java.net.URI;
import java.util.Set;

/**
 * Derived from VSAC: https://cts.nlm.nih.gov/fhir/
 */
public enum CodeSystemEnum implements CodeSystem {
    ActCode("ActCode", "http://hl7.org/fhir/v3/ActCode", "2.16.840.1.113883.5.4"),
    ActMood("ActMood", "http://hl7.org/fhir/v3/ActMood", "2.16.840.1.113883.5.1001"),
    ActPriority("ActPriority", "http://hl7.org/fhir/v3/ActPriority", "2.16.840.1.113883.5.7"),
    ActReason("ActReason", "http://hl7.org/fhir/v3/ActReason", "2.16.840.1.113883.5.8"),
    ActRelationshipType("ActRelationshipType", "http://hl7.org/fhir/v3/ActRelationshipType", "2.16.840.1.113883.5.1002"),
    ActStatus("ActStatus", "http://hl7.org/fhir/v3/ActStatus", "2.16.840.1.113883.5.14"),
    AddressUse("AddressUse", "http://hl7.org/fhir/v3/AddressUse", "2.16.840.1.113883.5.1119"),
    AdministrativeGender("AdministrativeGender", "http://hl7.org/fhir/v3/AdministrativeGender", "2.16.840.1.113883.5.1"),
    AdministrativeSex("AdministrativeSex", "http://hl7.org/fhir/v2/0001", "2.16.840.1.113883.18.2"),
    CDT("CDT", "http://www.nlm.nih.gov/research/umls/cdt", "2.16.840.1.113883.6.13"),
    CPT("CPT", "http://www.ama-assn.org/go/cpt", "2.16.840.1.113883.6.12"),
    CVX("CVX", "http://hl7.org/fhir/sid/cvx", "2.16.840.1.113883.12.292"),
    Confidentiality("Confidentiality", "http://hl7.org/fhir/v3/Confidentiality", "2.16.840.1.113883.5.25"),
    DischargeDisposition("DischargeDisposition", "http://hl7.org/fhir/v2/0112", "2.16.840.1.113883.12.112"),
    EntityNamePartQualifier("EntityNamePartQualifier", "http://hl7.org/fhir/v3/EntityNamePartQualifier", "2.16.840.1.113883.5.43"),
    EntityNameUse("EntityNameUse", "http://hl7.org/fhir/v3/EntityNameUse", "2.16.840.1.113883.5.45"),
    ICD10CM("ICD10CM", "http://hl7.org/fhir/sid/icd-10-cm", "2.16.840.1.113883.6.90"),
    ICD10PCS("ICD10PCS", "http://www.icd10data.com/icd10pcs", "2.16.840.1.113883.6.4"),
    // So far, this is the only code system that has two OIDS, per VSAC.
    ICD9CM("ICD9CM", "http://hl7.org/fhir/sid/icd-9-cm", "2.16.840.1.113883.6.103","2.16.840.1.113883.6.104"),
    LOINC("LOINC", "http://loinc.org", "2.16.840.1.113883.6.1"),
    LanguageAbilityMode("LanguageAbilityMode", "http://hl7.org/fhir/v3/LanguageAbilityMode", "2.16.840.1.113883.5.60"),
    LanguageAbilityProficiency("LanguageAbilityProficiency", "http://hl7.org/fhir/v3/LanguageAbilityProficiency", "2.16.840.1.113883.5.61"),
    LivingArrangement("LivingArrangement", "http://hl7.org/fhir/v3/LivingArrangement", "2.16.840.1.113883.5.63"),
    MaritalStatus("MaritalStatus", "http://hl7.org/fhir/v3/MaritalStatus", "2.16.840.1.113883.5.2"),
    MEDRT("MED-RT", "http://www.nlm.nih.gov/research/umls/MED-RT", "2.16.840.1.113883.6.345"),
    NCI("NCI", "http://ncimeta.nci.nih.gov", "2.16.840.1.113883.3.26.1.1"),
    NDFRT("NDFRT", "http://hl7.org/fhir/ndfrt", "2.16.840.1.113883.3.26.1.5"),
    NUCCPT("NUCCPT", "http://nucc.org/provider-taxonomy", "2.16.840.1.113883.6.101"),
    NullFlavor("NullFlavor", "http://hl7.org/fhir/v3/NullFlavor", "2.16.840.1.113883.5.1008"),
    ObservationInterpretation("ObservationInterpretation", "http://hl7.org/fhir/v3/ObservationInterpretation", "2.16.840.1.113883.5.83"),
    ObservationValue("ObservationValue", "http://hl7.org/fhir/v3/ObservationValue", "2.16.840.1.113883.5.1063"),
    ParticipationFunction("ParticipationFunction", "http://hl7.org/fhir/v3/ParticipationFunction", "2.16.840.1.113883.5.88"),
    ParticipationMode("ParticipationMode", "http://hl7.org/fhir/v3/ParticipationMode", "2.16.840.1.113883.5.1064"),
    ParticipationType("ParticipationType", "http://hl7.org/fhir/v3/ParticipationType", "2.16.840.1.113883.5.90"),
    RXNORM("RXNORM", "http://www.nlm.nih.gov/research/umls/rxnorm", "2.16.840.1.113883.6.88"),
    ReligiousAffiliation("ReligiousAffiliation", "http://hl7.org/fhir/v3/ReligiousAffiliation", "2.16.840.1.113883.5.1076"),
    RoleClass("RoleClass", "http://hl7.org/fhir/v3/RoleClass", "2.16.840.1.113883.5.110"),
    RoleCode("RoleCode", "http://hl7.org/fhir/v3/RoleCode", "2.16.840.1.113883.5.111"),
    RoleStatus("RoleStatus", "http://hl7.org/fhir/v3/RoleStatus", "2.16.840.1.113883.5.1068"),
    SNOMEDCT("SNOMEDCT", "http://snomed.info/sct", "2.16.840.1.113883.6.96"),
    SOP("SOP", "http://www.nlm.nih.gov/research/umls/sop", "2.16.840.1.113883.3.221.5"),
    UCUM("UCUM", "http://unitsofmeasure.org", "1.3.6.1.4.1.12009.10.3.1"),
    UMLS("UMLS", "http://www.nlm.nih.gov/research/umls", "2.16.840.1.113883.6.86"),
    UNII("UNII", "http://fdasis.nlm.nih.gov", "2.16.840.1.113883.4.9"),
    mediaType("mediaType", "http://hl7.org/fhir/v3/MediaType", "2.16.840.1.113883.5.79"),
    HCPCS("HCPCS", "http://www.oid-info.com/cgi-bin/display?oid=2.16.840.1.113883.6.285", "2.16.840.1.113883.6.285"), // verify
    HL7_V3("HL7V3", "http://hl7.org/fhir/v3/", "2.16.840.1.113883.1.9"), // verify
    UNKNOWN("UNKNOWN", "");

    private final CodeSystem codeSystem;

    public static CodeSystem byOid(String codeSystemOid) {
        return CodeSystem.byOid(codeSystemOid, values());
    }

    public static CodeSystem byUrn(String codeSystemUrn) {
        return CodeSystem.byUrn(codeSystemUrn, values());
    }

    CodeSystemEnum(
            String name,
            String urn,
            String... oids) {
        this.codeSystem = new CodeSystemImpl(name, null, URI.create(urn), oids);
    }

    @Override
    public String getName() {
        return codeSystem.getName();
    }

    @Override
    public URI getUrn() {
        return codeSystem.getUrn();
    }

    @Override
    public String getDescription() {
        return codeSystem.getDescription();
    }

    @Override
    public Set<Oid> getOids() {
        return codeSystem.getOids();
    }

    @Override
    public String toString() {
        return getUrnAsString();
    }
}
