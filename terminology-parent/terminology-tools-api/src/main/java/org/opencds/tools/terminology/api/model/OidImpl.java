package org.opencds.tools.terminology.api.model;

import org.opencds.tools.terminology.api.util.Utils;

import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Implementation of an OID with full validation.
 */
public class OidImpl implements Oid {

    /**
     * Converts a list of string values to a set of OIDs.  Insertion order is maintained.
     *
     * @param values String values to convert.
     * @return A set of OIDS.
     */
    public static Set<Oid> toOrderedSet(String... values) {
        return Utils.toOrderedSet(OidImpl::new, values);
    }

    private static final Pattern validator = Pattern.compile("^([0-2])((\\.0)|(\\.[1-9][0-9]*))*$");

    private final String oid;

    /**
     * For deserialization.
     */
    @SuppressWarnings("unused")
    private OidImpl() {
        oid = null;
    }

    /**
     * Create and OID from its string equivalent.
     *
     * @param oid The string value. The URN prefix is optional.
     */
    public OidImpl(String oid) {
        oid = oid.replaceAll("\\s", "");
        this.oid = oid.startsWith(URN_PREFIX) ? oid.substring(URN_PREFIX.length()) : oid;
        Utils.assertTrue(validator.matcher(this.oid).matches(), () -> "Not a valid OID: " + this.oid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return Objects.equals(oid, ((OidImpl) o).oid);
    }

    @Override
    public int hashCode() {
        return oid.hashCode();
    }

    /**
     * @return The string equivalent of the OID.
     */
    @Override
    public String toString() {
        return oid;
    }

}
