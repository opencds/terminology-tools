/*
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.api.store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class DataStoreReader {
    private static final Log log = LogFactory.getLog(DataStoreReader.class);

    protected <K, V> Map<K, V> updateGlobals(Map<K, V> cachedGlobals, Supplier<Connection> connectionSupplier,
            BiFunction<Map<K, V>, Supplier<Connection>, Map<K, V>> consumer) {
        if (cachedGlobals == null) {
            cachedGlobals = new HashMap<>();
            return consumer.apply(cachedGlobals, connectionSupplier);
        }
        return cachedGlobals;
    }

}
