package org.opencds.tools.terminology.api.client;

import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;

import java.sql.Connection;
import java.util.Collections;
import java.util.Set;
import java.util.function.Supplier;

public class NullTerminologyClient implements TerminologyClient {

    @Override
    public boolean supports(ValueSetSource terminologySource) {
        return false;
    }

    @Override
    public Set<ConceptReference> getConceptsInValueSet(String valueSetId, String valueSetVersion) {
        return Collections.emptySet();
    }

    @Override
    public boolean isConceptInValueSet(CodeSystem codeSystem, String code, String valueSetId, String version) {
        return false;
    }

    @Override
    public Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version) {
        return null;
    }

    @Override
    public void setConnectionSupplier(Supplier<Connection> connectionSupplier) {
        
    }

}
