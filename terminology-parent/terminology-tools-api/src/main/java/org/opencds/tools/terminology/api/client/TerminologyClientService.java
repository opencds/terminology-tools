package org.opencds.tools.terminology.api.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;

import java.sql.Connection;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class TerminologyClientService implements TerminologyClient {
    private static final Log log = LogFactory.getLog(TerminologyClientService.class);
    private static final TerminologyClient NULL_TERMINOLOGY_CLIENT = new NullTerminologyClient();
    
    private final ConcurrentMap<ValueSetSource, TerminologyClient> delegates;
    
    public TerminologyClientService() {
        this.delegates = new ConcurrentHashMap<>();
    }
    
    public TerminologyClientService registerDelegate(ValueSetSource source, TerminologyClient client) {
        delegates.put(source, client);
        return this;
    }

    @Override
    public Set<ConceptReference> getConceptsInValueSet(String valueSetId, String valueSetVersion) {
        return delegates.entrySet().parallelStream()
                .map(entry -> entry.getValue().getConceptsInValueSet(valueSetId, valueSetVersion))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isConceptInValueSet(CodeSystem codeSystem, String code, String valueSetId, String version) {
        Set<ValueSetSource> sources = ValueSetSource.resolve(codeSystem);
        return sources.parallelStream().anyMatch(s -> delegates.getOrDefault(s, NULL_TERMINOLOGY_CLIENT)
                .isConceptInValueSet(codeSystem, code, valueSetId, version));
    }
    
    @Override
    public Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version) {
        Set<ValueSetSource> sources = ValueSetSource.resolve(codeSystem);
        return sources.parallelStream()
                .map(s -> delegates.getOrDefault(s, NULL_TERMINOLOGY_CLIENT).getConcepts(codeSystem, code, version))
                .flatMap(Collection::stream).collect(Collectors.toSet());
    }
    
    @Override
    public boolean supports(ValueSetSource terminologySource) {
        return delegates.containsKey(terminologySource);
    }

    @Override
    public void setConnectionSupplier(Supplier<Connection> connectionSupplier) {
        log.warn("setConnectionSupplier not implemented.");
    }

}
