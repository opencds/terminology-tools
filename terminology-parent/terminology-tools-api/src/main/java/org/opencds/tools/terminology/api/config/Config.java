/*
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.api.config;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.util.Utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Config {
    public static final String RXNAV_API_URL = "rxnav.api.url";
    public static final String RXNAV_WIPE_EXISTING_CONTENT = "rxnav.wipe.existing.content";
    public static final String RXNAV_SKIP_LOOKUPS = "rxnav.skip.lookups";
    public static final String UMLS_API_KEY = "umls.api.key";
    public static final String UMLS_DATASTORE = "umls.datastore";
    public static final String VSAC_API_USERNAME = "vsac.api.username";
    public static final String VSAC_API_PASSWORD = "vsac.api.password";
    public static final String VSAC_DATASTORE = "vsac.datastore";
    private final PropertiesConfiguration props;

    /**
     * Uses {@link #findConfig()} to resolve the configuration.
     * <br/>
     * Attempts to find the config file (<tt>terminology-tools.properties</tt>) in one of two locations:
     * <ol>
     *     <li>in the current working directory</li>
     *     <li>in ${user.home}/.opencds/</li>
     * </ol>
     * <br/>
     * Use {@link #Config(String)} to specify an alternate location
     */
    public Config() {
        this(findConfig());
    }

    /**
     * Load the configuration specified at <tt>location</tt>.
     *
     * @param location the configuration file location
     */
    public Config(String location) {
        if (StringUtils.isBlank(location)) {
            throw new RuntimeException("location required");
        }
        props = new PropertiesConfiguration();
        try {
            props.read(new FileReader(location));
        } catch (IOException | ConfigurationException e) {
            throw Utils.toUnchecked(e);
        }
    }

    /**
     * Attempts to find the config file (<tt>terminology-tools.properties</tt>) in one of two locations:
     * <ol>
     *     <li>in the current working directory</li>
     *     <li>in ${user.home}/.opencds/</li>
     * </ol>
     *
     * @return the location, otherwise null
     */
    private static String findConfig() {
        Path configPath = Paths.get("terminology-tools.properties");
        if (configPath.toFile().exists()) {
            return configPath.toString();
        }
        configPath = Paths.get(System.getProperty("user.home") + File.separator + ".opencds" + File.separator + "terminology-tools.properties");
        if (configPath.toFile().exists()) {
            return configPath.toString();
        }
        return null;
    }

    public String getRxnavApiUrl() {
        return props.getString(RXNAV_API_URL);
    }

    public boolean isRxNavWipeExistingContent() {
        return props.getBoolean(RXNAV_WIPE_EXISTING_CONTENT);
    }

    public boolean isSkipRxNavLookups() {
        return props.getBoolean(RXNAV_SKIP_LOOKUPS);
    }

    public String getUmlsApiKey() {
        return props.getString(UMLS_API_KEY);
    }

    public String getUmlsDatastore() {
        return props.getString(UMLS_DATASTORE);
    }

    public String getRxNavDatastore() {
        return props.getString("rxnav.datastore");
    }

    public String getVsacApiUsername() {
        return props.getString(VSAC_API_USERNAME);
    }

    public String getVsacApiPassword() {
        return props.getString(VSAC_API_PASSWORD);
    }

    public String getVsacDatastore() {
        return props.getString(VSAC_DATASTORE);
    }
}
