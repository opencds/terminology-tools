package org.opencds.tools.terminology.api.model;

import org.opencds.tools.terminology.api.util.Utils;

import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * CodeSystem represents a terminology namespace such as the URI assigned
 * to an ontology. Typically code systems represents the namespace that ensure
 * the uniqueness of codes in the terminology.
 *
 */
public interface CodeSystem extends Serializable {

    /**
     * @return The name of the code system
     */
    String getName();

    /**
     * @return The description of this code system
     */
    String getDescription();

    /**
     * @return The URN of this code system
     */
    URI getUrn();

    /**
     * @return The OIDs associated with this code system
     */
    Set<Oid> getOids();

    /**
     * @return The Urn as a string value.
     */
    default String getUrnAsString() {
        return Objects.toString(getUrn());
    }

    /**
     * @return Returns the first OID in the set. Order is not guaranteed.
     */
    default Oid getFirstOid() {
        return getOids().stream().findFirst().orElse(null);
    }

    /**
     * @return Returns the sole OID or throws an error if set contains more than one element. Returns null if set
     *         contains no concept.
     */
    default Oid getSoleOid() {
        Utils.assertTrue(getOids().size() < 2, () ->
                "There is more than one OID in this set potentially resulting in a non-deterministic outcome");
        return getFirstOid();
    }

    /**
     * Returns true if this and the target are equal.  Each interface implementation's "equals" method should delegate
     * to this.  Note that the "equals" method of enums implementing this interface is set to final, so calling this
     * method should be the preferred way when enums are involved.
     *
     * @param target The object to compare.
     * @return True if equal.
     */
    default boolean areEqual(Object target) {
        if (this == target) return true;
        if (!(target instanceof CodeSystem)) return false;
        return Objects.equals(getUrn(), ((CodeSystem) target).getUrn()); // TODO: need to handle OIDs.
    }

    /**
     * Search for code system that contains the specified urn.
     *
     * @param urn         The urn to match.
     * @param codeSystems List of code systems to check.
     * @return The matching code system, or null if not found.
     */
    static CodeSystem byUrn(
            String urn,
            CodeSystem... codeSystems) {
        return Arrays.stream(codeSystems)
                .filter(cs -> (cs.getUrn() != null
                        && urn.equalsIgnoreCase(cs.getUrnAsString())))
                .findFirst().orElse(null);
    }

    /**
     * Search for code system that contains the specified urn.
     *
     * @param urn         The urn to match.
     * @param codeSystems List of code systems to check.
     * @return The matching code system, or null if not found.
     */
    static CodeSystem byUrn(
            URI urn,
            CodeSystem... codeSystems) {
        return Arrays.stream(codeSystems)
                .filter(cs -> urn.equals(cs.getUrn()))
                .findFirst().orElse(null);
    }

    /**
     * Search for code system that contains the specified OID.
     *
     * @param oid         The OID as a string value (with or without the OID prefix).
     * @param codeSystems List of code systems to check.
     * @return The matching code system, or null if not found.
     */
    static CodeSystem byOid(
            String oid,
            CodeSystem... codeSystems) {
        return Arrays.stream(codeSystems)
                .filter(cs -> cs.getOids().stream()
                        .anyMatch(value -> value.isEquivalent(oid))).findAny().orElse(null);
    }

    /**
     * Search for code system that contains the specified OID.
     *
     * @param oid         The OID to match.
     * @param codeSystems List of code systems to check.
     * @return The matching code system, or null if not found.
     */
    static CodeSystem byOid(
            Oid oid,
            CodeSystem... codeSystems) {
        return Arrays.stream(codeSystems)
                .filter(cs -> (cs.getOids().contains(oid)))
                .findFirst().orElse(null);
    }

    /**
     * Finds the code system with the specified URL or OID.  Performs a
     * case-insensitive search.
     *
     * @param value A URL or OID.
     * @return The corresponding code system, or null if not found.
     */
    static CodeSystem byUrnOrOid(
            String value,
            CodeSystem... codeSystems) {
        CodeSystem system = byUrn(value, codeSystems);
        return system != null ? system : byOid(value, codeSystems);
    }

}
