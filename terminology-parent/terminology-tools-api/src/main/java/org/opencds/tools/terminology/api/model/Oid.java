package org.opencds.tools.terminology.api.model;

import java.io.Serializable;

/**
 * Interface for representing an OID.
 * <p>
 * Note that the toString() method of an implementation must return the
 * string equivalent of the OID without the URN prefix.
 */
public interface Oid extends Serializable {

    String URN_PREFIX = "urn:oid:";

    /**
     * Returns the OID's string value with the URN prefix.
     *
     * @return The OID's string value with the URN prefix.
     */
    default String toURNString() {
        return URN_PREFIX + toString();
    }

    /**
     * Returns true if the specified string value matches this OID's string equivalent.
     *
     * @param value The string value (with or without the URN prefix).
     * @return True if equivalent.
     */
    default boolean isEquivalent(String value) {
        return (value.startsWith(URN_PREFIX) ? toURNString() : toString()).equals(value);
    }

}
