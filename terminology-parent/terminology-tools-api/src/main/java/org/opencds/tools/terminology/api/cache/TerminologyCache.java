package org.opencds.tools.terminology.api.cache;

public interface TerminologyCache<K, V> {
    V get(K key);
}
