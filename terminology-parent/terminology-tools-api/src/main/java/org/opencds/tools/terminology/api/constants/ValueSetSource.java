package org.opencds.tools.terminology.api.constants;

import org.opencds.tools.terminology.api.model.CodeSystem;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.opencds.tools.terminology.api.constants.CodeSystemEnum.*;

public enum ValueSetSource {
    RXNORM(CodeSystemEnum.RXNORM),
    UMLS(SNOMEDCT, CodeSystemEnum.RXNORM, LOINC, NCI, CPT, HCPCS, NDFRT, CVX, ICD9CM, ICD10CM, ICD10PCS),
    VSAC(SNOMEDCT, CodeSystemEnum.RXNORM, LOINC, UCUM, NCI, CPT, HCPCS, NDFRT, CVX, ICD9CM, ICD10CM),
    /**
     * CIMI
     */
    SOLOR
    ;

    private final Set<CodeSystem> supportedSources;

    ValueSetSource(CodeSystem... supportedTerminologies) {
        this.supportedSources = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(supportedTerminologies)));
    }

    public Set<CodeSystem> getSupportedSources() {
        return supportedSources;
    }

    public static Set<ValueSetSource> resolve(CodeSystem codeSystem) {
        return Arrays.stream(values()).filter(ts -> ts.supportedSources.contains(codeSystem))
                .collect(Collectors.toSet());
    }

}
