package org.opencds.tools.terminology.api.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class ApiCache<K, V> implements TerminologyCache<K, V> {
    private static final Log log = LogFactory.getLog(ApiCache.class);

    private final LoadingCache<K, V> cache;

    public ApiCache(Function<K, V> loader) {
        var cacheLoader = new CacheLoader<K, V>() {
            @Override
            public V load(K key) {
                log.debug("Cache Miss!!!");
                return loader.apply(key);
            }
        };
        cache = CacheBuilder.newBuilder()
                .expireAfterAccess(3, TimeUnit.HOURS)
                .build(cacheLoader);
    }

    @Override
    public V get(K key) {
        try {
            log.debug(key);
            return cache.get(key);
        } catch (ExecutionException e) {
            log.error("Error for key: " + key);
            log.error(e.getMessage(), e);
        }
        return null;
    }


}
