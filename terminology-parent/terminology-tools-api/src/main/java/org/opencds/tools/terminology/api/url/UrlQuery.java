package org.opencds.tools.terminology.api.url;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class UrlQuery {

    public static Map<String, String> query(Map.Entry<String, String>... params) {
        return Arrays.stream(params)
                .filter(m -> m.getKey() != null && m.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static Map.Entry<String, String> param(String key, String value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }

}
