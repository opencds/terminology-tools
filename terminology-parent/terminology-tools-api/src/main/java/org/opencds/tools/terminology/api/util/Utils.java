package org.opencds.tools.terminology.api.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Various convenience methods.
 */
public class Utils {
    private static final Log log = LogFactory.getLog(Utils.class);
    private static final String COMMA = ",";

    private Utils() {
    }

    /**
     * Returns true if the value is null or contains only whitespace characters.
     *
     * @param value The value.
     * @return True if the value is null or contains only whitespace characters.
     */
    public static boolean isEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    /**
     * Returns true if the collection is null or empty.
     *
     * @param collection The collection.
     * @return True if the collection is null or empty.
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Throws an IllegalArgument exception if the condition is not true.
     *
     * @param condition The condition.
     * @param message   The exception message.
     */
    public static void assertTrue(
            boolean condition,
            Supplier<String> message) {
        if (!condition) {
            throw new IllegalArgumentException(message.get());
        }
    }

    /**
     * Converts a checked exception to a RuntimeException.
     *
     * @param e The exception.
     * @return The unchecked exception.
     */
    public static RuntimeException toUnchecked(Exception e) {
        return e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e.getMessage(), e);
    }

    /**
     * Converts a list of string values to a set of objects, preserving insertion order.
     *
     * @param <T>     The type of target object.
     * @param factory The factory for producing the target objects.
     * @param values  A list of string values.
     * @return An ordered set of objects of the target type.
     */
    public static <T> Set<T> toOrderedSet(
            Function<String, T> factory,
            String... values) {
        return Arrays.stream(values)
                .map(factory)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static void requireNonEmptyString(String value, String message) {
        if (StringUtils.isBlank(value)) {
            throw new AssertionError(message);
        }
    }

    public static <T> void allowed(Set<T> allowed, T value, String message) {
        if (value != null && !allowed.contains(value)) {
            throw new AssertionError(
                    message + ": " +
                            allowed.stream()
                                    .sorted()
                                    .map(Objects::toString)
                                    .collect(Collectors.joining(", ")));
        }
    }

    private static <K, V> Predicate<Map.Entry<K, V>> logUnallowed(Set<K> allowed) {
        return e -> {
            if (!allowed.contains(e.getKey())) {
                log.warn("Unallowed value: " + e);
                return false;
            }
            return true;
        };
    }

    public static <K, V> Map<K, V> onlyAllowedKeys(Set<K> allowed, Map<K, V> map) {
        var allowedPredicate = Utils.<K, V>logUnallowed(allowed);
        return map == null ? Map.of() : map.entrySet().stream()
                .filter(allowedPredicate)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));
    }

    public static <T> void notAllowed(T allowed, T value, String message) {
        if (allowed == value || allowed.equals(value)) {
            throw new AssertionError(message);
        }
    }

    public static void requireFalse(Boolean preferred, String s) {
        if (preferred != null && preferred) {
            throw new AssertionError(s);
        }
    }

    public static void requireEmpty(String value, String message) {
        if (StringUtils.isNotBlank(value)) {
            throw new AssertionError(message);
        }
    }

    public static void requireNonEmptyList(List<?> value, String message) {
        if (value == null || value.isEmpty()) {
            throw new AssertionError(message);
        }
    }

    public static void requireNull(Object value, String message) {
        if (value != null) {
            throw new AssertionError(message);
        }
    }

    public static <T> void requireNonNull(T value, String message) {
        if (value == null) {
            throw new AssertionError(message);
        }
    }

    public static String booleanAsString(Object value) {
        if (value instanceof Boolean b) {
            return b.toString();
        }
        throw new IllegalArgumentException("value is not a boolean: " + value);
    }

    public static String listAsString(Object value) {
        if (value instanceof List<?> list) {
            return list.stream()
                    .map(Objects::toString)
                    .collect(Collectors.joining(COMMA));
        }
        throw new IllegalArgumentException("value is not a list: " + value);
    }

    public static <T> String value(Object value, Function<T, String> function) {
        return function.apply((T) value);
    }

    public static String string(Object value) {
        if (value instanceof String s) {
            return s;
        }
        throw new IllegalArgumentException("value is not a list: " + value);
    }

    public static String integerAsString(Object value) {
        if (value instanceof Integer i) {
            return i.toString();
        }
        throw new IllegalArgumentException("value is not a list: " + value);
    }

    public static void equals(String attributes, String poll, String message) {
        if (!StringUtils.equalsIgnoreCase(attributes, poll)) {
            throw new AssertionError(message);
        }
    }

    public static List<String> commaDelimToList(String value) {
        return Arrays.stream(value.split(COMMA))
                .map(String::trim)
                .toList();
    }

    public static boolean toBoolean(String s) {
        return Boolean.parseBoolean(s);
    }
}
