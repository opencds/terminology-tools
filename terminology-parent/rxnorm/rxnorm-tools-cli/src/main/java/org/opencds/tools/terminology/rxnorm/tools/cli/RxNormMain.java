/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.cli;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.config.Config;
import org.opencds.tools.terminology.api.store.AccessConnection;
import org.opencds.tools.terminology.rxclass.tools.client.RxClassClient;
import org.opencds.tools.terminology.rxnorm.tools.client.RxNormClient;
import org.opencds.tools.terminology.rxnorm.tools.store.RxNormLocalDataStoreWriter;

public class RxNormMain {
    private static final Log log = LogFactory.getLog(RxNormMain.class);

    public static void main(String[] args) {
        Config config = new Config();
        boolean wipeExistingContent = config.isRxNavWipeExistingContent();
        boolean skipRxNavLookups = config.isSkipRxNavLookups();

        RxNormLocalDataStoreWriter writer = new RxNormLocalDataStoreWriter(new RxClassClient(), new RxNormClient());

        try {
            writer.updateRxNavLocalDataStore(AccessConnection.supplier(new File(config.getRxNavDatastore())), wipeExistingContent, skipRxNavLookups);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
    }
}
