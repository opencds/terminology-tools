/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store

import org.apache.commons.lang3.function.TriConsumer
import org.opencds.tools.terminology.rxnorm.tools.client.RxNormClient

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.sql.Connection
import java.util.function.Supplier

import org.opencds.tools.db.api.DbConnection
import org.opencds.tools.terminology.api.store.AccessConnection

import spock.lang.Specification

class ValueSetFunctionalSpec extends Specification {
    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_RxNav_OpioidCds.accdb'
    
        File testFile
        String testFileName
    
        def setup() {
            testFile = File.createTempFile('test', 'accdb')
            Files.copy(new File(DBFILENAME).toPath(), testFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
            Thread.sleep 2000
            RxNormClient client = new RxNormClient()
        }
    
        def cleanup() {
            println testFile.absoluteFile
            testFile.delete()
        }

        def 'test updateEachValueSet'() {
            given:
            String dataStore = testFile.absolutePath
    
            and:
            Supplier<Connection> supplier = AccessConnection.supplier(new File(dataStore))
            DbConnection c1 = new DbConnection(supplier)
            DbConnection c2 = new DbConnection(supplier)
            
            when:
            ValueSet.forEachValueSetByExecutionOrder(c1, [accept: { id, sql, name ->
                println "ID:   ${id}"
                println "NAME: ${name}"
                println "SQL: ${sql}"
            }] as TriConsumer)
            
            then:
            true
        }    
}
