/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.rxnorm.tools.store

import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug
import org.opencds.tools.terminology.rxnorm.tools.client.RxNormClient

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.sql.Connection
import java.sql.Timestamp
import java.util.function.Consumer
import java.util.function.Supplier

import org.opencds.tools.db.api.DbConnection
import org.opencds.tools.db.api.Result
import org.opencds.tools.db.api.ResultSetIterable
import org.opencds.tools.terminology.api.store.AccessConnection

import spock.lang.Specification

class RxNormLocalDataStoreWriterFunctionalSpec extends Specification {
//    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_RxNav_OpioidCds.accdb'
    private static final String DBFILENAME = '/Users/phillip/tmp/LocalDataStore_RxNav_DMD.accdb'

    RxNormLocalDataStoreWriter writer

    File testFile
    String testFileName

    def setup() {
        testFile = File.createTempFile('test', 'accdb')
        Files.copy(new File(DBFILENAME).toPath(), testFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
        Thread.sleep 2000
        RxNormClient client = new RxNormClient()
        writer = new RxNormLocalDataStoreWriter(client)
    }

    def cleanup() {
        println testFile.absoluteFile
        testFile.delete()
    }

    def 'test updateRxNavLocalDataStore'() {
        given:
        String dataStore = testFile.absolutePath
        boolean wipeExistingContent = false
        boolean skipRxNavLookups = false
        Supplier<Connection> supplier = AccessConnection.supplier(new File(dataStore))

        when:
        writer.updateRxNavLocalDataStore(supplier, wipeExistingContent, skipRxNavLookups)

        and:
        DbConnection c = new DbConnection(supplier)
        String ts = c.executeTimestampQuery(TIMESTAMP_QUERY).toString()
        ts = ts.replaceFirst('(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}).*', '$1')
        Files.copy(testFile.toPath(), new File('/Users/phillip/tmp/terminology/test-rxnav.accdb').toPath(), StandardCopyOption.REPLACE_EXISTING)

        then:
        notThrown(Exception)
        ts
        println ts
    }

    def 'test updateRxNavLocalDataStore no skip lookups'() {
        given:
        String dataStore = testFile.absolutePath
        boolean wipeExistingContent = false
        boolean skipRxNavLookups = true
        Supplier<Connection> supplier = AccessConnection.supplier(new File(dataStore))

        when:
        writer.updateRxNavLocalDataStore(supplier, wipeExistingContent, skipRxNavLookups)

        and:
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))
        String ts = c.executeTimestampQuery(TIMESTAMP_QUERY).toString()
        ts = ts.replaceFirst('(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}).*', '$1')
        Files.copy(testFile.toPath(), new File('/Users/phillip/tmp/terminology/test-rxnav.accdb').toPath(), StandardCopyOption.REPLACE_EXISTING)

        then:
        notThrown(Exception)
        ts
        println ts
    }

    def 'test getDrugAndDoseForm'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        Map<String, Set<String>> medDrugToDoseFormGroupRxCuiSetMap = writer.getDrugAndDoseForm(c)

        then:
        medDrugToDoseFormGroupRxCuiSetMap
        println medDrugToDoseFormGroupRxCuiSetMap.containsKey('996484')
        println medDrugToDoseFormGroupRxCuiSetMap
    }

    def 'test getIngredientRxCUIs'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        Set<String> ingredientRxCuisForPopulatingDb = writer.getIngredientRxCUIs(c)
        ingredientRxCuisForPopulatingDb.each { String ingredientRxCuiForPopulatingDb ->
            Set<RxNormSemanticDrug> semanticDrugs = writer.rxNormClient.getSemanticDrugsWithIngredient(ingredientRxCuiForPopulatingDb)
            semanticDrugs.each { RxNormSemanticDrug sd ->
                println ingredientRxCuiForPopulatingDb
                if (sd.getRxcui() == '996484') {
                    println sd
                }
            }
        }

        then:
        ingredientRxCuisForPopulatingDb
        println ingredientRxCuisForPopulatingDb
    }

    def 'test population of medDrugRxCuiToSemanticDrugMap'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))
        Set<String> ingredientRxCuisForPopulatingDb = writer.getIngredientRxCUIs(c)
        Map<String, RxNormSemanticDrug> medDrugRxCuiToSemanticDrugMap = new HashMap<>()
        Set<String> doseFormRxCuiSet = writer.getDoseFormRxCUI(c)
        Set<String> medDrugRxCuis = writer.getMedDrugRxCuis(c)
        Map<String, Set<String>> ingredientToMedDrugRxCuiSetMap = writer.getMedDrugWithIngredients(c)

        when:
        writer.populateMedDrugWithSCDAndSBD(c, ingredientRxCuisForPopulatingDb, medDrugRxCuiToSemanticDrugMap,
                doseFormRxCuiSet, medDrugRxCuis, ingredientToMedDrugRxCuiSetMap)


        then:
        println medDrugRxCuiToSemanticDrugMap.get('996484')
        medDrugRxCuiToSemanticDrugMap.get('996484')
    }

    def 'test getMedDrugRxCuis'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        Set<String> medDrugRxCuis = writer.getMedDrugRxCuis(c)

        then:
        medDrugRxCuis
        println medDrugRxCuis.contains('996484')
    }

    def 'test getTimestampQuery'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        Timestamp ts = c.executeTimestampQuery(TIMESTAMP_QUERY)

        then:
        ts
        ts == Timestamp.valueOf('2017-12-07 15:37:41.0')
        println ts
        c.close()
        notThrown(Exception)
    }

    def 'test updateDbStatus'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))
        String status
        c.executeQuery("SELECT STATUS FROM DATABASE_STATUS", [accept: { ResultSetIterable rsIter ->
            Iterator<Result> r = rsIter.iterator()
            status = r.hasNext() ? r.next().getString(1) : null
        }] as Consumer)
        System.err.println status

        when:
        writer.updateDbStatus(c)

        and:
        Timestamp ts = c.executeTimestampQuery(TIMESTAMP_QUERY)

        then:
        ts == Timestamp.valueOf('2017-12-05 10:34:33.793')
        String status2
        c.executeQuery("SELECT STATUS FROM DATABASE_STATUS", [accept: { ResultSetIterable rsIter ->
            Iterator<Result> r = rsIter.iterator()
            status2 = r.hasNext() ? r.next().getString(1) : null
        }] as Consumer)
        System.err.println status2
        c.close()
        notThrown(Exception)
    }

    def 'test updateValueSets'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection conn1 = new DbConnection(AccessConnection.supplier(new File(dataStore)))
        DbConnection conn2 = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        writer.updateValueSets(conn1, conn2)

        then:
        conn1.close()
        conn2.close()
        notThrown(Exception)
    }


    def 'test updateValueSetTimestamp'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))
        Timestamp timestamp = new Timestamp(new Date().getTime())

        when:
        writer.updateValueSetTimestamp(c, timestamp, 1)

        and:
        Timestamp ts = c.executeTimestampQuery(TIMESTAMP_QUERY)

        then:
        ts == Timestamp.valueOf('2017-12-05 10:34:33.793')
        notThrown(Exception)
    }

    def 'test wipeExistingContent'() {
        given:
        String dataStore = testFile.absolutePath
        DbConnection c = new DbConnection(AccessConnection.supplier(new File(dataStore)))

        when:
        writer.wipeExistingContent(c)

        then:
        notThrown(Exception)
    }


    String TIMESTAMP_QUERY = '''
SELECT MAX(A.MAX_UPDATE_DTM) 
 FROM ( 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
    FROM MED_CLASS_TO_POPULATE_DB
   UNION
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_DOSE_FORM
   UNION
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_DOSE_FORM_GROUP   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_DRUG   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_DRUG_DOSE_FORM_GROUP   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_DRUG_WITH_INGREDIENT   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_INGREDIENT  
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_INGREDIENT_IN_CLASS   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_INGREDIENT_TYPE   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_SCDC   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM MED_SCDC_FOR_DRUG   
   UNION 
   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM 
     FROM VALUE_SET )  AS A;
'''
}
