/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.Set;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.PreparedStatementFacade;

class MedDrugValueSet {
    private static final Log log = LogFactory.getLog(MedDrugValueSet.class);

    private static final String DELETE_MED_DRUG_VALUE_SET = "DELETE FROM MED_DRUG_VALUE_SET";
    private static final String DELETE_MED_DRUG_VALUE_SET_WHERE_VALUE_SET_ID = "DELETE FROM MED_DRUG_VALUE_SET WHERE VALUE_SET_ID = ?";
    private static final String INSERT_MED_DRUG_VALUE_SET = "INSERT INTO MED_DRUG_VALUE_SET(VALUE_SET_ID, DRUG_RXCUI) VALUES (?, ?)";
    private static final String MED_DRUG_VALUE_SET_SELECT_DRUG_RXCUI_WHERE_VALUE_SET_ID = "SELECT DRUG_RXCUI FROM MED_DRUG_VALUE_SET WHERE VALUE_SET_ID = ?";
    private static final String DRUG_RXCUI_QUERY = "SELECT DRUG_RXCUI FROM MED_DRUG_VALUE_SET WHERE VALUE_SET_ID = ?";

    private static final String COL_DRUG_RXCUI = "DRUG_RXCUI";

    static void updateValueSetWithoutCommit(DbConnection conn, int valueSetId,
            String valueSetName, Set<String> drugRxCuis) {
        drugRxCuis.stream().forEach(drugRxCui -> {
            conn.executeUpdateNoCommit(INSERT_MED_DRUG_VALUE_SET, ps -> {
                ps.setInt(1, valueSetId);
                ps.setString(2, drugRxCui);
            });
        });
    }

    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DRUG_VALUE_SET);
    }

    static void deleteWithoutCommit(DbConnection conn, int valueSetId) {
        conn.executeUpdateNoCommit(DELETE_MED_DRUG_VALUE_SET_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, valueSetId);
        });
    }

    static void getExistingDrugsForValueSet(DbConnection conn, int valueSetId, Consumer<String> resultConsumer) {
        conn.executePreparedStatement(MED_DRUG_VALUE_SET_SELECT_DRUG_RXCUI_WHERE_VALUE_SET_ID, ps -> {
            ps.setInt(1, valueSetId);
        }, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_DRUG_RXCUI));
            });
        });
    }

    static void forEachDrugRxCui(DbConnection conn2, Integer valueSetId, Consumer<String> resultConsumer) {
        conn2.executePreparedStatement(DRUG_RXCUI_QUERY, (PreparedStatementFacade ps) -> {
            ps.setInt(1, valueSetId);
        }, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_DRUG_RXCUI));
            });
        });
    }

}
