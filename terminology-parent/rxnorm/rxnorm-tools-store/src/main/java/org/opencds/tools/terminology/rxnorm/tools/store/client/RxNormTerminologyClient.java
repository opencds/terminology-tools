/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.rxnorm.tools.store.client;

import org.opencds.tools.terminology.api.client.TerminologyClient;
import org.opencds.tools.terminology.api.constants.ValueSetSource;
import org.opencds.tools.terminology.api.model.CodeSystem;
import org.opencds.tools.terminology.api.model.ConceptReference;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug;
import org.opencds.tools.terminology.rxnorm.tools.store.RxNormLocalDataStoreReader;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class RxNormTerminologyClient implements TerminologyClient {
    private Supplier<Connection> connectionSupplier;
    private Map<String, RxNormSemanticDrug> rxCuiToSemanticDrug;
    private Map<String, Set<RxNormConcept>> valueSetIdToRxNormConceptSet;
    private Map<String, Set<RxNormSemanticDrug>> valueSetIdToSemanticDrugSet;

    public RxNormTerminologyClient() {}

    public RxNormTerminologyClient(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    public RxNormTerminologyClient(Map<String, RxNormSemanticDrug> rxCuiToSemanticDrug,
                                   Map<String, Set<RxNormConcept>> valueSetIdToRxNormConceptSet,
                                   Map<String, Set<RxNormSemanticDrug>> valueSetIdToSemanticDrugSet) {
        this.rxCuiToSemanticDrug = rxCuiToSemanticDrug;
        this.valueSetIdToRxNormConceptSet = valueSetIdToRxNormConceptSet;
        this.valueSetIdToSemanticDrugSet = valueSetIdToSemanticDrugSet;
    }

    /**
     * Also refreshes the data.
     */
    @Override
    public void setConnectionSupplier(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }

    public void refresh() {
        if (connectionSupplier == null) {
            throw new RuntimeException("connection is null");
        }
        RxNormLocalDataStoreReader reader = new RxNormLocalDataStoreReader(connectionSupplier);
        this.rxCuiToSemanticDrug = reader.getRxCuiToSemanticDrug();
        this.valueSetIdToRxNormConceptSet = reader.getValueSetIdToRxNormConceptSet();
        this.valueSetIdToSemanticDrugSet = reader.getValueSetIdToSemanticDrugSet();
    }

    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Deprecated
    public Set<RxNormConcept> getDrugsInValueSet(int valueSetId) {
        return Collections.unmodifiableSet(valueSetIdToSemanticDrugSet.get(Integer.toString(valueSetId)));
    }

    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Deprecated
    public Set<RxNormConcept> getRxNormConceptsOfDrugsInValueSet(int valueSetId) {
        return Collections.unmodifiableSet(valueSetIdToRxNormConceptSet.get(Integer.toString(valueSetId)));
    }

    /**
     * Returns true if drug in value set; returns false otherwise.
     *
     * @param semanticDrugRxCui
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Deprecated
    public boolean isDrugInValueSet(String semanticDrugRxCui, int valueSetId) {
        Set<RxNormConcept> valueSet = valueSetIdToRxNormConceptSet.get(Integer.toString(valueSetId));
        // only RxCui used for equivalency testing
        if (valueSet != null) {
            return valueSet.stream().anyMatch(c -> c.getCode().equals(semanticDrugRxCui));
        }
        return false;
    }

    /**
     * Returns null if drug cannot be found.
     *
     * @param semanticDrugRxCui
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    @Deprecated
    public RxNormSemanticDrug getDrug(String semanticDrugRxCui) {
        return rxCuiToSemanticDrug.get(semanticDrugRxCui);
    }

    @Override
    public Set<ConceptReference> getConcepts(CodeSystem codeSystem, String code, String version) {
        return Collections.singleton(rxCuiToSemanticDrug.get(code));
    }

    @Override
    public boolean supports(ValueSetSource terminologySource) {
        return ValueSetSource.RXNORM == terminologySource;
    }

    @Override
    public Set<ConceptReference> getConceptsInValueSet(String valueSetId, String version) {
        return Collections.unmodifiableSet(valueSetIdToSemanticDrugSet.get(valueSetId));
    }

    @Override
    public boolean isConceptInValueSet(CodeSystem codeSystem, String code, String valueSetId, String version) {
        Set<RxNormConcept> valueSet = valueSetIdToRxNormConceptSet.get(valueSetId);
        // only RxCui used for equivalency testing
        if (valueSet != null) {
            return valueSet.stream().anyMatch(c -> c.getCode().equals(code));
        }
        return false;
    }

}
