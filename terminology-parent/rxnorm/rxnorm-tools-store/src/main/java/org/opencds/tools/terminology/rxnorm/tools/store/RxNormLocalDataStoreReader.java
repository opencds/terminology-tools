/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrugComponent;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug;

public class RxNormLocalDataStoreReader {
    private static final Log log = LogFactory.getLog(RxNormLocalDataStoreReader.class);

    private Supplier<Connection> connectionSupplier;
    
    private Map<String, RxNormSemanticDrug> rxCuiToSemanticDrug;
    private Map<String, Set<RxNormConcept>> valueSetIdToRxNormConceptSet;
    private Map<String, Set<RxNormSemanticDrug>> valueSetIdToSemanticDrugSet;

    public RxNormLocalDataStoreReader(Supplier<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        refresh();
    }
    
    public RxNormLocalDataStoreReader(Map<String, RxNormSemanticDrug> rxCuiToSemanticDrug,
                                      Map<String, Set<RxNormConcept>> valueSetIdToRxNormConceptSet,
                                      Map<String, Set<RxNormSemanticDrug>> valueSetIdToSemanticDrugSet) {
        this.rxCuiToSemanticDrug = rxCuiToSemanticDrug;
        this.valueSetIdToRxNormConceptSet = valueSetIdToRxNormConceptSet;
        this.valueSetIdToSemanticDrugSet = valueSetIdToSemanticDrugSet;
    }
    
    public Map<String, RxNormSemanticDrug> getRxCuiToSemanticDrug() {
        return rxCuiToSemanticDrug;
    }

    public Map<String, Set<RxNormConcept>> getValueSetIdToRxNormConceptSet() {
        return valueSetIdToRxNormConceptSet;
    }

    public Map<String, Set<RxNormSemanticDrug>> getValueSetIdToSemanticDrugSet() {
        return valueSetIdToSemanticDrugSet;
    }

    public void refresh() {
        DbConnection conn1 = new DbConnection(connectionSupplier);
        DbConnection conn2 = new DbConnection(connectionSupplier);
        // shared across value sets

        // ------------------------------------------------------------
        // Collect contents for the cross-value set of Semantic Drugs
        // ------------------------------------------------------------

        // MED_DRUG + MED_DOSE_FORM
        Map<String, RxNormSemanticDrug> myRxCuiToSemanticDrug = new HashMap<>();
        MedDrug.getRxCuiToSemanticDrug(conn1, (drugRxCui, drug) -> {
            myRxCuiToSemanticDrug.put(drugRxCui, drug);
        });

        // MED_DRUG_DOSE_FORM_GROUP + MED_DOSE_FORM_GROUP
        MedDrugDoseFormGroup.forEachDoseFormGroup(conn1, (drugRxCui, rxNormConceptSupplier) -> {
            RxNormSemanticDrug drug = myRxCuiToSemanticDrug.get(drugRxCui);
            if (drug == null) {
                log.warn("RxCUI '" + drugRxCui + "' missing from semantic drug map");
            } else {
                drug.addDoseFormGroupConcept(rxNormConceptSupplier.get());
            }
        });

        // MED_SCDC + MED_SCDC_FOR_DRUG + MED_INGREDIENT
        Map<String, RxNormSemanticDrugComponent> scdcRxCuiToScdcMap = new HashMap<>();
        
        MedScdc.forEachSCD(conn1, (scdcRxCui, scdc) -> {
            scdcRxCuiToScdcMap.put(scdcRxCui, scdc);
        });

        MedScdcForDrug.forEachDrug(conn1, (drugRxCui, scdcRxCui) -> {
            RxNormSemanticDrugComponent scdc = scdcRxCuiToScdcMap.get(scdcRxCui);
            if (scdc == null) {
                log.warn("RxCUI '" + scdcRxCui + "' missing from SCDC RxCUI to SCDC map");
            } else {
                RxNormSemanticDrug drug = myRxCuiToSemanticDrug.get(drugRxCui);
                if (drug == null) {
                    log.warn("RxCUI '" + drugRxCui + "' missing from semantic drug map");
                } else {
                    drug.addSemanticClinicalDrugComponent(scdc);
                }
            }
        });

        // ------------------------------------------------------------
        // Collect contents for the value sets
        // ------------------------------------------------------------

        Map<String, Set<RxNormConcept>> myValueSetIdToRxNormConceptSet = new HashMap<>();
        Map<String, Set<RxNormSemanticDrug>> myValueSetIdToSemanticDrugSet = new HashMap<>();

        ValueSet.forEachValueSet(conn1, (valueSetId) -> {
            Set<RxNormSemanticDrug> drugSetForValueSet = new HashSet<>();
            Set<RxNormConcept> drugRxNormConceptSetForValueSet = new HashSet<>();
            
            MedDrugValueSet.forEachDrugRxCui(conn2, valueSetId, drugRxCui -> {
                RxNormSemanticDrug drug = myRxCuiToSemanticDrug.get(drugRxCui);
                if (drug == null) {
                    log.warn("RxCUI '" + drugRxCui + "' missing from semantic drug map");
                } else {
                    drugSetForValueSet.add(drug);
                    drugRxNormConceptSetForValueSet.add(drug);
                }
            });

            myValueSetIdToSemanticDrugSet.put(Integer.toString(valueSetId), drugSetForValueSet);
            myValueSetIdToRxNormConceptSet.put(Integer.toString(valueSetId), drugRxNormConceptSetForValueSet);
        });

        
        rxCuiToSemanticDrug = myRxCuiToSemanticDrug;
        valueSetIdToRxNormConceptSet = myValueSetIdToRxNormConceptSet;
        valueSetIdToSemanticDrugSet = myValueSetIdToSemanticDrugSet;
    }

}
