/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;

class MedDrugDoseFormGroup {
    private static final Log log = LogFactory.getLog(MedDrugDoseFormGroup.class);

    private static final String INSERT_MED_DRUG_DOSE_FORM_GROUP = "INSERT INTO MED_DRUG_DOSE_FORM_GROUP(DRUG_RXCUI, DOSE_FORM_GROUP_RXCUI) VALUES (?, ?)";
    private static final String DELETE_MED_DRUG_DOSE_FORM_GROUP = "DELETE FROM MED_DRUG_DOSE_FORM_GROUP";
    private static final String MED_DRUG_DOSE_FORM_GROUP_SELECT_DISTINCT_DRUG_RXCUI_DOSE_FORM_GROUP_RXCUI = "SELECT DISTINCT DRUG_RXCUI, DOSE_FORM_GROUP_RXCUI FROM MED_DRUG_DOSE_FORM_GROUP";
    private static final String MED_DRUG_DOSE_FORM_GROUP_MED_DOSE_FORM_GROUP_QUERY = "SELECT DRUG_DFG.DRUG_RXCUI, DRUG_DFG.DOSE_FORM_GROUP_RXCUI, DFG.DOSE_FORM_GROUP_NAME "
            + " FROM MED_DRUG_DOSE_FORM_GROUP DRUG_DFG INNER JOIN MED_DOSE_FORM_GROUP DFG ON DRUG_DFG.DOSE_FORM_GROUP_RXCUI = DFG.DOSE_FORM_GROUP_RXCUI ";

    private static final String COL_DOSE_FORM_GROUP_RXCUI = "DOSE_FORM_GROUP_RXCUI";
    private static final String COL_DRUG_RXCUI = "DRUG_RXCUI";

    static void insertWithoutCommit(DbConnection conn, String medDrugRxCui, String doseFormGroupRxCui) {
        conn.executeUpdateNoCommit(INSERT_MED_DRUG_DOSE_FORM_GROUP, ps -> {
            ps.setString(1, medDrugRxCui);
            ps.setString(2, doseFormGroupRxCui);
        });
    }
    
    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DRUG_DOSE_FORM_GROUP);
    }

    static void getDrugAndDoseForm(DbConnection conn, BiConsumer<String, String> resultConsumer) {
        conn.executeQuery(MED_DRUG_DOSE_FORM_GROUP_SELECT_DISTINCT_DRUG_RXCUI_DOSE_FORM_GROUP_RXCUI, rsIter -> {
           rsIter.forEach(r -> {
               String drugRxCui = r.getString(COL_DRUG_RXCUI);
               String doseFormGroupRxCui = r.getString(COL_DOSE_FORM_GROUP_RXCUI);
               resultConsumer.accept(drugRxCui, doseFormGroupRxCui);
           }); 
        });
    }

    static void forEachDoseFormGroup(DbConnection connection, BiConsumer<String, Supplier<RxNormConcept>> resultConsumer) {
        connection.executeQuery(MED_DRUG_DOSE_FORM_GROUP_MED_DOSE_FORM_GROUP_QUERY, rsIterable -> {
            rsIterable.forEach(r -> {
                String drugRxCui = r.getString(COL_DRUG_RXCUI);
                String doseFormGroupRxCui = r.getString(COL_DOSE_FORM_GROUP_RXCUI);
                String doseFormGroupName = r.getString(MedDoseFormGroup.COL_DOSE_FORM_GROUP_NAME);
                resultConsumer.accept(drugRxCui, () -> new RxNormConcept(doseFormGroupRxCui, doseFormGroupName));
            });
        });
    }

    
}
