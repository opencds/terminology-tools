/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;

class MedDoseForm {
    private static final Log log = LogFactory.getLog(MedDoseForm.class);

    private static final String INSERT_MED_DOSE_FORM = "INSERT INTO MED_DOSE_FORM(DOSE_FORM_RXCUI, DOSE_FORM_NAME) VALUES (?, ?)";
    private static final String DELETE_MED_DOSE_FORM = "DELETE FROM MED_DOSE_FORM";
    private static final String MED_DOSE_FORM_SELECT_DISTINCT_DOSE_FORM_RXCUI = "SELECT DISTINCT DOSE_FORM_RXCUI FROM MED_DOSE_FORM";
    
    static final String COL_DOSE_FORM_NAME = "DOSE_FORM_NAME";
    static final String COL_DOSE_FORM_RXCUI = "DOSE_FORM_RXCUI";

    static void insertWithoutCommit(DbConnection conn, String doseFormRxCui, String doseFormName) {
        conn.executeUpdateNoCommit(INSERT_MED_DOSE_FORM, ps -> {
            ps.setString(1,  doseFormRxCui);
            ps.setString(2, doseFormName);
        });
    }
    
    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DOSE_FORM);
    }

    static void getDoseFormRxCUI(DbConnection conn, Consumer<String> resultConsumer) {
        conn.executeQuery(MED_DOSE_FORM_SELECT_DISTINCT_DOSE_FORM_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_DOSE_FORM_RXCUI));
            });
        });
    }


}
