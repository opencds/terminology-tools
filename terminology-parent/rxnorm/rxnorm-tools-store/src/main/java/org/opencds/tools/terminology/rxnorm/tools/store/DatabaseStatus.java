/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.db.api.Result;

class DatabaseStatus {
    private static final Log log = LogFactory.getLog(DatabaseStatus.class);

    private static final String DB_STATUS_COUNT = "SELECT COUNT(*) FROM DATABASE_STATUS";
    private static final String DB_STATUS_SELECT_ALL = "SELECT * FROM DATABASE_STATUS ORDER BY LAST_UPDATE_DTM DESC, STATUS DESC";
    private static final String DB_STATUS_DELETE = "DELETE FROM DATABASE_STATUS";
    private static final String DB_STATUS_INSERT = "INSERT INTO DATABASE_STATUS(STATUS, LAST_UPDATE_DTM, LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB, LAST_UPDATE_DTM_MED_DOSE_FORM, LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP, LAST_UPDATE_DTM_MED_DRUG, LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP, LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT, LAST_UPDATE_DTM_MED_INGREDIENT, LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS, LAST_UPDATE_DTM_MED_INGREDIENT_TYPE,  LAST_UPDATE_DTM_MED_SCDC, LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG, LAST_UPDATE_DTM_VALUE_SET) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String DB_STATUS_UPDATE = "UPDATE DATABASE_STATUS SET STATUS = ?";
    private static final String DB_STATUS_UPDATING = "INSERT INTO DATABASE_STATUS(STATUS) VALUES ('UPDATING')";
    private static final String UPDATE_DB_STATUS_TS = "UPDATE DATABASE_STATUS SET STATUS = ?, LAST_UPDATE_DTM = ?, LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB = ?, "
    + "LAST_UPDATE_DTM_MED_DOSE_FORM = ?, LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP = ?, LAST_UPDATE_DTM_MED_DRUG = ?, "
    + "LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP = ?, LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT = ?, LAST_UPDATE_DTM_MED_INGREDIENT = ?, "
    + "LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS = ?, LAST_UPDATE_DTM_MED_INGREDIENT_TYPE = ?, LAST_UPDATE_DTM_MED_SCDC = ?, "
    + "LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG = ?, LAST_UPDATE_DTM_VALUE_SET = ?";
    private static final String SELECT_LATEST_TIMESTAMP = "SELECT MAX(A.MAX_UPDATE_DTM) " + " FROM ( "
    + "   SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_CLASS_TO_POPULATE_DB UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_DOSE_FORM UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_DOSE_FORM_GROUP UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_DRUG UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_DRUG_DOSE_FORM_GROUP UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_DRUG_WITH_INGREDIENT UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_INGREDIENT UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_INGREDIENT_IN_CLASS UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_INGREDIENT_TYPE UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_SCDC UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM "
    + "   FROM MED_SCDC_FOR_DRUG UNION SELECT MAX(UPDATE_DTM) AS MAX_UPDATE_DTM FROM VALUE_SET) AS A;";
    private static final String TS_MED_CLASS_TO_POPULATE_DB = "SELECT MAX(UPDATE_DTM) FROM MED_CLASS_TO_POPULATE_DB";
    private static final String TS_MED_DOSE_FORM = "SELECT MAX(UPDATE_DTM) FROM MED_DOSE_FORM";
    private static final String TS_MED_DOSE_FORM_GROUP = "SELECT MAX(UPDATE_DTM) FROM MED_DOSE_FORM_GROUP";
    private static final String TS_MED_DRUG = "SELECT MAX(UPDATE_DTM) FROM MED_DRUG";
    private static final String TS_MED_DRUG_DOSE_FORM_GROUP = "SELECT MAX(UPDATE_DTM) FROM MED_DRUG_DOSE_FORM_GROUP";
    private static final String TS_MED_DRUG_WITH_INGREDIENT = "SELECT MAX(UPDATE_DTM) FROM MED_DRUG_WITH_INGREDIENT";
    private static final String TS_MED_INGREDIENT = "SELECT MAX(UPDATE_DTM) FROM MED_INGREDIENT";
    private static final String TS_MED_INGREDIENT_IN_CLASS = "SELECT MAX(UPDATE_DTM) FROM MED_INGREDIENT_IN_CLASS";
    private static final String TS_MED_INGREDIENT_TYPE = "SELECT MAX(UPDATE_DTM) FROM MED_INGREDIENT_TYPE";
    private static final String TS_MED_SCDC = "SELECT MAX(UPDATE_DTM) FROM MED_SCDC";
    private static final String TS_MED_SCDC_FOR_DRUG = "SELECT MAX(UPDATE_DTM) FROM MED_SCDC_FOR_DRUG";
    private static final String TS_VALUE_SET = "SELECT MAX(UPDATE_DTM) FROM VALUE_SET";

    private static final String COL_LAST_UPDATE_DTM = "LAST_UPDATE_DTM";
    private static final String COL_LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB = "LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB";
    private static final String COL_LAST_UPDATE_DTM_MED_DOSE_FORM = "LAST_UPDATE_DTM_MED_DOSE_FORM";
    private static final String COL_LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP = "LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP";
    private static final String COL_LAST_UPDATE_DTM_MED_DRUG = "LAST_UPDATE_DTM_MED_DRUG";
    private static final String COL_LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP = "LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP";
    private static final String COL_LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT = "LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT";
    private static final String COL_LAST_UPDATE_DTM_MED_INGREDIENT = "LAST_UPDATE_DTM_MED_INGREDIENT";
    private static final String COL_LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS = "LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS";
    private static final String COL_LAST_UPDATE_DTM_MED_INGREDIENT_TYPE = "LAST_UPDATE_DTM_MED_INGREDIENT_TYPE";
    private static final String COL_LAST_UPDATE_DTM_MED_SCDC = "LAST_UPDATE_DTM_MED_SCDC";
    private static final String COL_LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG = "LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG";
    private static final String COL_LAST_UPDATE_DTM_VALUE_SET = "LAST_UPDATE_DTM_VALUE_SET";
    private static final String COL_STATUS = "STATUS";
    
    private static final String CONST_READY_TO_READ = "READY_TO_READ";
    private static final String CONST_UPDATING = "UPDATING";

    static void ensureSingleStatusEntry(DbConnection conn1) {
        int count = conn1.executeCountQuery(DB_STATUS_COUNT);
        if (count == 0) {
            conn1.executeUpdate(DB_STATUS_UPDATING);
        } else if (count > 1) {
            log.info("> Found more than one DATABASE_STATUS entry; taking latest and removing rest.");
            StringBuilder status = new StringBuilder();
            Map<String, Timestamp> tsMap = new HashMap<>();
            conn1.executeQuery(DB_STATUS_SELECT_ALL, rsIter -> {
                Result rs = rsIter.iterator().next();
                status.append(rs.getString(COL_STATUS));
                tsMap.put(COL_LAST_UPDATE_DTM, rs.getTimestamp(COL_LAST_UPDATE_DTM));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_DOSE_FORM, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_DOSE_FORM));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_DRUG, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_DRUG));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_INGREDIENT, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_INGREDIENT));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_INGREDIENT_TYPE, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_INGREDIENT_TYPE));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_SCDC, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_SCDC));
                tsMap.put(COL_LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG, rs.getTimestamp(COL_LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG));
                tsMap.put(COL_LAST_UPDATE_DTM_VALUE_SET, rs.getTimestamp(COL_LAST_UPDATE_DTM_VALUE_SET));
            });
            conn1.executeUpdate(DB_STATUS_DELETE);
            conn1.executeUpdate(DB_STATUS_INSERT, ps -> {
                ps.setString(1, status.toString());
                ps.setTimestamp(2, tsMap.get(COL_LAST_UPDATE_DTM));
                ps.setTimestamp(3, tsMap.get(COL_LAST_UPDATE_DTM_MED_CLASS_TO_POPULATE_DB));
                ps.setTimestamp(4, tsMap.get(COL_LAST_UPDATE_DTM_MED_DOSE_FORM));
                ps.setTimestamp(5, tsMap.get(COL_LAST_UPDATE_DTM_MED_DOSE_FORM_GROUP));
                ps.setTimestamp(6, tsMap.get(COL_LAST_UPDATE_DTM_MED_DRUG));
                ps.setTimestamp(7, tsMap.get(COL_LAST_UPDATE_DTM_MED_DRUG_DOSE_FORM_GROUP));
                ps.setTimestamp(8, tsMap.get(COL_LAST_UPDATE_DTM_MED_DRUG_WITH_INGREDIENT));
                ps.setTimestamp(9, tsMap.get(COL_LAST_UPDATE_DTM_MED_INGREDIENT));
                ps.setTimestamp(10, tsMap.get(COL_LAST_UPDATE_DTM_MED_INGREDIENT_IN_CLASS));
                ps.setTimestamp(11, tsMap.get(COL_LAST_UPDATE_DTM_MED_INGREDIENT_TYPE));
                ps.setTimestamp(12, tsMap.get(COL_LAST_UPDATE_DTM_MED_SCDC));
                ps.setTimestamp(13, tsMap.get(COL_LAST_UPDATE_DTM_MED_SCDC_FOR_DRUG));
                ps.setTimestamp(14, tsMap.get(COL_LAST_UPDATE_DTM_VALUE_SET));
            });
        }
    }
    
    static void updating(DbConnection conn1) {
        updateStatus(conn1, CONST_UPDATING);
    }
    
    static void updateDbStatus(DbConnection conn) {
        Timestamp lastUpdateDtm = conn.executeTimestampQuery(SELECT_LATEST_TIMESTAMP);
        Timestamp lastUpdateDtmMedClassToPopulateDb = conn.executeTimestampQuery(TS_MED_CLASS_TO_POPULATE_DB);
        Timestamp lastUpdateDtmMedDoseForm = conn.executeTimestampQuery(TS_MED_DOSE_FORM);
        Timestamp lastUpdateDtmMedDoseFormGroup = conn.executeTimestampQuery(TS_MED_DOSE_FORM_GROUP);
        Timestamp lastUpdateDtmMedDrug = conn.executeTimestampQuery(TS_MED_DRUG);
        Timestamp lastUpdateDtmMedDrugDoseFormGroup = conn.executeTimestampQuery(TS_MED_DRUG_DOSE_FORM_GROUP);
        Timestamp lastUpdateDtmMedDrugWithIngredient = conn.executeTimestampQuery(TS_MED_DRUG_WITH_INGREDIENT);
        Timestamp lastUpdateDtmMedIngredient = conn.executeTimestampQuery(TS_MED_INGREDIENT);
        Timestamp lastUpdateDtmMedIngredientInClass = conn.executeTimestampQuery(TS_MED_INGREDIENT_IN_CLASS);
        Timestamp lastUpdateDtmMedIngredientType = conn.executeTimestampQuery(TS_MED_INGREDIENT_TYPE);
        Timestamp lastUpdateDtmMedScdc = conn.executeTimestampQuery(TS_MED_SCDC);
        Timestamp lastUpdateDtmMedScdcForDrug = conn.executeTimestampQuery(TS_MED_SCDC_FOR_DRUG);
        Timestamp lastUpdateDtmValueSet = conn.executeTimestampQuery(TS_VALUE_SET);

        
        conn.executeUpdate(UPDATE_DB_STATUS_TS, ps -> {
            ps.setString(1, CONST_READY_TO_READ);
            ps.setTimestamp(2, lastUpdateDtm);
            ps.setTimestamp(3, lastUpdateDtmMedClassToPopulateDb);
            ps.setTimestamp(4, lastUpdateDtmMedDoseForm);
            ps.setTimestamp(5, lastUpdateDtmMedDoseFormGroup);
            ps.setTimestamp(6, lastUpdateDtmMedDrug);
            ps.setTimestamp(7, lastUpdateDtmMedDrugDoseFormGroup);
            ps.setTimestamp(8, lastUpdateDtmMedDrugWithIngredient);
            ps.setTimestamp(9, lastUpdateDtmMedIngredient);
            ps.setTimestamp(10, lastUpdateDtmMedIngredientInClass);
            ps.setTimestamp(11, lastUpdateDtmMedIngredientType);
            ps.setTimestamp(12, lastUpdateDtmMedScdc);
            ps.setTimestamp(13, lastUpdateDtmMedScdcForDrug);
            ps.setTimestamp(14, lastUpdateDtmValueSet);
        });
    }


    private static void updateStatus(DbConnection conn, String status) {
        conn.executeUpdate(DB_STATUS_UPDATE, ps -> {
            ps.setString(1, status);
        });
    }


}
