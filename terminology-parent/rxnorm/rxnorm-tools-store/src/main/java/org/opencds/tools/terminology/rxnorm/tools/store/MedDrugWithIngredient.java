/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.BiConsumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;

class MedDrugWithIngredient {
    private static final Log log = LogFactory.getLog(MedDrugWithIngredient.class);

    private static final String INSERT_MED_DRUG_WITH_INGREDIENT = "INSERT INTO MED_DRUG_WITH_INGREDIENT(INGREDIENT_RXCUI, DRUG_RXCUI) VALUES (?, ?)";
    private static final String DELETE_MED_DRUG_WITH_INGREDIENT = "DELETE FROM MED_DRUG_WITH_INGREDIENT";
    private static final String MED_DRUG_WITH_INGREDIENT_SELECT_DISTINCT_INGREDIENT_RXCUI_DRUG_RXCUI = "SELECT DISTINCT INGREDIENT_RXCUI, DRUG_RXCUI FROM MED_DRUG_WITH_INGREDIENT";
    
    private static final String COL_DRUG_RXCUI = "DRUG_RXCUI";
    private static final String COL_INGREDIENT_RXCUI = "INGREDIENT_RXCUI";

    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DRUG_WITH_INGREDIENT);
    }
    
    static void getMedDrugWithIngredients(DbConnection conn, BiConsumer<String, String> resultConsumer) {
        conn.executeQuery(MED_DRUG_WITH_INGREDIENT_SELECT_DISTINCT_INGREDIENT_RXCUI_DRUG_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                String ingredientRxCui = r.getString(COL_INGREDIENT_RXCUI);
                String drugRxCui = r.getString(COL_DRUG_RXCUI);
                resultConsumer.accept(ingredientRxCui, drugRxCui);
            });
        });
    }

    static void insertWithoutCommit(DbConnection conn, String ingredientRxCuiForPopulatingDb,
            String semanticDrugRxCui) {
        conn.executeUpdateNoCommit(INSERT_MED_DRUG_WITH_INGREDIENT, ps -> {
            ps.setString(1, ingredientRxCuiForPopulatingDb);
            ps.setString(2, semanticDrugRxCui);
        });
    }


}
