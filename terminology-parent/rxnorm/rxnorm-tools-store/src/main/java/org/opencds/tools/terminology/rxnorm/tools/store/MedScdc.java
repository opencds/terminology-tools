/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrugComponent;

class MedScdc {
    private static final Log log = LogFactory.getLog(MedScdc.class);

    private static final String INSERT_MED_SCDC = "INSERT INTO MED_SCDC(SCDC_RXCUI, SCDC_NAME, INGREDIENT_RXCUI, STRENGTH, STRENGTH_VALUE, STRENGTH_UNIT) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String DELETE_MED_SCDC = "DELETE FROM MED_SCDC";
    private static final String MED_SCDC_SELECT_DISTINCT_SCDC_RXCUI = "SELECT DISTINCT SCDC_RXCUI FROM MED_SCDC";
    private static final String MED_SCDC_MED_SCDC_FOR_DRUG_MED_INGREDIENT_QUERY = "SELECT SCDC.SCDC_RXCUI, SCDC.SCDC_NAME, SCDC.INGREDIENT_RXCUI, SCDC.STRENGTH, ING.INGREDIENT_NAME "
            + " FROM MED_SCDC SCDC INNER JOIN MED_INGREDIENT ING ON SCDC.INGREDIENT_RXCUI = ING.INGREDIENT_RXCUI ";

    private static final String COL_INGREDIENT_RXCUI = "INGREDIENT_RXCUI";
    private static final String COL_SCDC_NAME = "SCDC_NAME";
    private static final String COL_SCDC_RXCUI = "SCDC_RXCUI";
    private static final String COL_STRENGTH = "STRENGTH";

    static void insertWithoutCommit(DbConnection conn, String scdcRxCui, String scdcName, String ingredientRxCui,
            String strengthStr, Double strengthValue, String strengthUnit) {
        conn.executeUpdateNoCommit(INSERT_MED_SCDC, ps -> {
            ps.setString(1, scdcRxCui);
            ps.setString(2, scdcName);
            ps.setString(3, ingredientRxCui);
            ps.setString(4, strengthStr);
            ps.setDouble(5, strengthValue);
            ps.setString(6, strengthUnit);
        });
    }

    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_SCDC);        
    }
    
    static void getScdc(DbConnection conn, Consumer<String> resultConsumer) {
        conn.executeQuery(MED_SCDC_SELECT_DISTINCT_SCDC_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_SCDC_RXCUI));
            });
        });
    }

    static void forEachSCD(DbConnection connection, BiConsumer<String, RxNormSemanticDrugComponent> resultConsumer) {
        connection.executeQuery(MED_SCDC_MED_SCDC_FOR_DRUG_MED_INGREDIENT_QUERY, rsIter -> {
           rsIter.forEach(r -> {
               String scdcRxCui = r.getString(COL_SCDC_RXCUI);
               String scdcName = r.getString(COL_SCDC_NAME);
               String ingredientRxCui = r.getString(COL_INGREDIENT_RXCUI);
               String strength = r.getString(COL_STRENGTH);
               String ingredientName = r.getString(MedIngredient.COL_INGREDIENT_NAME);
               RxNormSemanticDrugComponent scdc = new RxNormSemanticDrugComponent(
                       new RxNormConcept(scdcRxCui, scdcName));
               scdc.addIngredient(new RxNormConcept(ingredientRxCui, ingredientName));
               scdc.setStrengthStr(strength);
               resultConsumer.accept(scdcRxCui, scdc);
           }); 
        });

        
    }

}
