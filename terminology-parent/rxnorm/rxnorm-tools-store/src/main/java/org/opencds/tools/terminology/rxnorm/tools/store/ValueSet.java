/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import org.apache.commons.lang3.function.TriConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;

class ValueSet {
    private static final Log log = LogFactory.getLog(ValueSet.class);

    private static final String UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_ID = "UPDATE VALUE_SET SET UPDATE_DTM = ? WHERE VALUE_SET_ID = ?";
    private static final String VALUE_SET_SELECT_ID_NAME_ORDER_BY_EXECUTE_ORDER = "SELECT VALUE_SET_ID, VALUE_SET_NAME, SQL FROM VALUE_SET ORDER BY EXECUTE_ORDER";
    private static final String VALUE_SETS_QUERY = "SELECT VALUE_SET_ID FROM VALUE_SET ORDER BY VALUE_SET_ID ";

    private static final String COL_SQL = "SQL";
    private static final String COL_VALUE_SET_ID = "VALUE_SET_ID";
    private static final String COL_VALUE_SET_NAME = "VALUE_SET_NAME";

    /**
     * Executes in a batch, which commits at the end.
     * 
     * @param conn
     * @param rsConsumer
     */
    static void forEachValueSetByExecutionOrder(DbConnection conn, TriConsumer<Integer, String, String> rsConsumer) {
            conn.executeQuery(VALUE_SET_SELECT_ID_NAME_ORDER_BY_EXECUTE_ORDER, rsIter -> {
                rsIter.forEach(result -> {
                    int valueSetId = result.getInt(COL_VALUE_SET_ID);
                    String sql = result.getString(COL_SQL);
                    String valueSetName = result.getString(COL_VALUE_SET_NAME);
                    rsConsumer.accept(valueSetId, sql, valueSetName);
                });
            });
    }

    static void updateValueSetTimestampWithoutCommit(DbConnection conn, Timestamp timestamp, int valueSetId) {
        conn.executeUpdateNoCommit(UPDATE_VALUE_SET_UPDATE_DTM_WHERE_VALUE_SET_ID, ps -> {
            ps.setTimestamp(1, timestamp);
            ps.setInt(2, valueSetId);
        });
    }

    static Set<String> getNewDrugsForValueSet(DbConnection conn, String sql) {
        Set<String> valueSet = new HashSet<>();
        conn.executeQuery(sql, rsIter -> {
            rsIter.forEach(r -> {
                valueSet.add(r.getString(1));
            });
        });
        return valueSet;
    }

    static void forEachValueSet(DbConnection connection, Consumer<Integer> resultConsumer) {
        connection.executeQuery(VALUE_SETS_QUERY, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getInt("VALUE_SET_ID"));
            });
            
        });
        
    }
    

}
