/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.HashSet;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;

class MedIngredient {
    private static final Log log = LogFactory.getLog(MedIngredient.class);
    
    private static final String INSERT_MED_INGREDIENT = "INSERT INTO MED_INGREDIENT(INGREDIENT_RXCUI, INGREDIENT_NAME, USE_TO_POPULATE_DB, SKIP) VALUES (?, ?, ?, 0)";
    private static final String UPDATE_MED_INGREDIENT_SET_INGREDIENT_NAME_WHERE_INGREDIENT_RXCUI = "UPDATE MED_INGREDIENT SET INGREDIENT_NAME = ? WHERE INGREDIENT_RXCUI = ?";
    private static final String UPDATE_MED_INGREDIENT_USE_TO_POPULATE_DB = "UPDATE MED_INGREDIENT SET USE_TO_POPULATE_DB = 1 WHERE INGREDIENT_RXCUI = ?";
    private static final String DELETE_MED_INGREDIENT_WHERE_USE_TO_POPULATE_DB_0 = "DELETE FROM MED_INGREDIENT WHERE USE_TO_POPULATE_DB = 0";
    private static final String MED_INGREDIENT_SELECT_DISTINCT_INGREDIENT_RXCUI = "SELECT DISTINCT INGREDIENT_RXCUI, USE_TO_POPULATE_DB FROM MED_INGREDIENT";
    private static final String MED_INGREDIENT_SELECT_DISTINCT_WHERE_USE_TO_POPULATE_DB_EQ_1_AND_SKIP_NE_1 = "SELECT DISTINCT INGREDIENT_RXCUI FROM MED_INGREDIENT WHERE USE_TO_POPULATE_DB = 1 AND SKIP != 1";
    private static final String MED_INGREDIENT_SELECT_INGREDIENT_RXCUI_WHERE_INGREDIENT_NAME_IS_NULL = "SELECT INGREDIENT_RXCUI FROM MED_INGREDIENT WHERE INGREDIENT_NAME IS NULL";
    
    static final String COL_INGREDIENT_NAME = "INGREDIENT_NAME";
    private static final String COL_INGREDIENT_RXCUI = "INGREDIENT_RXCUI";
    private static final String COL_USE_TO_POPULATE_DB = "USE_TO_POPULATE_DB";

    static void insertWithoutCommit(DbConnection conn, String ingredientRxCui, String ingredientName, int useToPopulateDb) {
        conn.executeUpdateNoCommit(INSERT_MED_INGREDIENT, ps -> {
            ps.setString(1, ingredientRxCui);
            ps.setString(2, ingredientName);
            ps.setInt(3, useToPopulateDb);
        });
    }

    static void insertMedIngredient(DbConnection conn3, String rxCui, String name, int useToPopulateDb) {
        conn3.executeUpdateNoCommit(INSERT_MED_INGREDIENT, ps -> {
            ps.setString(1, rxCui);
            ps.setString(2, name);
            ps.setInt(3, useToPopulateDb);
        });
    }

    static void useToPopulateDb(DbConnection conn3, String rxCui) {
        conn3.executeUpdateNoCommit(UPDATE_MED_INGREDIENT_USE_TO_POPULATE_DB, ps -> {
            ps.setString(1, rxCui);
        });
        
    }

    static void updateMissingIngredientNames(DbConnection conn1, Consumer<String> resultConsumer) {
        conn1.executeQuery(MED_INGREDIENT_SELECT_INGREDIENT_RXCUI_WHERE_INGREDIENT_NAME_IS_NULL, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_INGREDIENT_RXCUI));
            });
        });
    }
    
    static void updateMedIngredient(DbConnection conn3, String name, String ingredientRxCuiWithoutName) {
        conn3.executeUpdateNoCommit(UPDATE_MED_INGREDIENT_SET_INGREDIENT_NAME_WHERE_INGREDIENT_RXCUI, ps -> {
            ps.setString(1, name);
            ps.setString(2, ingredientRxCuiWithoutName);
        });

    }
    
    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_INGREDIENT_WHERE_USE_TO_POPULATE_DB_0);
    }

    static void getIncredientRxCUIs(DbConnection conn, HashSet<String> ingredientRxCuisToPopulateDb,
            HashSet<String> ingredientRxCuis, BiConsumer<String, Integer> resultConsumer) {
        conn.executeQuery(MED_INGREDIENT_SELECT_DISTINCT_INGREDIENT_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                String ingredientRxCui = r.getString(COL_INGREDIENT_RXCUI);
                int useToPopulateDb = r.getInt(COL_USE_TO_POPULATE_DB);
                resultConsumer.accept(ingredientRxCui, useToPopulateDb);
            });
        });
    }
    
    static void getIngredientRxCUIs(DbConnection conn, Consumer<String> resultConsumer) {
        conn.executeQuery(MED_INGREDIENT_SELECT_DISTINCT_WHERE_USE_TO_POPULATE_DB_EQ_1_AND_SKIP_NE_1, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_INGREDIENT_RXCUI));
            });
        });
    }

}
