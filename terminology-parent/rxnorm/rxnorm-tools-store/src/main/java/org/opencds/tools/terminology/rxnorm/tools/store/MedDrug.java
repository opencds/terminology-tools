/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept.TermTypeConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug;

class MedDrug {
    private static final Log log = LogFactory.getLog(MedDrug.class);

    private static final String INSERT_MED_DRUG = "INSERT INTO MED_DRUG(DRUG_RXCUI, DRUG_TYPE, DRUG_NAME, DOSE_FORM_RXCUI) VALUES (?, ?, ?, ?)";
    private static final String DELETE_MED_DRUG = "DELETE FROM MED_DRUG";
    private static final String MED_DRUG_SELECT_DISTINCT_DRUG_RXCUI = "SELECT DISTINCT DRUG_RXCUI FROM MED_DRUG";
    private static final String MED_DRUG_MED_DOSE_FORM_QUERY = "SELECT DRUG.DRUG_RXCUI, DRUG.DRUG_TYPE, DRUG.DRUG_NAME, DRUG.DOSE_FORM_RXCUI, DOSE_FORM.DOSE_FORM_NAME "
            + " FROM MED_DRUG DRUG INNER JOIN MED_DOSE_FORM DOSE_FORM ON DRUG.DOSE_FORM_RXCUI = DOSE_FORM.DOSE_FORM_RXCUI ";

    private static final String COL_DRUG_NAME = "DRUG_NAME";
    private static final String COL_DRUG_RXCUI = "DRUG_RXCUI";
    private static final String COL_DRUG_TYPE = "DRUG_TYPE";
    
    static void insertWithoutCommit(DbConnection conn, String semanticDrugRxCui, String semanticDrugType,
            String semanticDrugRxNormConceptName, String doseFormRxCui) {
        conn.executeUpdateNoCommit(INSERT_MED_DRUG, ps -> {
            ps.setString(1, semanticDrugRxCui);
            ps.setString(2, semanticDrugType);
            ps.setString(3, semanticDrugRxNormConceptName);
            ps.setString(4, doseFormRxCui);
        });
    }
    
    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DRUG);
    }

    static void getMedDrugRxCuis(DbConnection conn, Consumer<String> resultConsumer) {
        conn.executeQuery(MED_DRUG_SELECT_DISTINCT_DRUG_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                resultConsumer.accept(r.getString(COL_DRUG_RXCUI));
            });
        });
    }

    static void getRxCuiToSemanticDrug(DbConnection connection, BiConsumer<String, RxNormSemanticDrug> resultConsumer) {
        connection.executeQuery(MED_DRUG_MED_DOSE_FORM_QUERY, rsIterable -> {
            rsIterable.forEach(r -> {
                String drugRxCui = r.getString(COL_DRUG_RXCUI);
                TermTypeConcept drugType = TermTypeConcept.byTTY(r.getString(COL_DRUG_TYPE));
                String drugName = r.getString(COL_DRUG_NAME);
                String doseFormRxCui = r.getString(MedDoseForm.COL_DOSE_FORM_RXCUI);
                String doseFormName = r.getString(MedDoseForm.COL_DOSE_FORM_NAME);
                // FIXME: tty shouldn't really be null; but what does the access db have?
                RxNormSemanticDrug drug = new RxNormSemanticDrug(drugRxCui, drugName, null);
                drug.setTermTypeConcept(drugType);
                drug.setDoseFormConcept(new RxNormConcept(doseFormRxCui, doseFormName));
                resultConsumer.accept(drugRxCui, drug);
            });
        });
    }


}
