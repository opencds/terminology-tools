/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxclass.tools.client.RxClassClient;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrugComponent;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug;
import org.opencds.tools.terminology.rxnorm.tools.client.RxNormClient;

/**
 * This class writes RxNav information to locally specified data store,
 * including for defined periodic updates while the process is running.
 * Currently, the local data store is expected to be a Microsoft Access
 * database, and that it has the default structure already defined. Steps to
 * populate and update the data store:
 *
 * --- Ingredient based approach --- In DB -
 * Identify ingredients of interest, populate in MED_INGREDIENT table with a
 * USE_TO_POPULATE_DB flag of 1 and a SKIP = 0. In DB - Create entries in
 * MED_INGREDIENT_TYPE referencing the entries created in step 1, with an
 * INGREDIENT_TYPE label that can be used to identify medications of interest.
 *
 * --- Drug class based appraoch --- In DB - Identify Daily Med classes of
 * interest, populate in MED_CLASS table with a USE_TO_POPULATE_DB flag of 1 and
 * a SKIP = 0. This will lead to MED_INGREDIENT (but not MED_INGREDIENT_TYPE)
 * being populated, which will in turn do everything else that the
 * Ingredient-based appraoch above will do.
 *
 * -- Final steps In DB - Specify names and queries for value sets of MED_DRUGs
 * desired, under VALUE_SET.
 *
 * @author Kensaku Kawamoto
 */
public class RxNormLocalDataStoreWriter {
    private static final Log log = LogFactory.getLog(RxNormLocalDataStoreWriter.class);

    private final RxClassClient rxClassClient;
    private final RxNormClient rxNormClient;

    public RxNormLocalDataStoreWriter(RxClassClient rxClassClient, RxNormClient rxNormClient) {
        this.rxClassClient = rxClassClient;
        this.rxNormClient = rxNormClient;
    }

    /**
     * Updates the RxNav local data store as specified. Status of DB will be
     * updated in DB table.
     *
     * @param supplier supplier for the connection to a database (supplier must create a new connection each time)
     * @param wipeExistingContent
     *            Set to false for production runs. Deletes all existing
     *            content.
     * @param skipRxNavLookups
     *            Set to false for production runs. Skips this time-consuming
     *            step.
     * @throws Exception
     *             If there is any error, exception will be thrown.
     */
    public void updateRxNavLocalDataStore(Supplier<Connection> supplier, boolean wipeExistingContent,
            boolean skipRxNavLookups) {
        log.info("> Running updateRxNavLocalDataStore...");

        DbConnection conn1 = new DbConnection(supplier);
        DbConnection conn2 = new DbConnection(supplier);
        DbConnection conn3 = new DbConnection(supplier);
        // ensure there is 1 and only 1 DATABASE_STATUS entry
        DatabaseStatus.ensureSingleStatusEntry(conn1);

        // update status of database
        DatabaseStatus.updating(conn1);

        // If should wipe existing content, do so. Only content maintained are:
        // MED_INGREDIENT_TYPE entries
        // MED_INGREDIENT entries where USE_TO_POPULATE_DB = 1
        // MED_CLASS entries
        // DATABASE_STATUS

        if (wipeExistingContent) {
            log.info("> Wiping existing content...");
            wipeExistingContent(conn1);
        }

        // get Ingredient RXCUIs
        log.info("- Get ingredient RXCUIs.");
        HashSet<String> ingredientRxCuisToPopulateDb = new HashSet<String>();
        HashSet<String> ingredientRxCuis = new HashSet<String>();

        MedIngredient.getIncredientRxCUIs(conn1, ingredientRxCuisToPopulateDb, ingredientRxCuis, (ingredientRxCui, useToPopulateDb) -> {
            if (useToPopulateDb == 1) {
                ingredientRxCuisToPopulateDb.add(ingredientRxCui);
            }
            ingredientRxCuis.add(ingredientRxCui);
        });

        // add entries to MED_INGREDIENT based on MED_CLASS specifications, if
        // they do not exist.
        // refresh entries in ingredient HashSet
        // Always re-do this step; it's fast, and we want to avoid cases where
        // the med class specification has been updated in DB
        // and contents are not updated. Once specified in MED_INGREDIENT,
        // however, previously specified ingredients are not removed for the
        // purposes of identifying information on those ingredients unless
        // wipeExistingContent was set to true.

        log.info("- Refresh ingredients in specified classes for populating DB.");

        MedIngredientInClass.delete(conn1);

        MedClassToPopulateDb.findAndAddIngredients(conn1, (medClassId, relationSource, relation, conceptId) -> {
            Set<RxNormConcept> ingredientsForClass = rxClassClient.getIngredientsInRxClass(relationSource, relation,
                    conceptId);
            conn2.executeBatch(c -> {
                ingredientsForClass.stream().forEach(concept -> {
                    if (!ingredientRxCuisToPopulateDb.contains(concept.getRxcui())) {
                        if (!ingredientRxCuis.contains(concept.getRxcui())) {
                            MedIngredient.insertMedIngredient(conn2, concept.getRxcui(), concept.getName(), 1);
                            ingredientRxCuis.add(concept.getRxcui());
                        } else {
                            MedIngredient.useToPopulateDb(conn2, concept.getRxcui());
                        }
                    }
                    MedIngredientInClass.insertWithoutCommit(conn2, medClassId, concept.getRxcui());
                });
            });
        });

        // get Ingredient RXCUIs for populating DB
        log.info("- Get ingredient RXCUIs for populating DB.");
        Set<String> ingredientRxCuisForPopulatingDb = new HashSet<>();
        MedIngredient.getIngredientRxCUIs(conn1, ingredientRxCui -> {
            ingredientRxCuisForPopulatingDb.add(ingredientRxCui);
        });

        // get MED_DRUG RXCUIs
        log.info("- Get MED_DRUG RXCUIs.");
        Set<String> medDrugRxCuis = new HashSet<>();
        MedDrug.getMedDrugRxCuis(conn1, drugRxCui -> {
            medDrugRxCuis.add(drugRxCui);
        });

        // get MED_DRUG WITH_INGREDIENT entries
        log.info("- Get MED_DRUG_WITH_INGREDIENT entries.");
        Map<String, Set<String>> ingredientToMedDrugRxCuiSetMap = new HashMap<>();
        MedDrugWithIngredient.getMedDrugWithIngredients(conn1, (ingredientRxCui, drugRxCui) -> {
            Set<String> set = ingredientToMedDrugRxCuiSetMap.getOrDefault(ingredientRxCui, new HashSet<>());
            set.add(drugRxCui);
            ingredientToMedDrugRxCuiSetMap.put(ingredientRxCui, set);
        });

        // get Ingredient names for MED_INGREDIENT entries lacking names
        log.info("- Get ingredient names for MED_INGREDIENTs without names.");
        MedIngredient.updateMissingIngredientNames(conn1, ingredientRxCuiWithoutName -> {
            // connect to RxNav to get name
            conn1.executeBatch(c -> {
                String name = rxNormClient.getName(ingredientRxCuiWithoutName);
                MedIngredient.updateMedIngredient(conn1, name, ingredientRxCuiWithoutName);
            });
        });

        // get the RxNorm content and update DB
        if (!skipRxNavLookups) {
            // process MED_DRUG level first

            Map<String, RxNormSemanticDrug> medDrugRxCuiToSemanticDrugMap = new HashMap<>();
            Set<String> doseFormRxCuiSet = new HashSet<>();
            MedDoseForm.getDoseFormRxCUI(conn1, doseFormRxCui -> {
                doseFormRxCuiSet.add(doseFormRxCui);
            });

            populateMedDrugWithSCDAndSBD(conn1, ingredientRxCuisForPopulatingDb, medDrugRxCuiToSemanticDrugMap,
                    doseFormRxCuiSet, medDrugRxCuis, ingredientToMedDrugRxCuiSetMap);

            // get MED_DOSE_FORM_GROUP, MED_DRUG_DOSE_FORM_GROUP, MED_SCDC, and
            // MED_SCDC_FOR_DRUG entries
            log.info("- Get MED_DOSE_FORM_GROUP, MED_DRUG_DOSE_FORM_GROUP, MED_SCDC, and MED_SCDC_FOR_DRUG entries.");

            Set<String> doseFormGroupRxCuiSet = new HashSet<>();
            MedDoseFormGroup.getDoseFormGroup(conn1, doseFormGroupRxCui -> {
                doseFormGroupRxCuiSet.add(doseFormGroupRxCui);
            });

            Map<String, Set<String>> medDrugToDoseFormGroupRxCuiSetMap = new HashMap<>();
            MedDrugDoseFormGroup.getDrugAndDoseForm(conn1, (drugRxCui, doseFormGroupRxCui) -> {
                Set<String> set = medDrugToDoseFormGroupRxCuiSetMap.getOrDefault(drugRxCui, new HashSet<>());
                set.add(doseFormGroupRxCui);
                medDrugToDoseFormGroupRxCuiSetMap.put(drugRxCui, set);
            });

            Set<String> medScdcRxCuiSet = new HashSet<>();
            MedScdc.getScdc(conn1, scdcRxCui -> {
                medScdcRxCuiSet.add(scdcRxCui);
            });

            Map<String, Set<String>> medDrugRxCuiToScdcRxCuiSetMap = new HashMap<>();
            MedScdcForDrug.getDrugAndScdc(conn1, (drugRxCui, scdcRxCui) -> {
                Set<String> set = medDrugRxCuiToScdcRxCuiSetMap.getOrDefault(drugRxCui, new HashSet<>());
                set.add(scdcRxCui);
                medDrugRxCuiToScdcRxCuiSetMap.put(drugRxCui, set);
            });

            // populate above tables' information based on SCD/SBD-related
            // information
            populateWithScdAndSbdRelatedInfo(conn2, medDrugRxCuis, medDrugRxCuiToSemanticDrugMap,
                    medDrugToDoseFormGroupRxCuiSetMap, medDrugRxCuiToScdcRxCuiSetMap, doseFormGroupRxCuiSet,
                    ingredientRxCuis, medScdcRxCuiSet);
        }

        // Update the value sets
        log.info("> Evaluating value sets...");
        conn1.executeBatch(c -> {
            ValueSet.forEachValueSetByExecutionOrder(conn2, (valueSetId, sql, valueSetName) -> {
                Set<String> existingDrugRxCuisForValueSet = new HashSet<>();
                MedDrugValueSet.getExistingDrugsForValueSet(conn3, valueSetId, drugRxCui -> {
                    existingDrugRxCuisForValueSet.add(drugRxCui);
                });

                Set<String> newDrugRxCuisForValueSet = ValueSet.getNewDrugsForValueSet(conn3, sql);

                if (newDrugRxCuisForValueSet.equals(existingDrugRxCuisForValueSet)) {
                    // do nothing
                    log.info("No changes needed for value set " + valueSetId + ": " + valueSetName + "...");
                } else {
                    log.info("Updating value set " + valueSetId + ": " + valueSetName + "...");
                    MedDrugValueSet.deleteWithoutCommit(conn1, valueSetId);
                    MedDrugValueSet.updateValueSetWithoutCommit(conn1, valueSetId, valueSetName,
                            newDrugRxCuisForValueSet);
                    ValueSet.updateValueSetTimestampWithoutCommit(conn1, new Timestamp(new Date().getTime()),
                            valueSetId);
                }
            });
        });

        // Update Database Status
        log.info("> Updating database status...");
        DatabaseStatus.updateDbStatus(conn1);
        log.info("> Database update completed.");
        conn1.close();
        conn2.close();
    }

    private void wipeExistingContent(DbConnection conn) {
        MedScdcForDrug.delete(conn);
        MedScdc.delete(conn);
        MedDrugValueSet.delete(conn);
        MedIngredient.delete(conn);
        MedIngredientInClass.delete(conn);
        MedDrugDoseFormGroup.delete(conn);
        MedDrugWithIngredient.delete(conn);
        MedDrug.delete(conn);
        MedDoseForm.delete(conn);
        MedDoseFormGroup.delete(conn);
    }

    // TODO: Need to validate this method.  Compare against original implementation.
    private void populateMedDrugWithSCDAndSBD(DbConnection conn, Set<String> ingredientRxCuisForPopulatingDb,
            Map<String, RxNormSemanticDrug> medDrugRxCuiToSemanticDrugMap, Set<String> doseFormRxCuiSet,
            Set<String> medDrugRxCuis, Map<String, Set<String>> ingredientToMedDrugRxCuiSetMap) {
        conn.executeBatch(c -> {
            ingredientRxCuisForPopulatingDb.forEach(ingredientRxCuiForPopulatingDb -> {
                log.info("> Getting semantic drugs for ingredient: " + ingredientRxCuiForPopulatingDb + "...");
                Set<RxNormSemanticDrug> semanticDrugs = rxNormClient
                        .getSemanticDrugsWithIngredient(ingredientRxCuiForPopulatingDb);

                // populate MED_DRUG and MED_DRUG_WITH_INGREDIENT with semantic
                // clinical drugs (SCDs) and semantic branded drugs (SBDs) with
                // MED_INGREDIENT_TYPE entries.
                log.info("- Populate MED_DRUG and MED_DRUG_WITH_INGREDIENT based on MED_INGREDIENT entries for populating DB.");
                semanticDrugs.forEach(semanticDrug -> {
                    // should always be present
                    String semanticDrugRxCui = semanticDrug.getRxcui();
                    medDrugRxCuiToSemanticDrugMap.put(semanticDrugRxCui, semanticDrug);
                    RxNormConcept doseFormConcept = semanticDrug.getDoseFormConcept();
                    String doseFormRxCui = doseFormConcept.getRxcui();
                    String doseFormName = doseFormConcept.getName();

                    if (!doseFormRxCuiSet.contains(doseFormRxCui)) {
                        MedDoseForm.insertWithoutCommit(conn, doseFormRxCui, doseFormName);
                        doseFormRxCuiSet.add(doseFormRxCui);
                    }

                    if (!medDrugRxCuis.contains(semanticDrugRxCui)) {
                        MedDrug.insertWithoutCommit(conn, semanticDrugRxCui, semanticDrug.getTermTypeConcept().name(),
                                semanticDrug.getName(), doseFormConcept.getRxcui());
                        medDrugRxCuis.add(semanticDrugRxCui);
                    }

                    Set<String> drugRxCuiSetForIngredient = ingredientToMedDrugRxCuiSetMap
                            .get(ingredientRxCuiForPopulatingDb);
                    if (drugRxCuiSetForIngredient == null) {
                        drugRxCuiSetForIngredient = new HashSet<String>();
                        ingredientToMedDrugRxCuiSetMap.put(ingredientRxCuiForPopulatingDb, drugRxCuiSetForIngredient);
                    }

                    if (!drugRxCuiSetForIngredient.contains(semanticDrugRxCui)) {
                        MedDrugWithIngredient.insertWithoutCommit(conn, ingredientRxCuiForPopulatingDb, semanticDrugRxCui);
                        drugRxCuiSetForIngredient.add(semanticDrugRxCui);
                    }

                });
            });
        });
    }

    // TODO: Need to validate this method.  Compare against original implementation.
    private void populateWithScdAndSbdRelatedInfo(DbConnection conn, Set<String> medDrugRxCuis,
            Map<String, RxNormSemanticDrug> medDrugRxCuiToSemanticDrugMap,
            Map<String, Set<String>> medDrugToDoseFormGroupRxCuiSetMap,
            Map<String, Set<String>> medDrugRxCuiToScdcRxCuiSetMap, Set<String> doseFormGroupRxCuiSet,
            HashSet<String> ingredientRxCuis, Set<String> medScdcRxCuiSet) {
        Set<String> doseFormGroupRxCuiSetForMedDrug = new HashSet<>();
        Set<String> scdcRxCuiSetForMedDrug = new HashSet<>();
        conn.executeBatch(c -> {
            medDrugRxCuis.forEach(medDrugRxCui -> {
                RxNormSemanticDrug semanticDrug = medDrugRxCuiToSemanticDrugMap.get(medDrugRxCui);
                // we may or may not have a semantic drug. If not, let's log the
                // fact and continue.
                if (semanticDrug != null) {
                    Set<RxNormConcept> doseFormGroupConcepts = semanticDrug.getDoseFormGroupConcepts();
                    Set<RxNormSemanticDrugComponent> scdcs = semanticDrug.getSemanticClinicalDrugComponents();
                    doseFormGroupRxCuiSetForMedDrug.clear();
                    doseFormGroupRxCuiSetForMedDrug.addAll(medDrugToDoseFormGroupRxCuiSetMap.getOrDefault(medDrugRxCui, new HashSet<>()));
                    if (doseFormGroupRxCuiSetForMedDrug.isEmpty()) {
                        medDrugToDoseFormGroupRxCuiSetMap.put(medDrugRxCui, doseFormGroupRxCuiSetForMedDrug);
                    }
                    scdcRxCuiSetForMedDrug.clear();
                    scdcRxCuiSetForMedDrug.addAll(medDrugRxCuiToScdcRxCuiSetMap.getOrDefault(medDrugRxCui, new HashSet<>()));
                    if (scdcRxCuiSetForMedDrug.isEmpty()) {
                        medDrugRxCuiToScdcRxCuiSetMap.put(medDrugRxCui, scdcRxCuiSetForMedDrug);
                    }

                    doseFormGroupConcepts.forEach(doseFormGroupConcept -> {
                        String doseFormGroupRxCui = doseFormGroupConcept.getRxcui();
                        String doseFormGroupName = doseFormGroupConcept.getName();

                        // add to MED_DOSE_FORM_GROUP if does not exist; update set
                        if (!doseFormGroupRxCuiSet.contains(doseFormGroupRxCui)) {
                            MedDoseFormGroup.insertWithoutCommit(conn, doseFormGroupRxCui, doseFormGroupName);
                            doseFormGroupRxCuiSet.add(doseFormGroupRxCui);
                        }

                        // add to MED_DRUG_DOSE_FORM_GROUP if does not exist; update set
                        if (!doseFormGroupRxCuiSetForMedDrug.contains(doseFormGroupRxCui)) {
                            MedDrugDoseFormGroup.insertWithoutCommit(conn, medDrugRxCui, doseFormGroupRxCui);
                            doseFormGroupRxCuiSetForMedDrug.add(doseFormGroupRxCui);
                        }
                    });

                    scdcs.forEach(scdc -> {
                        RxNormConcept scdcRxNormConcept = scdc.getRxNormConcept();
                        String scdcRxCui = scdcRxNormConcept.getRxcui();
                        String scdcName = scdcRxNormConcept.getName();

                        RxNormConcept ingredientRxNormConcept = scdc.getIngredients().stream()
                                .filter(concept -> concept.getTermTypeConcept() == RxNormConcept.TermTypeConcept.IN)
                                .findFirst()
                                .orElseGet(() -> new RxNormConcept(null)); // TODO: do we hit this?
                        String ingredientRxCui = ingredientRxNormConcept.getRxcui();
                        String ingredientName = ingredientRxNormConcept.getName();

                        // add ingredient to MED_INGREDIENT if does not exist; update
                        // set
                        if (!ingredientRxCuis.contains(ingredientRxCui)) {
                            MedIngredient.insertWithoutCommit(conn, ingredientRxCui, ingredientName, 0);
                            ingredientRxCuis.add(ingredientRxCui);
                        }

                        // add to MED_SCDC if does not exist; update set
                        if (!medScdcRxCuiSet.contains(scdcRxCui)) {
                            log.info(scdc.toString());
                            MedScdc.insertWithoutCommit(conn, scdcRxCui, scdcName, ingredientRxCui, scdc.getStrengthStr(), scdc.getStrengthValue(), scdc.getStrengthUnit());
                            medScdcRxCuiSet.add(scdcRxCui);
                        }

                        // add to MED_SCDC_FOR_DRUG if does not exist; update set
                        if (!scdcRxCuiSetForMedDrug.contains(scdcRxCui)) {
                            MedScdcForDrug.insertWithoutCommit(conn, medDrugRxCui, scdcRxCui);
                            scdcRxCuiSetForMedDrug.add(scdcRxCui);
                        }
                    });
                } else {
                    log.warn("RxNormSemanticDrug not found for cui: " + medDrugRxCui);
                }
            });
        });
    }

}
