/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;
import org.opencds.tools.terminology.rxnorm.tools.store.util.QuadConsumer;

class MedClassToPopulateDb {
    private static final Log log = LogFactory.getLog(MedClassToPopulateDb.class);

    private static final String MED_CLASS_TO_POPULATE_DB_SELECT_WHERE_SKIP_0 = "SELECT MED_CLASS_ID, RELATION_SOURCE, RELATION, CONCEPT_ID FROM MED_CLASS_TO_POPULATE_DB WHERE SKIP = 0";

    private static final String COL_CONCEPT_ID = "CONCEPT_ID";
    private static final String COL_MED_CLASS_ID = "MED_CLASS_ID";
    private static final String COL_RELATION = "RELATION";
    private static final String COL_RELATION_SOURCE = "RELATION_SOURCE";

    static void findAndAddIngredients(DbConnection conn, QuadConsumer<Integer, String, String, String> resultConsumer) {
        conn.executeQuery(MED_CLASS_TO_POPULATE_DB_SELECT_WHERE_SKIP_0, rsIter -> {
            rsIter.forEach(r -> {
              int medClassId = r.getInt(COL_MED_CLASS_ID);
              String relationSource = r.getString(COL_RELATION_SOURCE);
              String relation = r.getString(COL_RELATION);
              String conceptId = r.getString(COL_CONCEPT_ID);
              resultConsumer.accept(medClassId, relationSource, relation, conceptId);
            });
        });
    }

}
