/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.BiConsumer;

import org.opencds.tools.db.api.DbConnection;

class MedScdcForDrug {
    private static final String INSERT_MED_SCDC_FOR_DRUG = "INSERT INTO MED_SCDC_FOR_DRUG(DRUG_RXCUI, SCDC_RXCUI) VALUES (?, ?)";
    private static final String DELETE_MED_SCDC_FOR_DRUG = "DELETE FROM MED_SCDC_FOR_DRUG";
    private static final String MED_SCDC_FOR_DRUG_SELECT_DISTINCT_DRUG_RXCUI_SCDC_RXCUI = "SELECT DISTINCT DRUG_RXCUI, SCDC_RXCUI FROM MED_SCDC_FOR_DRUG";
    private static final String DRUG_RXCUI_SCDC_RXCUI_QUERY = "SELECT DRUG_RXCUI, SCDC_RXCUI FROM MED_SCDC_FOR_DRUG ";

    private static final String COL_DRUG_RXCUI = "DRUG_RXCUI";
    private static final String COL_SCDC_RXCUI = "SCDC_RXCUI";

    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_SCDC_FOR_DRUG);
    }

    static void getDrugAndScdc(DbConnection conn, BiConsumer<String, String> resultConsumer) {
        conn.executeQuery(MED_SCDC_FOR_DRUG_SELECT_DISTINCT_DRUG_RXCUI_SCDC_RXCUI, rsIter -> {
            rsIter.forEach(r -> {
                String drugRxCui = r.getString(COL_DRUG_RXCUI);
                String scdcRxCui = r.getString(COL_SCDC_RXCUI);
                resultConsumer.accept(drugRxCui, scdcRxCui);
            });
        });
    }

    static void insertWithoutCommit(DbConnection conn, String medDrugRxCui, String scdcRxCui) {
        conn.executeUpdateNoCommit(INSERT_MED_SCDC_FOR_DRUG, ps -> {
            ps.setString(1, medDrugRxCui);
            ps.setString(2, scdcRxCui);
        });
    }

    static void forEachDrug(DbConnection connection, BiConsumer<String, String> resultConsumer) {
        connection.executeQuery(DRUG_RXCUI_SCDC_RXCUI_QUERY, rsIter -> {
            rsIter.forEach(r -> {
                String drugRxCui = r.getString("DRUG_RXCUI");
                String scdcRxCui = r.getString("SCDC_RXCUI");
                resultConsumer.accept(drugRxCui, scdcRxCui);
            });
        });
    }


}
