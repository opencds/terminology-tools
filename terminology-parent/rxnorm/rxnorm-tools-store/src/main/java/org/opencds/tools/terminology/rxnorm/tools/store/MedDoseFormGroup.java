/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.tools.store;

import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.db.api.DbConnection;

class MedDoseFormGroup {
    private static final Log log = LogFactory.getLog(MedDoseFormGroup.class);

    private static final String INSERT_MED_DOSE_FORM_GROUP = "INSERT INTO MED_DOSE_FORM_GROUP(DOSE_FORM_GROUP_RXCUI, DOSE_FORM_GROUP_NAME) VALUES (?, ?)";
    private static final String DELETE_MED_DOSE_FORM_GROUP = "DELETE FROM MED_DOSE_FORM_GROUP";
    private static final String MED_DOSE_FORM_GROUP_SELECT_DISTINCT_DOSE_FORM_GROUP_RXCUI = "SELECT DISTINCT DOSE_FORM_GROUP_RXCUI FROM MED_DOSE_FORM_GROUP";
    
    static final String COL_DOSE_FORM_GROUP_NAME = "DOSE_FORM_GROUP_NAME";
    private static final String COL_DOSE_FORM_GROUP_RXCUI = "DOSE_FORM_GROUP_RXCUI";


    static void insertWithoutCommit(DbConnection conn, String doseFormGroupRxCui, String doseFormGroupName) {
        conn.executeUpdateNoCommit(INSERT_MED_DOSE_FORM_GROUP, ps -> {
            ps.setString(1, doseFormGroupRxCui);
            ps.setString(2, doseFormGroupName);
        });
    }
    
    static void delete(DbConnection conn) {
        conn.executeUpdate(DELETE_MED_DOSE_FORM_GROUP);
    }

    static void getDoseFormGroup(DbConnection conn, Consumer<String> resultConsumer) {
        conn.executeQuery(MED_DOSE_FORM_GROUP_SELECT_DISTINCT_DOSE_FORM_GROUP_RXCUI, rsIter -> {
           rsIter.forEach(r -> {
               resultConsumer.accept(r.getString(COL_DOSE_FORM_GROUP_RXCUI));
           }); 
        });
    }


}
