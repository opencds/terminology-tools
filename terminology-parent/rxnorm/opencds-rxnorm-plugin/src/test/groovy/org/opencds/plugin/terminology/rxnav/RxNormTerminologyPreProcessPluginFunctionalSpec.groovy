/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.plugin.terminology.rxnav

import org.opencds.plugin.terminology.rxnorm.RxNormTerminologyPreProcessPlugin

import java.sql.SQLException

import org.opencds.plugin.api.PluginDataCache
import org.opencds.plugin.api.SupportingData
import org.opencds.plugin.api.SupportingDataPackage
import org.opencds.plugin.support.PreProcessPluginContextImpl
import org.opencds.plugin.support.PluginDataCacheImpl
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug

import spock.lang.Specification

class RxNormTerminologyPreProcessPluginFunctionalSpec extends Specification {
    private static final String DBFILENAME = '/Users/phillip/tmp/terminology/LocalDataStore_RxNav_OpioidCds.accdb'

    def 'test execute method'() {
        given:
        RxNormTerminologyPreProcessPlugin plugin = new RxNormTerminologyPreProcessPlugin()
        Map allFactLists = [:]
        Map namedObjects = [:]
        Map globals = [:]
        Map supportingData = [:]
        var supplier = () -> SupportingDataPackage.create(
                () -> new File(DBFILENAME),
                () -> new File(DBFILENAME).text.bytes)
        PluginDataCache cache = new PluginDataCacheImpl()
        SupportingData sd = SupportingData.create('id', 'kmid', 'loadedby', DBFILENAME, 'access', new Date(), supplier)
        supportingData.put(RxNormTerminologyPreProcessPlugin.ACCESS_DB_ID, sd)
        PreProcessPluginContextImpl context = PreProcessPluginContextImpl.create(
                supportingData as Map<String, SupportingData>,
                cache,
                allFactLists as Map<Class<?>, List<?>>,
                namedObjects as Map<String, Object>,
                globals as Map<String, Object>)

        when:
        plugin.execute(context)
        Map<String, Object> rxNavGlobals = globals.get(RxNormTerminologyPreProcessPlugin.RXNAV_GLOBALS) as Map<String, Object>
        RxNormSemanticDrug drug = getDrug(rxNavGlobals, "1043567")
        RxNormSemanticDrug drug2 = getDrug(rxNavGlobals, "977874")

        then:
        notThrown(Exception)
        drug == null
        !isDrugInValueSet(rxNavGlobals, "1043567", 1)
        getDrugsInValueSet(rxNavGlobals, 1).size() == 1094
        getRxNormConceptsOfDrugsInValueSet(rxNavGlobals, 1).size() == 1094
        drug2
        isDrugInValueSet(rxNavGlobals, "977874", 1)
        getDrugsInValueSet(rxNavGlobals, 2).size() == 253
        getRxNormConceptsOfDrugsInValueSet(rxNavGlobals, 2).size() == 253
    }


    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    Set<RxNormSemanticDrug> getDrugsInValueSet(Map globals, int valueSetId) {
        return globals.myValueSetIdToSemanticDrugSetMap.get(Integer.valueOf(valueSetId))
    }

    /**
     * Returns members of value set. Returns null if not found.
     *
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    Set<RxNormConcept> getRxNormConceptsOfDrugsInValueSet(Map globals, int valueSetId) {
        return globals.myValueSetIdToSemanticDrugRxNormConceptSetMap.get(Integer.valueOf(valueSetId))
    }

    /**
     * Returns true if drug in value set; returns false otherwise.
     *
     * @param semanticDrugRxCui
     * @param valueSetId
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    boolean isDrugInValueSet(Map globals, String semanticDrugRxCui, int valueSetId) {
        Set<RxNormConcept> rxNormConceptsOfDrugsInValueSet = globals.myValueSetIdToSemanticDrugRxNormConceptSetMap
                .get(Integer.valueOf(valueSetId))
        if (rxNormConceptsOfDrugsInValueSet != null) {
            // only RxCui used for equivalency testing
            if (rxNormConceptsOfDrugsInValueSet.contains(new RxNormConcept(semanticDrugRxCui))) {
                return true
            }
        }
        return false
    }

    /**
     * Returns null if drug cannot be found.
     *
     * @param semanticDrugRxCui
     * @return
     * @throws FileNotFoundException
     * @throws SQLException
     */
    RxNormSemanticDrug getDrug(Map globals, String semanticDrugRxCui) {
        return globals.mySemanticDrugRxCuiToSemanticDrugMap.get(semanticDrugRxCui)
    }


}
