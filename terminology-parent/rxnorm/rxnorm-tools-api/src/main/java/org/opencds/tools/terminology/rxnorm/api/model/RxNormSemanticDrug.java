/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.api.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Semantic Clinic Drug (SCD) or Semantic Branded Drug (SBD).
 *
 */
public class RxNormSemanticDrug extends RxNormConcept {
    private static final Set<TermTypeConcept> supportedTtys = new HashSet<>(
            Arrays.asList(
                    TermTypeConcept.SCD,
                    TermTypeConcept.GPCK,
                    TermTypeConcept.SBD,
                    TermTypeConcept.BPCK
            ));

    private RxNormConcept doseFormConcept;
    private Set<RxNormConcept> doseFormGroupConcepts = new HashSet<>();
    private Set<RxNormSemanticDrugComponent> semanticClinicalDrugComponents = new HashSet<>();

    public RxNormSemanticDrug(String rxCui, String name, TermTypeConcept tty) {
        super(rxCui, name, tty);
    }

    @Deprecated
    public RxNormConcept getRxNormConcept() {
        return this;
    }

    public RxNormConcept getDoseFormConcept() {
        return doseFormConcept;
    }

    public void setDoseFormConcept(RxNormConcept doseFormConcept) {
        this.doseFormConcept = doseFormConcept;
    }

    public Set<RxNormConcept> getDoseFormGroupConcepts() {
        return doseFormGroupConcepts;
    }

    public void addDoseFormGroupConcept(RxNormConcept doseFormGroupConcept) {
        this.doseFormGroupConcepts.add(doseFormGroupConcept);
    }

    public Set<RxNormSemanticDrugComponent> getSemanticClinicalDrugComponents() {
        return semanticClinicalDrugComponents;
    }

    public void addSemanticClinicalDrugComponent(RxNormSemanticDrugComponent semanticClinicalDrugComponent) {
        this.semanticClinicalDrugComponents.add(semanticClinicalDrugComponent);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().appendSuper(super.hashCode()).append(doseFormConcept).append(doseFormGroupConcepts)
                .append(semanticClinicalDrugComponents).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RxNormSemanticDrug rhs = (RxNormSemanticDrug) obj;
        return new EqualsBuilder().appendSuper(super.equals(rhs)).append(doseFormConcept, rhs.doseFormConcept)
                .append(doseFormGroupConcepts, rhs.doseFormGroupConcepts)
                .append(semanticClinicalDrugComponents, rhs.semanticClinicalDrugComponents).isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(doseFormConcept).append(doseFormGroupConcepts).append(semanticClinicalDrugComponents).build();
    }
}
