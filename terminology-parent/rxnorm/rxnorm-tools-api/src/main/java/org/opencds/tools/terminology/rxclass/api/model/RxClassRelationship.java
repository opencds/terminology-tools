package org.opencds.tools.terminology.rxclass.api.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;

public class RxClassRelationship {
    private final RxNormConcept rxNormConcept;
    private final RxClassConcept rxClassConcept;
    private final String rela;
    private final String relaSource;

    public RxClassRelationship(RxNormConcept rxNormConcept,
                               RxClassConcept rxClassConcept,
                               String rela,
                               String relaSource) {
        this.rxNormConcept = rxNormConcept;
        this.rxClassConcept = rxClassConcept;
        this.rela = rela;
        this.relaSource = relaSource;
    }

    public RxNormConcept getRxNormConcept() {
        return rxNormConcept;
    }

    public RxClassConcept getRxClassConcept() {
        return rxClassConcept;
    }

    public String getRela() {
        return rela;
    }

    public String getRelaSource() {
        return relaSource;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("rxNormConcept", rxNormConcept)
                .append("rxClassConcept", rxClassConcept)
                .append("rela", rela)
                .append("relaSource", relaSource)
                .toString();
    }
}
