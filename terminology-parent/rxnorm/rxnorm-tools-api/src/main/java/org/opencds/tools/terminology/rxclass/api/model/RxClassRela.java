package org.opencds.tools.terminology.rxclass.api.model;

public class RxClassRela {
    private String name;

    public RxClassRela(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
