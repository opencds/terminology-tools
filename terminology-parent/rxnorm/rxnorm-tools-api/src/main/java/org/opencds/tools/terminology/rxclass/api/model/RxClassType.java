package org.opencds.tools.terminology.rxclass.api.model;

public class RxClassType {
    private String name;

    public RxClassType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
