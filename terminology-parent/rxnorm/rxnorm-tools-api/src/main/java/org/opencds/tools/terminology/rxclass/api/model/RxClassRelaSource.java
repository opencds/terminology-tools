package org.opencds.tools.terminology.rxclass.api.model;

public class RxClassRelaSource {
    private String name;

    public RxClassRelaSource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
