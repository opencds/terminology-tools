package org.opencds.tools.terminology.rxclass.api.model;

public class RxClassConcept {
    private String classId;
    private String className;
    private String classType;

    public RxClassConcept(String classId, String className, String classType) {
        this.classId = classId;
        this.className = className;
        this.classType = classType;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }
}
