/*
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.api.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.constants.CodeSystemEnum;
import org.opencds.tools.terminology.api.model.ConceptReferenceImpl;

public class RxNormClassIdAndName extends ConceptReferenceImpl {
    private static final Log log = LogFactory.getLog(RxNormClassIdAndName.class);

    public enum TermTypeConcept {
        IN("Ingredient"),
        PIN("Precise Ingredient"),
        MIN("Multiple Ingredients"),
        SCDC("Semantic Clinical Drug Component"),
        SCDF("Semantic Clinical Drug Form"),
        SCDG("Semantic Clinical Dose Form Group"),
        SCD("Semantic Clinical Drug"),
        GPCK("Generic Pack"),
        BN("Brand Name"),
        SBDC("Semantic Branded Drug Component"),
        SBDF("Semantic Branded Drug Form"),
        SBDG("Semantic Branded Dose Form Group"),
        SBD("Semantic Branded Drug"),
        BPCK("Brand Name Pack"),
        PSN("Prescribable Name"),
        SY("Synonym"),
        TMSY("Tall Man Lettering Synonym"),
        DF("Dose Form"),
        ET("Dose Form Entry Term"),
        DFG("Dose Form Group");

        private final String name;

        TermTypeConcept(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static TermTypeConcept byTTY(String tty) {
            try {
                return valueOf(tty.toUpperCase());
            } catch (Exception e) {
                log.warn("TermType not supported or is null: " + tty);
            }
            return null;
        }

    }

    private TermTypeConcept termTypeConcept;

    public RxNormClassIdAndName(String rxCui) {
        super(CodeSystemEnum.RXNORM, rxCui);
    }

    public RxNormClassIdAndName(String rxCui, String name) {
        super(CodeSystemEnum.RXNORM, rxCui, name);
    }

    public RxNormClassIdAndName(String rxCui, String name, TermTypeConcept termTypeConcept) {
        super(CodeSystemEnum.RXNORM, rxCui, name);
        this.termTypeConcept = termTypeConcept;
    }

    public String getRxCui() {
        return getCode();
    }

    public String getName() {
        return getPreferredName();
    }

    public void setName(String name) {
        super.setPreferredName(name);
    }

    public TermTypeConcept getTermTypeConcept() {
        return termTypeConcept;
    }

    public void setTermTypeConcept(TermTypeConcept termTypeConcept) {
        this.termTypeConcept = termTypeConcept;
    }

    // equivalency tested by only the RxCUI
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(termTypeConcept).build();
    }

}
