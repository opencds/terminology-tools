/*
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.rxnorm.api.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.constants.CodeSystemEnum;
import org.opencds.tools.terminology.api.model.ConceptReferenceImpl;

public class RxNormClassTypeAndRelationship extends ConceptReferenceImpl {
    private static final Log log = LogFactory.getLog(RxNormClassTypeAndRelationship.class);

    public enum ClassTypeConcept {
        ATC("Anatomical Therapeutic Chemical"),
        DAILYMED("Precise Ingredient"),
        EPC("Established Pharmacologic Classes (EPC) from DailyMed"),
        MoA("Mechanism of Action"),
        MEDRT("Diseases, Life Phases and Physiologic States"),
        MESHPA("MeSH Pharmacologic Classes (EPC) from DailyMed"),
        PE("Physiologic Effect"),
        PK("Pharmacokinetics"),
        TC("Therapeutic Categories"),
        VA("VA Classes");

        private final String name;

        ClassTypeConcept(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static ClassTypeConcept byRelationship(String rela) {
            try {
                return valueOf(rela.toUpperCase());
            } catch (Exception e) {
                log.warn("TypeConcept not supported or is null: " + rela);
            }
            return null;
        }

    }

    private ClassTypeConcept classTypeConcept;

    public RxNormClassTypeAndRelationship(String rxCui) {
        super(CodeSystemEnum.RXNORM, rxCui);
    }

    public RxNormClassTypeAndRelationship(String rxCui, String name) {
        super(CodeSystemEnum.RXNORM, rxCui, name);
    }

    public RxNormClassTypeAndRelationship(String rxCui, String name, ClassTypeConcept termTypeConcept) {
        super(CodeSystemEnum.RXNORM, rxCui, name);
        this.classTypeConcept = termTypeConcept;
    }

    public String getRxCui() {
        return getCode();
    }

    public String getName() {
        return getPreferredName();
    }

    public void setName(String name) {
        super.setPreferredName(name);
    }

    public ClassTypeConcept getTermTypeConcept() {
        return classTypeConcept;
    }

    public void setTermTypeConcept(ClassTypeConcept termTypeConcept) {
        this.classTypeConcept = termTypeConcept;
    }

    // equivalency tested by only the RxCUI
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(classTypeConcept).build();
    }

}
