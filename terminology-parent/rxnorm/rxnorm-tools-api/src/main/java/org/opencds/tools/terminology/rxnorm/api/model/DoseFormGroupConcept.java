package org.opencds.tools.terminology.rxnorm.api.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.LinkedHashSet;
import java.util.Set;

public class DoseFormGroupConcept extends RxNormConcept {

    private Set<RxNormConcept> doseForms = new LinkedHashSet<RxNormConcept>();

    public DoseFormGroupConcept(String rxCui) {
        super(rxCui);
    }

    public DoseFormGroupConcept(String rxCui, String name) {
        super(rxCui, name, TermTypeConcept.DFG);
    }

    public DoseFormGroupConcept(String rxCui, String name, TermTypeConcept termTypeConcept) {
        super(rxCui, name, termTypeConcept);
    }

    public void addDoseForms(Set<RxNormConcept> doseForms) {
        this.doseForms.addAll(doseForms);
    }

    public void addDoseForm(RxNormConcept doseForm) {
        doseForms.add(doseForm);
    }

    public Set<RxNormConcept> getDoseForms() {
        return doseForms;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(super.getTermTypeConcept()).append(doseForms).build();
    }

}
