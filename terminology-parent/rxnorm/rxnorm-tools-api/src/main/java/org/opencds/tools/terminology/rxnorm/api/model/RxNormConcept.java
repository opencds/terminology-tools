/*
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.rxnorm.api.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.constants.CodeSystemEnum;
import org.opencds.tools.terminology.api.model.ConceptReferenceImpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class RxNormConcept extends ConceptReferenceImpl {
    private static final Log log = LogFactory.getLog(RxNormConcept.class);
    private static final RxNormConcept EMPTY = new RxNormConcept();

    public static RxNormConcept empty() {
        return EMPTY;
    }

    /**
     * TODO: This should be dynamic; retrieved from RxNorm
     */
    public enum TermTypeConcept {
        IN("Ingredient"),
        PIN("Precise Ingredient"),
        MIN("Multiple Ingredients"),
        SCDC("Semantic Clinical Drug Component"),
        SCDF("Semantic Clinical Drug Form"),
        SCDFP("Semantic Clinical Drug Form Precise"),
        SCDG("Semantic Clinical Dose Form Group"),
        SCDGP("Semantic Clinical Dose Form Group Precise"),
        SCD("Semantic Clinical Drug"),
        GPCK("Generic Pack"),
        BN("Brand Name"),
        SBDC("Semantic Branded Drug Component"),
        SBDF("Semantic Branded Drug Form"),
        SBDFP("Semantic Branded Drug Form Precise"),
        SBDG("Semantic Branded Dose Form Group"),
        SBD("Semantic Branded Drug"),
        BPCK("Brand Name Pack"),
        PSN("Prescribable Name"),
        SY("Synonym"),
        TMSY("Tall Man Lettering Synonym"),
        DF("Dose Form"),
        ET("Dose Form Entry Term"),
        DFG("Dose Form Group") {
            @Override
            public RxNormConcept createConcept(String rxCui, String name) {
                return new DoseFormGroupConcept(rxCui, name);
            }
        };

        public static final Set<TermTypeConcept> allIngredients = Collections.unmodifiableSet(
                new HashSet<>(Arrays.asList(IN, PIN, MIN, BN)));

        private final String name;

        TermTypeConcept(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static TermTypeConcept byTTY(String tty) {
            try {
                return valueOf(tty.toUpperCase());
            } catch (Exception e) {
                log.warn("TermType not supported or is null: " + tty
                        + ". Please report to maintainer.");
            }
            return null;
        }

        public RxNormConcept createConcept(String rxCui, String name) {
            return new RxNormConcept(rxCui, name, this);
        }

        public RxNormConcept createConcept(String rxCui, String name, String relaSource) {
            return new RxNormConcept(rxCui, name, this, relaSource);
        }

        public RxNormConcept createConcept(String rxCui, String name, String relaSource, String rela) {
            return new RxNormConcept(rxCui, name, this, relaSource, rela);
        }

    }

    private TermTypeConcept termTypeConcept;

    private String relaSource;

    private String rela;

    private RxNormConcept() {
        this(null, null);
    }

    public RxNormConcept(String rxcui) {
        this(rxcui, null);
    }

    public RxNormConcept(String rxcui, String name) {
        this(rxcui, name, null);
    }

    public RxNormConcept(String rxcui, String name, TermTypeConcept termTypeConcept) {
        this(rxcui, name, termTypeConcept, null);
    }

    public RxNormConcept(String rxcui, String name, TermTypeConcept termTypeConcept, String relaSource) {
        this(rxcui, name, termTypeConcept, relaSource, null);
    }

    public RxNormConcept(String rxcui, String name, TermTypeConcept termTypeConcept, String relaSource, String rela) {
        super(CodeSystemEnum.RXNORM, rxcui, name);
        this.termTypeConcept = termTypeConcept;
        this.relaSource = relaSource;
        this.rela = rela;
    }

    public String getRxcui() {
        return getCode();
    }

    public String getName() {
        return getPreferredName();
    }

    public void setName(String name) {
        super.setPreferredName(name);
    }

    public TermTypeConcept getTermTypeConcept() {
        return termTypeConcept;
    }

    public void setTermTypeConcept(TermTypeConcept termTypeConcept) {
        this.termTypeConcept = termTypeConcept;
    }

    public String getRelaSource() {
        return relaSource;
    }

    public void setRelaSource(String relaSource) {
        this.relaSource = relaSource;
    }

    public String getRela() {
        return rela;
    }

    public void setRela(String rela) {
        this.rela = rela;
    }

    // equivalency tested by only the RxCUI
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).appendSuper(super.toString())
                .append(termTypeConcept).build();
    }

}
