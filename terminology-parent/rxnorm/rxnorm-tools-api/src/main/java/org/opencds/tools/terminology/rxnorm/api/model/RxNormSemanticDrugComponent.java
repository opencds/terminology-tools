/**
 * Copyright 2017-2018 OpenCDS.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencds.tools.terminology.rxnorm.api.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * Semantic Drug Component (SCDC | SBDC).
 */
public class RxNormSemanticDrugComponent
{
	private RxNormConcept rxNormConcept;
	private String strength;
	private Double strengthValue; // derived from myStrengthStr. null if cannot be done.
	private String strengthUnit; // derived from myStrengthStr
	private List<RxNormConcept> ingredients = new ArrayList<>();

	public RxNormSemanticDrugComponent() {}

	public RxNormSemanticDrugComponent(String rxCui, String name, RxNormConcept.TermTypeConcept termTypeConcept) {
		this(new RxNormConcept(rxCui, name, termTypeConcept));
	}

	public RxNormSemanticDrugComponent(RxNormConcept rxNormConcept)
	{
		this.rxNormConcept = rxNormConcept;
	}

	public RxNormConcept getRxNormConcept()
	{
		return rxNormConcept;
	}

	public void setRxNormConcept(RxNormConcept rxNormConcept)
	{
		this.rxNormConcept = rxNormConcept;
	}


	public String getRxCui() {
		return rxNormConcept.getRxcui();
	}

	public String getName() {
		return rxNormConcept.getPreferredName();
	}

	public RxNormConcept.TermTypeConcept getTermTypeConcept() {
		return rxNormConcept.getTermTypeConcept();
	}

	public String getStrengthStr()
	{
		return strength;
	}

//	public void setStrengthStr(String strengthStr) {
//	    myStrengthStr = strengthStr;
//    }

	// TODO: See RxNormClient for a possibly better way.  Test for consistency.
	public void setStrengthStr(String strengthStr) {
		this.strength = strengthStr;
		if (strengthStr != null) {
			try {
				int spaceIndex = strengthStr.indexOf(" ");
				strengthValue = Double.parseDouble(strengthStr.substring(0, spaceIndex));
				strengthUnit = strengthStr.substring(spaceIndex + 1);
			} catch (Exception e) {
				System.out.println("> Unable to parse strength string (" + strengthStr + ") and store as discrete value and units; ignoring...");
			}
		}
	}

	public Double getStrengthValue()
	{
		return strengthValue;
	}

	public String getStrengthUnit()
	{
		return strengthUnit;
	}

	public List<RxNormConcept> getIngredients()
	{
		return ingredients;
	}

	public void setIngredients(List<RxNormConcept> ingredients) {
		this.ingredients = ingredients;
	}

	public void addIngredient(RxNormConcept ingredient) {
		ingredients.add(ingredient);
	}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder()
	            .append(ingredients)
	            .append(rxNormConcept)
	            .append(strength)
	            .append(strengthUnit)
	            .append(strengthValue)
	            .build();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RxNormSemanticDrugComponent rhs = (RxNormSemanticDrugComponent) obj;
		return new EqualsBuilder()
                .append(ingredients, rhs.ingredients)
                .append(rxNormConcept, rhs.rxNormConcept)
                .append(strength, rhs.strength)
                .append(strengthUnit, rhs.strengthUnit)
                .append(strengthValue, rhs.strengthValue)
		        .build();
	}

	@Override
	public String toString() {
	    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append(ingredients)
                .append(rxNormConcept)
                .append(strength)
                .append(strengthUnit)
                .append(strengthValue)
	            .build();
	}
}

