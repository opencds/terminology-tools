package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class ConceptGroup {
    private String tty;
    private List<Properties> conceptProperties;

    public String getTty() {
        return tty;
    }

    public List<Properties> getConceptProperties() {
        if (conceptProperties == null) {
            return Collections.emptyList();
        }
        return conceptProperties;
    }
}
