package org.opencds.tools.terminology.rxclass.tools.model;

import org.opencds.tools.terminology.rxnorm.tools.model.MinConcept;

public class RxclassDrugInfo {
    private MinConcept minConcept;
    private RxclassMinConceptItem rxclassMinConceptItem;
    private String rela;
    private String relaSource;

    public MinConcept getMinConcept() {
        return minConcept;
    }

    public RxclassMinConceptItem getRxclassMinConceptItem() {
        return rxclassMinConceptItem;
    }

    public String getRela() {
        return rela;
    }

    public String getRelaSource() {
        return relaSource;
    }
}
