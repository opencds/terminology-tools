package org.opencds.tools.terminology.rxclass.tools.model;

import org.opencds.tools.terminology.rxnorm.tools.model.MinConcept;

import java.util.Collections;
import java.util.List;

public class DrugMember {
    private MinConcept minConcept;
    private List<NodeAttr> nodeAttr;

    public MinConcept getMinConcept() {
        return minConcept;
    }

    public List<NodeAttr> getNodeAttr() {
        if (nodeAttr == null) {
            return Collections.emptyList();
        }
        return nodeAttr;
    }
}
