package org.opencds.tools.terminology.rxnorm.tools.model;

public class Candidate {
    private String rxcui;
    private String rxaui;
    private String score;
    private String rank;

    public String getRxcui() {
        return rxcui;
    }

    public String getRxaui() {
        return rxaui;
    }

    public String getScore() {
        return score;
    }

    public String getRank() {
        return rank;
    }
}
