package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class TermTypeList {
    private List<String> termType;

    public List<String> getTermType() {
        if (termType == null) {
            return Collections.emptyList();
        }
        return termType;
    }
}
