package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.List;

public class RxClassResponseContent {
    private ClassPathList classPathList;
    private ClassTypeList classTypeList;
    private DrugMemberGroup drugMemberGroup;
    private RxclassMinConceptList rxclassMinConceptList;
    private RelaSourceList relaSourceList;
    private RelaList relaList;
    private RxclassDrugInfoList rxclassDrugInfoList;
    private List<RxclassTree> rxclassTree;
    private String relaSourceVersion;

    public ClassPathList getClassPathList() {
        return classPathList;
    }

    public ClassTypeList getClassTypeList() {
        return classTypeList;
    }

    public DrugMemberGroup getDrugMemberGroup() {
        return drugMemberGroup;
    }

    public RxclassMinConceptList getRxclassMinConceptList() {
        return rxclassMinConceptList;
    }

    public RelaSourceList getRelaSourceList() {
        return relaSourceList;
    }

    public RelaList getRelaList() {
        return relaList;
    }

    public RxclassDrugInfoList getRxclassDrugInfoList() {
        return rxclassDrugInfoList;
    }

    public List<RxclassTree> getRxclassTree() {
        return rxclassTree;
    }

    public String getRelaSourceVersion() {
        return relaSourceVersion;
    }
}
