package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class AllRelatedGroup {
    private String rxcui;
    private List<ConceptGroup> conceptGroup;

    public String getRxcui() {
        return rxcui;
    }

    public List<ConceptGroup> getConceptGroup() {
        if (conceptGroup == null) {
            return Collections.emptyList();
        }
        return conceptGroup;
    }
}
