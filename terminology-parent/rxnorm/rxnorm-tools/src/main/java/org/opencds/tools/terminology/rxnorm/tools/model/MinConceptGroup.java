package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class MinConceptGroup {
    private List<MinConcept> minConcept;

    public List<MinConcept> getMinConcept() {
        if (minConcept == null) {
            return Collections.emptyList();
        }
        return minConcept;
    }
}
