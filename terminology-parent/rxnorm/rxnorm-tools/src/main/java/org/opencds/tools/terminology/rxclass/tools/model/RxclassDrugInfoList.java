package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class RxclassDrugInfoList {
    private List<RxclassDrugInfo> rxclassDrugInfo;

    public List<RxclassDrugInfo> getRxclassDrugInfo() {
        if (rxclassDrugInfo == null) {
            return Collections.emptyList();
        }
        return rxclassDrugInfo;
    }
}
