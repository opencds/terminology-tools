package org.opencds.tools.terminology.rxnorm.tools.client;


import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.rxnorm.api.model.DoseFormGroupConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrug;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormSemanticDrugComponent;
import org.opencds.tools.terminology.rxnorm.tools.model.AllRelatedGroup;
import org.opencds.tools.terminology.rxnorm.tools.model.ApproximateGroup;
import org.opencds.tools.terminology.rxnorm.tools.model.Candidate;
import org.opencds.tools.terminology.rxnorm.tools.model.IdGroup;
import org.opencds.tools.terminology.rxnorm.tools.model.MinConcept;
import org.opencds.tools.terminology.rxnorm.tools.model.MinConceptGroup;
import org.opencds.tools.terminology.rxnorm.tools.model.PropConcept;
import org.opencds.tools.terminology.rxnorm.tools.model.Properties;
import org.opencds.tools.terminology.rxnorm.tools.model.RelatedGroup;
import org.opencds.tools.terminology.rxnorm.tools.model.RxNormResponseContent;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.idNameTransform;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.idType;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.propName;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.propNameTransform;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.relationType;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.relationTypeTransform;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.sourceNameTransform;
import static org.opencds.tools.terminology.rxnorm.tools.client.RxNormClientUtil.sourceType;

/**
 * TODO: Error handling after calling apiutil
 */
public class RxNormClient {
    private static final Set<RxNormConcept.TermTypeConcept> supportedDrugTermTypes = new HashSet<>(
            Arrays.asList(
                    RxNormConcept.TermTypeConcept.SBD,
                    RxNormConcept.TermTypeConcept.SCD,
                    RxNormConcept.TermTypeConcept.BPCK,
                    RxNormConcept.TermTypeConcept.GPCK));
    private static final Set<RxNormConcept.TermTypeConcept> ingredients = new HashSet<>(
            Arrays.asList(
                    RxNormConcept.TermTypeConcept.IN,
                    RxNormConcept.TermTypeConcept.MIN,
                    RxNormConcept.TermTypeConcept.PIN,
                    RxNormConcept.TermTypeConcept.BN));

    private static final String[] ingredientsArray = ingredients.stream()
            .map(Enum::toString)
            .collect(Collectors.toSet())
            .toArray(new String[0]);

    private static final String STRENGTH = "STRENGTH";

    private final RxNormApi api;


    public RxNormClient() {
        this.api = new RxNormApi(new RestClient());
    }

    public RxNormClient(RxNormApi api) {
        this.api = api;
    }

    public RxNormClient(RestClient restClient) {
        this.api = new RxNormApi(restClient);
    }

    public String getRxNormVersion() {
        var response = api.getRxNormVersion();
        var content = response.getContent();
        return content.getVersion();

    }

    public RxNormConcept getIngredient(String rxCui) {
        var response = api.getRxConceptProperties(rxCui);
        var content = response.getContent();
        Properties props = content.getProperties();
        RxNormConcept concept = null;
        if (props != null) {
            RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(props.getTty());
            if (ingredients.contains(tty)) {
                concept = new RxNormConcept(props.getRxcui(), props.getName(), tty);
            }
        }
        return concept;
    }

    public Set<RxNormConcept> getIngredientsByName(String name) {
        var response = api.findRxcuiByString(name);
        var content = response.getContent();
        IdGroup ig = content.getIdGroup();
        if (ig == null) {
            return Collections.emptySet();
        }
        return ig.getRxnormId().parallelStream()
                .map(this::getIngredient)
                .collect(Collectors.toSet());

    }


    /**
     * @param ingredientName
     * @return
     */
    public Set<RxNormConcept> findIngredients(String ingredientName) {
        var response = api.getApproximateMatch(ingredientName);
        var content = response.getContent();
        ApproximateGroup ag = content.getApproximateGroup();
        if (ag == null) {
            return Collections.emptySet();
        }
        Set<String> rxcuis = ag.getCandidate().stream()
                .map(Candidate::getRxcui)
                .collect(Collectors.toSet());
        return rxcuis.stream()
                .map(api::getRxConceptProperties)
                .map(RxNormResponse::getContent)
                .map(RxNormResponseContent::getProperties)
                .filter(props -> ingredients.contains(RxNormConcept.TermTypeConcept.byTTY(props.getTty())))
                .map(this::createConcept)
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getPropNames() {
        return process(
                api.getPropNames(),
                propName,
                transform(propNameTransform));
    }

    public Set<RxNormConcept> getRelaTypes() {
        return process(
                api.getRelaTypes(),
                relationType,
                transform(relationTypeTransform));
    }

    public Set<RxNormConcept> getSourceTypes() {
        return process(
                api.getSourceTypes(),
                sourceType,
                transform(sourceNameTransform));
    }

    /*
     * Dose Form Groups and Dose Forms
     */

    public Set<RxNormConcept> getDoseFormsOfDoseFormGroup(String rxcui) {
        var response = api.getRelatedByRelationship(
                rxcui, "doseformgroup_of", "inverse_isa");
        var content = response.getContent();
        RelatedGroup relatedGroup = content.getRelatedGroup();
        if (relatedGroup == null) {
            return Collections.emptySet();
        }
        return relatedGroup.getConceptGroup().stream()
                .filter(group ->
                        RxNormConcept.TermTypeConcept.byTTY(group.getTty())
                                == RxNormConcept.TermTypeConcept.DF)
                .flatMap(group -> group.getConceptProperties().stream())
                .map(props ->
                        new RxNormConcept(
                                props.getRxcui(),
                                props.getName(),
                                RxNormConcept.TermTypeConcept.byTTY(props.getTty())))
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getDoseForms() {
        var response = api.getAllConceptsByTTY("DF");
        var content = response.getContent();
        MinConceptGroup minConceptGroup = content.getMinConceptGroup();
        if (minConceptGroup == null) {
            return Collections.emptySet();
        }
        return minConceptGroup.getMinConcept().stream()
                .map(mc -> new RxNormConcept(
                        mc.getRxcui(),
                        mc.getName(),
                        RxNormConcept.TermTypeConcept.byTTY(mc.getTty())))
                .collect(Collectors.toSet());
    }

    public Set<DoseFormGroupConcept> getDoseFormGroups() {
        var response = api.getAllConceptsByTTY("DFG");
        var content = response.getContent();
        MinConceptGroup minConceptGroup = content.getMinConceptGroup();
        if (minConceptGroup == null) {
            return Collections.emptySet();
        }
        return minConceptGroup.getMinConcept().stream()
                .map(mc -> new DoseFormGroupConcept(
                        mc.getRxcui(),
                        mc.getName(),
                        RxNormConcept.TermTypeConcept.byTTY(mc.getTty())))
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getIdTypes() {
        return process(
                api.getIdTypes(),
                idType,
                transform(idNameTransform));
    }

    public Set<RxNormConcept> getAllIngredients() {
        var response = api.getAllConceptsByTTY(ingredientsArray);
        var content = response.getContent();
        MinConceptGroup minConceptGroup = content.getMinConceptGroup();
        if (minConceptGroup == null) {
            return Collections.emptySet();
        }
        return minConceptGroup.getMinConcept().stream()
                .map(mc -> new RxNormConcept(
                        mc.getRxcui(),
                        mc.getName(),
                        RxNormConcept.TermTypeConcept.byTTY(mc.getTty())))
                .collect(Collectors.toSet());
    }

    /*
     * Drugs
     */

    public Set<RxNormConcept> getAllSemanticDrugs() {
        var response = api.getAllConceptsByTTY("SBD", "SCD", "GPCK", "BPCK");
        var content = response.getContent();
        if (content.getMinConceptGroup() == null) {
            return Collections.emptySet();
        }
        return content.getMinConceptGroup().getMinConcept().stream()
                .map(this::createConcept)
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getAllSemanticDrugComponents() {
        var response = api.getAllConceptsByTTY("SBDC", "SCDC");
        var content = response.getContent();
        if (content.getMinConceptGroup() == null) {
            return Collections.emptySet();
        }
        return content.getMinConceptGroup().getMinConcept().stream()
                .map(this::createConcept)
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getDrugsContainingIngredient(String ingredientRxCui) {
        var response = api.getRelatedByRelationship(ingredientRxCui, "SBD", "SCD");
        var content = response.getContent();
        RelatedGroup relatedGroup = content.getRelatedGroup();
        if (relatedGroup == null) {
            return Collections.emptySet();
        }
        return relatedGroup.getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createConcept)
                .collect(Collectors.toSet());
    }

    public Map<RxNormConcept.TermTypeConcept, Set<RxNormConcept>> getAllRelatedConcepts(String rxCui) {
        Map<RxNormConcept.TermTypeConcept, Set<RxNormConcept>> concepts = new HashMap<>();

        var response = api.getAllRelatedInfo(rxCui);
        var content = response.getContent();
        AllRelatedGroup allRelatedGroup = content.getAllRelatedGroup();
        return allRelatedGroup.getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createConcept)
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(
                        RxNormConcept::getTermTypeConcept,
                        HashMap::new,
                        Collectors.mapping(
                                Function.identity(),
                                Collectors.toSet())));
    }

    public Set<RxNormConcept> getIngredientsFromMIN(String rxcui) {
        var response = api.getRelatedByType(rxcui, RxNormConcept.TermTypeConcept.IN.toString());
        var content = response.getContent();
        RelatedGroup relatedGroup = content.getRelatedGroup();
        if (relatedGroup == null) {
            return Collections.emptySet();
        }
        return relatedGroup.getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createConcept)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public Set<RxNormConcept> getIngredientsForScdc(String rxCui) {
        var response = api.getRelatedByType(
                rxCui,
                ingredients.stream()
                        .map(RxNormConcept.TermTypeConcept::toString)
                        .collect(Collectors.toList()));
        var content = response.getContent();
        RelatedGroup relatedGroup = content.getRelatedGroup();
        return relatedGroup.getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createConcept)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public String getName(String rxcui) {
        var response = api.getRxConceptProperties(rxcui);
        var content = response.getContent();
        if (content.getProperties() == null) {
            return null;
        }
        return content.getProperties().getName();
    }

    /**
     * Returns set of Semantic Drugs (SCD or SBD) with the ingredient.
     *
     * @param ingredientRxCui
     * @return
     */
    public Set<RxNormSemanticDrug> getSemanticDrugsWithIngredient(String ingredientRxCui) {
//        Set<RxNormSemanticDrug> semanticDrugs = new HashSet<RxNormSemanticDrug>()

        var response = api.getAllRelatedInfo(ingredientRxCui);
        var content = response.getContent();
        if (content.getAllRelatedGroup() == null) {
            return Collections.emptySet();
        }
        return content.getAllRelatedGroup().getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .filter(this::acceptableSemanticDrugTty)
                .map(this::createDrugConcept)
                .peek(this::populateSemanticDrugAttributes)
                .collect(Collectors.toSet());
    }


    public Set<RxNormSemanticDrugComponent> getSDCs(String rxcui) {
        var response = api.getRelatedByType(rxcui, Arrays.asList("SCDC", "SBDC"));
        var content = response.getContent();
        if (content.getRelatedGroup() == null) {
            return Collections.emptySet();
        }
        return content.getRelatedGroup().getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createSDC)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public String getStrength(String rxcui) {
        var response = api.getRxProperty(rxcui, STRENGTH);
        var content = response.getContent();
        if (content.getPropConceptGroup() == null) {
            return null;
        }
        return content.getPropConceptGroup().getPropConcept().stream()
                .filter(pc -> STRENGTH.equalsIgnoreCase(pc.getPropName()))
                .map(PropConcept::getPropValue)
                .findFirst()
                .orElse(null);
    }

    public Set<RxNormConcept.TermTypeConcept> getTermTypes() {
        var response = api.getTermTypes();
        var content = response.getContent();
        return content.getTermTypeList().getTermType().stream()
                .map(RxNormConcept.TermTypeConcept::byTTY)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }


    private boolean acceptableSemanticDrugTty(Properties properties) {
        return supportedDrugTermTypes.contains(
                RxNormConcept.TermTypeConcept.byTTY(properties.getTty()));
    }


    /**
     * Populates attributes of semantic drug.  Includes DF, DFG, SCDC, SBDC.
     *
     * @param semanticDrug
     */
    private void populateSemanticDrugAttributes(RxNormSemanticDrug semanticDrug) {
        if (semanticDrug != null && StringUtils.isNotBlank(semanticDrug.getRxcui())) {
            var response = api.getAllRelatedInfo(semanticDrug.getRxcui());
            var content = response.getContent();
            if (content.getAllRelatedGroup() == null) {
                return;
            }
            content.getAllRelatedGroup().getConceptGroup().stream()
                    .flatMap(cg -> cg.getConceptProperties().stream())
                    .map(this::createConcept)
                    .forEach(concept -> {
                        if (concept.getTermTypeConcept() == RxNormConcept.TermTypeConcept.DF) {
                            semanticDrug.setDoseFormConcept(concept);
                        } else if (concept.getTermTypeConcept() == RxNormConcept.TermTypeConcept.DFG) {
                            semanticDrug.addDoseFormGroupConcept(concept);
                        } else if (concept.getTermTypeConcept() == RxNormConcept.TermTypeConcept.SCDC
                                || concept.getTermTypeConcept() == RxNormConcept.TermTypeConcept.SBDC) {
                            RxNormSemanticDrugComponent sdc = new RxNormSemanticDrugComponent(concept);
                            populateAttributes(sdc);
                            semanticDrug.addSemanticClinicalDrugComponent(sdc);
                        }
                    });
        }
    }


    /**
     * Returns semantic clinical drug component.  Includes namem strength and ingredients.
     *
     * @param sdc
     */
    private void populateAttributes(RxNormSemanticDrugComponent sdc) {
        var response = api.getAllRelatedInfo(sdc.getRxCui());
        var content = response.getContent();
        if (content.getAllRelatedGroup() == null) {
            return;
        }
        // find all ingredients
        content.getAllRelatedGroup().getConceptGroup().stream()
                .flatMap(cg -> cg.getConceptProperties().stream())
                .map(this::createConcept)
                .forEach(concept -> {
                    // TODO: What does it mean to keep a record of all associated ingredients?
                    if (ingredients.contains(concept.getTermTypeConcept())) {
                        sdc.addIngredient(concept);
                    }
                });


        // get strength
        response = api.getAllProperties(sdc.getRxCui(), Collections.singletonList("ATTRIBUTES"));
        content = response.getContent();
        Strength strength = new Strength();
        if (content.getPropConceptGroup() != null) {
            content.getPropConceptGroup().getPropConcept().stream()
                    .forEach(pc -> {
                        switch (pc.getPropName()) {
                            case "STRENGTH":
                                strength.setText(pc.getPropValue());
                                break;
                            case "AVAILABLE_STRENGTH": // some drugs don't have a STRENGTH, while some don't have either.
                                if (StringUtils.isBlank(strength.getText())) {
                                    strength.setText(pc.getPropValue());
                                }
                                break;
                            case "Numerator_Units":
                                strength.setUnitsNumerator(pc.getPropValue());
                                break;
                            case "Denominator_Units":
                                strength.setUnitsDenominator(pc.getPropValue());
                                break;
                            case "Numerator_Value":
                                strength.setValueNumerator(pc.getPropValue());
                                break;
                            case "Denominator_Value":
                                strength.setValueDenominator(pc.getPropValue());
                                break;
                            default:
                                break;
                        }
                    });

        }
        sdc.setStrengthStr(strength.getText());
        // TODO: See RxNormSemanticDrugComponent.setStrengthStr; reimpl with the details below?
//        scdcToReturn.setMyStrengthUnit((unitsNumerator ?: '') + ((unitsDenominator == '1' ? '' : '/' + unitsDenominator) ?: ''))
//        scdcToReturn.setMyStrengthValue(toDouble(valueNumerator, valueDenominator))
    }

    private static class Strength {
        private String text;
        private String unitsNumerator;
        private String unitsDenominator;
        private String valueNumerator;
        private String valueDenominator;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getUnitsNumerator() {
            return unitsNumerator;
        }

        public void setUnitsNumerator(String unitsNumerator) {
            this.unitsNumerator = unitsNumerator;
        }

        public String getUnitsDenominator() {
            return unitsDenominator;
        }

        public void setUnitsDenominator(String unitsDenominator) {
            this.unitsDenominator = unitsDenominator;
        }

        public String getValueNumerator() {
            return valueNumerator;
        }

        public void setValueNumerator(String valueNumerator) {
            this.valueNumerator = valueNumerator;
        }

        public String getValueDenominator() {
            return valueDenominator;
        }

        public void setValueDenominator(String valueDenominator) {
            this.valueDenominator = valueDenominator;
        }

    }


    private RxNormConcept createConcept(MinConcept minConcept) {
        if (minConcept == null) {
            return null;
        }
        RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(minConcept.getTty());
        if (tty == null) {
            return null;
        }
        return tty.createConcept(minConcept.getRxcui(), minConcept.getName());
    }

    private RxNormConcept createConcept(Properties props) {
        if (props == null) {
            return null;
        }
        RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(props.getTty());
        if (tty == null) {
            return null;
        }
        return tty.createConcept(props.getRxcui(), props.getName());
    }

    private RxNormSemanticDrug createDrugConcept(Properties props) {
        if (props == null) {
            return null;
        }
        RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(props.getTty());
        if (tty == null) {
            return null;
        }
        return new RxNormSemanticDrug(props.getRxcui(), props.getName(), tty);
    }

    private RxNormSemanticDrugComponent createSDC(Properties props) {
        if (props == null) {
            return null;
        }
        RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(props.getTty());
        if (tty == null) {
            return null;
        }
        return new RxNormSemanticDrugComponent(props.getRxcui(), props.getName(), tty);
    }

    private <X, T> Set<T> process(RxNormResponse response,
                                  Function<RxNormResponseContent, Stream<X>> contentFunction,
                                  Function<Stream<X>, Set<T>> streamProcessor) {
        return Optional.ofNullable(response)
                .filter(RxNormResponse::noError)
                .map(RxNormResponse::getContent)
                .map(contentFunction)
                .map(streamProcessor)
                .orElseGet(HashSet::new);
    }

    private <R, T> Function<Stream<R>, Set<T>> transform(Function<R, Optional<T>> transform) {
        return stream -> stream
                .map(transform)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }
}
