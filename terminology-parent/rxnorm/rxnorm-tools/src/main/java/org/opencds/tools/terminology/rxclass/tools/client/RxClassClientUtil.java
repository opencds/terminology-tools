package org.opencds.tools.terminology.rxclass.tools.client;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.rxclass.api.model.RxClassConcept;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRela;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRelaSource;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRelationship;
import org.opencds.tools.terminology.rxclass.api.model.RxClassType;
import org.opencds.tools.terminology.rxclass.tools.model.ClassTypeList;
import org.opencds.tools.terminology.rxclass.tools.model.DrugMember;
import org.opencds.tools.terminology.rxclass.tools.model.DrugMemberGroup;
import org.opencds.tools.terminology.rxclass.tools.model.RelaList;
import org.opencds.tools.terminology.rxclass.tools.model.RelaSourceList;
import org.opencds.tools.terminology.rxclass.tools.model.RxClassResponseContent;
import org.opencds.tools.terminology.rxclass.tools.model.RxclassDrugInfo;
import org.opencds.tools.terminology.rxclass.tools.model.RxclassDrugInfoList;
import org.opencds.tools.terminology.rxclass.tools.model.RxclassMinConcept;
import org.opencds.tools.terminology.rxclass.tools.model.RxclassMinConceptList;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.tools.model.MinConcept;
import org.opencds.tools.terminology.util.Functions;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.opencds.tools.terminology.util.Functions.stringFilterStream;

public class RxClassClientUtil {
    public static final Function<RxClassResponseContent, Stream<String>> classTypeName =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getClassTypeList)
                    .map(ClassTypeList::getClassTypeName)
                    .stream()
                    .flatMap(stringFilterStream);

    public static final Function<String, Optional<RxClassType>> classTypeNameTransform =
            classTypeName -> Optional.ofNullable(classTypeName)
                    .filter(StringUtils::isNotBlank)
                    .map(RxClassType::new);

    public static final Function<RxClassResponseContent, Stream<String>> relaName =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getRelaList)
                    .map(RelaList::getRela)
                    .stream()
                    .flatMap(stringFilterStream);

    public static final Function<String, Optional<RxClassRela>> relaNameTransform =
            classTypeName -> Optional.ofNullable(classTypeName)
                    .filter(StringUtils::isNotBlank)
                    .map(RxClassRela::new);

    public static final Function<RxClassResponseContent, Stream<String>> relaSourceName =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getRelaSourceList)
                    .map(RelaSourceList::getRelaSourceName)
                    .stream()
                    .flatMap(Functions.stringFilterStream);

    public static final Function<String, Optional<RxClassRelaSource>> relaSourceNameTransform =
            classTypeName -> Optional.ofNullable(classTypeName)
                    .filter(StringUtils::isNotBlank)
                    .map(RxClassRelaSource::new);

    public static final Function<RxClassResponseContent, Stream<RxclassDrugInfo>> rxclassDrugInfo =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getRxclassDrugInfoList)
                    .map(RxclassDrugInfoList::getRxclassDrugInfo)
                    .stream()
                    .flatMap(Collection::stream);

    public static final Function<RxclassDrugInfo, Optional<RxClassRelationship>> rxclassDrugInfoTransform =
            rxclassDrugInfo -> Optional.ofNullable(rxclassDrugInfo)
                    .map(drugInfo -> {
                        RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(drugInfo.getMinConcept().getTty());
                        return new RxClassRelationship(
                                new RxNormConcept(
                                        drugInfo.getMinConcept().getRxcui(),
                                        drugInfo.getMinConcept().getName(),
                                        tty),
                                new RxClassConcept(
                                        drugInfo.getRxclassMinConceptItem().getClassId(),
                                        drugInfo.getRxclassMinConceptItem().getClassName(),
                                        drugInfo.getRxclassMinConceptItem().getClassType()),
                                drugInfo.getRela(),
                                drugInfo.getRelaSource());
                    });

    public static final Function<RxClassResponseContent, Stream<MinConcept>> drugMemberMinConcept =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getDrugMemberGroup)
                    .map(DrugMemberGroup::getDrugMember)
                    .stream()
                    .flatMap(Collection::stream)
                    .map(DrugMember::getMinConcept);

    public static Function<MinConcept, Optional<RxNormConcept>> minConceptTransform(String relaSource, String rela) {
        return minConcept -> {
            if (minConcept == null) {
                return Optional.empty();
            }
            RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(minConcept.getTty());
            if (tty != null) {
                return Optional.of(tty.createConcept(minConcept.getRxcui(), minConcept.getName(), relaSource, rela));
            }
            return Optional.empty();
        };
    }

    public static Function<MinConcept, Optional<RxNormConcept>> minConceptTransform(String relaSource) {
        return minConcept -> {
            if (minConcept == null) {
                return Optional.empty();
            }
            RxNormConcept.TermTypeConcept tty = RxNormConcept.TermTypeConcept.byTTY(minConcept.getTty());
            if (tty != null) {
                return Optional.of(tty.createConcept(minConcept.getRxcui(), minConcept.getName(), relaSource));
            }
            return Optional.empty();
        };
    }

    public static final Function<RxClassResponseContent, Stream<RxclassMinConcept>> rxclassMinConcept =
            content -> Optional.ofNullable(content)
                    .map(RxClassResponseContent::getRxclassMinConceptList)
                    .map(RxclassMinConceptList::getRxclassMinConcept)
                    .stream()
                    .flatMap(Collection::stream);

    public static final Function<RxclassMinConcept, Optional<RxClassConcept>> rxclassMinConceptTransform =
            rxclassMinConcept -> Optional.ofNullable(rxclassMinConcept)
                    .map(mc -> new RxClassConcept(mc.getClassId(), mc.getClassName(), mc.getClassType()));

}
