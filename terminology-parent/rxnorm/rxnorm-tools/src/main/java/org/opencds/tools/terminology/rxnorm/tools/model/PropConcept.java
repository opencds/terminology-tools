package org.opencds.tools.terminology.rxnorm.tools.model;

public class PropConcept {
    private String propCategory;
    private String propName;
    private String propValue;

    public String getPropCategory() {
        return propCategory;
    }

    public String getPropName() {
        return propName;
    }

    public String getPropValue() {
        return propValue;
    }
}
