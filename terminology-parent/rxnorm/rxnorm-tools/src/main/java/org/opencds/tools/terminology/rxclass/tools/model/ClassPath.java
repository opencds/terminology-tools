package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class ClassPath {
    private List<RxclassMinConcept> rxclassMinConcept;

    public List<RxclassMinConcept> getRxclassMinConcept() {
        if (rxclassMinConcept.isEmpty()) {
            return Collections.emptyList();
        }
        return rxclassMinConcept;
    }
}
