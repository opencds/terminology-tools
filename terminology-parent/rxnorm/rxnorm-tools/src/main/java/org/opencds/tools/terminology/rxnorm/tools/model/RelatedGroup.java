package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class RelatedGroup {
    private String rxcui;
    private List<String> rela;
    private List<ConceptGroup> conceptGroup;

    public String getRxcui() {
        return rxcui;
    }

    public List<String> getRela() {
        return rela;
    }

    public List<ConceptGroup> getConceptGroup() {
        if (conceptGroup == null) {
            return Collections.emptyList();
        }
        return conceptGroup;
    }
}
