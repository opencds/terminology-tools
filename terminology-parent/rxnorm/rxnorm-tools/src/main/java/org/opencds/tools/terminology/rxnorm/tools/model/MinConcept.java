package org.opencds.tools.terminology.rxnorm.tools.model;

public class MinConcept {
    private String rxcui;
    private String name;
    private String tty;

    public String getRxcui() {
        return rxcui;
    }

    public String getName() {
        return name;
    }

    public String getTty() {
        return tty;
    }
}
