package org.opencds.tools.terminology.rxclass.tools.client;

import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.rxclass.api.model.RxClassConcept;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRela;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRelaSource;
import org.opencds.tools.terminology.rxclass.api.model.RxClassRelationship;
import org.opencds.tools.terminology.rxclass.api.model.RxClassType;
import org.opencds.tools.terminology.rxclass.tools.model.RxClassResponseContent;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.classTypeName;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.classTypeNameTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.drugMemberMinConcept;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.minConceptTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.relaName;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.relaNameTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.relaSourceName;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.relaSourceNameTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.rxclassDrugInfo;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.rxclassDrugInfoTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.rxclassMinConceptTransform;
import static org.opencds.tools.terminology.rxclass.tools.client.RxClassClientUtil.rxclassMinConcept;

public class RxClassClient {

    private final RxClassApi api;

    public RxClassClient() {
        this.api = new RxClassApi(new RestClient());
    }

    public RxClassClient(RxClassApi api) {
        this.api = api;
    }

    /**
     * <b>NOTE: </b> <tt>relation</tt> may be null or blank when the relationSource is not associated with a relation
     * (relaSource -> rela); otherwise no results are returned.
     *
     * @param classId        the classId
     * @param relationSource the relationship source (relaSource)
     * @param relation       the relation (rela)
     * @return members of the provided class/relaSource
     */
    public Set<RxNormConcept> getIngredientsInRxClass(String classId, String relationSource, String relation) {
        return process(
                api.getClassMembers(classId, relationSource, relation, RxClassApi.Transitive.INDIRECT_DIRECT),
                drugMemberMinConcept,
                transform(minConceptTransform(relationSource, relation)));
    }

    /**
     * <b>NOTE: </b> This method should only be used when the <tt>relaSource</tt> does not have one or more associated
     * <tt>rela</tt>s, otherwise no results are returned.
     *
     * @param classId        the classId
     * @param relationSource the relationship source (relaSource)
     * @return members of the provided class/relaSource
     */
    public Set<RxNormConcept> getIngredientsInRxClass(String classId, String relationSource) {
        return process(
                api.getClassMembers(classId, relationSource),
                drugMemberMinConcept,
                transform(minConceptTransform(relationSource)));
    }

    public Set<RxClassConcept> findClassesById(String classId) {
        return process(
                api.findClassesById(classId),
                rxclassMinConcept,
                transform(rxclassMinConceptTransform));
    }

    public Set<RxClassConcept> getAllClasses() {
        return process(
                api.getAllClasses(),
                rxclassMinConcept,
                transform(rxclassMinConceptTransform));
    }

    public Set<RxClassType> getClassTypes() {
        return process(
                api.getClassTypes(),
                classTypeName,
                transform(classTypeNameTransform));
    }

    public Set<RxClassRelationship> getClassByRxNormDrugId(String rxcui, Set<String> relas) {
        return process(
                api.getClassByRxNormDrugId(rxcui, relas),
                rxclassDrugInfo,
                transform(rxclassDrugInfoTransform));
    }

    public Set<RxClassRelationship> getClassByRxNormDrugId(String rxcui, String relaSource, Set<String> relas) {
        return process(
                api.getClassByRxNormDrugId(rxcui, relaSource, relas),
                rxclassDrugInfo,
                transform(rxclassDrugInfoTransform));
    }

    public Set<RxClassRelationship> getClassByRxNormDrugName(String name, Set<String> relas) {
        return process(
                api.getClassByRxNormDrugName(name, relas),
                rxclassDrugInfo,
                transform(rxclassDrugInfoTransform));
    }

    public Set<RxClassRelaSource> getSourcesOfDrugClassRelations() {
        return process(
                api.getSourcesOfDrugClassRelations(),
                relaSourceName,
                transform(relaSourceNameTransform));
    }

    public Set<RxClassRela> getRelas() {
        return process(
                api.getRelas(),
                relaName,
                transform(relaNameTransform));
    }

    public Set<RxClassRela> getRelas(String relaSource) {
        return process(
                api.getRelas(relaSource),
                relaName,
                transform(relaNameTransform));
    }

    private <X, T> Set<T> process(RxClassResponse response,
                                  Function<RxClassResponseContent, Stream<X>> contentFunction,
                                  Function<Stream<X>, Set<T>> streamProcessor) {
        return Optional.ofNullable(response)
                .filter(RxClassResponse::noError)
                .map(RxClassResponse::getContent)
                .map(contentFunction)
                .map(streamProcessor)
                .orElseGet(HashSet::new);
    }

    private <R, T> Function<Stream<R>, Set<T>> transform(Function<R, Optional<T>> transform) {
        return stream -> stream
                .map(transform)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }
}
