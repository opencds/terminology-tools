package org.opencds.tools.terminology.rxclass.tools.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.cache.ApiCache;
import org.opencds.tools.terminology.api.cache.TerminologyCache;
import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.api.rest.RestRequest;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.opencds.tools.terminology.api.rest.util.Const.IMMUTABLE_EMPTY_HEADERS;
import static org.opencds.tools.terminology.api.rest.util.Const.PLUS;
import static org.opencds.tools.terminology.api.url.UrlQuery.param;
import static org.opencds.tools.terminology.api.url.UrlQuery.query;

public class RxClassApi {
    private static final Log log = LogFactory.getLog(RxClassApi.class);
    private static final String RXCLASS_API_URL = "https://rxnav.nlm.nih.gov/REST/rxclass";
    private static final String API_CLASS_PATH = "class";
    private static final String API_BYID_PATH = "byId.json";
    private static final String API_ALLCLASSES_PATH = "allClasses.json";
    private static final String API_CLASSCONTEXTS_PATH = "classContext.json";
    private static final String API_CLASSMEMBERS_PATH = "classMembers.json";
    private static final String API_CLASSTREE_PATH = "classTree.json";
    private static final String API_CLASSTYPES_PATH = "classTypes.json";
    private static final String API_CLASSBYRXNORMDRUGID_PATH = "byRxcui.json";
    private static final String API_CLASSBYRXNORMDRUGNAME_PATH = "byDrugName.json";
    private static final String API_RELA_SOURCES_PATH = "relaSources.json";
    private static final String API_RELAS_PATH = "relas.json";
    private static final String API_RELA_SOURCE_VERSION_PATH = "relaSource.json";

    private static final String VERSION = "version";

    private static final String SLASH = "/";

    private final TerminologyCache<RestRequest, RxClassResponse> cache;
    private final RestClient restClient;
    private final String baseUrl;

    private final BiFunction<Integer, String, RxClassResponse> errorHandler = (status, message) -> {
        log.error(" - Status: " + status);
        log.error(" - Response: " + message);
        var response = new RxClassResponse();
        response.setError(status, message);
        return response;
    };

    private final Function<String, RxClassResponse> contentHandler = content -> {
        var response = new RxClassResponse();
        response.setContent(content);
        return response;
    };

    private final Function<RestRequest, RxClassResponse> responseHandler = request -> {
        AtomicReference<RxClassResponse> response = new AtomicReference<>(new RxClassResponse());
        restClient().get(
                request,
                (status, message) -> response.set(errorHandler.apply(status, message)),
                content -> response.set(contentHandler.apply(content)));
        return response.get();
    };

    public RxClassApi(RestClient restClient) {
        this(restClient, null);
    }

    public RxClassApi(RestClient restClient, String baseUrl) {
        this.restClient = restClient;
        this.baseUrl = baseUrl;
        cache = new ApiCache<>(responseHandler);
    }

    private RestClient restClient() {
        return restClient;
    }

    private String baseUrl() {
        return Optional.ofNullable(baseUrl)
                .orElse(RXCLASS_API_URL);
    }

    private String url(String... comps) {
        return Stream.concat(Stream.of(baseUrl()), Stream.of(comps))
                .collect(Collectors.joining(SLASH));

    }

    public RxClassResponse findClassesById(String classId) {
        return getResult(
                url(API_CLASS_PATH, API_BYID_PATH),
                query(param("classId", classId)
                ));
    }

    public RxClassResponse getAllClasses(String... classTypes) {
        return getResult(
                url(API_ALLCLASSES_PATH),
                classTypes == null || classTypes.length == 0 ? query() : query(param("classTypes", String.join(PLUS, classTypes))
                ));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGID_PATH),
                query(
                        param("rxcui", rxcui)
                ));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui, String relaSource) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGID_PATH),
                query(
                        param("rxcui", rxcui),
                        param("relaSource", relaSource)
                ));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui, String... relas) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGID_PATH),
                query(
                        param("rxcui", rxcui),
                        param("relas", String.join(PLUS, relas))
                ));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui, Collection<String> relas) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGID_PATH),
                query(
                        param("rxcui", rxcui),
                        param("relas", String.join(PLUS, relas))
                ));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui, String relaSource, String... relas) {
        return getClassByRxNormDrugId(rxcui, relaSource, Arrays.asList(relas));
    }

    public RxClassResponse getClassByRxNormDrugId(String rxcui, String relaSource, Collection<String> relas) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGID_PATH),
                query(
                        param("rxcui", rxcui),
                        param("relaSource", relaSource),
                        param("relas", String.join(PLUS, relas))
                ));
    }

    public RxClassResponse getClassByRxNormDrugName(String name, Set<String> relas) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSBYRXNORMDRUGNAME_PATH),
                query(
                        param("drugName", name),
                        param("relas", String.join(PLUS, relas))
                ));
    }

    public RxClassResponse getClassContexts(String classId) {
        return getResult(
                url(API_CLASS_PATH, API_CLASSCONTEXTS_PATH),
                query(
                        param("classId", classId)
                ));
    }

    /**
     * <b>NOTE: </b> This method should only be used when the <tt>relaSource</tt> does not have one or more associated
     * <tt>rela</tt>s, otherwise no results are returned.
     *
     * @param classId    the classId
     * @param relaSource the relaSource
     * @return members of the provided class/relaSource
     */
    public RxClassResponse getClassMembers(String classId, String relaSource) {
        return getResult(
                url(API_CLASSMEMBERS_PATH),
                query(
                        param("classId", classId),
                        param("relaSource", relaSource)
                ));
    }

    public RxClassResponse getClassTree(String classId) {
        return getResult(
                url(API_CLASSTREE_PATH),
                query(param("classId", classId))
        );
    }

    public enum Transitive {
        INDIRECT_DIRECT("0"),
        DIRECT("1");

        private final String option;

        Transitive(String option) {
            this.option = option;
        }

        public String getOption() {
            return option;
        }
    }

    /**
     * <b>NOTE: </b> <tt>rela</tt> must not be null or blank; otherwise no results are returned.
     *
     * @param classId    the classId
     * @param relaSource the relaSource
     * @return members of the provided class/relaSource
     */
    public RxClassResponse getClassMembers(String classId, String relaSource, String rela, Transitive transitive) {
        return getResult(
                url(API_CLASSMEMBERS_PATH),
                query(
                        param("classId", classId),
                        param("relaSource", relaSource),
                        param("rela", rela),
                        param("trans", transitive.getOption())
                ));
    }

    public RxClassResponse getClassTypes() {
        return getResult(
                url(API_CLASSTYPES_PATH));
    }

    public RxClassResponse getSourcesOfDrugClassRelations() {
        return getResult(
                url(API_RELA_SOURCES_PATH));
    }

    public RxClassResponse getRelas() {
        return getResult(
                url(API_RELAS_PATH),
                query());
    }

    public RxClassResponse getRelas(String relaSource) {
        return getResult(
                url(API_RELAS_PATH),
                query(param("relaSource", relaSource)));
    }

    public RxClassResponse getRelaSourceVersion(String relaSource) {
        return getResult(
                url(VERSION, API_RELA_SOURCE_VERSION_PATH),
                query(param("relaSource", relaSource)));
    }

    private RxClassResponse getResult(String url) {
        return cache.get(RestRequest.create(url, IMMUTABLE_EMPTY_HEADERS, query()));
    }

    private RxClassResponse getResult(String url, Map<String, String> query) {
        return cache.get(RestRequest.create(url, IMMUTABLE_EMPTY_HEADERS, query));
    }
}
