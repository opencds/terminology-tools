package org.opencds.tools.terminology.rxnorm.tools.model;

public class Properties {
    private String rxcui;
    private String name;
    private String synonym;
    private String tty;
    private String language;
    private String suppress;
    private String umlscui;

    public String getRxcui() {
        return rxcui;
    }

    public String getName() {
        return name;
    }

    public String getSynonym() {
        return synonym;
    }

    public String getTty() {
        return tty;
    }

    public String getLanguage() {
        return language;
    }

    public String getSuppress() {
        return suppress;
    }

    public String getUmlscui() {
        return umlscui;
    }
}
