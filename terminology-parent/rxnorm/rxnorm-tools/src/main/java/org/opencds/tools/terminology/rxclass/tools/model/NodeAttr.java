package org.opencds.tools.terminology.rxclass.tools.model;

public class NodeAttr {
    private String attrName;
    private String attrValue;

    public String getAttrName() {
        return attrName;
    }

    public String getAttrValue() {
        return attrValue;
    }
}
