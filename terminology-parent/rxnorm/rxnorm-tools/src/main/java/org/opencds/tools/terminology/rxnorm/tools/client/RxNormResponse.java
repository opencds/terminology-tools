package org.opencds.tools.terminology.rxnorm.tools.client;

import org.opencds.tools.terminology.api.rest.json.GsonSupport;
import org.opencds.tools.terminology.rxnorm.tools.model.RxNormResponseContent;

public class RxNormResponse {
    private int statusCode;
    private String message;
    private boolean error;
    private String contentType;
    private String content;

    public void setError(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
        this.error = true;
    }

    public boolean noError() {
        return !error;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public RxNormResponseContent getContent() {
        return GsonSupport.fromJson(content, RxNormResponseContent.class);
    }

    public String getMessage() {
        return message;
    }
}
