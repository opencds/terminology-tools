package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class ApproximateGroup {
    private String inputTerm;
    private String maxEntries;
    private String comment;
    private List<Candidate> candidate;

    public String getInputTerm() {
        return inputTerm;
    }

    public String getMaxEntries() {
        return maxEntries;
    }

    public String getComment() {
        return comment;
    }

    public List<Candidate> getCandidate() {
        if (candidate == null) {
            return Collections.emptyList();
        }
        return candidate;
    }
}
