package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.Collections;
import java.util.List;

public class PropConceptGroup {
    private List<PropConcept> propConcept;

    public List<PropConcept> getPropConcept() {
        if (propConcept == null) {
            return Collections.emptyList();
        }
        return propConcept;
    }
}
