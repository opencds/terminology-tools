package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class ClassPathList {
    private List<ClassPath> classPath;

    public List<ClassPath> getClassPath() {
        if (classPath == null) {
            return Collections.emptyList();
        }
        return classPath;
    }
}
