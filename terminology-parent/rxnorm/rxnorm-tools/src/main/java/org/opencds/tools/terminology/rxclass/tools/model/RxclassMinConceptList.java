package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class RxclassMinConceptList {
    private List<RxclassMinConcept> rxclassMinConcept;

    public List<RxclassMinConcept> getRxclassMinConcept() {
        if (rxclassMinConcept == null) {
            return Collections.emptyList();
        }
        return rxclassMinConcept;
    }
}
