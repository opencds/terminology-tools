package org.opencds.tools.terminology.rxclass.tools.model;

public class RxclassMinConceptItem {
    private String classId;
    private String className;
    private String classType;

    public String getClassId() {
        return classId;
    }

    public String getClassName() {
        return className;
    }

    public String getClassType() {
        return classType;
    }
}
