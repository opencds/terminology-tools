package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.List;

public class PropNameList {
    private List<String> propName;

    public List<String> getPropName() {
        return propName;
    }
}
