package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.List;

public class IdTypeList {
    private List<String> idName;

    public List<String> getIdName() {
        return idName;
    }
}
