package org.opencds.tools.terminology.rxnorm.tools.model;

public class RxNormResponseContent {
    private AllRelatedGroup allRelatedGroup;
    private ApproximateGroup approximateGroup;
    private MinConceptGroup minConceptGroup;
    private PropConceptGroup propConceptGroup;
    private Properties properties;
    private RelatedGroup relatedGroup;
    private RelationTypeList relationTypeList;
    private SourceTypeList sourceTypeList;
    private TermTypeList termTypeList;
    private IdGroup idGroup;
    private IdTypeList idTypeList;
    private PropNameList propNameList;
    private String version;
    private String apiVersion;

    public AllRelatedGroup getAllRelatedGroup() {
        return allRelatedGroup;
    }

    public ApproximateGroup getApproximateGroup() {
        return approximateGroup;
    }

    public MinConceptGroup getMinConceptGroup() {
        return minConceptGroup;
    }

    public PropConceptGroup getPropConceptGroup() {
        return propConceptGroup;
    }

    public Properties getProperties() {
        return properties;
    }

    public RelatedGroup getRelatedGroup() {
        return relatedGroup;
    }

    public RelationTypeList getRelationTypeList() {
        return relationTypeList;
    }

    public SourceTypeList getSourceTypeList() {
        return sourceTypeList;
    }

    public TermTypeList getTermTypeList() {
        return termTypeList;
    }

    public IdGroup getIdGroup() {
        return idGroup;
    }

    public IdTypeList getIdTypeList() {
        return idTypeList;
    }

    public PropNameList getPropNameList() {
        return propNameList;
    }

    public String getVersion() {
        return version;
    }

    public String getApiVersion() {
        return apiVersion;
    }
}
