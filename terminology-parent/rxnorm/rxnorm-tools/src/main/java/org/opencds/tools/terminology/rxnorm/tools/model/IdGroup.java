package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.List;

public class IdGroup {
    private String name;
    private List<String> rxnormId;

    public String getName() {
        return name;
    }

    public List<String> getRxnormId() {
        return rxnormId;
    }

}
