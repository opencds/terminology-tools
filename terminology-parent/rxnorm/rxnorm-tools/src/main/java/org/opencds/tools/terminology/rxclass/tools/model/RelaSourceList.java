package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class RelaSourceList {
    private List<String> relaSourceName;

    public List<String> getRelaSourceName() {
        if (relaSourceName == null) {
            return Collections.emptyList();
        }
        return relaSourceName;
    }
}
