package org.opencds.tools.terminology.rxclass.tools.client;

import org.opencds.tools.terminology.api.rest.json.GsonSupport;
import org.opencds.tools.terminology.rxclass.tools.model.RxClassResponseContent;

public class RxClassResponse {
    private int statusCode;
    private String message;
    private boolean error;
    private String contentType;
    private String content;

    public void setError(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
        this.error = true;
    }

    public boolean hasError() {
        return statusCode >= 400;
    }

    public boolean noError() {
        return !error;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public RxClassResponseContent getContent() {
        return GsonSupport.fromJson(content, RxClassResponseContent.class);
    }
}
