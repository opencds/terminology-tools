package org.opencds.tools.terminology.rxnorm.tools.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.tools.terminology.api.cache.ApiCache;
import org.opencds.tools.terminology.api.cache.TerminologyCache;
import org.opencds.tools.terminology.api.rest.RestClient;
import org.opencds.tools.terminology.api.rest.RestRequest;
import org.opencds.tools.terminology.api.rest.RestResult;
import org.opencds.tools.terminology.api.rest.util.Const;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.opencds.tools.terminology.api.url.UrlQuery.param;
import static org.opencds.tools.terminology.api.url.UrlQuery.query;

public class RxNormApi {
    private static final Log log = LogFactory.getLog(RxNormApi.class);

    private static final String SLASH = "/";
    private static final String SPACE = " ";

    private static final String URL_RXNORM_API = "https://rxnav.nlm.nih.gov/REST";
    private static final String API_RXCUI_PATH_ELEMENT = "rxcui";
    private static final String API_RXCUI_PATH = "rxcui.json";
    private static final String API_ALLCONCEPTS_PATH = "allconcepts.json";
    private static final String API_ALLPROPERTIES_PATH = "allProperties.json";
    private static final String API_ALLRELATED_PATH = "allrelated.json";
    private static final String API_APPROXIMATE_TERM_PATH = "approximateTerm.json";
    private static final String API_IDTYPES_PATH = "idtypes.json";
    private static final String API_PROPERTIES_PATH = "properties.json";
    private static final String API_PROPERTY_PATH = "property.json";
    private static final String API_PROPNAMES_PATH = "propnames.json";
    private static final String API_RELATED_PATH = "related.json";
    private static final String API_RELATYPES_PATH = "relatypes.json";
    private static final String API_SOURCETYPES_PATH = "sourcetypes.json";
    private static final String API_TERMTYPES_PATH = "termtypes.json";
    private static final String API_VERSION_PATH = "version.json";

    private final TerminologyCache<RestRequest, RxNormResponse> cache;

    private final RestClient restClient;

    private final String baseUrl;

    private final BiFunction<RestResult, Optional<String>, RxNormResponse> responseBuilder = (result, content) -> {
        RxNormResponse response = new RxNormResponse();
        if (result.getStatusCode() >= 400) {
//            log.warn("-- Query returned no response for query: " + url);
            content.ifPresent(c -> log.debug(" - Response: " + c));
            log.error(" - Status: " + result.getStatusCode());
            log.error(" - Reason: " + result.getReasonPhrase());
            response.setError(result.getStatusCode(), result.getReasonPhrase());
        } else {
            content.ifPresent(response::setContent);
        }
        return response;
    };

    private final BiFunction<Integer, String, RxNormResponse> errorHandler = (status, message) -> {
        log.error(" - Status: " + status);
        log.error(" - Response: " + message);
        RxNormResponse response = new RxNormResponse();
        response.setError(status, message);
        return response;
    };

    private final Function<String, RxNormResponse> contentHandler = content -> {
        RxNormResponse response = new RxNormResponse();
        response.setContent(content);
        return response;
    };

    private final Function<RestRequest, RxNormResponse> responseHandler = request -> {
        AtomicReference<RxNormResponse> response = new AtomicReference<>(new RxNormResponse());
        restClient().get(
                request,
                (status, message) -> response.set(errorHandler.apply(status, message)),
                content -> response.set(contentHandler.apply(content)));
        return response.get();
    };

    public RxNormApi(RestClient restClient) {
        this.restClient = restClient;
        this.baseUrl = null;
        cache = new ApiCache<>(responseHandler);
    }

    public RxNormApi(RestClient restClient, String baseUrl) {
        this.restClient = restClient;
        this.baseUrl = baseUrl;
        cache = new ApiCache<>(responseHandler);
    }

    private RestClient restClient() {
        return restClient;
    }

    private String baseUrl() {
        return Optional.ofNullable(baseUrl)
                .orElse(URL_RXNORM_API);
    }

    private String url(String... comps) {
        return Stream.concat(Stream.of(baseUrl()), Stream.of(comps))
                .collect(Collectors.joining(SLASH));

    }


    public RxNormResponse getRxNormVersion() {
        return getResult(
                url(API_VERSION_PATH));
    }

    /**
     * Defaults to normalized search
     *
     * @param name
     * @return rxNormResponse
     * @see <a href="https://rxnav.nlm.nih.gov/api-RxNorm.findRxcuiByString.html">NLM's RxNorm API findRxcuiByString</a>
     */
    public RxNormResponse findRxcuiByString(String name) {
        return findRxcuiByString(name, 1);
    }

    /**
     * Search by name, allowing search precision.
     *
     * @param name   name
     * @param search 0: exact, 1: normalized, 2: exact, then normalized
     * @return rxNormResponse
     * @see <a href="https://rxnav.nlm.nih.gov/api-RxNorm.findRxcuiByString.html">NLM's RxNorm API findRxcuiByString</a>
     */
    public RxNormResponse findRxcuiByString(String name, int search) {
        return getResult(
                url(API_RXCUI_PATH),
                query(
                        param("name", name),
                        param("search", Integer.toString(search))));
    }

    public RxNormResponse getAllConceptsByTTY(String tty) {
        return getResult(url(API_ALLCONCEPTS_PATH), query(param("tty", tty)));
    }

    public RxNormResponse getAllConceptsByTTY(String... tty) {
        return getResult(url(API_ALLCONCEPTS_PATH), query(param("tty", String.join(SPACE, tty))));
    }

    public RxNormResponse getAllProperties(String rxcui, Collection<String> prop) {
        return getResult(url(API_RXCUI_PATH_ELEMENT, rxcui, API_ALLPROPERTIES_PATH), query(param("prop", String.join(SPACE, prop))));
    }

    /**
     * Get RxNorm concepts related to the concept identified by rxcui. Related concepts
     * may be of term types "IN", "MIN", "PIN", "BN", "SBD", "SBDC", "SBDF", "SBDG",
     * "SCD", "SCDC", "SCDF", "SCDG", "DF", "DFG", "BPCK" and "GPCK".
     * See <a href="https://rxnav.nlm.nih.gov/RxNavViews.html#label:appendix">Default Paths</a>
     * for the RxNorm relationship paths traveled to get concepts for each term type.
     *
     * @param rxcui
     * @return
     */
    public RxNormResponse getAllRelatedInfo(String rxcui) {
        return getResult(
                url(API_RXCUI_PATH_ELEMENT, rxcui, API_ALLRELATED_PATH));
    }

    public RxNormResponse getApproximateMatch(String term) {
        return getResult(
                url(API_APPROXIMATE_TERM_PATH),
                query(param("term", term)));
    }

    public RxNormResponse getIdTypes() {
        return getResult(
                url(API_IDTYPES_PATH));
    }

    public RxNormResponse getPropNames() {
        return getResult(
                url(API_PROPNAMES_PATH));
    }

    public RxNormResponse getRelaTypes() {
        return getResult(
                url(API_RELATYPES_PATH));
    }

    public RxNormResponse getSourceTypes() {
        return getResult(
                url(API_SOURCETYPES_PATH));
    }

    public RxNormResponse getRelatedByRelationship(String rxcui, String... rela) {
        return getRelatedByRelationship(rxcui, Arrays.asList(rela));
    }

    public RxNormResponse getRelatedByRelationship(String rxcui, Collection<String> rela) {
        return getResult(
                url(API_RXCUI_PATH_ELEMENT, rxcui, API_RELATED_PATH),
                query(param("rela", String.join(SPACE, rela))));
    }

    public RxNormResponse getRelatedByType(String rxcui, String... tty) {
        return getRelatedByType(rxcui, Arrays.asList(tty));
    }

    public RxNormResponse getRelatedByType(String rxcui, Collection<String> type) {
        return getResult(
                url(API_RXCUI_PATH_ELEMENT, rxcui, API_RELATED_PATH),
                query(param("tty", String.join(SPACE, type))));
    }


    public RxNormResponse getRxConceptProperties(String rxcui) {
        return getResult(
                url(API_RXCUI_PATH_ELEMENT, rxcui, API_PROPERTIES_PATH));
    }

    public RxNormResponse getRxProperty(String rxcui, String propName) {
        return getResult(
                url(API_RXCUI_PATH_ELEMENT, rxcui, API_PROPERTY_PATH),
                query(param("propName", propName)));
    }

    public RxNormResponse getTermTypes() {
        return getResult(
                url(API_TERMTYPES_PATH)
        );
    }

    private RxNormResponse getResult(String url) {
        return cache.get(RestRequest.create(url, Const.IMMUTABLE_EMPTY_HEADERS, query()));
    }

    private RxNormResponse getResult(String url, Map<String, String> query) {
        return cache.get(RestRequest.create(url, Const.IMMUTABLE_EMPTY_HEADERS, query));
    }
}
