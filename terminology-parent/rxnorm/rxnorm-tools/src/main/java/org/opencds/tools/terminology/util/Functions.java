package org.opencds.tools.terminology.util;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public class Functions {
    public static final Function<List<String>, Stream<String>> stringFilterStream = list ->
            list.stream()
                    .filter(Objects::nonNull)
                    .filter(StringUtils::isNotBlank);
}
