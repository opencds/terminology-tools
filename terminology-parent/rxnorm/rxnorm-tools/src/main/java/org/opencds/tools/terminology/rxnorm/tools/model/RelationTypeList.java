package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.List;

public class RelationTypeList {
    private List<String> relationType;

    public List<String> getRelationType() {
        return relationType;
    }
}
