package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class ClassTypeList {
    private List<String> classTypeName;

    public List<String> getClassTypeName() {
        if (classTypeName == null) {
            return Collections.emptyList();
        }
        return classTypeName;
    }
}
