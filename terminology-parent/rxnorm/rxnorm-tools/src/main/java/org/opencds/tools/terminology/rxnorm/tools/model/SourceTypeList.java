package org.opencds.tools.terminology.rxnorm.tools.model;

import java.util.List;

public class SourceTypeList {
    private List<String> sourceName;

    public List<String> getSourceName() {
        return sourceName;
    }
}
