package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class DrugMemberGroup {
    List<DrugMember> drugMember;

    public List<DrugMember> getDrugMember() {
        if (drugMember == null) {
            return Collections.emptyList();
        }
        return drugMember;
    }
}
