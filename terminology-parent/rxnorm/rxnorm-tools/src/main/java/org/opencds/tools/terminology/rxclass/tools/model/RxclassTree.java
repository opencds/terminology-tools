package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.ArrayList;
import java.util.List;

public class RxclassTree {
    private RxclassMinConceptItem rxclassMinConceptItem;
    private List<RxclassTree> rxclassTree;

    public RxclassMinConceptItem getRxclassMinConceptItem() {
        return rxclassMinConceptItem;
    }

    public List<RxclassTree> getRxclassTree() {
        if (rxclassTree == null) {
            rxclassTree = new ArrayList<>();
        }
        return rxclassTree;
    }
}
