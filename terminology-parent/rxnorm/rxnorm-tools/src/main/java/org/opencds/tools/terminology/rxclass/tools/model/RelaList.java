package org.opencds.tools.terminology.rxclass.tools.model;

import java.util.Collections;
import java.util.List;

public class RelaList {
    private Object rela;

    public List<String> getRela() {
        List<String> list = null;
        if (rela == null) {
            list = Collections.emptyList();
        }
        if (rela instanceof List) {
            list = (List<String>) rela;
        }
        if (rela instanceof String) {
            list = Collections.singletonList((String) rela);
        }
        return list;
    }
}
