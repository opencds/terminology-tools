package org.opencds.tools.terminology.rxnorm.tools.client;

import org.apache.commons.lang3.StringUtils;
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept;
import org.opencds.tools.terminology.rxnorm.tools.model.IdTypeList;
import org.opencds.tools.terminology.rxnorm.tools.model.PropNameList;
import org.opencds.tools.terminology.rxnorm.tools.model.RelationTypeList;
import org.opencds.tools.terminology.rxnorm.tools.model.RxNormResponseContent;
import org.opencds.tools.terminology.rxnorm.tools.model.SourceTypeList;
import org.opencds.tools.terminology.util.Functions;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class RxNormClientUtil {
    public static Function<RxNormResponseContent, Stream<String>> propName =
            content -> Optional.ofNullable(content)
                    .map(RxNormResponseContent::getPropNameList)
                    .map(PropNameList::getPropName)
                    .stream()
                    .flatMap(Functions.stringFilterStream);

    public static final Function<String, Optional<RxNormConcept>> propNameTransform =
            propName -> Optional.ofNullable(propName)
                    .filter(StringUtils::isNotBlank)
                    .map(RxNormConcept::new);

    public static Function<RxNormResponseContent, Stream<String>> relationType =
            content -> Optional.ofNullable(content)
                    .map(RxNormResponseContent::getRelationTypeList)
                    .map(RelationTypeList::getRelationType)
                    .stream()
                    .flatMap(Functions.stringFilterStream);

    public static final Function<String, Optional<RxNormConcept>> relationTypeTransform =
            relationType -> Optional.ofNullable(relationType)
                    .map(RxNormConcept::new);

    public static Function<RxNormResponseContent, Stream<String>> sourceType =
            content -> Optional.ofNullable(content)
                    .map(RxNormResponseContent::getSourceTypeList)
                    .map(SourceTypeList::getSourceName)
                    .stream()
                    .flatMap(Functions.stringFilterStream);

    public static final Function<String, Optional<RxNormConcept>> sourceNameTransform =
            sourceName -> Optional.ofNullable(sourceName)
                    .map(RxNormConcept::new);

    public static Function<RxNormResponseContent, Stream<String>> idType =
            content -> Optional.ofNullable(content)
                    .map(RxNormResponseContent::getIdTypeList)
                    .map(IdTypeList::getIdName)
                    .stream()
                    .flatMap(Functions.stringFilterStream);

    public static final Function<String, Optional<RxNormConcept>> idNameTransform =
            idName -> Optional.ofNullable(idName)
                    .map(RxNormConcept::new);
}
