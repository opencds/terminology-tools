package org.opencds.tools.terminology.rxclass.tools.client

import org.opencds.tools.terminology.rxclass.api.model.RxClassConcept
import org.opencds.tools.terminology.rxclass.api.model.RxClassRelationship
import spock.lang.Ignore
import spock.lang.Specification

class RxClassResponseContentClientFunctionalSpec extends Specification {

    RxClassClient client

    void setup() {
        client = new RxClassClient()
    }

    def 'test getAllClasses'() {
        when:
        long t0 = System.nanoTime()
        Set<RxClassConcept> classes = client.getAllClasses()
        println "${(System.nanoTime()-t0)/1e9} s"

        then:
        classes.size() == 22404
    }

    @Ignore
    def 'test getClassByRxNormDrugId'() {
        given:
        String rxcui = '389183'
        String relaSource = 'MEDRT'
        Set<String> relas = ['has_chemical_structure', 'has_moa', 'isa_structure']

        when:
        Set<RxClassRelationship> relationships = client.getClassByRxNormDrugId(rxcui, relaSource, relas)
        println relationships

        then:
        notThrown(Exception)
        relationships
    }

}
