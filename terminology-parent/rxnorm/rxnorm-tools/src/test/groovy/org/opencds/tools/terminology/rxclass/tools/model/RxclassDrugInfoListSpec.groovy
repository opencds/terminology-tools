package org.opencds.tools.terminology.rxclass.tools.model

import com.google.gson.Gson
import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RxclassDrugInfoListSpec extends Specification {
    private static final String byRxcui = 'src/test/resources/rxclass/byRxcui.json'

    def 'test serialization'() {
        given:
        String payload = new File(byRxcui).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

}
