package org.opencds.tools.terminology.rxnorm.tools.client

import groovy.json.JsonOutput
import org.opencds.tools.terminology.rxnorm.api.model.RxNormConcept
import spock.lang.Specification

class RxNormClientFunctionalSpec extends Specification {

    static RxNormClient client

    def setupSpec() {
        client = new RxNormClient()
    }

    def 'test getAllIngredients'() {
        when:
        Set<RxNormConcept> ingredients = client.getAllIngredients()

        then:
        notThrown(Exception)
        ingredients
        ingredients.size() == 26851
    }

    def "test getAllRelatedConcepts"() {
        given:
        String rxcui = '54552'

        when:
        Map response = client.getAllRelatedConcepts(rxcui)
        println JsonOutput.toJson(response)

        then:
        notThrown(Exception)
        response
    }

    def 'test getAllRelatedConcepts - sd'() {
        given:
        String rxcui = '308191'

        when:
        Map response = client.getAllRelatedConcepts(rxcui)
        println JsonOutput.toJson(response)

        then:
        notThrown(Exception)
        response
    }

    def 'test getAllRelatedConcepts - ingredient'() {
        given:
        String rxcui = '1050797'

        when:
        Map response = client.getAllRelatedConcepts(rxcui)
        println JsonOutput.toJson(response)

        then:
        notThrown(Exception)
        response
    }
}
