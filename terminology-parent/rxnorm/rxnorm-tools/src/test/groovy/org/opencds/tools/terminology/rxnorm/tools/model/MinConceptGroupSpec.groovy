package org.opencds.tools.terminology.rxnorm.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class MinConceptGroupSpec extends Specification {
    private static final String properties = 'src/test/resources/rxnorm/allconcepts.json'

    def 'test serialization'() {
        given:
        String payload = new File(properties).text

        when:
        RxNormResponseContent props = GsonSupport.fromJson(payload, RxNormResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

}
