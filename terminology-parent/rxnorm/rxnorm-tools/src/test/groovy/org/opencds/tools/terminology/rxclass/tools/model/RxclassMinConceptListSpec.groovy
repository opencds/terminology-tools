package org.opencds.tools.terminology.rxclass.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RxclassMinConceptListSpec extends Specification {
    private static final String properties = 'src/test/resources/rxclass/byid.json'

    def 'test serialization'() {
        given:
        String payload = new File(properties).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

}
