package org.opencds.tools.terminology.rxnorm.tools.client

import org.opencds.tools.terminology.api.rest.RestClient
import spock.lang.Specification

class RxNormApiSpec extends Specification {

    RxNormApi api

    def 'test url'() {
        given:
        RestClient restUtil = Mock(RestClient)
        String baseUrl = 'https://rxnav.nlm.nih.gov/REST'
        api = new RxNormApi(restUtil, baseUrl)
        String rxcui = '12345'

        when:
        String url = api.url(RxNormApi.API_RXCUI_PATH_ELEMENT, rxcui, RxNormApi.API_ALLRELATED_PATH)
        println url

        then:
        notThrown(Exception)
        url == 'https://rxnav.nlm.nih.gov/REST/rxcui/12345/allrelated.json'
    }

}
