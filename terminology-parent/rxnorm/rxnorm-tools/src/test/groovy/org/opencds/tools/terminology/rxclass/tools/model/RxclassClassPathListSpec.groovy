package org.opencds.tools.terminology.rxclass.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RxclassClassPathListSpec extends Specification {
    private static final String CLASS_CONTEXTS = 'src/test/resources/rxclass/classContexts.json'

    def 'test deserialization'() {
        given:
        String payload = new File(CLASS_CONTEXTS).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        var ser = GsonSupport.toJson(props)
        ser = ser.replaceAll('\n','')

        then:
        notThrown(Exception)
        props
    }
}
