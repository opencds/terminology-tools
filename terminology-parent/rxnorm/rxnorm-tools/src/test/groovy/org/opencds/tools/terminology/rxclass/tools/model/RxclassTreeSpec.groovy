package org.opencds.tools.terminology.rxclass.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RxclassTreeSpec extends Specification {
    private static final String CLASS_TREE = 'src/test/resources/rxclass/classTree.json'

    def 'test deserialization'() {
        given:
        String payload = new File(CLASS_TREE).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        var ser = GsonSupport.toJson(props)
        ser = ser.replaceAll('\n','')

        then:
        notThrown(Exception)
        props
        props.getRxclassTree()
        props.getRxclassTree().size() == 1 // always 1
        props.getRxclassTree().get(0).rxclassMinConceptItem // always the item queried in the URL
        props.getRxclassTree().get(0).rxclassMinConceptItem.classId == 'N0000193784'
        props.getRxclassTree().get(0).rxclassMinConceptItem.className == 'Terminology Extensions for Classification'
        props.getRxclassTree().get(0).rxclassMinConceptItem.classType == 'CHEM'
        props.getRxclassTree().get(0).rxclassTree
        props.getRxclassTree().get(0).rxclassTree.size() == 28
    }
}
