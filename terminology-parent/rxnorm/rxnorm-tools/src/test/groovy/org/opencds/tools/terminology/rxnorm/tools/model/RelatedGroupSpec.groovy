package org.opencds.tools.terminology.rxnorm.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RelatedGroupSpec extends Specification {
    private static final String propertiesRela = 'src/test/resources/rxnorm/relatedByRelationship.json'
    private static final String propertiesType = 'src/test/resources/rxnorm/relatedByType.json'

    def 'test deserialization - rela'() {
        given:
        String payload = new File(propertiesRela).text

        when:
        RxNormResponseContent props = GsonSupport.fromJson(payload, RxNormResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

    def 'test deserialization - type'() {
        given:
        String payload = new File(propertiesType).text

        when:
        RxNormResponseContent props = GsonSupport.fromJson(payload, RxNormResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

}
