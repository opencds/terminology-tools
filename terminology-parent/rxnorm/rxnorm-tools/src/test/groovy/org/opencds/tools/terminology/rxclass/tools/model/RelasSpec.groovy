package org.opencds.tools.terminology.rxclass.tools.model


import org.opencds.tools.terminology.api.rest.json.GsonSupport
import spock.lang.Specification

class RelasSpec extends Specification {
    private static final String relas = 'src/test/resources/rxclass/relas.json'
    private static final String relasATC = 'src/test/resources/rxclass/relasATC.json'

    def 'test serialization'() {
        given:
        String payload = new File(relas).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

    def 'test serialization - ATC'() {
        given:
        String payload = new File(relasATC).text

        when:
        RxClassResponseContent props = GsonSupport.fromJson(payload, RxClassResponseContent.class)
        println GsonSupport.toJson(props)

        then:
        notThrown(Exception)
        props
    }

}
