package org.opencds.tools.terminology.rxnorm.tools.client


import groovy.json.JsonOutput
import org.opencds.tools.terminology.api.rest.RestClient
import org.opencds.tools.terminology.rxnorm.tools.model.RxNormResponseContent
import spock.lang.Specification

class RxNormApiFunctionalSpec extends Specification {

    static RxNormApi util

    def setupSpec() {
        util = new RxNormApi(new RestClient(), null)
    }

    def 'test getAllConceptsByTTY'() {
        given:
        def tty = 'DFG'

        when:
        RxNormResponse response = util.getAllConceptsByTTY(tty)
        RxNormResponseContent rxNorm = response.getContent()
        println JsonOutput.toJson(rxNorm)

        then:
        response
    }


}
