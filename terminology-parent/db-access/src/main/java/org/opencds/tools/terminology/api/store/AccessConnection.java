/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.terminology.api.store;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AccessConnection {
    private static final Log log = LogFactory.getLog(AccessConnection.class);

    public static Supplier<Connection> supplier(File location) {
        return () -> {
            try {
                Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
                return DriverManager.getConnection("jdbc:ucanaccess://" + location.getAbsolutePath() + ";memory=false");
            } catch (ClassNotFoundException | SQLException e) {
                log.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        };
    }
}
